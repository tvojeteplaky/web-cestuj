/* jQuery Basic Slider plugin by Pavel Herink */

/* HTML code :
*
*  {* sablona galerie - slideru *}
*<div class="basicSlider">
*  <a href="#" class="previous">previous</a> <!--  a ".previous" - width, height and margin-right (and optionally background) must be specified in CSS -->
*  <div class="restriction">
*    <div class="itemBelt">
*       <!--  div ".item" - width, height and margin-right must be specified in CSS -->
*       <div class="item">
*          <!--  any content -->
*       </div>
*       <div class="item">
*          <!--  any content -->
*       </div>
*       ...
*    </div>
*  </div>
*  <a href="#" class="next">next</a> <!--  a ".¨next" - width, height and margin-left (and optionally background) must be specified in CSS -->
*</div>
*
* example:
* JS initializing 
*
* $(document).ready(function(){
*   $(".basicSlider").basicSlider({'showItems': 5});
* });
* 
* CSS:
*.basicSlider{padding-top: 30px; display: none;}
*.basicSlider .previous, .basicSlider .next{width: 21px; height: 29px; overflow: hidden; text-indent: -1000em;}
*.basicSlider .previous{background-image: url(../img/img_basic_slider_left.gif); margin-right: 15px;}
*.basicSlider .next{background-image: url(../img/img_basic_slider_right.gif); margin-left: 15px;}
*.basicSlider .item{width: 142px; height: 93px; margin-right: 15px;}
*
*/

(function($){
    $.fn.basicSlider = function(settings){
        var config = {
            'showItems': 4
        };
        if (settings){$.extend(config, settings);}
 
        return this.each(function(e){
           var sliderObj=$(this);  
           var shiftPrevious=sliderObj.children(".previous");
           var shiftNext=sliderObj.children(".next");
           var restriction=sliderObj.children(".restriction");
           var itemBelt=restriction.children(".itemBelt");
           var item=itemBelt.children(".itemBelt .item");
           
           var showItems=config.showItems;
           
           var itemHeight=parseInt(item.height());
           var itemWidth=parseInt(item.width());
           var itemMarginRight=parseInt(item.css('margin-right').replace(/[^-\d\.]/g, ''));
           var itemTotalWidth=itemWidth + itemMarginRight;
           /* slider layout building */
           
           sliderObj.css("clear","both");
           sliderObj.css("height",itemHeight+"px");
           
           restriction.css("position","relative");
           restriction.css("z-index","2");
           restriction.css("float","left");
           restriction.css("overflow","hidden");
           restriction.css("height",itemHeight+"px");
           var rWidth = showItems * itemTotalWidth - itemMarginRight;
           restriction.css("width",rWidth);
           
           //item.css("z-index","90");
           //item.css("position","relative");
           item.css("float","left");
           item.css("text-align","center");
           
           shiftPrevious.css("float","left");
           shiftNext.css("float","left");
           var shiftHeight = shiftPrevious.height();
           var posTop = Math.floor((itemHeight-shiftHeight)/2);
           shiftPrevious.css("margin-top",posTop+"px");
           shiftNext.css("margin-top",posTop+"px");
           
           /* funcionality */
           var photoCount = item.length; 
           var maxBeltWidth = itemTotalWidth * photoCount;  
           var maxPosition = itemTotalWidth * (photoCount - showItems);
           var actualPosition = 0; // aktualni pozice v px
           
           itemBelt.css("position","relative");
           itemBelt.css("z-index","1"); 
           itemBelt.css("width",maxBeltWidth+"px");
           itemBelt.css("height",itemHeight+"px");
        
           shiftPrevious.css("visibility","hidden");
           if(photoCount<=showItems) shiftNext.css("visibility","hidden");
           
           shiftPrevious.click(function(){itemBelt.animate({left:-(getPosition(-itemTotalWidth))},1000); return(false);});
           shiftNext.click(function(){itemBelt.animate({left:-(getPosition(itemTotalWidth))},1000); return(false);});
           
           sliderObj.show();
                 
           function getPosition(incr){
                actualPosition=actualPosition+incr;
                // kontrola mezi a zneviditelneni tlacitek
                if(actualPosition <= 0){actualPosition = 0; shiftPrevious.css("visibility","hidden");}else{shiftPrevious.css("visibility","visible");}
                if(actualPosition >= maxPosition){actualPosition = maxPosition; shiftNext.css("visibility","hidden");}else{shiftNext.css("visibility","visible");}
                 
                return(actualPosition);
           }
            
            // toto je zde kvuli IE 6/7, jinak nefunguje spravne posuvnik galerii pri prvnim nacteni
            //$("#tabs-4").click(function(){
            //$(".defaultSlider .itemBelt").css("width","200px");
            //$(".defaultSlider .itemBelt").css("width",maxBeltWidth+"px");
            //});
           
            
        });
    };
})(jQuery);