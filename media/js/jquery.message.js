jQuery.fn.center = function () {
	this.css("position","absolute");
	this.css("top", ( $(window).height() - this.height() ) / 2+$(window).scrollTop() + "px");
  this.css("left", ( $(window).width() - this.width() ) / 2+$(window).scrollLeft() + "px");
	return this;
}

function show_message()
{
    $('#OverlayMessage').height($(document).height());
    $('#OverlayMessage .content-msg').center();
		$('#OverlayMessage, #OverlayMessage .content-msg').fadeIn(); 
}