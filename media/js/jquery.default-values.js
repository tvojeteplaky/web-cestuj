(function ( $ ) {
	$.fn.defaultValues = function(options) {
		var settings = $.extend( {
			'color': '#000000'
		}, options );

		return this.each(function() {
			var defaultValue = $(this).attr('data-default-value');

			$(this).css('color', $(this).val() != defaultValue ? settings.color : '');

			$(this).focus(function() {
				if($(this).val() == defaultValue) {
					$(this).val('');
					$(this).css('color', settings.color);
				}
			});

			$(this).blur(function() {
				if($(this).val() == '') {
					$(this).val(defaultValue);
					$(this).css('color', '');
				}
			});
		});
	};
})( jQuery );