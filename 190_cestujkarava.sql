-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Počítač: localhost
-- Vytvořeno: Pon 25. dub 2016, 09:19
-- Verze serveru: 5.6.28-0ubuntu0.15.10.1
-- Verze PHP: 5.6.11-1ubuntu3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `190_cestujkarava`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `admin_preferences`
--

CREATE TABLE IF NOT EXISTS `admin_preferences` (
  `id` int(10) unsigned NOT NULL,
  `kod` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(20) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `admin_settings`
--

CREATE TABLE IF NOT EXISTS `admin_settings` (
  `id` int(11) NOT NULL,
  `nazev_dodavatele` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `email_podpora_dodavatele` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `logo_dodavatele` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `kohana_debug_mode` tinyint(4) NOT NULL DEFAULT '0',
  `smarty_console` tinyint(4) NOT NULL DEFAULT '0',
  `shutdown` tinyint(4) NOT NULL DEFAULT '0',
  `disable_login` int(11) NOT NULL DEFAULT '0',
  `interni_poznamka` text COLLATE utf8_czech_ci
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `admin_settings`
--

INSERT INTO `admin_settings` (`id`, `nazev_dodavatele`, `email_podpora_dodavatele`, `logo_dodavatele`, `kohana_debug_mode`, `smarty_console`, `shutdown`, `disable_login`, `interni_poznamka`) VALUES
(1, 'all: dg studio; www.dgstudio.cz; info@dgstudio.cz', 'info@dgstudio.cz', NULL, 0, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `admin_structure`
--

CREATE TABLE IF NOT EXISTS `admin_structure` (
  `id` int(11) unsigned NOT NULL,
  `module_code` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `submodule_code` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `module_controller` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `admin_menu_section_id` int(11) NOT NULL,
  `poradi` int(10) unsigned NOT NULL,
  `parent_id` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `global_access_level` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=COMPACT;

--
-- Vypisuji data pro tabulku `admin_structure`
--

INSERT INTO `admin_structure` (`id`, `module_code`, `submodule_code`, `module_controller`, `admin_menu_section_id`, `poradi`, `parent_id`, `zobrazit`, `global_access_level`) VALUES
(1, 'environment', 'module', 'default', 1, 1, 0, 1, 0),
(2, 'environment', 'setting', 'edit/1/1', 3, 15, 1, 1, 0),
(3, 'environment', 'samodules', 'list', 3, 4, 1, 1, 3),
(4, 'environment', 'salanguages', 'list', 3, 14, 1, 0, 0),
(5, 'environment', 'routes', 'list', 3, 6, 1, 1, 3),
(6, 'environment', 'mainsetup', 'edit/1/1', 3, 1, 1, 1, 3),
(7, 'environment', 'inmodules', 'list', 3, 2, 1, 0, 3),
(8, 'environment', 'registry', 'list', 3, 5, 1, 1, 3),
(9, 'page', 'item', 'list', 1, 2, 0, 1, 0),
(10, 'page', 'item', 'list', 3, 1, 9, 1, 0),
(11, 'page', 'unrelated', 'list', 3, 2, 9, 1, 0),
(12, 'static', 'item', 'list', 3, 3, 9, 1, 0),
(13, 'user', 'item', 'list', 1, 11, 0, 1, 0),
(14, 'article', 'item', 'list', 1, 3, 0, 0, 0),
(15, 'product', 'item', 'list', 1, 5, 0, 0, 0),
(16, 'product', 'item', 'list', 2, 2, 15, 1, 0),
(17, 'product', 'category', 'list', 2, 3, 15, 1, 0),
(18, 'product', 'manufacturer', 'list', 2, 5, 15, 1, 0),
(19, 'product', 'order', 'list', 2, 1, 15, 0, 0),
(20, 'product', 'shipping', 'list', 2, 6, 15, 0, 0),
(21, 'product', 'payment', 'list', 2, 7, 15, 0, 0),
(22, 'product', 'price', 'list', 2, 8, 15, 0, 0),
(23, 'product', 'tax', 'list', 2, 9, 15, 0, 0),
(24, 'page', 'system', 'list', 3, 4, 9, 1, 3),
(25, 'email', 'queue', 'list', 1, 10, 0, 0, 0),
(26, 'email', 'queue', 'list', 3, 1, 25, 1, 0),
(27, 'email', 'type', 'list', 3, 2, 25, 1, 0),
(28, 'email', 'receiver', 'list', 3, 3, 25, 1, 0),
(29, 'email', 'smtp', 'edit/1/1', 3, 4, 25, 1, 0),
(30, 'product', 'shopper', 'list', 2, 4, 15, 0, 0),
(31, 'product', 'orderstates', 'list', 2, 10, 15, 0, 0),
(32, 'product', 'eshopsettings', 'edit/1/1', 2, 11, 15, 0, 0),
(33, 'product', 'voucher', 'list', 2, 12, 15, 0, 0),
(35, 'reference', 'item', 'list', 1, 6, 0, 1, 0),
(40, 'newsletter', 'item', 'list', 1, 12, 0, 0, 0),
(41, 'newsletter', 'item', 'list', 3, 1, 40, 1, 0),
(42, 'newsletter', 'recipient', 'list', 3, 2, 40, 1, 0),
(43, 'catalog', 'item', 'list', 1, 4, 0, 0, 0),
(44, 'catalog', 'item', 'list', 2, 1, 43, 1, 0),
(45, 'catalog', 'category', 'list', 2, 2, 43, 1, 0),
(46, 'shop', 'item', 'list', 1, 13, 0, 1, 0),
(47, 'reservation', 'item', 'list', 1, 14, 0, 1, 0),
(48, 'reservation', 'price', 'list', 2, 2, 47, 1, 0),
(49, 'reservation', 'item', 'list', 2, 1, 47, 1, 0),
(50, 'reservation', 'currency', 'list', 2, 3, 47, 1, 0),
(51, 'reservation', 'customer', 'edit', 2, 4, 47, 0, 0),
(52, 'report', 'item', 'list', 1, 15, 0, 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `admin_structure_data`
--

CREATE TABLE IF NOT EXISTS `admin_structure_data` (
  `id` int(11) NOT NULL,
  `admin_structure_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL DEFAULT '',
  `nadpis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `popis` text COLLATE utf8_czech_ci
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

--
-- Vypisuji data pro tabulku `admin_structure_data`
--

INSERT INTO `admin_structure_data` (`id`, `admin_structure_id`, `language_id`, `nazev`, `nadpis`, `title`, `description`, `keywords`, `popis`) VALUES
(1, 1, 1, 'Základní', 'Základní', 'Základní', '', '', NULL),
(6, 6, 1, 'Nastavení projektu', 'Základní nastavení', 'Základní nastavení', '', '', NULL),
(3, 3, 1, 'Struktura administrace', 'Struktura administrace', 'Struktura administrace', '', '', NULL),
(7, 7, 1, 'Instalátor modulů', 'Instalátor modulů', 'Instalátor modulů', '', '', NULL),
(8, 8, 1, 'Registry modulů', 'Registry modulů', 'Registry modulů', '', '', NULL),
(5, 5, 1, 'Záznamy rout', 'Záznamy rout', 'Záznamy rout', '', '', NULL),
(4, 4, 1, 'Nastavení jazyků', 'Nastavení jazyků', 'Nastavení jazyků', '', '', NULL),
(2, 2, 1, 'Obecné nastavení', 'Obecné nastavení', 'Obecné nastavení', '', '', NULL),
(9, 9, 1, 'Stránky', 'Stránky', 'Stránky', '', '', NULL),
(10, 10, 1, 'Stránky ve struktuře menu', 'Stránky ve struktuře menu', 'Stránky ve struktuře menu', '', '', NULL),
(11, 11, 1, 'Nezařazené stránky', 'Nezařazené stránky', 'Nezařazené stránky', '', '', NULL),
(12, 12, 1, 'Statický obsah', 'Statický obsah', 'Statický obsah', '', '', NULL),
(13, 13, 1, 'Uživatelé', 'Uživatelé', 'Uživatelé', '', '', NULL),
(14, 14, 1, 'Novinky', 'Novinky', 'Novinky', '', NULL, NULL),
(15, 15, 1, 'E-shop', 'E-shop', 'E-shop', '', NULL, NULL),
(16, 16, 1, 'Seznam produktů', 'Seznam produktů', 'Seznam produktů', '', NULL, NULL),
(17, 17, 1, 'Kategorie produktů', 'Kategorie produktů', 'Kategorie produktů', '', NULL, NULL),
(18, 18, 1, 'Výrobci', 'Výrobci', 'Výrobci', '', NULL, NULL),
(19, 19, 1, 'Objednávky', 'Objednávky', 'Objednávky', '', NULL, NULL),
(20, 20, 1, 'Doprava', 'Doprava', 'Doprava', '', NULL, NULL),
(21, 21, 1, 'Platba', 'Platba', 'Platba', '', NULL, NULL),
(22, 22, 1, 'Cenové skupiny', 'Cenové skupiny', 'Cenové skupiny', '', NULL, NULL),
(23, 23, 1, 'DPH', 'DPH', 'DPH', '', NULL, NULL),
(24, 24, 1, 'Systémové stránky', 'Systémové stránky', 'Systémové stránky', '', NULL, NULL),
(25, 25, 1, 'Email', 'Email', 'Email', '', NULL, NULL),
(26, 26, 1, 'Fronta emailů', 'Fronta emailů', 'Fronta emailů', '', NULL, NULL),
(27, 27, 1, 'Typy emailů', 'Typy emailů', 'Typy emailů', '', NULL, NULL),
(28, 28, 1, 'Příjemci emailů', 'Příjemci emailů', 'Příjemci emailů', '', NULL, NULL),
(29, 29, 1, 'Nastavení SMTP', 'Nastavení SMTP', 'Nastavení SMTP', '', NULL, NULL),
(30, 30, 1, 'Uživatelé', 'Uživatelé', 'Uživatelé', '', NULL, NULL),
(31, 31, 1, 'Stavy objednávek', 'Stavy objednávek', 'Stavy objednávek', '', NULL, NULL),
(32, 32, 1, 'Nastavení eshopu', 'Nastavení eshopu', 'Nastavení eshopu', '', NULL, NULL),
(33, 33, 1, 'Slevové kupóny', 'Slevové kupóny', 'Slevové kupóny', '', NULL, NULL),
(35, 35, 1, 'Galerie', 'Galerie', 'Galerie', '', NULL, NULL),
(46, 44, 1, 'Seznam produktů', 'Seznam produktů', 'Seznam produktů', '', NULL, NULL),
(40, 40, 1, 'Newsletter', 'Newsletter', 'Newsletter', '', NULL, NULL),
(41, 41, 1, 'Newsletter', 'Newsletter', 'Newsletter', '', NULL, NULL),
(42, 42, 1, 'Newsletter - příjemci', 'Newsletter - příjemci', 'Newsletter - příjemci', '', NULL, NULL),
(47, 45, 1, 'Kategorie produktů', 'Kategorie produktů', 'Kategorie produktů', '', NULL, NULL),
(45, 43, 1, 'Katalog', 'Katalog', 'Katalog', '', NULL, NULL),
(48, 46, 1, 'Karavany', 'Karavany', 'Karavany', 'Karavany', NULL, NULL),
(49, 47, 1, 'Rezervace', 'Rezervace', 'Rezervace', '', NULL, NULL),
(50, 48, 1, 'Ceny ', 'Ceny ', 'Ceny ', '', NULL, NULL),
(51, 49, 1, 'Přehled', 'Přehled', 'Přehled', '', NULL, NULL),
(52, 50, 1, 'Měny', 'Měny', 'Měny', '', NULL, NULL),
(53, 51, 1, 'Zákazníci', 'Zákazníci', 'Zákazníci', '', NULL, NULL),
(54, 52, 1, 'Finanční report', 'Finanční report', 'Finanční report', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `poradi` int(11) NOT NULL,
  `article_category_id` int(11) NOT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `zobrazit_rotator` tinyint(4) NOT NULL DEFAULT '0',
  `available_languages` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `article_data`
--

CREATE TABLE IF NOT EXISTS `article_data` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nadpis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `autor` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `article_category_id` int(11) NOT NULL DEFAULT '0',
  `zobrazit` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `article_photos`
--

CREATE TABLE IF NOT EXISTS `article_photos` (
  `id` int(11) NOT NULL,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `article_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `article_photo_data`
--

CREATE TABLE IF NOT EXISTS `article_photo_data` (
  `id` int(11) NOT NULL,
  `article_photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `currencies`
--

CREATE TABLE IF NOT EXISTS `currencies` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` float NOT NULL,
  `ucet` varchar(255) NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `value`, `ucet`, `shop_id`) VALUES
(1, 'EUR', 0.0363636, '1202151020/3030', 0),
(2, 'CZK', 1, '1202151012/3030', 0),
(3, 'CZK MOHR', 27.5, '273584742/0300', 5),
(4, 'EUR MOHR', 1, '121279812/0100', 5);

-- --------------------------------------------------------

--
-- Struktura tabulky `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `born` date NOT NULL,
  `reservation_id` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=368 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `customers`
--

INSERT INTO `customers` (`id`, `firstname`, `lastname`, `born`, `reservation_id`) VALUES
(4, 'Hernichová', '', '2015-04-23', 3),
(3, '', 'Hernichová', '0000-00-00', 0),
(5, 'MOHR-Doktorka-zdarma', '', '2015-04-23', 4),
(6, 'MOHR brácha ', '', '0000-00-00', 5),
(7, 'Marcel', 'Holeš', '1968-09-04', 6),
(8, 'Tomáš', 'Vašíček', '1968-12-20', 7),
(9, 'Petra', 'Kameníková', '1982-04-14', 8),
(92, 'Lukáš', 'Hakl', '1983-05-11', 9),
(96, 'Miloš', 'Mohr', '2015-06-28', 62),
(12, 'Michal Výmola', '', '0000-00-00', 11),
(13, 'Michal Výmola', '', '2015-04-23', 12),
(14, 'Iva', 'Vaněčková', '1971-01-08', 13),
(15, 'Michal Výmola', '', '0000-00-00', 14),
(16, 'Michal Výmola', '', '2015-04-23', 15),
(17, 'Jaromír', 'Paleček', '1966-08-18', 16),
(18, 'Tomáš', 'Bílek', '1972-06-26', 17),
(19, 'Nikola', 'Motejlová', '1997-09-20', 18),
(20, 'Dušan', 'Oškera', '1974-12-12', 19),
(21, 'Zdeněk', 'Peřina', '1951-07-17', 20),
(22, 'Petr', 'Zelík', '1966-02-25', 21),
(23, 'Ivo Dědek', '', '0000-00-00', 22),
(24, 'Michal Výmola', '', '2015-04-23', 23),
(25, 'Dušan', 'Vidlička', '1976-12-23', 24),
(26, 'Roman', 'Dřevojánek', '1976-09-23', 25),
(27, 'Milan', 'Mrázek', '1963-05-13', 26),
(262, 'Jan', 'Jelínek', '1976-10-23', 89),
(29, 'Dagmar', 'Marková', '1957-06-26', 28),
(30, 'Jaroslav', 'Prchal', '2008-11-30', 29),
(263, 'Jan', 'Hejtmánek', '1980-10-25', 25),
(34, 'Radim', 'Kluz', '1974-08-18', 33),
(35, 'Dagmar', 'Kluzová', '1977-03-18', 33),
(36, 'Radim', 'Swaczyna', '1977-09-28', 34),
(37, 'Robert', 'Šteffek', '1973-06-02', 35),
(38, 'Marek', 'Zábojník', '1975-03-06', 36),
(39, 'Tomáš', 'Míček', '1969-08-03', 37),
(41, 'Adéla', 'Běťáková', '1986-02-12', 0),
(42, 'Adéla', 'Běťáková', '1986-02-12', 0),
(206, 'Daniela', 'Oškerová', '1975-09-24', 19),
(126, 'Suntel', 'Řezníček', '1960-05-20', 77),
(125, 'Suntel', 'Řezníček', '1970-05-20', 76),
(48, 'Radim', 'Kořínek', '1973-10-15', 46),
(49, 'Radim', 'Kořínek', '1971-08-11', 46),
(50, 'Radim', 'Kořínek', '1972-09-28', 47),
(122, 'Marcela', 'Suchánková', '1960-10-16', 73),
(121, 'Aleš', 'Suchánek', '1959-06-08', 73),
(120, 'Eva', 'Běťáková', '1965-07-27', 73),
(119, 'Jiří', 'Běťák', '1962-04-24', 73),
(65, 'Radek', 'Kocurek', '1965-10-10', 56),
(66, 'Lenka', 'Kocurková', '1966-12-12', 56),
(67, 'Dušan', 'Machala', '1967-11-11', 56),
(68, 'Marcela ', 'Vrbíková', '1966-12-12', 56),
(69, 'Petra', 'Holešová', '1970-03-28', 6),
(70, 'Marcel', 'Holeš', '2001-10-30', 6),
(71, 'Ivana', 'Vašíčková', '1968-03-26', 7),
(72, 'Dominika', 'Vašíčková', '1998-04-17', 7),
(73, 'Pavel', 'Vaněček', '1965-08-08', 13),
(74, 'Iveta ', 'Vaněčková', '1995-03-16', 13),
(75, 'Patrik', 'Vaněček', '1999-08-12', 13),
(76, 'Adam', 'Rakovský', '1976-07-17', 57),
(80, 'Hana', 'Rakovská', '1977-01-21', 58),
(78, 'Barbora', 'Rakovská', '2001-09-19', 57),
(79, 'Vít', 'Rakovský', '2003-07-15', 57),
(81, 'David', 'Rakovský', '2008-07-29', 58),
(82, 'Alžběta', 'Rakovská', '2011-11-01', 58),
(83, 'Zdenek', 'Šeda / Paša', '1910-10-10', 59),
(84, 'Jiří ', 'Gistr', '1969-04-14', 60),
(85, 'Beata', 'Gistrova', '1969-05-17', 60),
(86, 'Tereza', 'Gistrova', '1999-05-12', 60),
(87, 'David', 'Gistr', '2005-02-10', 60),
(130, 'Karel', 'Malošík', '1973-06-30', 81),
(89, 'Ivo', 'Ludvík', '1979-04-11', 8),
(90, 'Michaela', 'Ludvíková', '1987-05-18', 8),
(91, 'Michal', 'Burian', '1981-09-16', 8),
(93, 'Tereza', 'Haklová', '1986-04-15', 9),
(94, 'Gabriela', 'Chasáková', '1982-05-31', 9),
(95, 'Dalibor', 'Chasák', '1983-06-10', 9),
(97, 'Honza', 'Brůžička', '2015-06-11', 63),
(98, 'Honza', 'Lutonský', '2015-06-09', 64),
(105, 'Lukáš', 'Polák', '2001-05-28', 66),
(104, 'Miroslava', 'Poláková', '1973-10-17', 66),
(103, 'Karel', 'Polák', '1974-04-26', 66),
(106, 'Tomáš', 'Polák', '2003-11-02', 66),
(143, 'Vojtěch', 'Holub', '1973-09-02', 91),
(142, 'Michaela', 'Salvetová', '1981-09-03', 91),
(140, 'Michal', 'Juřena', '1978-09-12', 89),
(139, 'Marcel', 'Bryška', '1975-04-25', 88),
(141, 'Antonín', 'Výmola', '1947-06-13', 90),
(117, 'David', 'Masarik', '1981-08-26', 72),
(115, 'Michal', 'Vymola', '2015-08-21', 71),
(116, 'Ivo', 'Dedek', '2015-08-14', 70),
(118, 'Petr', 'Pastyrik', '2015-08-29', 72),
(123, 'Balazs', 'Toth', '2015-08-28', 74),
(124, 'Jiří', 'Běťák', '1962-04-24', 75),
(128, 'Adéla', 'Běťáková', '1986-02-12', 79),
(200, 'Tomáš', 'Zacha', '1976-08-05', 122),
(131, 'Radim', 'Malošík', '1975-04-13', 81),
(132, 'Miroslav', 'Straka', '1972-04-12', 81),
(133, 'Zdeněk', 'Martinů', '1956-02-27', 81),
(236, 'Martina', 'Neumanová', '1975-05-13', 139),
(235, 'Václav', 'Tauš', '1973-08-19', 138),
(138, 'Ivo', 'Dědek ml.', '2015-09-25', 87),
(144, 'Michael', 'Trejbal', '1968-04-29', 92),
(145, 'Hana', 'Vymětalíková', '1969-01-31', 92),
(146, 'Lucie', 'Vymětalíková', '1999-06-03', 92),
(147, 'Petr', 'Vymětalík', '1997-03-27', 92),
(148, 'Jana', 'Nováková', '1971-08-05', 94),
(149, 'Bohuslava', 'Králová', '1953-08-11', 95),
(150, 'Roman', 'Stiksa (CK MAXIM)', '1969-05-05', 93),
(151, 'Petra', 'Kameníková', '1982-04-14', 96),
(152, 'Michaela', 'Stašová', '1982-12-11', 96),
(153, 'Jiří', 'Klein', '1976-06-25', 96),
(154, 'Elbl', 'Roman', '2015-09-10', 97),
(155, 'Alena', 'Palečková', '1972-05-31', 16),
(156, 'Jan', 'Paleček', '1996-06-01', 16),
(157, 'Alena', 'Palečková', '1992-05-03', 16),
(158, 'Kateřina', 'Bílková', '2006-06-28', 17),
(159, 'Aleš', 'Pavel', '1973-05-03', 17),
(160, 'Maxmilián', 'Pavel', '2007-09-27', 17),
(161, 'Filip', 'Motejl', '1997-09-20', 18),
(162, 'Dagmar', 'Motejlová', '1976-01-29', 18),
(163, 'Michal', 'Motejl', '1973-01-15', 18),
(164, 'Pavla', 'Zelíková', '1962-11-07', 21),
(165, 'Matyáš', 'Zelík', '2007-05-02', 21),
(166, 'Pavel', 'Obdržálek', '2015-09-10', 98),
(167, 'Fiala ', 'Jiří MUDr.', '2015-09-10', 99),
(168, 'Fialová', 'Tamara', '2015-09-10', 99),
(169, 'CK MAXIM ', 'p. Bryška', '2015-09-04', 100),
(170, 'Fiala', 'Jiří', '2015-09-16', 101),
(171, 'Jiří', 'Kolář', '1966-06-06', 102),
(172, 'Kateřina', 'Kolářová', '1969-02-07', 102),
(173, 'Kristýna', 'Zelíková', '1993-10-17', 21),
(174, 'Michaela', 'Mohrova', '2015-10-03', 103),
(175, 'Veronika', 'Kadleckova', '1978-11-03', 104),
(176, 'Michal', 'Kadleček', '1975-11-29', 104),
(177, 'Natálie', 'Kadleckova', '2006-01-29', 104),
(178, 'David', 'Kadleček', '2009-07-23', 104),
(179, 'Vilím', 'Daniel', '1971-09-04', 105),
(180, 'Pavel', 'Václavek', '1977-05-06', 106),
(181, 'Petra', 'Kameníková', '1982-04-14', 107),
(182, 'Tereza ', 'Haklová', '1983-03-13', 107),
(183, 'Michaela', 'Ludvíková', '1981-05-15', 107),
(184, 'Iva', 'Kramářová', '1980-06-16', 107),
(185, 'Mohr', '', '2015-10-06', 108),
(186, 'Tereza', 'Gorcová', '1989-02-27', 79),
(187, 'Petra', 'Tichavová', '1984-08-16', 79),
(188, 'Filip', 'Zelenka', '1982-11-08', 79),
(189, 'Miloš', 'Mohr', '1960-05-20', 109),
(190, 'Miloš', 'Mohr', '1960-05-20', 110),
(194, 'Vladimír', 'Kudela', '1965-10-08', 114),
(195, 'Vladimír', 'Kudela', '1978-10-10', 115),
(199, 'Stanislav', 'Zacha', '1956-11-14', 121),
(198, 'Mohr', 'Miloš', '4444-11-01', 117),
(201, 'Miloš', 'Mohr', '1960-01-01', 123),
(202, 'Zdenek', 'Šeda', '1979-09-20', 124),
(203, 'Šárka', 'Mrázková', '1965-12-16', 26),
(204, 'Václav', 'Rážek', '1975-02-20', 26),
(205, 'Martina', 'Rážková', '1974-11-26', 26),
(207, 'Matěj', 'Oškera', '2003-07-03', 19),
(208, 'Liliana', 'Oškerová', '2013-10-08', 19),
(209, 'Ludmila Oškerová', 'Peřinová', '1951-09-19', 20),
(210, 'Jiřina', 'Halčařová', '1955-04-18', 20),
(211, 'Jiří', 'Halčař', '1953-08-06', 20),
(212, 'Lukáš', 'žaludek', '2015-11-06', 125),
(213, 'Lukáš', 'žaludek', '2015-11-19', 126),
(298, 'Michaela', 'Kyseláková', '1989-10-11', 156),
(297, 'Michal', 'Zaoral', '1980-10-16', 156),
(296, 'Petr', 'Drábek', '1990-11-24', 156),
(295, 'Barbora ', 'Štáblová', '1991-04-16', 156),
(218, 'Vymola', '', '2015-12-04', 128),
(219, 'Vymola', '', '2015-12-04', 129),
(220, 'Vymola', '', '2015-12-04', 130),
(313, 'Eva', 'Obdržálková', '1970-02-19', 98),
(222, 'Jiří', 'Melkus (CK MAUTHNER)', '1948-01-20', 132),
(223, 'Petra', 'Kameníková', '1982-04-14', 133),
(325, 'Ivo', 'Dusík', '1970-01-01', 169),
(324, 'Ivo', 'Dusík', '1970-01-01', 168),
(323, 'Michal', 'Výmola', '1973-06-07', 163),
(227, 'Jaromír', 'Paleček', '2015-12-10', 135),
(228, 'Jaromír', 'Paleček', '2015-12-15', 136),
(229, 'Jaromír', 'Paleček', '2015-12-09', 137),
(230, 'Lucie', 'Matyášová', '2007-11-16', 132),
(231, 'Karel', 'Matyáš', '1975-05-26', 132),
(232, 'Jan', 'Matyáš', '1971-06-20', 132),
(233, 'Šimoník', 'Přemysl', '1979-05-20', 88),
(234, 'Jana', 'Krčmová', '1978-12-11', 24),
(237, 'Maroš', 'Novák', '1969-12-15', 94),
(238, 'Alena', 'Slezáková', '1971-07-21', 94),
(239, 'Jan', 'Slezák', '1961-04-13', 94),
(240, 'Petr', 'Král', '1953-03-10', 95),
(241, 'Jitka', 'Králová', '1954-01-06', 95),
(242, 'Luboš', 'Mleziva', '1956-08-13', 95),
(243, 'David', 'Klobáska', '1985-03-15', 105),
(244, 'Jaromír ', 'Brázda', '1979-04-28', 105),
(245, 'Zbyněk', 'Vrobel', '1979-06-23', 105),
(246, 'Bohumír', 'Dragoun', '1971-03-23', 106),
(247, 'Zbyněk', 'Lach', '1977-07-30', 106),
(248, 'Jiří', 'Tyl', '1979-09-06', 106),
(249, 'Bára', 'Havelková', '2016-01-22', 141),
(250, 'Vojtěch', 'Marek', '1949-02-07', 28),
(251, 'Jaroslav', 'Prchal', '1979-12-22', 28),
(252, 'Dagmar', 'Prchalová', '1979-01-25', 28),
(253, 'Vojtěch', 'Prchal', '2008-11-30', 29),
(254, 'František', 'Prchal', '2013-03-05', 29),
(255, 'Matouš ', 'Prchal', '2015-12-04', 29),
(256, 'Olga', 'Taušová', '1972-09-26', 138),
(257, 'Gabriela ', 'Suchá', '1975-04-25', 138),
(258, 'Marta', 'Štefanidesová', '1977-11-08', 138),
(259, 'Klára', 'Cíglerová', '1973-07-29', 139),
(260, 'Pavel', 'Kozárek', '1982-01-11', 139),
(261, 'Zuzana', 'Wolná', '1978-09-03', 139),
(264, 'Jiří', 'Běťák', '1962-04-24', 143),
(265, 'Eva', 'Běťáková', '1965-07-27', 143),
(266, 'Denisa', 'Stiksová', '1975-07-31', 93),
(267, 'Sara', 'Stiksová', '2001-12-30', 93),
(268, 'Samuel', 'Stiksa', '2005-02-11', 93),
(269, 'Iva', 'Kramářová', '1980-05-20', 144),
(270, 'Iva', 'Kramářová', '1980-05-20', 145),
(271, 'Jiří', 'Strnad', '1970-03-18', 142),
(272, 'Jaroslava', 'Strnadová', '1972-10-15', 142),
(273, ' Adorjàn ', 'Salomon', '1975-02-20', 146),
(274, 'Noémi', 'Salomon', '1976-03-03', 146),
(275, 'Barbara', 'Salomon', '2005-12-21', 146),
(276, 'Dalma', 'Salomon', '2008-01-17', 146),
(277, 'Markéta', 'Havrlantová', '2016-01-12', 147),
(278, 'Háněl', 'Petr', '1974-05-07', 148),
(279, 'Hánělová ', 'Ilona', '1975-05-25', 148),
(280, 'Háněl', 'Filip', '2002-09-05', 148),
(281, 'Háněl ', 'Kryštof', '2008-02-13', 148),
(282, 'Michal', 'Brandl', '1974-03-27', 149),
(283, 'Michal', 'Brandl', '1997-10-09', 149),
(284, 'Lenka', 'Brandlová', '1974-04-10', 149),
(285, 'Anna', 'Brandlová', '1999-07-27', 149),
(286, 'Jeřábek', 'Luboš', '1971-06-03', 150),
(287, 'Jeřábková ', 'Pavlína', '1968-01-15', 150),
(288, 'Jeřábek', 'Vojtěch', '1998-03-04', 150),
(289, 'Jeřábek', 'Martin', '2001-01-25', 150),
(290, 'Petra', 'Kameníková', '2016-02-11', 151),
(291, 'Petra', 'Kameníková', '2016-02-10', 152),
(292, 'Petra', 'Kameníková', '2016-02-10', 153),
(293, 'Laďa', 'Kudela', '2016-02-09', 154),
(294, 'Radek', 'Pešák', '1970-04-13', 155),
(299, 'Filip', 'Svozilek', '1983-05-19', 157),
(300, 'Mohrova', 'Michaela', '2016-02-14', 159),
(301, 'Pešáková', 'Andrea', '1996-09-13', 155),
(302, 'Jan', 'Pata', '1980-03-23', 133),
(303, 'Michaela ', 'Stašová', '1982-12-11', 133),
(304, 'Jiří', 'Klein', '1977-01-01', 133),
(306, 'Hana', 'Štěpánová', '1953-04-11', 161),
(307, 'Hana', 'Štěpánová', '1953-04-11', 162),
(308, 'Klára', 'Kluzová', '2005-04-07', 33),
(309, 'Jakub', 'Kluz', '2007-09-01', 33),
(310, 'Markéta', 'Swaczynová', '1976-03-06', 34),
(311, 'Dvořák', 'Břetislav', '1970-12-16', 166),
(312, 'Šárka', 'Mrvíková', '1979-06-30', 166),
(314, 'Tomáš', 'Holoubek', '1981-09-24', 157),
(315, 'Marek', 'Marušák', '1977-12-26', 157),
(316, 'Michal', 'Hanslián', '1982-09-15', 157),
(317, 'Eva', 'Zachová', '1957-06-19', 121),
(326, 'Ivo', 'Dusík', '1970-01-01', 170),
(320, 'Klára', 'Zachová', '1978-03-28', 122),
(321, 'Emily', 'Zachová', '2007-06-16', 122),
(322, 'Kryštof', 'Zacha', '2011-03-09', 122),
(327, 'Jitka', 'Zámorská', '1975-03-01', 118),
(328, 'Jitka', 'Zámorská', '1975-03-01', 119),
(329, 'Jitka', 'Zámorská', '1975-03-01', 120),
(330, 'Ivo', 'Dusík', '1970-01-01', 171),
(331, 'Ivo', 'Dusík', '1970-01-01', 172),
(332, 'Ivo', 'Dusík', '1970-01-01', 173),
(333, 'Zdenek', 'Šeda', '2016-03-17', 174),
(335, 'Brabec', 'Petr', '2016-03-01', 175),
(342, 'Jana', 'Kudelová', '1968-10-12', 114),
(343, 'Jana', 'Kudelová', '1988-10-21', 115),
(344, 'Alžběta', 'Běťáková', '1993-06-14', 143),
(345, 'Libor', 'Výmola', '1977-03-19', 176),
(346, 'Jana', 'Míčková', '1977-03-10', 37),
(347, 'David', 'Výmola', '1968-03-12', 176),
(348, 'Patrik ', 'Výmola', '1991-03-18', 176),
(349, 'Radek', 'Výmola', '1980-03-11', 176),
(350, 'Markéta', 'Šteffková', '1977-05-10', 163),
(351, 'Tereza', 'Šteffková', '2010-10-02', 163),
(352, 'Markéta', 'Šteffková', '1977-05-10', 35),
(353, 'Tereza', 'Šteffková', '2010-10-02', 35),
(354, 'Kateřina', 'Zábojníková', '1978-01-10', 36),
(355, 'Ema', 'Zábojníková', '2004-06-01', 36),
(356, 'Justina ', 'Zábojníková', '2006-06-08', 36),
(357, 'Marek', 'Zábojník', '2016-03-11', 178),
(358, 'Marek', 'Zábojník', '2016-04-07', 179),
(359, 'Ivo', 'Dědek', '1984-03-09', 180),
(360, 'Libor ', 'Výmola', '1971-07-27', 181),
(361, 'Petra ', 'Výmolová', '1972-11-03', 181),
(362, 'Patrik', 'Výmola', '2000-03-23', 181),
(363, 'David', 'Výmola', '2000-03-23', 181),
(364, 'Jana', 'Sirotková', '1975-12-16', 183),
(365, 'Jiří', 'Sirotek', '1974-04-29', 183),
(366, 'Eliška ', 'Sirotková', '2005-03-23', 183),
(367, 'Anna', 'Sirotková', '2007-07-16', 183);

-- --------------------------------------------------------

--
-- Struktura tabulky `email_queue`
--

CREATE TABLE IF NOT EXISTS `email_queue` (
  `id` bigint(20) NOT NULL,
  `queue_to_email` varchar(255) NOT NULL,
  `queue_to_name` varchar(255) DEFAULT NULL,
  `queue_cc_email` varchar(255) DEFAULT NULL,
  `queue_cc_name` varchar(255) DEFAULT NULL,
  `email_queue_body_id` int(11) NOT NULL,
  `queue_sent` tinyint(4) NOT NULL DEFAULT '0',
  `queue_sent_date` datetime DEFAULT NULL,
  `queue_priority` int(11) NOT NULL DEFAULT '0',
  `queue_date_to_be_send` datetime DEFAULT NULL,
  `queue_create_date` datetime NOT NULL,
  `queue_errors_count` tinyint(4) NOT NULL DEFAULT '0',
  `queue_error` text
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `email_queue`
--

INSERT INTO `email_queue` (`id`, `queue_to_email`, `queue_to_name`, `queue_cc_email`, `queue_cc_name`, `email_queue_body_id`, `queue_sent`, `queue_sent_date`, `queue_priority`, `queue_date_to_be_send`, `queue_create_date`, `queue_errors_count`, `queue_error`) VALUES
(1, 'jiri.novak.mail@gmail.com', '', NULL, NULL, 1, 1, '2013-10-06 12:52:02', 2, '2013-10-06 12:52:02', '2013-10-06 12:52:02', 0, NULL),
(2, 'jiri.novak.mail@gmail.com', '', NULL, NULL, 2, 1, '2014-04-03 12:23:01', 2, '2014-04-03 12:22:46', '2014-04-03 12:22:46', 0, NULL),
(3, 'jiri.novak.mail@gmail.com', '', NULL, NULL, 3, 1, '2014-04-03 12:23:17', 2, '2014-04-03 12:23:02', '2014-04-03 12:23:02', 0, NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `email_queue_bodies`
--

CREATE TABLE IF NOT EXISTS `email_queue_bodies` (
  `id` bigint(20) NOT NULL,
  `queue_subject` varchar(255) DEFAULT NULL,
  `queue_from_email` varchar(255) NOT NULL,
  `queue_from_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `queue_body` text NOT NULL,
  `queue_attached_file` varchar(255) DEFAULT NULL,
  `queue_newsletter_id` int(11) DEFAULT NULL,
  `queue_shopper_id` bigint(20) DEFAULT NULL,
  `queue_branch_id` bigint(20) DEFAULT NULL,
  `queue_order_id` bigint(20) DEFAULT NULL,
  `queue_user_id` bigint(20) DEFAULT NULL,
  `queue_email_type_id` int(11) DEFAULT NULL,
  `queue_send_by_cron` tinyint(4) NOT NULL DEFAULT '0',
  `queue_language_id` int(10) unsigned NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Vypisuji data pro tabulku `email_queue_bodies`
--

INSERT INTO `email_queue_bodies` (`id`, `queue_subject`, `queue_from_email`, `queue_from_name`, `queue_body`, `queue_attached_file`, `queue_newsletter_id`, `queue_shopper_id`, `queue_branch_id`, `queue_order_id`, `queue_user_id`, `queue_email_type_id`, `queue_send_by_cron`, `queue_language_id`) VALUES
(1, 'Zpráva z kontaktního formuláře', 'jiri.novak.mail@gmail.com', 'jirka', '\r\n<br /><br />\r\n<table cellpadding="5" style="border: none; border-collapse: collapse;">\r\n  <tr>\r\n    <td><strong>Zpráva z kontaktního formuláře www stránek</strong></td>\r\n    <td>Pekařství Střelná</td>\r\n  </tr>\r\n  <tr>\r\n    <td><strong>Název stránky</strong></td>\r\n    <td>O pekařství</td>\r\n  </tr>\r\n  <tr>\r\n    <td><strong>URL</strong></td>\r\n    <td>http://strelna.dgsbeta.cz/o-pekarstvi</td>\r\n  </tr>\r\n  \r\n  <tr>\r\n    <td><strong>Jméno odesílatele</strong></td>\r\n    <td>jirka</td>\r\n  </tr>\r\n   <tr>\r\n    <td><strong>E-mail odesílatele</strong></td>\r\n    <td>jiri.novak.mail@gmail.com</td>\r\n  </tr>\r\n  \r\n    <tr>\r\n    <td><strong>Telefon</strong></td>\r\n    <td>27858585757857</td>\r\n  </tr>\r\n   \r\n\r\n  <tr>\r\n    <td colspan="2">&nbsp;</td>\r\n  </tr>\r\n  <tr>\r\n    <td colspan="2">Text dotazu:</td>\r\n  </tr>\r\n  <tr>\r\n    <td colspan="2">čgterwyřreřr</td>\r\n  </tr>\r\n  <tr>\r\n</table>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(2, '', 'jiri.novak.mail@gmail.com', 'jirka', '\r\n<br /><br />\r\n<table cellpadding="5" style="border: none; border-collapse: collapse;">\r\n  <tr>\r\n    <td><strong>Objednávka apartmánu</strong></td>\r\n    <td>Alpine Living</td>\r\n  </tr>\r\n  <tr>\r\n    <td><strong>Název stránky</strong></td>\r\n    <td>Rezervace</td>\r\n  </tr>\r\n  <tr>\r\n    <td><strong>URL</strong></td>\r\n    <td>http://www.alpineliving.cz/rezervace</td>\r\n  </tr>\r\n  \r\n  <tr>\r\n    <td><strong>Jméno odesílatele</strong></td>\r\n    <td>jirka</td>\r\n  </tr>\r\n   <tr>\r\n    <td><strong>E-mail odesílatele</strong></td>\r\n    <td>jiri.novak.mail@gmail.com</td>\r\n  </tr>\r\n  \r\n    <tr>\r\n    <td><strong>Telefon</strong></td>\r\n    <td>27858585757857</td>\r\n  </tr>\r\n    \r\n     <tr>\r\n    <td><strong>Apartmán</strong></td>\r\n    <td>APARTMÁN KAIBLING B10 - pro 4 osoby</td>\r\n  </tr>\r\n    \r\n     <tr>\r\n    <td><strong>Termín</strong></td>\r\n    <td>zítra odpo (test funkčnosti)</td>\r\n  </tr>\r\n   \r\n\r\n  <tr>\r\n    <td colspan="2">&nbsp;</td>\r\n  </tr>\r\n\r\n</table>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(3, '', 'jiri.novak.mail@gmail.com', 'jirka', '\r\n<br /><br />\r\n<table cellpadding="5" style="border: none; border-collapse: collapse;">\r\n  <tr>\r\n    <td><strong>Objednávka apartmánu</strong></td>\r\n    <td>Alpine Living</td>\r\n  </tr>\r\n  <tr>\r\n    <td><strong>Název stránky</strong></td>\r\n    <td>Rezervace</td>\r\n  </tr>\r\n  <tr>\r\n    <td><strong>URL</strong></td>\r\n    <td>http://www.alpineliving.cz/rezervace</td>\r\n  </tr>\r\n  \r\n  <tr>\r\n    <td><strong>Jméno odesílatele</strong></td>\r\n    <td>jirka</td>\r\n  </tr>\r\n   <tr>\r\n    <td><strong>E-mail odesílatele</strong></td>\r\n    <td>jiri.novak.mail@gmail.com</td>\r\n  </tr>\r\n  \r\n    <tr>\r\n    <td><strong>Telefon</strong></td>\r\n    <td>27858585757857</td>\r\n  </tr>\r\n    \r\n     <tr>\r\n    <td><strong>Apartmán</strong></td>\r\n    <td>APARTMÁN KAIBLING B10 - pro 4 osoby</td>\r\n  </tr>\r\n    \r\n     <tr>\r\n    <td><strong>Termín</strong></td>\r\n    <td>zítra odpo (test funkčnosti)</td>\r\n  </tr>\r\n   \r\n\r\n  <tr>\r\n    <td colspan="2">&nbsp;</td>\r\n  </tr>\r\n\r\n</table>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `email_receivers`
--

CREATE TABLE IF NOT EXISTS `email_receivers` (
  `id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `email_settings`
--

CREATE TABLE IF NOT EXISTS `email_settings` (
  `id` int(11) NOT NULL,
  `mailer` varchar(16) NOT NULL DEFAULT 'mail',
  `host` varchar(32) NOT NULL DEFAULT 'localhost',
  `port` int(11) NOT NULL DEFAULT '25',
  `SMTPSecure` varchar(32) DEFAULT NULL,
  `SMTPAuth` tinyint(4) NOT NULL DEFAULT '0',
  `username` varchar(64) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `SMTPDebug` tinyint(4) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `email_settings`
--

INSERT INTO `email_settings` (`id`, `mailer`, `host`, `port`, `SMTPSecure`, `SMTPAuth`, `username`, `password`, `SMTPDebug`) VALUES
(1, 'smtp', 'mail.dghost.cz', 25, '', 1, 'smtp@dghost.cz', 'smtpmageror', 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `email_types`
--

CREATE TABLE IF NOT EXISTS `email_types` (
  `id` int(11) NOT NULL,
  `nazev` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `template` varchar(255) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `from_nazev` varchar(255) DEFAULT '',
  `from_email` varchar(255) DEFAULT '',
  `use_email_queue` tinyint(4) NOT NULL DEFAULT '0',
  `send_by_cron` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `email_types`
--

INSERT INTO `email_types` (`id`, `nazev`, `code`, `template`, `subject`, `from_nazev`, `from_email`, `use_email_queue`, `send_by_cron`) VALUES
(1, 'Newsletter', 'newsletter', '', '', '', '', 1, 1),
(2, 'Kontaktní formulář', 'form_contact', '', 'Zpráva z kontaktního formuláře', 'Pekařství střelná', 'info@pekarstvi-strelna.cz', 1, 0),
(3, 'Rezervace', 'form_reserve', '', '', 'Objednávky - Alpine Living', 'rezervace@alpineliving.cz', 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `email_types_receivers`
--

CREATE TABLE IF NOT EXISTS `email_types_receivers` (
  `id` int(11) NOT NULL,
  `email_type_id` int(11) NOT NULL,
  `email_receiver_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `language_strings`
--

CREATE TABLE IF NOT EXISTS `language_strings` (
  `id` int(10) unsigned NOT NULL,
  `read_only` tinyint(4) NOT NULL DEFAULT '0',
  `zobrazit` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `language_string_data`
--

CREATE TABLE IF NOT EXISTS `language_string_data` (
  `id` int(10) unsigned NOT NULL,
  `language_id` int(11) NOT NULL,
  `string` varchar(255) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `manufacturers`
--

CREATE TABLE IF NOT EXISTS `manufacturers` (
  `id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci DEFAULT '',
  `photo` varchar(255) COLLATE utf8_czech_ci DEFAULT '',
  `poradi` int(11) NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `manufacturer_data`
--

CREATE TABLE IF NOT EXISTS `manufacturer_data` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(10) unsigned NOT NULL,
  `kod` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `verze` varchar(10) COLLATE utf8_czech_ci DEFAULT NULL,
  `datum` date DEFAULT NULL,
  `autor` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `popis` text COLLATE utf8_czech_ci,
  `poznamka` text COLLATE utf8_czech_ci,
  `poradi` int(11) NOT NULL DEFAULT '0',
  `admin_zobrazit` tinyint(4) NOT NULL DEFAULT '0',
  `available` tinyint(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `modules`
--

INSERT INTO `modules` (`id`, `kod`, `nazev`, `verze`, `datum`, `autor`, `url`, `email`, `popis`, `poznamka`, `poradi`, `admin_zobrazit`, `available`) VALUES
(1, 'page', 'Stránky', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul zobrazující klasický statický obsah ve stránkách.', NULL, 1, 1, 1),
(2, 'link', 'Odkazy', '1', NULL, 'Pavel Herink', NULL, NULL, 'Odkaz na externí stránku.', NULL, 2, 1, 1),
(3, 'article', 'Články', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul článků a novinek.', NULL, 3, 1, 1),
(4, 'contact', 'Kontaktní formulář', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul kontaktního formuláře.', NULL, 4, 1, 1),
(5, 'reference', 'Reference', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul fotogalerie.', NULL, 5, 1, 1),
(6, 'product', 'Eshop', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul e-shop.', NULL, 6, 0, 1),
(7, 'shoppingcart', 'Košík', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul objednávky a košíku.', NULL, 7, 0, 1),
(8, 'user', 'Zákazník', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul zákaznického účtu.', NULL, 8, 0, 1),
(9, 'order', 'Objednávka', '1', NULL, 'Pavel Herink', NULL, NULL, NULL, NULL, 9, 0, 1),
(10, 'search', 'Vyhledávání', '1', NULL, 'Pavel Herink', NULL, NULL, NULL, NULL, 10, 0, 1),
(20, 'catalog', 'Katalog produktů', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul katalogu produktů.', NULL, 11, 1, 1),
(21, 'shop', 'Prodejny', '1', NULL, 'Pavel Herink', NULL, NULL, NULL, NULL, 12, 1, 1),
(22, 'reservation', 'Rezervace', '0', NULL, 'Jiří Novák', NULL, NULL, 'Rezervace apartmánů', NULL, 0, 0, 1),
(23, 'vp', 'Virtuální prohlídka', '1', NULL, NULL, NULL, NULL, NULL, NULL, 13, 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `module_actions`
--

CREATE TABLE IF NOT EXISTS `module_actions` (
  `id` int(10) unsigned NOT NULL,
  `module_id` int(11) NOT NULL,
  `kod` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `povoleno` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `owner_data`
--

CREATE TABLE IF NOT EXISTS `owner_data` (
  `id` int(11) NOT NULL,
  `copyright` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `default_title` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `default_description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `default_keywords` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `firma` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `ulice` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `mesto` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `psc` varchar(16) COLLATE utf8_czech_ci DEFAULT NULL,
  `stat` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `ic` varchar(16) COLLATE utf8_czech_ci DEFAULT NULL,
  `dic` varchar(16) COLLATE utf8_czech_ci DEFAULT NULL,
  `tel` varchar(16) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `www` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `ga_script` text COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `owner_data`
--

INSERT INTO `owner_data` (`id`, `copyright`, `default_title`, `default_description`, `default_keywords`, `firma`, `ulice`, `mesto`, `psc`, `stat`, `ic`, `dic`, `tel`, `email`, `www`, `ga_script`) VALUES
(1, NULL, 'Cestuj karavanem', '', '', '', '', '', '', NULL, NULL, NULL, '', 'jiri.novak.mail@gmail.com', NULL, '');

-- --------------------------------------------------------

--
-- Struktura tabulky `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) unsigned NOT NULL,
  `page_category_id` int(11) NOT NULL,
  `poradi` int(10) unsigned NOT NULL,
  `indexpage` tinyint(4) NOT NULL,
  `new_window` tinyint(4) NOT NULL DEFAULT '0',
  `show_in_menu` tinyint(4) DEFAULT '1',
  `direct_to_sublink` tinyint(4) NOT NULL DEFAULT '0',
  `show_in_submenu` tinyint(4) NOT NULL DEFAULT '0',
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `nav_class` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `show_contactform` tinyint(4) NOT NULL DEFAULT '0',
  `show_child_pages_index` tinyint(4) NOT NULL DEFAULT '0',
  `protected` tinyint(4) NOT NULL DEFAULT '0',
  `photo_src` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `show_photo_detail` tinyint(4) NOT NULL DEFAULT '0',
  `youtube_code` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `anchor` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=COMPACT;

--
-- Vypisuji data pro tabulku `pages`
--

INSERT INTO `pages` (`id`, `page_category_id`, `poradi`, `indexpage`, `new_window`, `show_in_menu`, `direct_to_sublink`, `show_in_submenu`, `available_languages`, `nav_class`, `show_contactform`, `show_child_pages_index`, `protected`, `photo_src`, `show_photo_detail`, `youtube_code`, `parent_id`, `anchor`) VALUES
(1, 3, 1, 1, 0, 1, 0, 0, 5, '', 0, 1, 0, 'index', 0, NULL, 0, 1),
(4, 3, 5, 0, 0, 1, 0, 0, 5, NULL, 0, 0, 0, NULL, 0, NULL, 0, 1),
(5, 2, 16, 0, 0, 1, 0, 0, 1, '', 0, 0, 0, NULL, 0, NULL, 0, 1),
(6, 2, 17, 0, 0, 1, 0, 0, 1, '', 0, 0, 0, NULL, 0, NULL, 0, 1),
(7, 2, 18, 0, 0, 1, 0, 0, 1, '', 0, 0, 0, NULL, 0, NULL, 0, 1),
(8, 2, 19, 0, 0, 1, 0, 0, 1, '', 0, 0, 0, NULL, 0, NULL, 0, 1),
(11, 3, 22, 0, 0, 1, 0, 0, 5, '', 1, 0, 0, 'kontakty', 0, NULL, 0, 1),
(12, 2, 20, 0, 0, 1, 0, 0, 1, '', 0, 0, 0, NULL, 0, NULL, 0, 1),
(13, 3, 2, 0, 0, 1, 0, 0, 5, '', 0, 0, 0, 'apartmany', 0, NULL, 0, 1),
(14, 3, 10, 0, 0, 1, 0, 0, 5, NULL, 0, 0, 0, NULL, 0, NULL, 0, 1),
(15, 2, 21, 0, 0, 1, 0, 0, 1, '', 0, 0, 0, 'pro-golfisty', 0, NULL, 0, 1),
(16, 3, 14, 0, 0, 1, 0, 0, 5, NULL, 0, 0, 0, 'leto', 0, NULL, 0, 0),
(17, 3, 7, 0, 0, 1, 0, 0, 5, '', 0, 0, 0, 'zima', 0, NULL, 0, 1),
(18, 3, 6, 0, 0, 1, 0, 0, 5, '', 0, 0, 0, 'webkamery', 0, NULL, 0, 1),
(19, 3, 13, 0, 0, 1, 0, 0, 5, '', 0, 0, 0, NULL, 0, NULL, 0, 1),
(20, 3, 15, 0, 0, 1, 0, 0, 5, NULL, 0, 0, 0, NULL, 0, NULL, 0, 1),
(21, 3, 2, 0, 0, 1, 0, 0, 5, NULL, 0, 0, 0, 'turistika', 0, NULL, 16, 1),
(22, 3, 3, 0, 0, 1, 0, 0, 5, NULL, 0, 0, 0, 'golf', 0, NULL, 16, 1),
(23, 3, 4, 0, 0, 1, 0, 0, 5, NULL, 0, 0, 0, 'kolo', 0, NULL, 16, 1),
(24, 3, 5, 0, 0, 1, 0, 0, 5, NULL, 0, 0, 0, 'koupani', 0, NULL, 16, 1),
(25, 3, 4, 0, 0, 1, 0, 0, 5, '', 0, 0, 0, 'ke-stazeni', 0, NULL, 0, 1),
(26, 3, 6, 0, 0, 1, 0, 0, 5, NULL, 0, 0, 0, NULL, 0, NULL, 16, 0),
(27, 3, 7, 0, 0, 1, 0, 0, 1, '', 0, 0, 0, 'rafty-kajak', 0, NULL, 16, 0),
(29, 2, 23, 0, 0, 1, 0, 0, 1, '', 0, 0, 0, NULL, 0, NULL, 0, 0),
(30, 3, 1, 0, 0, 1, 0, 0, 1, '', 0, 0, 0, 'sommercard', 0, NULL, 16, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `page_categories`
--

CREATE TABLE IF NOT EXISTS `page_categories` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `poradi` int(10) unsigned NOT NULL,
  `admin_zobrazit` tinyint(3) unsigned NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `page_categories`
--

INSERT INTO `page_categories` (`id`, `code`, `nazev`, `poradi`, `admin_zobrazit`) VALUES
(1, 'system', 'Systémové stránky', 1, 1),
(2, 'unrelated', 'Nezařazené stránky', 2, 1),
(3, 'nav', 'Stránky v hlavní navigaci', 3, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `page_data`
--

CREATE TABLE IF NOT EXISTS `page_data` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL DEFAULT '',
  `nadpis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `url` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

--
-- Vypisuji data pro tabulku `page_data`
--

INSERT INTO `page_data` (`id`, `page_id`, `language_id`, `route_id`, `nazev`, `nadpis`, `uvodni_popis`, `popis`, `url`) VALUES
(1, 1, 1, 1, 'Úvod', 'Luxusní Ski-in apartmány', '', NULL, ''),
(19, 12, 1, 66, 'Novinky', 'Novinky', '', NULL, ''),
(21, 14, 1, 81, 'Rezervace', 'PŘEHLED REZERVACÍ', '', '<p>Apartmány můžete rezervovat:</p>\n\n<ol>\n	<li>e-mailem: <a href="mailto:rezervace@alpineliving.cz?subject=Rezervace%20apartm%C3%A1n%C5%AF">rezervace@alpineliving.cz</a>&nbsp;<br />\n	&nbsp;</li>\n	<li>telefonicky na čísle <strong>+420 605 238 595</strong><br />\n	&nbsp;</li>\n	<li><a href="" id="openForm">Rezervačním formulářem</a></li>\n</ol>\n\n<p>&nbsp;&nbsp;<span style="color:#FF0000;">&nbsp;&nbsp; (minimální délka pobytu jsou 3 noci)</span></p>\n', ''),
(23, 16, 1, 83, 'Léto', 'Nabídka v letních měsících', '', '<ul>\n	<li>velký obývací pokoj s kuchyní a rozkládací pohovkou pro 2 osoby, SAT TV</li>\n	<li>plně vybavená kuchyň, kávovar, myčka, trouba, mikrovlnná trouba</li>\n	<li>terasa 9m2 s posezením a výhledem na sjezdovku</li>\n	<li>velký obývací pokoj s kuchyní a rozkládací pohovkou pro 2 osoby, SAT TV</li>\n	<li>plně vybavená kuchyň, kávovar, myčka, trouba, mikrovlnná trouba</li>\n	<li>terasa 9m2 s posezením a výhledem na sjezdovku</li>\n</ul>\n\n<p><a class="special-offer" href="/pro-golfisty">Speciální nabídka pro golfisty</a> <!--<aside class="right fg4">\n					<img src="img/plan/apartment.jpg" alt="apartment">\n				</aside>--></p>\n\n<div class="shadow-corner-right">&nbsp;</div>\n<!-- end page content --><!-- track plans -->\n\n<section class="trackplans-widget">\n<h2>Sjezdovky</h2>\n\n<div class="wrapper row fg-no-gutter">\n<div class="item"><a href="./"><img alt="hp-01" src="media/img/plan/track-01-summer.jpg" /></a></div>\n\n<div class="item"><a href="./"><img alt="hp-02" src="media/img/plan/track-02-summer.jpg" /></a></div>\n</div>\n\n<div class="download row fg-no-gutter"><a class="map-dl" href="./">Turistické mapy ke stažení</a></div>\n</section>\n\n<div class="shadow-corner-right">&nbsp;</div>\n<!-- end track plans --><!-- costs -->\n\n<article class="triptips">\n<h2>Tipy na výlet, lázně</h2>\n\n<ul>\n	<li>informace, zajímavé turistické cíle</li>\n	<li>tipy na výlety</li>\n	<li>informace, zajímavé turistické cíle</li>\n	<li>tipy na výlety</li>\n</ul>\n</article>\n', ''),
(24, 17, 1, 84, 'Tipy na výlety', 'Tipy na výlety', '', '<ul>\n	<li>region patří do TOP 5 lyžařských oblastí RAKOUSKA</li>\n	<li>organizátor mistrovství světa v lyžování 1982 a 2013</li>\n	<li>tradiční pořadatel &bdquo;nočního&ldquo; slalomu v seriálu Světového poháru FIS</li>\n	<li>100%&nbsp; pokrytí supermoderním zasněžovacím zařízením umožňující lyžování od konce listopadu až do konce dubna</li>\n	<li>123 km perfektně upravených sjezdovek a 56 pohodlných kabinových či sedačkových lanovek a vleků</li>\n	<li>4,6 km dlouhá FIS sjezdovka z Planai končící v centru Schladmingu</li>\n	<li>skipas AMADÉ platící v neuvěřitelně rozsáhlé oblasti - 860 km sjezdovek</li>\n	<li>několik freeridových zón v HAUSER KAIBLING a nový snowpark na Planai</li>\n	<li>ráj příznivců bílé stopy lákající na 472 km perfektně upravených klasických a skatingových drah</li>\n</ul>\n\n<section class="trackplans-widget">\n<h2>HAUSER KAIBLING &ndash; mapa střediska</h2>\n<a href="http://alpine.dgsbeta.cz/media/userfiles/Obrazky-mix/Horni_mapa_vetsi__hauser_kaibling_m.jpg"><img alt="" src="http://alpine.dgsbeta.cz/media/userfiles/Obrazky-mix/hauser_kaibling_m.jpg" /></a>\n\n<h2>&nbsp;</h2>\n\n<h2>SCHLADMING / DACHSTEIN</h2>\n\n<p><a href="http://alpine.dgsbeta.cz/media/userfiles/Obrazky-mix/spodni-mapa_vetsi_strediska_celkova.jpg"><img alt="" src="http://alpine.dgsbeta.cz/media/userfiles/Obrazky-mix/spodni-mapa_mensi_strediska_celkova.jpg" style="width: 933px; height: 538px;" /></a></p>\n\n<h2>Další aktivity</h2>\n\n<div class="row fg-no-gutter">\n<div class="item"><img alt="" src="http://alpine.dgsbeta.cz/media/userfiles/Obrazky-mix/lazne.jpg" />\n<ul>\n	<li>AQUAPARK SCHLADMING (vzdálenost z&nbsp;apartmánů 5 min) <a href="http://www.erlebnisbad-schladming.at">http://www.erlebnisbad-schladming.at</a></li>\n</ul>\n</div>\n\n<div class="item"><img alt="" src="http://alpine.dgsbeta.cz/media/userfiles/Obrazky-mix/sanky_hochwurzen.jpg" />\n<ul>\n	<li>SÁŇKAŘSKÁ DRÁHA HOCHWURZEN &ndash; délka dráhy 6,3km (vzdálenost z&nbsp;apartmánů 10 min)<a href="http://www.nachtrodeln.at/"> </a><a href="http://www.nachtrodeln.at/">http://www.nachtrodeln.at/</a></li>\n</ul>\n</div>\n\n<div class="item"><img alt="" src="http://alpine.dgsbeta.cz/media/userfiles/Obrazky-mix/bezky_ramsau.jpg" />\n<ul>\n	<li>BĚŽKAŘSKÝ RÁJ č.1 v&nbsp;RAKOUSKU &ndash; (vzdálenost z&nbsp;apartmánů 10 min)<a href="http://www.ramsau.com"> www.ramsau.com</a></li>\n	<li>PŘEHLED VEŠKERÝCH ZIMNÍCH AKTIVIT V&nbsp;REGIONU&nbsp; <a href="http://www.schladming-dachstein.at/">http://www.schladming-dachstein.at/</a></li>\n</ul>\n</div>\n</div>\n</section>\n', ''),
(11, 4, 1, 37, 'Fotogalerie', 'Fotogalerie', '', NULL, ''),
(12, 5, 1, 42, 'Prohlášení o přístupnosti', 'Prohlášení o přístupnosti', '', '<h2>Prohlášení o přístupnosti</h2>\n\n<p><span style="color:rgb(0, 0, 0)">Internetové stránky společnosti</span><span style="color:rgb(0, 0, 0)">&nbsp;jsou přístupné podle specifikace definované&nbsp;</span><a href="http://www.pristupnost.cz/zakon-365-2000-sb-o-informacnich-systemech-verejne-spravy/">zákonem č. 365/2000 Sb.</a><span style="color:rgb(0, 0, 0)">&nbsp;a použitá technická řešení vychází z metodik&nbsp;</span><a href="http://www.blindfriendly.cz/doc/bfw.php">Blink Friendly Web</a><span style="color:rgb(0, 0, 0)">,&nbsp;</span><a href="http://www.blindfriendly.cz/wcag20/">WCAG&nbsp;2.0</a><span style="color:rgb(0, 0, 0)">&nbsp;nebo poznatky a zkušenosti samotných slabozrakých a nevidomých uživatelů Internetu.</span><br />\n&nbsp;</p>\n\n<h3>Klávesové zkratky na tomto webu - rozšířené</h3>\n\n<p><span style="color:rgb(0, 0, 0)">Tato stránka dodržuje&nbsp;</span><a href="http://www.ippi.cz/standard-klavesovych-zkratek/">standard klávesových zkratek</a><span style="color:rgb(0, 0, 0)">&nbsp;vytvořený Iniciativou pro přístupnější internet.&nbsp;</span><br />\n&nbsp;</p>\n\n<h3>Seznam zkratek</h3>\n\n<ul>\n	<li>0 - Na obsah stránky</li>\n	<li>1 - Nápověda ke klávesovým zkratkám</li>\n	<li>2 - Úvodní strana</li>\n	<li>3 - Mapa stránek</li>\n	<li>4 - Vyhledávání</li>\n</ul>\n\n<p><br />\n<span style="color:rgb(0, 0, 0)">Použití zkratek v prohlížeči Pro volbu čísel používejte horní řádek na klávesnici, nikoliv numerický blok vpravo.&nbsp;</span><br />\n&nbsp;</p>\n\n<h3>Použití klávesových zkratek v prostředí Microsoft Windows</h3>\n\n<ul>\n	<li>Internet Explorer -&nbsp;Levý Alt + Shift + přiřazené číslo, nutno potvrdit&nbsp;ENTER</li>\n	<li>Mozilla Firefox -&nbsp;Levý Alt + Shift + přiřazené číslo</li>\n	<li>Opera -&nbsp;Shift + Esc + přiřazené číslo</li>\n</ul>\n\n<p>&nbsp;</p>\n\n<h3>Použití klávesových zkratek v prostředí Mac OS X</h3>\n\n<ul>\n	<li>Mozilla Firefox -&nbsp;Ctrl + přiřazené číslo</li>\n	<li>Safari -&nbsp;Ctrl + Alt + přiřazené číslo</li>\n</ul>\n\n<p>&nbsp;</p>\n\n<h3>Použití klávesových zkratek v prostředí Linux</h3>\n\n<ul>\n	<li>Mozilla Firefox -&nbsp;Levý Alt + Shift + přiřazené číslo</li>\n</ul>\n\n<p>&nbsp;</p>\n\n<h3>Kód a design stránek</h3>\n\n<p><span style="color:rgb(0, 0, 0)">Zdrojový kód těchto stránek důsledně dodržuje syntaktická a sémantická pravidla definovaná mezinárodním konzorciem&nbsp;</span>W3C<span style="color:rgb(0, 0, 0)">&nbsp;a je validní podle standard&nbsp;</span>XHTML<span style="color:rgb(0, 0, 0)">&nbsp;1.0 Strict.</span><br />\n<span style="color:rgb(0, 0, 0)">Jednotnou a univerzálně funkční vizuální prezentaci obsahu garantují důsledně strukturalizované kaskádové styly, které jsou zcela odděleny od obsahu i funkčního výkoného jádra webu.</span><br />\n<span style="color:rgb(0, 0, 0)">Základní stylopis striktně odpovídá standardu&nbsp;</span>CSS<span style="color:rgb(0, 0, 0)">&nbsp;2.0 a zajištuje jednotné zobrazení a plnou funkčnost stránek pro všechny hlavní internetová prohlížeče.&nbsp;</span><br />\n<span style="color:rgb(0, 0, 0)">Technická omezení a implementační odlišnosti některých starších prohlížečů (</span>IE6<span style="color:rgb(0, 0, 0)">), mobilních klientů (mobilní telefony,&nbsp;</span>PDA<span style="color:rgb(0, 0, 0)">) a alternativních výstupních zařízení (čtečky pro nevidomé, tisk) jsou ošetřena striktně oddělenými stylopisy s využitím specifických vlastností a důsledně cílenou a omezenou platností odpovídající konkrétní platformě.&nbsp;</span><br />\n&nbsp;</p>\n\n<h3>Přístupnost informačního obsahu</h3>\n\n<p><span style="color:rgb(0, 0, 0)">Informačního obsahu webu je vždy stejný a plnohodnotný nezávisle na technologii použité k jeho prezentaci.</span><br />\n<span style="color:rgb(0, 0, 0)">Plnou vizuální i funkční podporu mají stránky v těchto prohlížečích pro Windows a na platformě PC:</span></p>\n\n<ul>\n	<li>Internet Explorer 7 a vyšší</li>\n	<li>FireFox 2.5 a vyšší</li>\n	<li>Safari 3.2 a vyšší</li>\n	<li>Opera 9 a vyšší</li>\n	<li>Chrome</li>\n</ul>\n\n<p>Dále klade plná podpora stránek na internetový prohlížeč tyto požadavky:</p>\n\n<ul>\n	<li>automatické načítání obrázků</li>\n	<li>zpracování kaskádových stylů</li>\n	<li>interpretace JavaScriptu</li>\n</ul>\n\n<p><span style="color:rgb(0, 0, 0)">Bez povolení těchto vlastností a pro ostatní prohlížeče, platformy a výstupní zařízení nemusí být vždy dodržen jednotný vzhled a funkčnost prezentace. V případě nekompatibility některé části webu je však vždy stejný informační obsah v plném rozsahu zpřístupněn alternativním způsobem.&nbsp;</span><br />\n&nbsp;</p>\n\n<h3>Texty a obsah stránek</h3>\n\n<p><span style="color:rgb(0, 0, 0)">Veškeré texty na tomto webu mají standardní formát, kontrastní barvy vzhledem k pozadí a jsou zobrazeny v jednotné znakové sadě (</span>UTF-8<span style="color:rgb(0, 0, 0)">). Velikost písma je definována v relativních jednotkách a lze ji pomocí standardních nástrojů integrovaných v internetových prohlížeči jednoduše zvětšovat a zmenšovat.</span><br />\n<span style="color:rgb(0, 0, 0)">Uživatelské rozhraní prezentace je navrženo s důrazem na snadnou orientaci v obsahu a jednoduché ovládání nejen v alternativních prohlížečích ale i pro návštěvníky se specifickými potřebami.&nbsp;</span><br />\n&nbsp;</p>\n\n<h3>Přepínání klávesou&nbsp;TAB&nbsp;(tabindex)</h3>\n\n<p><span style="color:rgb(0, 0, 0)">Ovládání stránek není závislé na použití myši, ale navigační odkazy lze je procházet pomocí klávesy&nbsp;</span>TAB<span style="color:rgb(0, 0, 0)">.</span></p>\n', ''),
(13, 6, 1, 43, 'Ochrana osobních údajů', 'Ochrana osobních údajů', '', '<h2>Ochrana osobních údajů</h2>\n\n<p><span style="color:rgb(0, 0, 0)">Údaje získané prostřednictvím komunikačních formulářů jsou uchovávány v souladu s platnými zákony České republiky, zejména se zákonem o ochraně osobních údajů č. 101/2000 Sb. ve znění pozdějších dodatků a předpisů.</span><br />\n<br />\n<strong>Alpine Living</strong><span style="color:rgb(0, 0, 0)">&nbsp;veškeré takto získané údaje užívá výhradně pro svou vnitřní potřebu a neposkytuje je třetím osobám. S těmito osobními údaji je nakládáno pouze a výhradně v rozsahu, který připouští zákon o ochraně osobních údajů, a to především v &sect; 5 odst. 2 písm. b) a v &sect; 5 odst. 6.</span></p>\n', ''),
(14, 7, 1, 45, 'Mapa stránek', 'Mapa stránek', '', NULL, ''),
(15, 8, 1, 46, 'Vyhledávání', 'Vyhledávání', '', NULL, ''),
(18, 11, 1, 65, 'Kontaktujte nás', 'Jak se k nám dostanete', '', '<p>Adresa:</p>\n\n<p>XXX 2222</p>\n\n<p>22222</p>\n\n<p>Austria</p>\n\n<p>&nbsp;</p>\n\n<p>Kontaktní osoba:</p>\n\n<p>Jméno Příjmení</p>\n\n<p>&nbsp;</p>\n\n<p>Popis trasy automobilem, trasy automobilem, trasy automobilem</p>\n\n<p style="text-align: center;">&nbsp;</p>\n', ''),
(22, 15, 1, 82, 'Pro golfisty', 'Pro golfisty', '', '<p>Břopy o hur nidich tacle zkuc břou mivuni. Dimřa krychru hý měteň. V trých diraj chráhlý k ditrašt o plicla, hlis uhřar moclou těč a judimo mlepi. Nachaškas ponič bláb tě. Réni hlir třanti sládě nivochly bip těčděl zlaclák clič nihů.</p>\n\n<p>Digru děbuclo vuže drodichlýmly a něllkýžlous. A běd clevibre s dlezko i hluh. Titlyř něškuh di tětiz, hu niplav chrýdry mu unin chůdišu ni deklid kru flokyt nitlu v břa.</p>\n\n<h3>nadpis</h3>\n\n<p>Dlatke písty hru i dici děhlouš bob. Chlyblitižlyš vřatěr chlym. Okly zlufrou k puplyt guktoř děč a něžludi pytřá škešty skop bler. šlechlich umřa ryn lýhluzle meblou kom dlupo úďouš gá, sattlá diglybév mlev. Buř.</p>\n', ''),
(20, 13, 1, 79, 'Půjčovna', 'Půjčovna', '', '<p>APARTMÁN RAMSAU &ndash; pro 4 osoby<a href="http://www.alpineliving.cz/media/userfiles/Obrazky-mix/pudB9.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/Obrazky-mix/pubB9small.jpg" style="width: 250px; height: 216px; float: right;" /></a></p>\n\n<ul>\n	<li>velký obývací pokoj s kuchyní a rozkládací pohovkou pro 2 osoby, SAT TV</li>\n	<li>plně vybavená kuchyň, kávovar, toustovač, varná konvice, myčka, trouba, mikr. trouba</li>\n	<li>terasa 9 m<sup>2</sup> s posezením a výhledem na sjezdovku</li>\n	<li>ložnice s manželskou postelí, SAT TV</li>\n	<li>samostatné WC, koupelna se sprchovým koutem, privátní finská sauna, pračka v apartmánu</li>\n	<li>ZDARMA Wi-Fi internet, iPAD k dispozici na apartmánu</li>\n	<li>podzemní garážové stání a privátní vyhřívaná lyžárna</li>\n	<li>bezbariérový přístup</li>\n</ul>\n', ''),
(25, 18, 1, 87, 'Video tipy', 'Video tipy', '', '<p><strong>Další webkamery</strong></p>\n\n<p>&nbsp;</p>\n\n<div style="width:100%">\n<div class="full-width-img" style="width:400px;display:inline-block"><a href="http://www.hauser-kaibling.at/de/wetter-schnee/webcams/"><img alt="" src="http://alpine.dgsbeta.cz/media/userfiles/Obrazky-mix/cam7.jpg" style="width: 400px; height: 300px;" /></a></div>\n\n<div class="full-width-img" style="width:450px;display:inline-block"><iframe src="http://www.vymola.cz/alpinecam.php?type=cam&amp;h=300&amp;w=400" style="width: 100%; height: 320px; border: none;margin-top: -8px;margin-left: 117px;"></iframe></div>\n</div>\n\n<p><a href="http://www.hauser-kaibling.at/de/wetter-schnee/webcams/">Více zde..</a></p>\n\n<p>Pohled na sjezdovky: <a href="http://www.planai.at/winter/en/Home.html">PLANAI TOP STATION, DACHSTEIN a SCHLADMING-DACHSTEIN</a></p>\n', ''),
(26, 19, 1, 89, 'O nás', 'O nás', '', '<!-- costs -->\n<section class="tariff">\n<div class="row fg-no-gutter">\n<div class="winter fg8">\n<h3>Zimní sezona 2016/2017</h3>\n\n<div class="row fg-no-gutter">\n<article class="item rounded">\n<header class="rounded shadow">Vedlejší sezona</header>\n\n<section class="date">\n<p>26.11.2016 - 20.12.2016<br />\n06.03.2017 - 16.04.2017</p>\n\n<p>130 EUR / NOC</p>\n\n<p>(apartmán pro 4 osoby)</p>\n</section>\n</article>\n\n<article class="item rounded">\n<header class="rounded shadow">Hlavní sezona</header>\n\n<section class="date">\n<p>21.12.2016 - 05.03.2017<br />\n&nbsp;</p>\n</section>\n\n<footer class="cost rounded">\n<p>160 EUR / NOC</p>\n\n<p>(apartmán pro 4 osoby)</p>\n</footer>\n</article>\n</div>\n</div>\n\n<div class="summer fg4">\n<h3>Léto 2016</h3>\n\n<article class="item rounded">\n<header class="rounded shadow">Léto 2016</header>\n\n<section class="date">\n<p>20.05.2016 - 16.10.2016<br />\n&nbsp;</p>\n</section>\n\n<footer class="cost rounded">\n<p>70 EUR / NOC</p>\n\n<p>(apartmán pro 4 osoby)</p>\n</footer>\n</article>\n<img alt="" src="http://alpineliving.cz/media/userfiles/SOMMERCARD/sommercard2.jpg" style="width: 282px; height: 177px; margin-left: 5px; margin-top: 11px;" /></div>\n</div>\n\n<div class="surcharge">\n<p>&nbsp;</p>\n\n<h3><strong>Příplatky - povinné</strong></h3>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">ZÁVĚREČNÝ ÚKLID APARTMÁNU VČ. LOŽNÍHO PRÁDLA, RUČNÍKŮ A OSUŠEK PRO 4 OSOBY, ENERGIE &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>65 EUR / APARTMÁN</strong></span></p>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">POBYTOVÁ TAXA OBCE HAUS / NOC (POUZE OSOBY STARŠÍ&nbsp;15 LET) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong>1,50 EUR / OSOBU / NOC</strong></span></p>\n&nbsp;\n\n<h3><strong>V CENĚ APARTMÁNU JE ZAHRNUTO</strong></h3>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">LETNÍ KARTA &quot;SOMMERCARD&quot; (V OBDOBÍ OD 20.5.2016 DO 16.10.2016) PRO APARTMÁNY RAMSAU A KAIBLING</span></p>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">NEOMEZENÉ Wi-Fi PŘIPOJENÍ K INTERNETU</span></p>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">DĚTSKÁ POSTÝLKA S MATRACÍ (PRO DĚTI DO 3 LET) - NUTNO OBJEDNAT MAILEM NEBO TELEFONICKY!</span></p>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">NEOMEZENÉ UŽÍVÁNÍ PRIVÁTNÍ SAUNY NEBO INFRASAUNY V APARTMÁNU</span></p>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">JEDNO GARÁŽOVÉ STÁNÍ PRO OSOBNÍ AUTOMOBIL</span></p>\n&nbsp;\n\n<h3><strong>STORNOPOLATKY</strong></h3>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">REZERVACI MŮŽETE ZRUŠIT BEZPLATNĚ DO 90 DNŮ PŘED PŘÍJEZDEM</span></p>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">V PŘÍPADĚ STORNA 89 AŽ 31 DNŮ PŘED PŘÍJEZDEM ČINÍ STORNO POPLATEK 50% Z CELKOVÉ CENY REZERVACE</span></p>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">V PŘÍPADĚ STORNA 30 AŽ 7 DNŮ PŘED PŘÍJEZDEM ČINÍ STORNO POPLATEK 70% Z CELKOVÉ CENY REZERVACE</span></p>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">V PŘÍPADĚ STORNA 7 A MÉNĚ DNŮ PŘED PŘÍJEZDEM ČINÍ STORNO POPLATEK 90% Z CELKOVÉ CENY REZERVACE</span></p>\n\n<h3><br />\n<strong>DÁRKOVÉ POUKAZY</strong></h3>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128);">PRODEJ POBYTŮ FORMOU DÁRKOVÝCH POUKÁZEK - DÁRKOVÉ POUKAZY VYSTAVUJEME NA ČÁSTKU OD 1.000 Kč</span></p>\n\n<p style="line-height: 20.8px;"><span style="color: rgb(0, 0, 128);">(poukaz objednávejte na rezervace@alpineliving.cz)</span></p>\n\n<h3><br />\n<img alt="" src="http://www.alpineliving.cz/media/userfiles/prijezdove%20informace/DP_L.jpg" style="width: 350px; height: 250px; line-height: 1.2em; font-size: 13px;" /><span style="line-height: 1.2em; font-size: 13px;">&nbsp; &nbsp; &nbsp; &nbsp;</span><img alt="" src="http://www.alpineliving.cz/media/userfiles/prijezdove%20informace/DP_Z.jpg" style="width: 350px; height: 250px; line-height: 1.2em; font-size: 13px;" /></h3>\n</div>\n</section>\n<!-- end costs -->\n\n<p>&nbsp;</p>\n', ''),
(27, 20, 1, 90, 'Počasí', 'Počasí', '', '<h2>POČASÍ NA SJEZDOVKÁCH V HAUSER KAIBLING ( 2015 m.n.m.)</h2>\n<link href="http://www.snow-forecast.com/stylesheets/feed.css" media="screen" rel="stylesheet" type="text/css" />\n<div id="wf-weatherfeed"><iframe allowtransparency="true" frameborder="0" height="272" marginheight="0" marginwidth="0" scrolling="no" src="http://www.snow-forecast.com/resorts/Haus/forecasts/feed/top/m" style="overflow:hidden;border:none;" width="469">&amp;amp;amp;amp;amp;lt;p&amp;amp;amp;amp;amp;gt;Your browser does not support iframes.&amp;amp;amp;amp;amp;lt;/p&amp;amp;amp;amp;amp;gt;</iframe>\n<div id="wf-link"><a href="http://www.snow-forecast.com/"><img alt="Snow Forecast" src="http://www.snow-forecast.com/images/feed/snowlogo-150.png" /></a>\n<p id="cmt">View detailed snow forecast for <a href="http://www.snow-forecast.com/resorts/Haus?utm_source=embeddable&amp;utm_medium=widget&amp;utm_campaign=Haus">Hauser Kaibling</a> at:<br />\n<a href="http://www.snow-forecast.com/resorts/Haus?utm_source=embeddable&amp;utm_medium=widget&amp;utm_campaign=Haus"><strong>snow-forecast.com</strong></a></p>\n\n<div style="clear: both;">&nbsp;</div>\n</div>\n</div>\n\n<h2>POČASÍ V OBCI HAUS (752 m.n.m.)</h2>\n<link href="http://www.snow-forecast.com/stylesheets/feed.css" media="screen" rel="stylesheet" type="text/css" />\n<div id="wf-weatherfeed"><iframe allowtransparency="true" frameborder="0" height="272" marginheight="0" marginwidth="0" scrolling="no" src="http://www.snow-forecast.com/resorts/Haus/forecasts/feed/bot/m" style="overflow:hidden;border:none;" width="469">&amp;amp;amp;lt;p&amp;amp;amp;gt;Your browser does not support iframes.&amp;amp;amp;lt;/p&amp;amp;amp;gt;</iframe>\n<div id="wf-link"><a href="http://www.snow-forecast.com/"><img alt="Snow Forecast" src="http://www.snow-forecast.com/images/feed/snowlogo-150.png" /></a>\n<p id="cmt">View detailed snow forecast for <a href="http://www.snow-forecast.com/resorts/Haus?utm_source=embeddable&amp;utm_medium=widget&amp;utm_campaign=Haus">Hauser Kaibling</a> at:<br />\n<a href="http://www.snow-forecast.com/resorts/Haus?utm_source=embeddable&amp;utm_medium=widget&amp;utm_campaign=Haus"><strong>snow-forecast.com</strong></a></p>\n\n<div style="clear: both;">&nbsp;</div>\n</div>\n</div>\n', ''),
(28, 21, 1, 98, 'Turistika', 'Turistika - přehledová mapa cílů', '', '<p><img alt="" class="full-width-img" src="http://alpineliving.cz/media/userfiles/turistika/prehledova-mapa-turistika.jpg" style="line-height: 1.6em; width: 900px; height: 437px;" /></p>\n\n<p>&nbsp;</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num1.png" style="width: 20px; height: 20px;" />&nbsp;VODOPÁDY WILDE WASSER - tůra pro děti<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num2.png" style="width: 20px; height: 20px;" />&nbsp;VODOPÁDY WILDE WASSER - PREINTALERHÜTTE<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num3.png" style="width: 20px; height: 20px;" />&nbsp;OKRUH KOLEM ŠTÝRSKÉHO BODAMSKÉHO JEZERA - pro děti<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num4.png" style="width: 20px; height: 20px;" />&nbsp;STODERZINKEN 2048 m.n.m<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num5.png" style="width: 20px; height: 20px;" />&nbsp;DUISITZKARSEE/OBERTAL- tůra pro děti<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num6.png" style="width: 20px; height: 20px;" />&nbsp;DACHSTEINSKÝ LEDOVEC</p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num1.png" style="width: 20px; height: 20px;" />&nbsp;VODOPÁDY WILDE WASSER - tůra pro děti<br />\nVzdálenost autem z HAUSU 15min na parkoviště pod vodopády. (HAUS-SCHLADMING-ROHRMOOS-UNTERTAL). Kolem vodopádů k plesu RIESACHSEE je tůra vhodná pro rodiny s dětmi. (délka 1,9 km - 1:30 hod tam).</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/hod1.png" class="full-width-img" style="width: 785px; height: 26px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/turistika/1.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/1.jpg" style="width: 175px; height: 116px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/11.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/11.jpg" style="width: 175px; height: 116px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/111.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/111.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/1111.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/1111.jpg" style="width: 175px; height: 116px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/11111.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/11111.jpg" style="width: 175px; height: 117px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num2.png" style="width: 20px; height: 20px;" />&nbsp;VODOPÁDY WILDE WASSER &ndash; PREINTALERHÜTTE<br />\nVzdálenost autem z HAUSU 15min na parkoviště pod vodopády. (HAUS-SCHLADMING-ROHRMOOS-UNTERTAL). Kolem vodopádů k plesu RIESACHSEE pokračujeme přes KERSCHBAUERALM až k horské chatě PREINTALERHÜTTE 1661 m.n.m. (je otevřena od května do září, vzdálenost 5,7km - 3:00 hod tam, převýšení 662m)</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/hod2.png" class="full-width-img" style="width: 783px; height: 26px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/turistika/2.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/2.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/22.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/22.jpg" style="width: 175px; height: 233px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/222.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/222.jpg" style="width: 175px; height: 120px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/2222.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/2222.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/22222.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/22222.jpg" style="width: 175px; height: 117px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num3.png" style="width: 20px; height: 20px;" />&nbsp;OKRUH KOLEM ŠTÝRSKÉHO BODAMSKÉHO JEZERA - pro děti<br />\nVzdálenost autem z HAUSU 10 min na parkoviště pod jezerem. (HAUS-RUPERTING-NIEDERBERG-SEEWIGTAL). Lehká a velmi příjemná procházka v horské krajině, okruh kolem jezera je přístupný celoročně! Doporučujeme navštívit místní restauraci (délka trasy 2,3 km - 1:00 hod)</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/hod3.png" class="full-width-img" style="width: 783px; height: 25px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/turistika/3.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/3.jpg" style="width: 175px; height: 116px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/33.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/33.jpg" style="width: 175px; height: 105px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/333.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/333.jpg" style="width: 175px; height: 110px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/3333.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/333.jpg" style="width: 175px; height: 110px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/33333.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/33333.jpg" style="width: 175px; height: 117px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num4.png" style="width: 20px; height: 20px;" />&nbsp;STODERZINKEN 2048 m.n.m<br />\nVzdálenost autem z HAUSU 10min na parkoviště v obci ASSACH. (HAUS-AICH-ASSACH-). Po lesní cestě z obce ASSACH, stezka č. 100; 679; 675; 676 stoupáme k horské hospodě STEINERHUTTE a dále k vrcholu a nádhernému místu rozhledu STODERZINKEN 2048 m.n.m. (trasa je otevřena od května do září, vzdálenost 8,9km - 2:30 hod tam, převýšení 1369m)</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/hod4.png" class="full-width-img" style="width: 784px; height: 26px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/turistika/4.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/4.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/44.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/44.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/444.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/444.jpg" style="width: 175px; height: 98px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/4444.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/4444.jpg" style="width: 175px; height: 130px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/44444.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/44444.jpg" style="width: 175px; height: 131px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num5.png" style="width: 20px; height: 20px;" />&nbsp;DUISITZKARSEE/OBERTAL- tůra pro děti<br />\nVzdálenost autem z HAUSU 15min na parkoviště u naučné stezky č. 775. (HAUS-SCHLADMING-ROHRMOOS-OBERTAL). Zpočátku stezka strmě stoupá k horské hospodě SAGHUTTEN, dále pokračujete za nádherných výhledů k malebnému jezeru DUISITZKARSEE. Tůra vhodná pro rodiny s dětmi. (okruh délky 7,1 km - celkem 3:00 hod, převýšení 608m).</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/hod5.png" class="full-width-img" style="width: 783px; height: 26px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/turistika/5.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/5.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/55.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/55.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/555.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/555.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/5555.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/5555.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/55555.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/55555.jpg" style="width: 175px; height: 117px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num6.png" style="width: 20px; height: 20px;" />&nbsp;DACHSTEINSKÝ LEDOVEC&nbsp;<a href="http://www.derdachstein.at/dachstein/en/Home.html">http://www.derdachstein.at/dachstein/en/Home.html</a><br />\nVzdálenost autem z HAUSU 15min na parkoviště k lanovce na DACHSTEIN. (HAUS-SCHLADMING-RAMSAU).<br />\nDachsteinský ledovec je nejnavštěvovanější turistickou destinací ve Štýrsku . Velkým zážitkem je lanová dráha na nejvýchodnější alpský ledovec s převýšením 1000m kde zažijete fantastické panoramatické výhledy do okolních hor. Dachstein, to je celoroční ráj pro lyžaře a především pro běžkaře a také výchozí bod pro pěší turistiku.<br />\nDachstein Sky Walk<br />\nNapínavé - ale v bezpečí! Pod nohama :Hunerkogel, kolmá skalní stěna klesá rovně dolů . Všude kolem vás je bezkonkurenční horské panorama . Vítejte na Dachstein Sky Walk , nejokázalejší vyhlídkové plošině v Alpách .<br />\nDachstein Ice Palace<br />\nNávštěva Dachstein Ice Palace vás zavede hluboko do nitra ledovce Dachstein . Ponořte se do fascinujícího světa ledu , světla a zvuku . Nechte se okouzlit zářící čistotou ledu a mystickou hudbou. VHODNÁ DESTINACE PRO DĚTI !</p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/turistika/6.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/6.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/66.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/66.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/666.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/666.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/6666.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/6666.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/66666.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/66666.jpg" style="width: 175px; height: 117px;" /></a></p>\n', ''),
(29, 22, 1, 99, 'Golf', 'Golf - přehledová mapa cílů', '', '<p><img alt="" src="http://alpineliving.cz/media/userfiles/golf/prehledova%20mapa%20golf.jpg" class="full-width-img" style="width: 900px; height: 437px;" /></p>\n\n<p>&nbsp;</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num1.png" style="width: 20px; height: 20px;" />&nbsp;Golf &amp; Country Club Dachstein Tauern<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num2.png" style="width: 20px; height: 20px;" />&nbsp;Golf Club Radstadt<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num3.png" style="width: 20px; height: 20px;" />&nbsp;Golf &amp; Country Club Schloss Pichlarn<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num4.png" style="width: 20px; height: 20px;" />&nbsp;Golf und Landclub ENNSTAL</p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num1.png" style="width: 20px; height: 20px;" />&nbsp;Golf &amp; Country Club Dachstein Tauern<br />\nNáročné 18-ti jamkové golfové hřiště navrhl Bernhard Langer. Hřiště nese název &ldquo;oblázková pláž alp&rdquo;. Vzdálenost z apartmánů ALPINE LIVING pouze 1km. <a href="http://www.schladming-golf.at/">http://www.schladming-golf.at/</a></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/golf/1.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/1.jpg" style="width: 175px; height: 93px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/11.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/11.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/111.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/111.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/1111.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/1111.jpg" style="width: 175px; height: 115px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/11111.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/11111.jpg" style="width: 175px; height: 131px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num2.png" style="width: 20px; height: 20px;" />&nbsp;Golf Club Radstadt<br />\nVzdálenost autem z HAUSU 25 km do obce RADSTADT. (HAUS-SCHLADMING-RADSTADT). V obci RADSTADT pokračujte směrem na OBERTAUERN, za supermarketem BILLA jste u hřiště. 18-ti jamkové mistrovské hřiště, pravidelně oceňované jako nejlepší hřiště v Rakousku, s technickou raritou- kabinkovou lanovkou BIRDIE JET, která Vás odveze z jamky č.11 k jamce č.12 - jedné z nejvýše položených jamek v Rakousku! <a href="http://www.radstadtgolf.at/">http://www.radstadtgolf.at/</a></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/golf/2.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/2.jpg" style="width: 175px; height: 116px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/22.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/22.jpg" style="width: 175px; height: 116px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/222.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/222.jpg" style="width: 175px; height: 115px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/2222.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/2222.jpg" style="width: 175px; height: 116px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/22222.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/22222.jpg" style="width: 175px; height: 131px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num3.png" style="width: 20px; height: 20px;" />&nbsp;Golf &amp; Country Club Schloss Pichlarn<br />\nKrásné , v přírodě citlivě zasazené 18-ti jamkové hřiště vzdálené z našich apartmánů 33km (HAUS, směr LIEZEN, na křižovatce TRAUTENFELS vpravo, směr DONNERSBACH). Každý rok zde můžete soupeřit s hráči REALU MADRID v rámci jejich letní přípravy! <a href="http://www.golfpichlarn.at/">http://www.golfpichlarn.at/</a></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/golf/3.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/3.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/33.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/33.jpg" style="width: 175px; height: 88px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/333.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/333.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/3333.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/3333.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/33333.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/33333.jpg" style="width: 175px; height: 116px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num4.png" style="width: 20px; height: 20px;" />&nbsp;Golf und Landclub ENNSTAL<br />\nVzdálenost autem z HAUSU 37 km (směr LIEZEN, sjezd u obce Weißenbach). 18-ti jamkové rovinaté hřiště, obklopené úchvatnou Alpskou přírodou a velmi rozsáhlou flórou i faunou. <a href="http://www.glcennstal.at/">http://www.glcennstal.at/</a></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/golf/4.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/4.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/44.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/44.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/444.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/444.jpg" style="width: 175px; height: 86px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/4444.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/4444.jpg" style="width: 175px; height: 120px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/44444.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/44444.jpg" style="width: 175px; height: 116px;" /></a></p>\n', '');
INSERT INTO `page_data` (`id`, `page_id`, `language_id`, `route_id`, `nazev`, `nadpis`, `uvodni_popis`, `popis`, `url`) VALUES
(30, 23, 1, 100, 'Kolo', 'Kolo - přehledová mapa - cyklistika / mtb', '', '<p><img alt="" src="http://www.alpineliving.cz/media/userfiles/Obrazky-mix/mtb.jpg" class="full-width-img" style="width: 899px; height: 437px;" /></p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num1.png" style="width: 20px; height: 20px;" /> OKRUH KAIBLING - oranžová barva (mapa č. 06)<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num2.png" style="width: 20px; height: 20px;" />&nbsp;OKRUH STORM ALM - modrá barva (mapa č. 07)<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num3.png" style="width: 20px; height: 20px;" />&nbsp;OKRUH VIEHBERG (mapa č. 12)<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num4.png" style="width: 20px; height: 20px;" />&nbsp;POHODOVÝ UNTERTAL - zelená barva (mapa č. 18)<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num5.png" style="width: 20px; height: 20px;" />&nbsp;POHODOVÝ ROHRMOOS - fialová barva (mapa č. 19)<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num6.png" style="width: 20px; height: 20px;" />&nbsp;OKRUH AICH- modrá barva (mapa č. 09)<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num7.png" style="width: 20px; height: 20px;" />&nbsp;OKRUH STRUBSCHLUCHT - červená barva (mapa č. 10)<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num8.png" style="width: 20px; height: 20px;" />&nbsp;OKRUH REITERALM- oranžová barva (mapa č. 03)<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num9.png" style="width: 20px; height: 20px;" />&nbsp;OKRUH ROHRMOOS - modrá barva (mapa č. 04)<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num10.png" style="width: 20px; height: 20px;" />&nbsp;PANORAMATICKÝ OKRUH (mapa č. 05)<br />\n<img alt="" height="22" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCAFoAWgDAREAAhEBAxEB/8QAHQABAAIDAQEBAQAAAAAAAAAAAAgJBQYHBAIDAf/EAGMQAAEDAgMBBgsRCgsHAwUAAAIAAwQFBgEHEggTFSIyUlQJERQ3QUJidZOysxcYISMxNTZWcnN0gpKUlaLSFjM4Q1FVV2HC0xkkNFNjcXaBkaG0g6OkscPi8ETB0SZl4ePx/8QAHQEBAAEFAQEBAAAAAAAAAAAAAAYCAwQFBwgBCf/EAEkRAAIBAgIGBQgGCQIFBQEAAAACAwQFEhMBBiIyUnIRFDRCUxUWMTNikqKyBxcjNVTSITZDVXOCk8LwJOIlQVGDoWFkcbHBgf/aAAwDAQACEQMRAD8As7pNHgb1Q/4lG+8B+KHkoD17zweZRvBCgP7vTB5lG8EKA+d6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQH93ng8yjeCFAN54PMo3ghQH9pHrVD94DxcEByPaH2iaVkXRB4AVG4Zg49RwMC6WHS5Z8kf+axZplhUjd5vMdqj4pG9GggddO1Lmddrr2Mi6JcNky/k8HSwGA8ngrT6amVu8clqNYblUN6zDymtebJfPttrHztxW8x+I1/lOt8Zj+ebFfPttrHztxMx+IeU63xmHmxXz7bax87cTMfiHlOt8Zh5sV8+22sfO3EzH4irynW+Mw82K+fbbWPnbiZj8Q8p1vjMPNivn221j524mY/EU+U63xmHmxXz7bax87cTMfiHlOt8Zh5sV8+22r/ADtxMx+IeU63xm94ebFfPttq/wA7cTOkHlOt8ZveHmxXz7bav87cTOkHlOt8ZveHmxXz7bav87cTOkHlOt8ZveHmxXz7bav87cTOkHlOt8ZveHmxXz7bav8AO3EzpB5TrfGb3h5sV8+22r/O3EzpB5TrfGb3h5sV8+22r/O3EzpB5TrfGb3h5sV8+22r/O3EzpB5TrfGYebJfPttq3ztxM6Qq8p1vjMPNkvn221b524mbIPKdb4zDzZL59ttW+duJnSDynW+Mw82S+fbbVvnbiZsg8p1vjMPNkvn221b524mbIPKdb4zDzZL59ttW+duJnSDynW+Mw82K+fbbV/nbiZ0g8p1vjMPNivn221f524mdIU+U63xmHmxXz7bav8AO3EzpB5TrfGb3h5sV8+22r/O3EzpB5TrfGb3h5sV8+22r/O3EzpB5TrfGb3h5sV8+22r/O3EzpB5TrfGb3h5sV8+22r/ADtxM6QeU63xm94ebFfPttq/ztxM6QeU63xm94ebFfPttq/ztxM6QeU63xm94ebFfPttrHztxMx+IeU63xmHmxXz7bax87cTMfiHlOt8Zh5sV8+22sfO3EzH4h5TrfGYebFfPttrHztxMx+IeU63xmHmxXz7bax87cTMfiHlOt8Zh5sV8+22sfO3EzH4h5TrfGYebFfPttrHztxMx+IeU63xmHmxXz7bax87cTMfiHlOt8ZveNhtjaezMtR5ko10zJDYY9PqeYW7gXc6SVxKiVe8Z1PrBcKdtmTETo2bNpWm54UtyJLbbptzRQ1SIYljodH+cb6fY/V2FuIKhZuY6vZb3HdUw6dmRe6d2WUSk8dI9aofvAeLggKstqe7HbvzzuiQb2LzMZ/qJjUXBFtvg8H6xfGUdqNOKVjgWsNQ1RcpPZ2TlCxiOBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAdAyAut2zM4bVqQPYtNYzAZe0lxm3C0kP1lfp2wyKbqzT6ae4RP7RbgpIehjx0j1qh+8B4uCAqKzk6612d8n/AChKMyb7HnG6dtm5jUFaNaEAQBAEAQBAfbLLshzQ0BuOF2ojqJCpdDNum92xkBmLeQi5SbRqL7JDqF51rcWy+M5pFX1hkbum3gs1wqdpImNcu6ya9YdUKn1+kyaTMH8XIb06vclxS+Krbo0eyxgVNJPRyZc64TCKgxQgCAIAgCAIAgCAIAgCAIAgCAIDN2Xa797XRT6HHeCO9Mc3MXHOKPB1fsrVXW4La6OasdcSxriNlbaJrlVx0iNhaRsJuFSyttSj1CVAm5uWdDnRXSYfjvThFxoxLSQkOrgkJKGR62V8qK6WqVlb2TpWn6PZ9DdHWYzy+Z/ZP6ZLI+kB+0q/Oi5fuif3R9Xs34uMeZ/ZP6ZLI+kB+0nnRcv3RP7o+r2b8XGPM/sn9MlkfSA/aTzouX7on90fV7N+LjHmf2T+mSyPpAftJ50XL90T+6Pq9m/FxjzP7J/TJZH0gP2k86Ll+6J/dH1ezfi4x5n9k/pksj6QH7SedFy/dE/uj6vZvxcY8z+yf0yWR9ID9pPOi5fuif3R9Xs34uMeZ/ZP6ZLI+kB+0nnRcv3RP7o+r2b8XGPM/sn9MlkfSA/aTzouX7on90fV7N+LjHmf2T+mSyPpAftJ50XL90T+6Pq9m/FxjzP7J/TJZH0gP2l986Ll+6p/dH1ezfi4zw5gZbjY9NoNRj1yn3BT6w265GlU0tTZCGnhau24y29j1h8tSTRNTtDJFvKxEb/q7NYsvMkWTHwmmKWkRMzZPs1t/vhG8oKrTeUzKPtMfMXKj2FKD0kvoPLSPWqH7wHi4IVFRWcXXWuvvk/46jMm+x5xufbZuZjUFaNaEAQBAEAQEsdk/Zvta97KqN63u05IgMukLDO6k2zubY6nHC08Iv8AtWzpqdWXMkOiavWSmqqZqqr3TdD2pMkMsAJqz7Q6teDg4ORITbOBF745wle6zFHuqbRr7aKLR/pocX8po91dELu2oagoNv0yjNcuSRSnP+nh9VWWrm7qmoqNcqlvURqvxH71nbEtfNDLypUi/wCzwerAsF1JIg4CTZO6eCQ6uE18ok01ayR4ZFLsusdNXUjRV0O13f8AO6RLWsOchAEB6YtLmzm9ceG/IHi6mWiLxVWuhi8kMsi4lUyTNi3LIHU1b1VcHuYTpfspgbhLy0dS37NvdPYzlfeUgtIWpWyLve79lVLHJwlS26tbdhb3TF161aza7wtVikzKW4XFGYwTer3OrjKllZd4szU09P61WUxioMcIAgCAIAgCAIAgN/yD68Fr/CS8mSh2uP3BV8pKtVfv2m5jr+RuS9i39JzIqVx2pS61UPu1q7XVE1gXD0jJLSK4zrfrDdbVJRQUNS0a5KbKnqqkgik0SM6946f52HKb9H9A+ZCudeeusX42T3jY9Ug8Medhym/R/QPmQp566xfjZPeHVIPDHnYcpv0f0D5kKeeusX42T3h1SDwx52HKb9H9A+ZCnnrrF+Nk94dUg8Medhym/R/QPmQp566xfjZPeHVIPDHnYcpv0f0D5kKeeusX42T3h1SDwx52HKb9H9A+ZCnnrrF+Nk94dUg8Medhym/R/QPmQp566xfjZPeHVIPDHnYcpv0f0D5kKeeusX42T3h1SDwx52HKb9H9A+ZCnnrrF+Nk94dUg8Mxt1bM+VUW16u+xYNCbeahPOA4MQdQkLZcJbS2a4awTV0MclbJ0My94sSUcGBtki/XPwdcle98ryja9B2P9YrvzL8pwf6QfVUnKxoi6KcZMzZPs1t/vhG8oKrTeUzKPtMfMXKj2FKD0kvoPLSPWqH7wHi4IVFRWcXXWuvvk/46jMm+x5xufbZuZjUFaNaEAQBAEAQFmLGWFQqeyhAs60p8WNOm0pocZTham3N00m9hqHlaiHp90pBgbKwqd20UDtZ1pKZvSpAfMrJO8co3G/ujo7kSK6e5NTGiFyO6XJEh7bglwS4S00kMke8cgr7VV2/167JoysGoCAIAgCAmbkztr2nZ1j0O3qzbUiKVNjBGJ+BgBi5pH75px08IuMXdEtrFVxqqqx1C260UlPBHBLHhwnfrO2q8sLzkMw4lxswpjpYCDFQAmMSx/Jqxw0/5rNSpjb9Ckupr9b6psCSbXtbJ2PHHAMOxgskkREbbyzFtobIatTdY824nZIPi02Wo4gD6pFydXF6X61rqx1w4TnuttZT9W6t+0IFLSnIQgCAIAgCAIAgCA3/IPrwWv8JLyZKHa4/cFXykq1V+/abmJJ7MH8izK/t1WP8AUEvOX0gdpov4EZ6yod2TmY6jeV6UTL+3ZleuCos0ulRB1HIeLg9yI8oi5KgVttlVdqhaSjTE7GbJJoj0YmId310TKlQZhMWlaj1TZH/1FSe3DV7kR1Lvts+hyokTFcKjC3Cq4jRyXdV3FMxlT0R62rmqTcC8qQdsk6QiM5k93jj752wj3SwL39EVbSRtNbpc3D3W2WK4bskjYXXCS+p9Si1iFHmwpDcyG+AuNSGHBMDEuyJCuCz08lLJpimXCy93Sb1W0No6dB6FiF4IAgOBZ7bZVk5KyXqXqxr9xAPCp8Ex0sFyXXO19zxl1jVr6O7nrAuidvsouJu9ymmqbhFT7PpY4JTeifSt9P4/YzI0/V/6eYW7CPxh0rp830MxZP2VW2Z7S7JrtF54lJT5I7R1l58QXioEw2akwOp+lzODIAeVp7Ye6FcT1j1QuerTaOtrijbdZd03FPWJU/pQ3m8vYjW/gD/kyUbtPb6fmX5jKk3GIK1z8HXJXvfK8o2vW1j/AFiu/Mvynnr6QfVUnKxoi6KcZMzZPs1t/vhG8oKrTeUzKPtMfMXKj2FKD0kvoPLSPWqH7wHi4IVFRmcfXWuzvk/5RRmTfY86XPts3MxpytGqCAIAgCAICSWw2dzVPN6NEg1eZHoUGO7LnQsHi3Ex06BHTxdWsh+SSz6PpzMJONVNNVJWYVfYXeJC7YWUF75zNW9TbYZiOU6ETsiRjIk4N6nS0iPyR1fKWfUxSTbKkz1jttXdFjhp93RvEZy2GM2BL1tgF3Q1Bta7qcpBvNO5cK+8fzzjObH5sgfSDadTlKfNS5cPxH5ubD2bI49LCiwz7rCoNfaTqko807lwr7w84/m5+Yon0gx9pOqyjzUunCvvHnkbE+bzGA6LbZe96qUbg/KcFOqy8Jb06q3Rf2fxKfl5y3OP2pj9JRP3y+dWk4SnzXu3h/Ev5jrOzPsfXHRMwma7flKCDCpg4PxIvVLTvVD/AGurQRcEeN7rT3SyqekZWxSEiserc0FTnVq/oXd5jC7dWcMqq31DtClSjZhUYd0kuMOYjrkuDxfQ5IdL+8iVFZLtZamPrXc3adaSFt3e5iKhEREREWoi4RES1hzvS2JsTH8QBAEAQBAEAQBAEBv+QfXgtf4SXkyUO1x+4KvlJVqr9+03MST2YP5FmV/bqsf6gl5y+kDtNF/AjPWNDuyczESOiO5mT6xmdT7MB1xuk0eM3JcZ7V2S6OrdPihpEfjcpdu+iS0RU9re5No+0kbD/Kpp7rKzPg7qkQV3s0IPsoCR2yLtWTMkK+3RK5IelWXMc0uNkWrqIy/Gt9zyhXIte9SItY6brVIuGpX4vZY29FW9XbC26WkUuqRK3To9Qp8puZBkhg61IZLUBiXbCS8VVNLLSStBOuFlJiraG0dOg9SxC8Q92zNsQLBZkWTZMzBy5DEgn1BktXUGHIEv53xV6F+j7UBrmy3S5r9j3V4v9pHK6uytiPeK43HjkPOOuni44ZEROOFqIi5RL1sqLGuFd0irMfKqPhs2WuYFUyvvqj3NSHSbmU98XNOrSLods2XckPBWmu9sgvFDLRzriVlL8MjQyKylzlyTGqhY1TlMFqZfprrrZdzi0RCvz5t8WmG6wxt3ZF+YnjtiibSQbrn4OuSve+V5Rter7H+sV35l+U8+/SD6qk5WNEXRTjJmbJ9mtv8AfCN5QVWm8pmUfaY+YuVHsKUHpJfQeWketUP3gPFwQqKis4uutdffJ/x1GZN9jzjc+2zczGoK0a0IAgCAIAgJ/dD9sjCjZf1e5Hg0vVWTuIY/0TX/AHES3dCmGPEdf1Qpcukafi/tMFmBt7VC0b0rNEiWjGlR6fJOMLz0smyc0lp1adPBVt63C2HCWKzW1qWoaDRDu+0YIeiOVfEuFZEMh7moGP8A01T11uEw/PWT8P8AF/tP2HokE/VwrDjEPc1Qv3Kdd08I89W8D4v9p+n8JDK9oLP0rj+5Tr3slXns34f4v9p+g9EhcwIenYA6e201j/8AQquu+yVee3/t/i/2nq/hHmfaM79JYfu0677JX56R+F8RtuV22rjmtfFMtqnWRIaelnjicjGaJCwGHGMuB2uCuxVWY2FVNnb9ZvKFSsCRbxKbUs8nRV5teZZSsvM4ak+W6O02tEVQiSHNRatRemN6uUJfVIeUo/UxYZDhWslDpo65m7sm0cTWIRQIAgCAIAgCAIAgCA3/ACD68Fr/AAkvJkodrj9wVfKSrVX79puYknswfyLMr+3VY/1BLzl9IHaaL+BGesqHdk5mIzbVWzHf+bmdF7XRQqXg5S4caMLROHpKUQMNiTbQ9sXGXXtTdcrRZLNSUNTJ9o3w7XeNRWUcs0rOpCd5l2LIcYfaNl5oibcbcHSQkPakvQ6Osi5kbbJoGU27K/KurZuVaqUujG3vhDprtSbZc4O7i2Q6mxLlaS1fFWlvN6prHBHPV+rZlXlxGRDC0zYVNNJshIhIeEPBJbzFiXEYxYF0NStXHOo9yQpFXZkW9CIOp6e4Wp5p4uMQ8kP2l5c+mGmoo5oJUhwytvN3WX8xJ7U7ti0M2ySe2gKjXqPk9dU62ahGpdYYhE41KlFpEBHjaS7UtOrT3S4zqpDSVN6po65MUbNu6DcVOllibShTHKmPzpT0iQ6b0h1wnHHHC1EZFxiJfoPFFHHGsca4VUgfSzNtG3M5U1YspZWYLuIM0VuoN0tofxj7pCRFp7kdP1loXvcC3ZbQvrGVm5VL2iBmizTS9SkRjHWMsdmW/M2rTqlx29SeqKfD4Le6FuZSi7YWuVpUHvGuFosdXHRVcmGRvh5jNho5ZlxKWrxxdHJtsX2jZe3hHW24PCAupuEJLxIunRpv3Svi/wBxNP2P8pDCufg65K975XlG16jsf6xXfmX5Tz/9IPqqTlY0RdFOMmZsn2a2/wB8I3lBVabymZR9pj5i5UewpQekl9B5aR61Q/eA8XBCoqKzi661198n/HUZk32PONz7bNzMagrRrQgCAIDJ0m16zXj002kT6gRFp0xYzj3iiq9Ghm3VMiKmnm9XGzGUmZV3pBJsZVpV2KRlpHqimut6vlCqst+EyvJtbutC3ulpth0On5PZUUimTZTUSHR4I4SZDhaQEvVcLp+6xxUiRVjTCd6pYo7fSrG2yqqct2idlij5zRca9Q9wgXViHTGUPoMzB9UcHNPZ7v8A/HSxaimWZcSmivVhiua5sWy/zFeV1WrV7Krsqj1yA9TajGLS7HeHSXuu6Huh4JLS6dDRthY4zU00tHJlTrhYxSoMYIAgCAnxsH5OuW/a0q96lHxbnVfDcoIuYcIYw/jPjl/kOHKW5o48K42OwapW3q8HW5N5t3lNXz12qZVubRNEZpUoioVuPExPZZMtMonOC8JaeNpHi8khVM1ThlXCYN1v7Q3SNY22I97+47ltHZUtZ85TiNI0yao1pnUx3Vp19MeL0+SQl2VlTR5ybJKLzQLdqL7Le3lIy2t0Pm8agAuVus02j4Y/i2tUhwfFFa9aJ23iC02p9S/r5FU2ST0OedgwW4XkwT3a7pELT4yudR9ozm1L4Zjld+7FuZFkxSlx4LVyRR4Rb0kRvD/seMXxdSx3pJFI9War3Cl0Y1XMX2ThUiO7FecYfaNl4C0k24OkhLuhWERRlZWwsfCFIQBAEAQBAb/kH14LX+El5MlDtcfuCr5SVaq/ftNzEk9mD+RZlf26rH+oJecvpA7TRfwIz1lQ7snMx2vX+tcrNoRD2xtjkMyGZl6WXDBu6gHdJlPZHSNREe2H+l8b3S9Aaga/6bUy2y5t9j3W4f8Ab8poq6hztuLeOU9DzyjuWPfFUveZCODQ40J+n/xpshcfdIh1CI9zp4RfFU6+lK/0LUUdrjbFKzK3KpgWyB1kaViI92cK6q1wdP8AHn/KEu60PZIeVTSyb7GYyrzUr+Tt4Rbit6YUeU1wXWS+9yA7Ztwe2ElgXqy0d9o2o6xcSt8PtKVQzPTtiQ6xtMbYFbz6jxaRCYcodssiDj8EXNRSH+U4XbCJcUfjKD6oag0urDNUSfaSt3uFTNqq56jZ3VI76V1U1ZNS08s6zm1sBx6da8YJFQp9ddnPReKUgWxISFvlF6YPydK88V13p7Jr+0tc2FZI1VW4cRIEj01FDhQ0XZV2O6pnNUN/blYfpNoxHdBboJNvTTEuE23yR5RfF9zJdd9fqbV+Hq1E2ZO3w+0Y9FQtUNik3Szyi0aBbtIh0umw2YFOhti1HjMhpbAB4oiK8Z1dXNXTNUTtidvTpJciaF0dCnjvL2I1v4A/5MlkWnt9PzL8wk3GIK1z8HXJXvfK8o2vW1j/AFiu/Mvynnr6QfVUnKxoi6KcZMzZPs1t/vhG8oKrTeUzKPtMfMXKj2FKD0kvoPLSPWqH7wHi4IVFRmcfXWuzvk/5RRmTfY86XPts3MxpytGqCA27LHKyv5uXK1RaBHwcdx4T0hzULTDfKMldiiaRsKmyt9vnuUuVET6yp2MbHy8jNvVhgLnqw8IpM1vDBkS7lvi/K1LdR0kcZ1236tUdFoxS/aN7Rv1czqy3y7ZGLLuSk00WuCMWOY46f6gBXmlSP0sbmW5W+jXC0iqfVjZ+2JmRVypVu19moVHASPcNzNssRHjFhqFEmSRsKnyku1FXPlwSYmNnvOz6ZftsVCgVZrF6nTg0PNgWnH1dXq/14K46Ky4WM6ppo6qJoJd1jgUL7qNk59pibJk3Zla4Wnqk8NUukFjyh7Zr/wA4PbYejFT8pFk6zYWwyNmQfEv+03jNTJu0dpKz48oH2cZBta4Fdh6TIeT0+UPc/wDJXpIkmU2VwttLeoMXusVx5nZV3DlLcbtHr8QmTw4TMhvhNSG+UBLQyxNG2Fjitwt89tlypVNRVo1YQG7ZM5cyM1Mx6PbzOGODD72qS4PaMDwjL5KvxJmSKptrVRNcKuOAsezxzGhZFZPyZcIWmJDLAwaVH7XdNOlv0PyDhhq+Kt5M+TGdrudYlpoWZeVSEOzts3VfPmtPVaqvPRLcBzE5VQLhOynO2bDV23KJaeGnaZsTHLbLZJbvJny7KfMWP21QafZ1BptEp+G4QobODEdsj6ZaRw/zW+0LhXCdphhSnjWKPdU5PtIbRcjIOPSyats63vhgYhIKTuTTRD2C4JY4rHnnye6R+9XnySq/Z4sRwWL0Rmsi+3jIsyE4z0/TMG5hiXS7nHSSw+veyRFddHxbUJ2rLXbSy/zCkNwpLsi2ai5jpFmqYCLZl3Lo46flaVlRVUcn6CUUOstDWNhxZbe0ZbPTZktjOqC7MxEaXcOj0iqMYeieOng4OD2w/wCaqmgWZTIutjpromLTsycRWve1l1fL25p1BrkXGLUIrmlwe1LkkJdsJLQujRthY4fVUstHK0Eq7SmEVBihAEAQBAb/AJB9eC1/hJeTJQ7XH7gq+UlWqv37TcxJPZg/kmZX9uKz5cl5y+kHtFF/AjPWVD6H5jta5WbcID5babbb0C3gI+iWkRVel2bTi0sW+grT25dmQcsa4V7282A25VX9MiKPGhyS4XB/oy+qS9j/AEa64teafyXVetjXe4l/MRG40mU2am6RNXczRjR+pAb1krlLUs6swqbbFNPCPu5bo/KLisNDxnNPbe5UZ1hvkGrtvkrpdrDurxMZMELVEmWpcBlvl3Rsq7OpttUGN1PT4YaeFxnS7ZwuURLwNeLtU3utkrattpv/AB/6E5hiWFMCmyNtiyOgBwbHkiK0bPpbTiYyOg+lSXDD3l7Ea38Af8mS29p7fT8y/MY8m4xBWufg65K975XlG162sf6xXfmX5Tz19IPqqTlY0RdFOMmZsn2a2/3wjeUFVpvKZlH2mPmLlR7ClB6SX0HlpHrVD94DxcEKiorOLrrXX3yf8dRmTfY843Pts3MxqCtGtPfb9BmXRXINJpzRPTpz4sNN8oiLSq9GhmbCpehhkqJVij3mLR8rsvLb2ccsMW3nGWAiMYyapUXMOE6fbFj+rsDgpAiLDGd7oKOnstJy7zEKs+9rm5MzalNptBlvUS1OE2DTXpb0ofymXG4XJWqmqWk2VOYXjWSeuZooGwxkfSLUWouESwSGsTH6HbZoyKzdFzOh08IzQQ2i7o+EX1RH5S2tCm8x0zUymxNJU/ymQ2p9pG48t87qZGtudpj0yIPVkFzHUxIJzHVpPD3On0e1VVRUNHJsmRf75PQ16x07bu8SGyZzkoGftmHLjNg3IAdxqFMe4WLRF4wl2CWbFKsykvttygu8GNf5lI8XdW61sW5nCdNFyo5bV14nhprmOOPU5fjBbLtSHteUPG5SwnZqVvZIhUyy6s1eKPagfu8JISXBsTaky34zdUpUnD0t4OkL8R3pfUMVmfZ1CkvZaO+UnErf+CJrWxhAtq7p0C9Llco9DPRvVVWRHRIIi07m5q4p8XgrX9Uwt9oxz/RqvHDOy1cmFO6xom0vkhbOSUumwaRcb1XqTxF1XFkCIkwOkdJaR5Ss1EKw7ppr5aqa16VSKTExJvYXydws6yX7vqDGA1auiIsavVaiDxflFwsfcis+kjwriYnWqtt6rTdZfek+U4/tI3PN2h9oKl2BQXscafAk9RA5jxN1/wDUO+5ERIfilyliVDZ8yxqR28zNebpHQwbq7P5iZ7zluZF5ZEeGAU63qHF9TtsftGRY/wB5EttswodLbTBbKThjVSvk9qu56jnTTb0myXMYEOSQtUsMSFluMXBJvAeVp7blLS9ZbNxnIfOGdrgtW7bK932Sb+elgxM+slZTVLJuQ+/GCo0qR2Cc06h+UPTH4y2syZ0eydOutIt2oGVOZSrN5k47zjToYsvNkQuNuDpIS5JKPdBwJlZWwsfC+AmFsW7SMtisxsv7kmHIiSvQpcqQfCac/miIu1Lte64PbLa0k/7NjpmrN7bH1Gpbe3W/tOi7dGUMe6rCG8YTABWKHh6e4PquxMeMJcrSXSIfjcpXqyPEmNTda1W1aqm6yu9H8pXwtIcZCAIAgCA3/IPrwWv8JLyZKHa4/cFXykq1V+/abmJJ7MH8kzK/txWfLkvOX0g9oov4EZ6yofQ/MdrXKzbhAajmhmlb2UNpyq9cc4IkRofS29Xpj58hse2JSWy2Stv1WtJRR4m/8L/8mJPOlOuZIVQ7QG0JX8/rscqNSdOLR2CIafSWy9LjN8rujLti/ZXuPVbVaj1Yo1hgXFI283eYhFVVPUv06d05WpqYYQGStu5qpZtch1miTXqbVIrguMSGS4QF/wCdqsOso4LhA1NUrijbulaO0bYlLSNlna2pee1NapFU3Gm3lGa1PRenpblaeM414xD2q8Wa6ahVOrkmmpptqmbvcPssTCirlqNlt4kMfZXITdaAh9MPeXsRrfwB/wAmS29p7fT8y/MY8m4xBWufg65K975XlG162sf6xXfmX5Tz19IPqqTlY0RdFOMmZsn2a2/3wjeUFVpvKZlH2mPmLlR7ClB6SX0HlpHrVD94DxcEKiorOLrrXX3yf8dRmTfY843Pts3MxqCtGtJQbAdiM3DmdUrhkt62aFFEmdX8+7qES+SLn1VsaNMUmIn2p9KstW07dxfmN66ILmc7CbotjQnja6pb3wnafUINRCyPyhMviir1bL+z0G21wr2jWOiTvbTEJVqDlYQFnmyHZmFg5CUc5A7hIqW6VWTg52N04v8AuxBSGmTLiO76u0vVLbHi3m2iuzNS8McwMxriuHp46ahNcdZ1cbc9Wlv6oitHI+KRmOMXCp65VyS8TG47L2Z72V2bdJk4uY4UyoGMCaOPbNuFwS+KWklcgly5DZWCva31q8LbLE5Nr6w2L4yNrj2A4dV0dvfNhzpcXc+E5/u9S3NSmZGdV1io1q7fJ/1XaK+8oc47gyauUKpRJHTZdxEJcFz71IDkkPK5JdqtJDM0O6cftlzntkuOJtksZy7zKs3aZsKS0INS23Q3KfR5X31kv/jkuD/zwW8R0qFO1UdfS3qm2f5lMdmrs4WXmbXKTUrgwqj05psIIvRMOmT2nUQ4ukLZdLtuFj0h4X618lp45GxMWq6y0dwkWSfF0qbLnJU6lYmTVckWtBJ2bBg7nGZY4zIadOofcjwv7lXJiWPZMu5PJS0MjU67SqRE2ALbOfmhWq3Kac1w4OOAOODxicLhLW0a4mxaTnmqMDSVck78JvvRCbrmjQ7ateHuhMy3jnS228C4Qt8FvVp7XURF7oR/Ir1c+zh0G21wqHyo6aPvbRCFmiz5JaGoElwuSLJEtThY5Top5WbZVieWwhe9SO0ptm1eHKjlTTxkwnZDJNjiyZYkTY6uSWOr4y3NI7Yctjr2qtTNkNSzrp2fQcR20clJVi37LuenQTxt2rHuzjzQ+lsSS4wFp4urjd101g1UOmPTi0bpFtZ7U9LU9ZjXYb5iOCwSDH70+oSKTUIs2G6ceVGeF1p5stJAYlqEhJfV04WxFyJ2jdXXeUtwtmdFzSymgPvmMiPWqUIvFp9XE29Jeh/X01JtH2iHoqJlrqNW095So+oRxizpDA8Vpwmx+KSjbbx51l0ZcjKfgqC2EAQBAb/kH14LX+El5MlDtcfuCr5SVaq/ftNzEk9mD+SZlf24rPlyXnL6Qe0UX8CM9ZUPofmO1rlZtzRM4s47byRtJ6u3DLwb7WNDbIeqJTnIbH9rtVLNX9Xa3WOr0U1EvM3dUwaioSnTE5U9nhnlcGe14OVmtu4txW9TcKntl6XFb5I91yiXuPVvVqj1Zo+rUy7XebvMQqpqZKiTExztS4xDc8qMorkznuuPQbchHIcMh3eUX3mK32zjhdqPjKO3y/UOr9I1XWyYeFe83sqZEEDz6ehFOl7SWyDcOQoR6lHN24LZcERcqTLWnqZ3kuD2okXFJRHVLXyh1nxQN9nKvdbvL7JmVVC9NtbynAV1A1Z7KPWJtt1aHVKXKcg1CI6LrEhktLgGPFISWPU00VZE0E64lbZYr0acLYizbZO2xIWdDLVt3ITdNvBoOAXFaniPbN8k+UPye58ca86gS6vtprqLap2+H/aS6jrtFRsPvEnlxE3ph7y9iNb+AP8AkyW3tPb6fmX5jHk3GIK1z8HXJXvfK8o2vW1j/WK78y/KeevpB9VScrGiLopxkzNk+zW3++EbygqtN5TMo+0x8xcqPYUoPSS+g8tI9aofvAeLghUVFZxdda6++T/jqMyb7HnG59tm5mNQVo1pO7oc8cQs+7X9OGop7TertuC3/wBy3NFusdc1M0f6aVvaOC7a816XtFXCDhYk1GZitMjj2o7g2XjESw6v1pD9aWZrnIun2ThawiJnUNmeyKLmHnFRKNXjwwpx63MWCLTu5COoQ+MsmnRZJMLEgsNLFWVyJPuk+9p68o+XORtfJgxjSJMbe6G2JaeEeGn0Pcj0/wDBbqofLjOuXyoWht8mleHCpVoo4cDP624TbgmJaSEtQoVLpwsrFwdysMVHLWqNTAFyO9SXReHH1MRJnHUpPp3D0dNtUbYuH+0p7UYPN7HfNiCoMws+oDb7+LO7xH2wHp6RM9PBFZtG32pMNVHVbguJicA592/AzDnWdXjxoFWbfFuLjM4LU1svvZtn6nc9LlLb5y4sLHVdF1gWpakn2W7vtG8lX6c7FqbrzwBFgETcs3h6QBpHUXT6fq4dIlexaDa6ZI8LM26phLIzLsq9XZEK1a3T6k5HDA3WoJYY6MOnp1KhHRt0xKWupKr9FNIrYT1XhmPatg4R/ukrkKj7v09y6rc06/6l9kdV3iqprKal9e2hTVZG0/lXFEsfu0pZ6f5tzUrPWIuIwNN7tq/tlMeG1xlQ6+21hdkfAjx06iacER91jp9Bfesx8Ra84bbiw5pou07tCZfTco61RYdYiV2fVY24xo8P03SRcVzHk6eN/crNRMmDCam+3ei00UkKviZiu9aI4wF8YoLYtnGM7ByJslmQ0TLwU1vU24PSxw7Kk0Pq1PRVp2bfD08JVhdEoZ9zVaQDQR23Zbrgst8UNThcEVHX3jgVU2KeRvaMYqDFCAIAgN/yD68Fr/CS8mSh2uP3BV8pKtVfv2m5iSezB/JMyv7cVny5Lzl9IPaKL+BGesqH0PzGy54Z7W3kTarlVrknApR4F1FTW8fTpR8ke55RdqtBq3qxXazVeRTLs95u6pfqalKZcTFTeb2cVx52XhIr1wyt0IuCxFb+8xQ7Vtsf2u2XuSw2Ci1dolpKJeZu83tEKnnaobFIaSpIYx0zInIG48+rsbptIaxj01oh6uqjjfpMdv8AaLkiodrPrRR6tUrT1Lfad1e8xm01M9Q3Qpazk7kzbeSNqM0K3o2Ijxn5jvCelHyzL/wV4fv+sVZrJVtVVbcq/wDJSZwQJTphQ3GqUuJWabJgT47cyHJAmnY7w6hMS7UhUepaiWklWeFsLKZDaNDaOjSVt7WmxdKyxkSLpsth6daePpkmHxnIBftB3Xar17qL9IcV50LQXRsM/dbi/wBxE623tFtx7pE1d1NGein1CVSZ0ebCfdizGHBcaeZLSQEPbCSszQx1EbRSriVj6ull2lLJNkHbJazQCPaN5Ptx7qAdMSZxW548nuXfGXkTXz6Pms+K5WxcUHe0cP8AtJZQ12d9k+8SevL2H1v4A/5Mlxq09vp+ZfmNxJuMQTrn4OuSve+V5RtetrH+sV35l+U89/SD6qk5WNEXRTjJmbJ9mtv98I3lBVabymZR9pj5i5UewpQekl9B5aR61Q/eA8XBCoqMzj6612d8n/KKMyb7HnG59tm5mNOVo1pL3od93NQ7oui3HncAcnRmpbAEXqk2RC50v16TH5K2tDp2mU6RqZU4ZZoG720eDogtiv02/KPdbTfTh1OKMR1wcOK+0Rcb3QEPgyVNcmFlYt640rLPHUrusRRWsOdn3FlPwZDciO6ceQ2WptxstJCXckvvtFSaWjbEu8Zi5L6uO8dx3+rlQrG4cFvq6S49o9zqJXNLs28xkTVc9Vhz5GYwitGKbTlbZ79/ZiW/QWR1FMlttudy3q1EXydSuxrikVTZW2masq44lLMto26RsLIy6preOl/qHGGxj+Q3fShL4urV8Vb+ZsuNjuF5qOp2+ST2cJVIo2efT96bUpVHqEedCkOxZkdwXmnmS0kBDxSFfV04SqN2jZZE2WUlNQdsyh3LS4MfMyx4tyzoeI4t1FllvpkQ9vpLil7n0P1Cthoq1w/aKdCh1ngmRVuEOJl7x0DaeztqN1bPFIrdoNYs0C4Hii1N9wfTo/8ARckdRCQkX2lkVM2KHFGbi+3OSa1rNSr9nJvHBNja+wsrO6mNSHtxh1cSp56j0jqL73q+MIj8ZYNI+GQhurFZ1W4Krtstskq9tvKx7MLLBusU5vF+p2+4UkWw9EnGS9B4cMOVh0hL4uK2VXHmR4joetFuaso8yPeQriWiOJBAEAQG35TZdVDNTMCk27T2sSKS7qdcxw4LDQ8Jwy7kR+tpHtldiTMbCbO20UlwqVgT/FLNc4rsjZQ5MVecxhi2MKDhEhjh/OYjubeH+PSW+lbLjO53KfRQUMknCpU444TjhGZaiItREo4ee204to/iFIQBAEBv+QfXgtf4SXkyUO1x+4KvlJVqr9+03MST2YP5FmV/bqsf6gl5y+kDtNF/AjPWVDuyczHPtsTZBk5zPFdttzHvumYYFoqfId1R5DY/zer7259UvdcIpJqDr6li0eTq5fsW73/NebiUx62hzttN4rXrFHn27VpVNqkR6n1CI4TT8WQ3pcAh7UhXr2mqYqyJZ6ZsUbbpE9KMrYWOvbNuy/XtoCuC6IuUu1YzmmXViH/dtco/FUB1u1zotV4OKdt1f7mM6jo2qm9ktSy9y8oOVtqw7dt6CEGmxB6WkeM4XbGZdsRcpeJLxeKy91TVlY2JmJlFEsK4UNkWjMsID5cbB5smnW8HGzHSQkOoSFXEdo9OJNP6S1p0YiAu1tsPnBdmXjlzA1RNJOzqHH/FcpxgeT/R/J5K9UaifSQs2hbbeW2u6/FzEYrbdh+1iIN8XgkvSK7W0pHSVGyrsY1nNCZT7ouXqqg2q2YvsadTcmbpL8X2zY/0nyeUuJ67fSDSWZJKCjwyT7vEq8xuqS3tK2KTdLHLqjjFsmrsBr3NunuhhuhERacGy4xFwiXkq3PplucLt3pF+YlT7mnSQZrn4OuSve+V5RtesbH+sV35l+U8+fSD6qk5WNEXRTjJmbJ9mtv98I3lBVabymZR9pj5i5UewpQekl9B5aR61Q/eA8XBCoqMzj6612d8n/KKMyb7HnG59tm5mNOVo1pteVl+yMsr/otyRumeMF8ScbEtOtsuC4PxhIldifLkxGwt9Y1DVx1K90szvO2Le2j8oxaB0HYNUYGRCmD6JMudqX92PoFh/WpCyrMh3SpggvVHh7rbpWXmVljXsqLkfo1eiEy6GPpMjDDptSG+1MMezgo9IjRthY4bX0E9tlyp1NVVo1oQH6w4cipTGYsVhyRKdIW22Wx1EZckRX3RtbpUiNI2FV2iw7ZG2aXMqac5c1xtB9005rS1HxHp9RNY9r7su2/wW8pqfL2m3js+rtk8nx58/rG+E47t25zMXPXYtkUmUMiJS3N2nE1j0xKT2rfxR+sSxayXE2WpGtbbnomkWkibd3uYictYc7CAICTtD2z4NBtGLbzOW9DOA0Aa45YDuTruAjwyb06dRadWpbFavCuHCT+LWdIYlgWmXCRzuCrYVy4qlVGo7cEZck5Ix2eC21qLVpHuRWCzYmxEGmkzJWlXZLJdl/PmHnTZQwZ7gfdLT2cGp8c/xw8XdfjdnulvoJlmXoO4WK7JdIML+sXe/McC2lNjaoUydMuaw4hz6e+eLsmkND6bH7pse2HueNgsGopP04oyI3zVh1ZqmiXZ4fykSZEV+G8TEho47w8Em3B0kK1u7vHOXRo2wsp8L4Um55dZPXVmlVGYdApEiQBlpKY42Qxmu6M9OkVdSJpN02tDa6u4SYYl/m7pYtkFs90bISgvFg+E6tvj/HKmQ6emOHo6R5IrfQwrCp2i02iG0xfo3u8xETbHz/DNO6G7eocjdLZpLhemDxZcjikfuR4o+6Ilq6ubMbCpzrWa7rXS9Wgb7NfiYjisAg4QBAEAQG/5B9eC1/hJeTJQ7XH7gq+UlWqv37TcxJPZg/kmZX9uKz5cl5y+kHtFF/AjPWVD6H5jta5Wbc5DnZsu2RnpKhza7DOLU4xjqqEHS28+0P4twu2HxV0TV3XW66txtDStiRu627ob/qpq56OKo/Sx0q17XpVmUGDRKLCbp9KhhuTEdkeCI/8AnZUMr6+puU7VVU2J2MxE0RrhUyi1xkBAEAQBAcNq2xvltWs0m71kUkd0++O0sREYj7+r76Qfs8Ul1CD6Qb3T2ryYsn6OLvaF4TUaaCBpc3CdyHAWxEQHARHgiI9hcxZ9LacWk2fQYe8vYjW/gD/kyW1tPb6fmX5i3JuMQVrn4OuSve+V5RtetrH+sV35l+U89fSD6qk5WNEXRTjJmbJ9mtv98I3lBVabymZR9pj5i5UewpQekl9B5aR61Q/eA8XBCoqKzi661198n/HUZk32PONz7bNzMagrRrQgO57N207U8lqkNOqGDlStN9z06Lq4UcsfxjX7Q9ss2nqGh2W3SXWO/SWtsmTajYniL2Xe0bahBgUC5aaWHCH8awXjAS23THMp1rpobxB3ZFOJ3b0PK1qm+Ttv3HUKIJfiZDIy2x9zwmy+URLF00KNusRWo1OpJGxRSMvxGEpfQ4obUsSqV8PyovbNxaaLR4/GJwvFVK0K95jEj1LTp25vhO8Zb7P9g5LROrKfBawlNDqcqtScwcew/XqL0B+LpWakMce6SyhtNDa1xov8zHGNo/bRp9Hp8q3LCl9WVY/S36q395jD225l2x91xRWJPVKuyhGr1rPHGrQUTYm4iCTzxyHnHXTxccMtRERaiIlpjkulsTYmP4gCAIAgCAy9p3ZV7Hrker0Oe9T6gwWoHmS+qXKHuVWjsrYlMqmqZaOTNgbCxOrJzbntu5YLEC9P/p+siOAnM6XTiPFyuU3/AFFwe6W5jq1bZY6xbNaqaoVY6rZb4TtUqm5Y5m6X3mbdrxu4ffMdycMv7+MsnCkhJmS31202FjyQ9nnK+gkUlq0KOz6Hom40JYDh8ZUZMXCW0tFvj2lhU8l1595a5S0vqd6s05gWR9Lp9L0uF/UIN+ovrTRx94onu1vt64WkXlUhjn7tiV7Ndp6kUMHretwtQuNif8YlD/SEPFHuR+US1c1S0mypzO8ayS1+KKDZT5iPCwCFBAEAQBAEBv8AkH14LX+El5MlDtcfuCr5SVaq/ftNzHSsqdoK0sparmNRrkKpx5x3hV5LYx6W+8JNk+WktTYkK5JrNqtcL/1SpocLKsUa7yqep6eoSDM0PxG++fQyw53W/oWX+7UK+rm/cKe+pm9fg/zQPPoZYc7rf0LL/dp9XN+4U99R1+D/ADQPPoZYc7rf0LL/AHafVzfuFPfUdfg/zQfXn0MsOdVr6El/u1T9XN+4U99Sjr8H+aB59DLDnVa+hJf7tPq5v3CnvqOvwf5oHn0MsOdVr6El/u0+rm/cKe+o6/B/mgefQyw51WvoSX+7T6ub9wp76jr8H+aB59DLDnVa+hJf7tPq5v3CnvqOvwf5oHn0MsOdVr6El/u0+rm/cKe+o6/B/mgefQyw51WvoSX+7T6ub9wp76jr8H+aDHXJti5a1C3apDYk1onn4jrIDvJL4xAQj+LWzt/0f3ynqopXWPDoZW31Pj1sLaG0L/8ARwe4o7sXZ7yXafaNlwafL1NuDpIfTG12mw6VbWC6svEvynC/pC9XScrGgLo5xkzNk+zW3++EbygqtN5TMo+0x8xcqPYUoPSS+g8tI9aofvAeLghUVGZx9da7O+T/AJRRmTfY86XPts3MxpytGqCAIDJW5dNYtGot1CiVOVSpYeo7EdIC+qq1Zl2lMinqZaVsyBsLHdLd2680KG0DUp+m1ocMOl050PSfymyFZmiskXeJVDrbcI1wthY9czb7zNlCQtMUOL0+2ZhuFiPynCTrkhd0633Bu6vunH78zhvPMuUT1xV+ZPDVqGPq3NgPctjwRWO80km8R2sudZXN9vJi+U05WDVBAEAQBAEAQBAEGE+2ZDsctTTptl3JaUKl0su6ehytT3h0nNkkPF4TpKvExc0zSt3mPGXCLUXGVBZP6gCAIAgCAIAgPfb9en2xWItUpr3Us2MW6NPbmJaS9yXBWJWUcFwgamqVxRtvGXSVU1DOtTA2F13Tei2jMwyLUVcbIi/+3xP3KhvmLYfBb+pJ+Yl/nvf/AB/hX8o88ZmD+fWvo2J+5TzFsPgt/Uk/MPPe/wDj/Cv5R54zMH8+tfRsT9ynmLYfBb+pJ+Yee9/8f4V/KPPGZg/n1r6NifuU8xbD4Lf1JPzDz3v/AI/wr+UeeMzB/PrX0bE/cp5i2HwW/qSfmHnvf/H+FfyjzxmYP59a+jYn7lPMWw+C39ST8w897/4/wr+UeeMzB/PrX0bE/cp5i2HwW/qSfmHnvf8Ax/hX8o88ZmD+fWvo2J+5TzFsPgt/Uk/MPPe/+P8ACv5R54zMH8+tfRsT9ynmLYfBb+pJ+Yee9/8AH+FfyjzxmYP59a+jYn7lPMWw+C39ST8w897/AOP8K/lHnjMwfz619GxP3KeYth8Fv6kn5h573/x/hX8o88ZmD+fWvo2J+5TzFsPgt/Uk/MPPe/8Aj/Cv5TXL0zIuPMHqPf8AqIzuo9YsaWGmtGrTq4gjyRW+tVht9lxdRjw4t7aZvmNFdL3XXjD1yTFh3dlV+U1lb80RmbJ9mtv98I3lBVabymZR9pj5i5UewpQekl9B5aR61Q/eA8XBCoqKzi661198n/HUZk32PONz7bNzMagrRrQgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgMzZPs1t/vhG8oKrTeUzKPtMfMXKj2FKD0kvoPLSPWqH7wHi4IVFRWcXXWuvvk/46jMm+x5xufbZuZjUFaNaEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEBmbJ9mtv8AfCN5QVWm8pmUfaY+YuVHsKUHpJfQeWketUP3gPFwQqKis4uutdffJ/x1GZN9jzjc+2zczGoK0a0IAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIDM2T7Nbf74RvKCq03lMyj7THzFyo9hSg9JL6Dy0j1qh+8B4uCFRUZnH11rs75P+UUZk32PONz7bNzMacrRrQgxBCnpCDpCFQQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEBmbJ9mtv98I3lBVabymZR9pj5i5UewpQekl9B5aR61Q/eA8XBCoqKzi661198n/HUZk32PONz7bNzMagrRrSZ2xhkjZOZGXFRqNyUFiqS2qgTQOuEQ4iOgeDwSW3pIo5I8TKdQ1YtlJWUbPOmJsRIPzpuU/tOieEc+0szq8XCS/yBbfBUedNyn9p0Twjn2k6vFwjyBbfBU/GRsk5TPtE2VoRxwLstuujj/jgSdWi4SnTq/bW/ZGi3hsD5f1mG5jQ3qhbsvpcAhexktYe6E/RL5SstRxtumpqdU6CRfssS6SF+cmR9x5J14YFbaF6I/q6mqEfVuL4/sl3K1MsLQ7xzK52qe1yZcv8rHPlYNKEBLXZu2SLQzhyyj3HWalWos05LrRNQX2Rb0iXB4zRF9ZbSnpo5I8THRLHq/SXGkz5WbF/6HVv4PDLr893P86jfuFf6lGSHzOoOJvh/KP4PDLr893P86jfuE6lGPM6g4m+H8o/g8Muvz3c/wA6jfuE6lGPM6g4m+H8o/g8Muvz3c/zqN+4TqUY8zqDib4fykQto7LCl5QZnSbbo0iZKgtR2nhcnOCTmoh7kRH6q1tREscmFTnF7oo7dWNBFunMVjGhCAIAgCAIAgCAIAgCAIAgCAIAgCAID3yLeqUWixau9Cebpcpwm2Jm5+lmQ8YdXKVXQ28X2p5ljWVl2W7x4FSWAgCAIAgCAIDM2T7Nbf74RvKCq03lMyj7THzFyo9hSg9JL6Dy0j1qh+8B4uCFRUVnF11rr75P+OozJvsecbn22bmY1BWjWsWFdD260dW76ueTbW7ovVnZdUOwNzEpS9RbAnZG2Vt55bRJT0dyPXN0aMmy0ww42H+0WC1ZGpCn1st0baUbF7pkrZ22sr7lnsxDqMyjm7jpFyox9DfT7ohIsB+MqtFXCxk0+s9tqGwYsPMd7ZdbkNiYFgbZYdMSwx9DHBZhKtGnFtKcu2kMt42Z2UtepzrQuzY7BTILmGHCbfbwxIel7r0Rx/USsTJmIaS80S11FJHp3vSpVMo2efAgJ27IGdljWLk1FpdfuaDSqiMx9wmJBFq0kXBxW4pZUWPDpY61q1caOlocuaRVbEx3Dzz2Vft3pfyy/wDhZ2dHxEq8tW7xlHnnsq/bvS/ll/8ACZ0fEPLVu8ZT3UDP7L26arGplIuuBUKhILQ1HZIsSPH8nqL4ssbbKsXYbpRVD5cMqsx0VXjblYe2tUotS2gKz1K7g9uEdhh3T2piPCH6y0NXpxSnCdaGV7k2E4WsIioQBAEAQBAEAQBAEAQBAEAQBAEAQBAWH7H1r0q8tmhqlVqAxUqfJmSRcZeHUJcL/Iv1reUyq0WFjtGrkEdRaVjlXEu0cQ2gti6q2EMmuWeL1aoIanDhcaTGH/qCsSakZdqMit41ZelxT0u0vykXuKtcQE/qAIAgCAIDM2T7Nbf74RvKCq03lMyj7THzFyo9hSg9JL6Dy0j1qh+8B4uCFRUZnH11rs75P+UUZk32PONz7bNzMacrRrWLCuh7daOrd9XPJtrd0XqzsuqHYG5iUpeotgTspfuD19qXwl3xiUXbePNNT6+TmPAqDFLTNk24pF07P9pTJr+MiU207ENwvVxwaeNsfqiKkVK2KFTvtgn01FthZv8AOg6vVRFymSxLi4tHgX+Cv6fQb+TcYpnrDYM1icDX3sX3BHTydSi7HmmX1jHkXwshAEAQoJrbAOUeIDPzAqDfCLVDpol/vnP2cPjLb0cX7TSdU1PtuFWrpOVSU2bF/wAfLHL6t3LJHB3qFjEmmcceluruPoNt/GLHDBbCR8tcRPrhWLQ0z1Dd0qOrFWlV6rTKnNd3aZMdcfec5ThFqIlG2bE2I87TSNNI0km8x5FQWQgCAIAgCAIAgCAIAgCAIAgCAIAgCAso2GPwfqf8Nk+UW+o/VHcdVvu1f5iRCzSXkZNoLY3o2ZQSK1a+DNDuX0TINOmNKLuulxS7oVgzUyybSkIvGrcNdilg2ZPmIB3dZtZsSuSKRXqe9TagwWkm3h43dCXbD3S0ro0bYWOQVNLLRyNFOuFjDKgxQgCAIDM2T7Nbf74RvKCq03lMyj7THzFyo9hSg9JL6Dy0j1qh+8B4uCFRUVnF11rr75P+OozJvsecbn22bmY1BWjWsWFdD260dW76ueTbW7ovVnZdUOwNzEpS9RbAnZS/cPsgqXwl3xiUXbePM9T6+TmMfxlQY+8WrbLtrSrLyKtSmz2sY8vcHJDoF6o7q6TmGBfr0kKkkCYI1U9BWOmalt8Ubek3DMy4mLQy/uKsSMfSYcF10vk4qt2wq2kz62bRT00krd1SnsiJwiIi1EXCIlGDzg28fxCkIAgPbQ6PIuCtU+lw2sXpkx9uM02PGIyLSKrVcTYS9DC00qxrvMW+5e2hGsOyqNb8URFmnxgZ4PZLDDhY/wB+PTUlTRhXCejaSmWlgSBe6RR6IZmDg1CoFmR38d0eLfCU2HJHULfT+Nq/wWurpO4c/wBca3CsdIvMxCNag5YEAQBAEAQBAEAQBAEAQH3HjnMkMstDqcdcFsR5REhUiNIyqp2fzm2bHta/4lv7Sy+qTEm82bl4Z/fOcZse1r/iW/tJ1SYebNy8Mec4zY9rX/Et/aTqkw82bl4Y85xmx7Wv+Jb+0nVJh5s3Lwx5zjNj2tf8S39pOqTDzZuXhn5FseZsCXsYMvcvt/aXzqsvCPNq6eGTh2T7FrWXWT0OjXBDxg1EJT7hM4kJdISPg8VbimRo48LHVLBSS0dCsU67R2lZJJAgNBzTybtrOChHT6/CwcMcMdwmN49J+OXKEv8A29RWZY1kXCxq66201yjwTqV255bM1zZJyzkvN76W+R6WapHHgjyRcHtC+qtLNA0Jxe7WGe1ti3o+I4+sQjYQBAZmyfZrb/fCN5QVWm8pmUfaY+YuVHsKUHpJfQeWketUP3gPFwQqKis4uutdffJ/x1GZN9jzjc+2zczGoK0a1iwroe3Wjq3fVzyba3dF6s7Lqh2BuYlLjh08FsCdkWZnQ/rJmzX5Dlaq4k84RkIk322PuVgtRxsQWTVGjkkaRmbaM9Y+xFl7ZlWj1N0JtckMHujbc8xJrDH9YYD0iVSUsa7RmUmrFBTPmYcTe0d4qNRg0SA5KnSmKfEaHhPyDwbbDD9eOPoYLM6eglLOsa9LbOgg1tf7UVNvWjnZNpSxmU3FzAqhUA9EHtBahab5WGoRLV2dK09TUYtmM5ZrJfo6iPqdM2Je8xEVaw5uEAQBASN2FbBwurOHfh4MDh0CNjJHp4CQ4vnwG/k8IvdCKz6NMUmIm2qVHnV2a37MsjW8O1FTe0pen3d52XTVAPdIrcrqSNpLUO5NDuYkPclpIvjKO1D4pGPPt8qut3CV/wCU5msY0IQBAEAQBAEAQBAEAQBAZG2/ZFS/hbXlBVabxlUnr4+ZS59Sg9LBAEAQBAEAQBAEAQGPqNNi1aC9Dmx2pcR8MQdYeDA2zHH1cCHH1cE06MRbdFdcLeghdtB7DpMjJruXje6BhqddohY+j/sS/ZWpmpO9GczvGqu9PQ+7+UhrKivwZDkaS0ceQ0WlxtwdJCXJIVq22TmL6GjbCx+S+FJmbJ9mtv8AfCN5QVWm8pmUfaY+YuVHsKUHpJfQeWketUP3gPFwQqKis4uutdffJ/x1GZN9jzjc+2zczGoK0atiwroe3Wjq3fVzyba3dF6s7Pqh2BuYlMtgTs5sW0RlmMgmCvejC8JaSb6pH0MVZzo+I0zXi3q2HTMpsdCzAtu6cRwpNep9QxLijHkiRY/3dNVaHVt1jMirIJvVvoYzNSpkWrRTizYrUyOfGafbwMC+LirhkuivowsQ22w9mWhUy0ZF7WnACnPQSEqhEjj6W40Rad0Ee1IcccNXc+5WrqadcOYhzfWSxwrA1XTLhZd4hKtQcrCAIAgLFtg2yBt7KAq04GAyK1KN3V/RB6WP+Ykt7RphjxHadVKTJoc7jO05qXUNj5d3FXMSxEoUJ1xvEeXp4P1uksqRsK6WJNXz9VppJuFSoN57GQ846ZaiMiIlGDzk2nEzMx8IUhAEAQBAEAQBAEAQBAEBkbb9kVL+FteUFVpvGVSevj5lLn1KD0sEAQBAEAQBAEAQBAEBh7kual2hR36pWJrNPgMDqcfeLSIqltKr+ljGmmjp0zJWwqVqbUuctt5u3iMi3aE1BZjamyqxDpfm90QjwdPJ1cLxVo6iVZG2TiWsFzguU+KCPd73EcSWERUzNk+zW3++EbygqtN5TMo+0x8xcqPYUoPSS+g8tI9aofvAeLghUVGZx9da7O+T/lFGZN9jzjc+2zczGnK0a1iwroe3Wjq3fVzyba3dF6s7Lqh2BuYlKXqLYE7KX7g9fal8Jd8YlF33jzPU+vk5jzwZ0ilym5UN9yLIaLU28yWkhJU6NndLaO0LYo2LZMg71kZiZP2vcEv+Wyoul8uU4BE2RfGxHV/epJC+KNdJ6FtdS1ZRRztvMpm8y6cNWy9uWIY4EL9NkBiOPvZKt91jJrUzKaRfZYp5UXPN7bwQpCA/WDBfqU6PDjgT0h9wWWmx4xERaRFfejEXETMkWNe8XCZe2oxYdkUO3o+nEKbDajahHTgZCOGovjFqx/vUoRcK6FPR9HAtLBHAvdU4Tt73TjRMmY1Max9Mq9QbYLpY+juYCThfWER+MsGsbDHhIprdPk0GDiYrsWkOLhAEAQBAEAQBAEAQBAEAQHtocgItap77uO5thJBwi5IiQqtd4yKd1WWNm4i07z0OVft1p31/sqQZ8fEd88t23xlHnocq/brTvr/ZTPj4h5btvjKPPQ5V+3WnfX+ymfHxDy3bfGUeehyr9utO+v8AZTPj4h5btvjKfB7UmVTQ4kV607T3OvH9lOsRcRS18tq/tlPP57PKX26Q/AvfYTrMXEWvOC1+Mpu1hZkW5mXTZE+2qo3VobDu4OPNiQ6T0iWnhYYdghVxHWTaU21LWQVseZA2JTalcM0IAgOUZ2bQ1sZJ0zp1J7qyrOBqYpcch3Zz9Zckf1rHlnWPQaC53imtidMm9wldGcOetz501kpdYlYswWy/i1NZL0hgf2i7olpZZmm3jjVyu9TdJMUjbPCc9WMaMIDM2T7Nbf74RvKCq03lMyj7THzFyo9hSg9JL6Dy0j1qh+8B4uCFRUVnF11rr75P+OozJvsecbn22bmY1BWjWsWFdD260dW76ueTbW7ovVnZdUOwNzEpS9RbAnZS/cPsgqXwl3xiUXbePNNT6+TmPAqDFLNdiOSUnZ1t4C/EPy28PDmX7S39J6lTuurGnFa4/wCb5jsV1ejbFX+CPeISzG3SRz+qb/4KaFFDzS28EKQgOobMVtfdTnpaUUgxcYZljLc09qLfpnjCKyadcUykgsEWfcYlLYFIj0AQI6IlcXVV42pQhwx0woLssulj2zp6f+j9ZaiubaVTkuuc2KWKDhXERGWrOchAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBBhLAuh29aq4u/JeQZW7ovVnYtTuwNzErlsCfH4vPBHaJxwsAAcOniWPqYIUadOH9LERdoLbdh24cmgWETdRqI4Ytu1fjR2S/o/5wu64vulrZqrDsxnPbzrQlPigo9puIg1WK1NuKqSqlU5T02dJcJx2Q8WpwyWp0ti2mOUSzSVEjSSNiY8ioLYQBAZmyfZrb/fCN5QVWm8pmUfaY+YuVHsKUHpJfQeWketUP3gPFwQqKis4uutdffJ/x1GZN9jzjc+2zczGoK0a1iwroe3Wjq3fVzyba3dF6s7Lqh2BuYlKXqLYE7KX7h9kFS+Eu+MSi7bx5pqfXycx4FQYpZDsGSDeyDZAi1CzUpLY/qHgl+1it7R+qO3ap6f+GLzMd9rkfqujz4+GPSxdYcD/ABHFZundJdJoxIylNE6KUGdIikWomHCbIvclpUVPM76MLMp+CFsICUXQ+aE1PzYq9Rc6eqn00iD3Tjgj4q2NCv2jMT7U6HFVyO3dUsOW6OxFYm2vUMZu0HXMMDxIGGWGhHV6nSaH9paGr0/anDdaHzLk3s4ThSwiIhAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAWBdDs61dxd+S8gyt5R+rOxandgbmJB35mPb2WVDOq3DUG4EQcODq4RuFyRH1SxWW7rGuJiYVdbBRR5k7YSvbaA2trgzcedplKJ6hWxhjpwjiWl6RhynSw8UeD7paWapaTZU49eNYpbh9lFsx/McDWEQ4IAgCAIDM2T7Nbf74RvKCq03lMyj7THzFyo9hSg9JL6Dy0j1qh+8B4uCFRUVnF11rr75P+OozJvsecbn22bmY1BWjWsWFdD260VW76ueTbW7ovVnY9T+wNzEosSH8q2BO9OkpiuD19qXwl3xiUXbePNdT6+TmPAqDFLB+h7VpqXlPWKb+Oh1UzL3Jthp8XFbuib7M7HqfMrUDR8LEpi6RD0lsCdFRWeNnSLDzXuajyMOni1NcdbLlAfpgl8khUbmXDIynnq7UzUtZJE3EaOrBpwgJq9Dkp4YjfE8hw3QSisCXc+mEXiitrQrvHUtS02Zn5SbPofrW2Om9JU5tJy8Zue96uYniYYVAxHUXaio5UeuY8/Xx8y5TN7RzRY5oQgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgJL7PG0rR8isoK3D6nOo3DMqhOxoXFAR3Jsd0MuTqEvkrY086wx+0TyzXuK00LLvPiOIZiZnXJmpXzq1xVJ2c7jj6Wz6jTA8kA4uA/8AhLDeVpGxMROtuFTcJMydjV1aNeEAQBAEAQGZsn2a2/3wjeUFVpvKZlH2mPmLlR7ClB6SX0HlpHrVD94DxcEKiorOLrrXX3yf8dRmTfY843Pts3MxqCtGtPbBr1SpbO5Q6jKhtkWrSy+TY/VVeh2XdLyVEsa4Y2wno+66vfnuofO3PtKrG/EXOt1PiMYsiIi1EWoi7ZWjFP4gPXT6xPpIuDCnyYYnxup3Sb1fJVehmXdLiTTR+rbCer7sK9+eqj87c+0qsb8Re65U+I3vGPmTpFQkE/KfckPFxnHnCIi+MSpZsRju7SNikbEfkqCkID10+tVGliQwp8mGJ8bqd0m9XyVXoZl3S5FNNH6tsJ6vuwr356qPztz7SqxvxF7rVT4je8Y1552Q8Trpm444WonHC1EStGLiZmxMfCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAzNk+zW3++EbygqtN5TMo+0x8xcqPYUoPSS+g8tI9aofvAeLghUVFZxdda6++T/jqMyb7HnG59tm5mNQVo1oQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQGZsn2a2/wB8I3lBVabymZR9pj5i5UewpQekl9B5aR61Q/eA8XBCoqMzj6612d8n/KKMyb7HnG59tm5mNOVo1oQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQGZsn2a2/3wjeUFVpvKZlH2mPmLlR7ClB6SX0HlpHrVD94DxcEKiorOLrrXX3yf8AHUZk32PONz7bNzMagrRrQgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgMzZPs1t/vhG8oKrTeUzKPtMfMXKj2FKD0kvoPLSPWqH7wHi4IVFRWcXXWuvvk/wCOozJvsecbn22bmY1BWjWhAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQYggCAIAgMzZPs1t/vhG8oKrTeUzKPtMfMXKj2FKD0kvoPLSPWqH7wHi4IVFRWcXXWuvvk/wCOozJvsecbn22bmY1BWjWnaNm3Z4DPyTXmzrBUrewGC1A1um6bpun7tZ9PTZy4sRJLJZ1vDyLmYcJ3X+Dlje3Rz5kP2lkdRXiJb5lr4xx7aO2X2shqDSai3Xiq3Vz5MbmTG56NI6tXGWDUQ5LL7RoL1YFtMCyrJi2jb7E2GX75y/pdxs3ODLs+JhJCKUbilj2urUszqezixGdQ6q9cpVqczeI82vZL9ezEptpyDKDIl1BunuOEP3oic3MuCtfCmcyqQ1KVmq1pG3sWH4jrG0NsrY5E2pT6zjXsKt1VMGLuPU+56eARauN3KvVEOSyklu+ry2qmz8zFtdB6Nn7ZMxzysqRX8Lgwpe4zDh7juG6atLbZatWr+kV6KjzI1bEfLNYPK0DS5mHC2E5Vm1lvLylv6p21NPqgopCTcjTpF4CHUJD8pYLLhbCxobpQNbaloGO03LsYFb2UEi+MLlweFmmBUeo+puNqAS06tXdLMnpMlcWIlD6r4aHrmZ3cRyHJjJ6rZ0XbhRKYQR2m291kynOKyH5Vbp4WqG5SL222y3SfIiJdReh42oEUQk3PUnZOnhEINiPT9ys7qabuI6Mmp1Mq7cjYiO20VsyVPIpyJUG5e+1vzHNxbmadLjR9LVoMe6HVwu5WtmhaFiJXqwSWtVkRsUZvWTexUxmxltR7qcuY6eU8XcepxjatGh1xvjau5WctDiVdOI2Fr1aW5Ui1OZhxYvmN3/g5Y3t0c+ZD9pVdRXiNp5lr4xzTP3ZDayXsQrjbuJyqEMltjcCY3PDhdtxlh1NOsKqxprvq2ttps/MxHiyB2Sn87rOeuEq8NJZGWUYG9w19PSIkRfWWRFR5iY8RYs2rnlSBp2kwmsbRez3JyCqlGYOojVItTaccCRue56XAIdQ/WH5Sw548l+gxL3ZPJDR7WLEa1kvln5rmYEG2Rm73lJFwuqNz1adI6uKqoIc1sJq7bR+UKlabFhxGz7RWz7jkLUKPFxq+FW6vaNzVuW56NJaVTPHkthNte7J5Jy2zMWI4+rBFyRez3smM54WRIr7lwnSyanOQ9xFjdNWltstXG/pFsYqNZEV8RM7Lq+t0gafMw7WE6a90ORrcy3K8z1d1E/7le6l7Rv8AzLXxvhOI567K9yZJwm6o9JZrFDNzcurI4kJNEXFEx7X3SwZoWh9JF7rq9Pa0zd6M4qsYipLmztgV+6LVpFXfuoIbk6I3Ixj9S6tGsdWnVqW36j7R0Sj1R61BHO0mHFtEcM1LBkZX5g1m2JLvVB050QF7Tp1tkIkJfGEhWpZcLMpDblRabdVNT6e6dm2f9khrOyxSuJy4TpZDKcjbiLG6cXTwtWrulsoqTMRZMRILNq8t2gaZpMO0dKLocrOktN5nq+B/9yudS9okHmWvjfCR9z22ca/kZMYcmut1KjyiIWKhHHSOrkkPaktbNC0LYWIjd7HPacLadpW7x0axNi7C+sroV3x7owZwkxDk9SlG4pDq4OrV3KzNNHhTHiNtQ6sdepVqlm3vZOEZa2O7mHf1HtkH+pXJ7+4btp1aO6WLBHnPhIjS0rVVStNxNhOj7RWzO/kLFpErGr4VZie4bfTFrc9BDp+0qp4cll0G/vdha0xrJmYsRkdnvZUdzztWbW8a5vS3HllGFsmN018ES1fWV+GkzEx4iuzWDytE0rSYcJgrDyAYvTPSr5fDWiabp5yW+rxa1bpuRaeKqKeHOZvZMeCzrUXNqHM3e8d7/g5Y3t0c+ZD9pZXUV4iV+Za+MaFnZsYsZR5d1C527kcqRRSbHqco256tRCPG1LFqafJXFiNZc9V1t9I1Tm4sJr2zrsn1DOqmuVyfOKjUEXSabdANTkgh42keSPF1K5DS5i4m3TW2bV+S6LnM2GM7vK6HhajkcgjXNU25HS4JOA2Qjj7lX9NEmHZYmDam0jLhWRiJWdmS1ZyRuveip4i/HfDdYk4B6Tb4fskPbCtU6NG2Wxz67WqW0y5cm0rbpz5UGkMzZPs1t/vhG8oKrTeUzKPtMfMXKj2FKD0kvoPLSPWqH7wHi4IVFRWcXXWuvvk/46jMm+x5xufbZuZjUFaNaTM6HB65X571D8Z5bqj9VpOkal+vm5VND2tcybtt7P25YFNuWq0+E0Mbc48eW423hqjNkWkRLlLXTySLI2FjB1lramG4yJFIyrs/KcKuC+Lhuxltis1yoVZlotTbcySTwiXc6lZ0uzbxD5qyeoXDLIzFoeQc5im5C2Y9IPBtrCnNYYkXY6f/APVJdGn9Gg7pZHVbXCzcJFvOnLvCw9siz6rHa3OnV+rQ5g6eLg7uwi8PyuF/tFqoky6voIRe6Pqt5gnXdkZfexHR+iH9amgd+R8g8vtf6Yzf64fd68y//p7+h+FueRtRx/JWX8f9y0s2D1ClvUzsMnN/apoG39ZTFUpttX/ThweZdAYj7zfbNl6YyXjLX1yYXxGDrdSrNFHWRnacyy3XY8nl+W2GvIis6v8AUsSJ9OKxf9v+04l0OeRCCXerJYhjPIIxhytzw3TV9YhVui0/ZMpEdS9K5sy6d7ZNa2yo97WbnOxcwTZzNFPcTp0hhwhbZIRHU3we21ai7rUsOVpIZ8TFGtGirhrFmVmwG47RW0nl5mfkdLosCt4yq+71M9hH6ifb6TgkJFwib08rtlerJYpl2DYXW9UNdbWgWT7TTh7rbxEuk5mXbQaezApty1WnwWtWiPHluNthqLVwREuUS16yyYd45xHW1MK5ccjKpZDd1Znxtk+VVG5shuoja4SMJgnjuu6dTiWrVytS3dfp0rE+H/P0nbmkfyNm9O1l/wBpWxW8xLouaD1JVrhqVSi6tW4ypbjjer3JEtEzs28xw+SuqZlwyyMy8xYVl84GSuyFCnOkIPt0jq3EtOn06SWoBL4zoj/cpA2nK0Iv/TD/APe0dgt2jybYtDtw4veNc6IBao1nKalV1pvBx6kTx1O8lh3DSX18GVg3Bd1ixrZFnW5Zl7rfMRq2K/whaD70/wCTVug9b/KQDVv70jOqdEZ9frN+DveMrdb65eUlmum7EQ6WEctLFOh9dZOo9+3/ACTKkVN6hTsmp3YW5v7VInXtnHfVCzfuEYF0VYRjVuS2xH6rcJvSL5aW9PF09qtJDNJiXaIFX3GtjuEqxyNssxODaTeGZsz3E9UGwbfOntOELg8V0iH/AD1LbV+zGdVuunMtMjScJWvY1undt50GitloOozmIurTq063BHV9ZaumTMlVThUEPWJVi4mwlrNy3RHti67Et5vEW8anKfZBv+iaiuF425rd6W+1VfZb+09CTTrStBDxNh+EhZ0QK1MKTmzS6020LbdWp44GX8460RCX1NyWkqtHRO2k5trjT4amOXiX5Tu+wL1j3e+j/iit1T+oUkGpnYW5iJlZz3va0M26pJZuapuR4dVe/ibkkiZJsXS9L0lwdOlaeGaRZFxEFq7tWU9dI2iRtluImptbU+PcmzdXpbjeomGmJzXclrD9kiWwr1+zOoXtFqLTI/s4jA7DNbwuDInGnuHuhwJb8Yh5IlwsB+sr0P2kCmt1RmzKBk4WOA7KlnEG1VUWSY9LpBzHCEu04WkfGWDb12mYidnpui/MvCzHddvGjt17JIKmyG6lSqk2WLg9oJETRfWxFVVy7KsTHWqPOtrPo7rGQ2MmRtnZxjT3w0iTkqYf6xwxx6X+QrMX7OBSjVNMq24/aYjnsZVAqxtNyp7mOopLEx3UXdFqWFb/AEsQ/V5868tK3exG87eF83Fal828xRa5UKWy7TyNxuHJNsSLWXCx0q3VyyLLhU3GttVPTyx6IZGUipWsyLruKnuQqpctVqEM9OqPKlOONl8UiWEzs28c8kr6uZWR5GZeYsdt50srtkmLNpw4NSqdbHVgeh+PxY3TV8vHprf1WnLibCdroNHULOrJ3VxEEslc4qxaebVGrVTuCaMF6YO+Tkh1xwTaIvTNQ9stVSzZb7bbJya3XOeGtSeSTvbR2nbVzbsXNG0LfG3K2zVKpDmkRC22YkDRBwuMPKEVVWOkjKyEt1nuVDXUyrBJiZWIhLAOZmZsn2a2/wB8I3lBVabymZR9pj5i5UewpQekl9B5aR61Q/eA8XBCoqKzi661198n/HUZk32PONz7bNzMagrRrSZnQ4PXK/PeofjPLdUfqtJ0jUv183Kp2HM6/wDIyg3tPg3jApb1xN4N9UHIp26mWpsSHhaeFwdKuM8GL9JK6+qs8M7LVKuZykStqq58t7mn0E8vI8OO000Yy+o4nU+otXB1cHhLU1OBn2N057rDPb5svqGH2sKkuLdN0NjmI6wWh5m392DH8mI4ah/5LczacuLEvdOgUP3EvKeDM8Y2aOV+W2YEMcHDplTp1SIuyIE82Lw/FLxUk0faxyr/AJiLFdhuVthrV3lwt+Y17oiHWqoHfkfIPLCr/Spb1v8Au1eZf/09/Q+esjUu/b/kmVlwaOmFf/6W9TOwyc39qn3Z0IM7Nmu6rLfPByq0d6VS28HMfRFxhwiil7nSLY/FJWNP+opVb/n+UyqTDcrfUUbbysy/lM3eb+MnYtlGQ4gQ2wAYiXYxFsR/9lcrdOjTB08peb7h0q3AV+ZZ5l1nKe7Y1wUV3AX2OC4y597fb7Zsu5WphmaFsSnG6GsmoZ1ngLGsss6LF2lraepj0eOco2v43Q5+kiw5WI8oe6wW7Vo6lTtVBdqO9RaYWXa7ysRJ2otlg8p8HLlt9w5VrvO9Jxlz0XIRFxcNXbB+TFaeop8lvZIHrBq91H/U025w8JHFYxA1LPL1/A2lf2Qb/wBMKkNf6pju+n7j/wC3/aVtWbQTuq7KPRg1ap0tqNqEdWnUQjqWlg0ZkyqcOhizpVi4iw3bS6vh5FYUekQ3pZypcaMTMZonC3IPTOKPdNgtlW6W0quE7VrFoeO1tFFo4VMreFIdzF2TJEZ2O7jNdoDbu5yBxbPB9psS9HV3TavVmjMibSZDRtV2bLkXawEN9iv8IWg+9P8Ak1g0G/8AynLdW/vSM6p0Rn1+s34O94yt1vrl5SWa6bsRDpYRy0sS6H11kqj37f8AJMqQ03qFOyandhbm/tUzUGnbPv3fvuhhQCurq4yc6pMtXVOvhcfg6tSoiyOlcsztKWPrO1hzMXxGp7eFMu+Tl03Igy2PuXYfAp0Vtkhfxx7UsS1aSDAu5H4yxK/Q+zwmHrWlS1H0xtsd4jzsR2v90OfVMlEPpNKYfmF6HTEi07mPlNXxV8oF22cgOrUHWLlHi7u0SG2j63VmNpDK3GNBmSKZSyF916KwZDhi67pcEiHuWxWTp0t1pdBOr9JKtwo8C7KtiPz6IXbG+GXlCrjbWpynztycc5Lbg/aEVj167SsNcIMyjWXhYzewH1j3u+j/APyFbCn9Qo1M7C3MQLzI64lzd8pPliUfTeU5Xde2zczFkOfvT86tX9Xq7zNfsLeV/qWO3VWn/gzYvD/tOHdDpuHVjeNDLHDARxYmNj+XjCXiiqKLTii0qQ3UubC80H8x0XJCy95tp7OOonh0tzJjc+Tpk+nfsilMuWsmn2iQW+lwXuqk5fiPo5bWbuyhezm6boTmNWkjh+Qgkuvt4eIqJtGZSLpLyOtxtdQvOfvDeLLzYoF0nNzeboBaS7p3i+MrlVpwwYS3S6ep2HF7JGTYQ6/kfvfI8UVjUHpblIDqr95K3MTBzpvHKe2axT2swokCRUXGMSjFKhbviLer8unlLMkeJWwyHULnU22Fl69hIsbUF75PXHYcGPYEOnx6wM8DdKJB3Atx0OauFp5Wla6p0xsy4CBX2rtUtJholXFi4SUuYn4I9S/sqP8Ap8Fn1vqGJ233N/2/7SLOzNsy2ZnLZT9Vr1YqdPqATCjtswZLADjhpHtTbItXCWNBSRyJiY55YbJSXKBnnZlbEfltU7MNs5HWlSKpQqhVpkiZN6mMak60Y4DuZFwdDY8LgqxVwrDhwl2/2GmtkCywsxGVYJATM2T7Nbf74RvKCq03lMyj7THzFyo9hSg9JL6Dy0j1qh+8B4uCFRUVnF11rr75P+OozJvsecbn22bmY1BWjWkzOhweuV+e9Q/GeW6o/VaTpGpfr5uVTle2NSJ8vaJul1iFJebIYmkm2iIS/izS1k2hs1tk1GtELyXORlXh+U4dKpsyCIlIivRxLik82QqzpXiIg0ci7ylmdmBgeyFEHH1Mbbc8mS31T6huU7pbtGKyJyHJ9he72rxy9uPL2pOYkMfU9Gwxx4rLvGEfcnwvjKmkfMiw8JG9U6lZoZqGT/MW8Z/oh/WqoPfkf9O8sav9MZtNcPu3RzL/AHGR6Hx1kah37f8AJMrNg9Qpb1M7DJzf2qcz2ZMwsLY2nr6td53TDrdUnYNDjxd3becIfq6lg26XZy9JqbVWdWvs8DbsjMSMz+pbNG2dL0iR/vQU98h+MWr/AN1frP0wdHKTm6Jl22dV4WIz7Btl2reca8GLgodKrj7BxnGMKhFbfJsS3TVp1D6Har5RojQ/pXvHO9U6amqnmWePQ276f5jmO0NQp+V20XUztyCVDFt9iTTBgN7kOA7mPEEe61LXaMcU+FeI1d+gkobmzQLh3cOEm3nw+7M2Y7jfqreDcx2jCbzZYaek6Qj0x+UtxXYctjp1xZmtEjS72Aq3H1Foe8cEUs9vX8DaV/ZBv/TCt/X+qY7vp+4/+3/aQy2MrX+6PPuhuYiRM08XJpEI+ppHg6vjEsCi0beI5Zq7T9YuUfs7RM3P7agp+Q9XpVPl0R6ruT2Df6bL4t6BEtPo6sFlzVSxNhY6peL3HaWjWRcWI2XJzNKm582A7V2YGMJh5x2I9EdcFwh7Ho4j+XAlkeuixL3jLtVyS7U7SquHukKdmKhu2ztaDSH2xbcgvzGCEe106hWroN/+U5daIer33J4WY6D0Q2nSp9etDqaK9I0x3tWLQYl0uEqK1WaVcPCSTXJJJFiwLiIgPUOox2yN2BJbbHhERNEIisLToZVxMcvyZV2sJYP0PrrJ1Hv2/wCSZUhpvUKde1O7C3N/apBjNB44+bl3OgWlxuuyyEu63clHodllOX3TT/xCbmYsN2gicn7KtZeMtTpUph08ceyXAW8r9zTzHYrpiazM3snHuh1WvjhGuyvuYFwiahtljh6BcYi/ZXyi0YYukiupdPtTT/ym55k7cdJy8vmr207a8qoHTndwKU3KAcDLpcnpKjry4sOE3Vx1ohoahqdo8WE3faFhMZm7NdblRWsHBepwVNj0en0tOAueLqVytTpiNrctC3C1SMneXEapsC9Y93vo/wCKKvU/qFNNqZ2FuYhlVstrhvnOOs0ym0ma65Kq7uG6YMFpASdL0wi7UVpIY2aRTn1VRT1VwkREZsTE6NrmqMWvs21iE6fpkkI0Frui1iXitktnXv8AZnVL4y0tokRuHCRP2FrjwoefUOIXT0VWC/D9HtS0i8PkvrKxQ6elmX2Tm2q9Rk3ONeLEpOKuxRseHmZdQ44NvPxsHxP3qIIj9ZZc2xFIddkTRT6aip9n5VOF7BtUG4cp7tt58dQhKPgl2wuhpLxVap/tKXCQ7VCbOgnibi+Yy219LKy9lmkUPVpcknBp5fEb3QvJf5qi4NsqpsNYdPVbNlcqkftg/r+R+98jxRVFD6W5SDaqfeS/zG69EHpsudf1slGiPyBGmlqJsCLT6YStVmhs03WuMTyTxYV7pE+RR58NsnX4Ullse2caIRWD0MpzpoZF2mUs8jR3cx9k9uLAwF6VU7U3JkB7LvU2nT8v0FIKzRmwNhO7Uv8ArLMqr3oyCOzRZlUuHPO14jUWQPUMwZcvU2Q7gDRai1cni6fdEtdRaG0y4jklnpJ5LhHGq7rbX8pKLoifW3trvsXkTVdf3ToWuPY4+YgItYceMzZPs1t/vhG8oKrTeUzKPtMfMXKj2FKD0kvoPLSPWqH7wHi4IVFRmcfXWuzvk/5RRmTfY86XPts3MxpytGqO7bLO0JSchpNxOVamzqkNTBjBvqPc+Bue6atWou7Wxp6lYVwsSqw3aG0vI8q4sRIT+EWs/wBq9b+U19pZHXY+EmnnnSeGxxPaf2nKHntblHp1LpM+muQZJPkcwm9JCQ6eDpJYNTMszLh7pGb9fYLpAsUSMuFjYrf2zaNR8mGbJO3JrkpullT+qhfb3PUQkOrT8ZXZqpZImQzKXWaCnoVpGjbFhwnDsjc1XcnMwoNxYMuSooCTMiK2WknQIeL4qs002Tp6SJWuv8m1a1K7veOobS+1NSs9bOp1Gg0OZS3Is4ZZOSHRISw3NwelwfdKqqnWfDhJHetYYrpTZCRsu10no2btrGk5H2HKoM6hTKm87OOZusd0RERJtselwve1kw1axosbFuxX+K1U7RPGzYmxHD6jfb/mnTbwpeuHIKrnVIwlxmyJ4nBElrYXyWVlI5U1mZWNVxbO1iJN5obctGzBy4rluNWxPhyqlFKNuxvtk23iXbLOqalZkaNSc1WtkNVSSQZbYmUj5klnHU8kryCtwWsJjLje4y4ZY6ReD8mrtSVmnqMlvZITbLk9snz0JmMbc+VVVajS6jTKm3Oaw1CLsAHTax7ktS2XW4d46do1qtr6OmVWxcpH7aS2tns5Kf8Ac/RIL1Kt3WJulILDd5WI8XUI8ER7nUS1lRUNNs90id71k8pJ1aBcKfER1WKQVSWFd2zaNV8kXrFG3ZzclyjDTOqyfb3PULWDerTyfQWyqapaiNlU6O2s8DW/qeW2LDh+E5rsy55UjImvVeq1CkSqtImRxjNdTuiGgdWotWr3IqimqFhViPWK5xWuVpXXFsmM2jM5Gs8L9arsWE/TorUJuM3HkOCZDpIiIuD7pYsz5kjMU3y6Ldp1ljXCqqbpszbUsPIm3avSKjRZVVbmS8JLRR3RHQWnSQlq9yKzqerWNMLGfYr5Hao3jkXFiMXRc/KHSdpaZmS3RpjFJkmTu97ZCTomTQi4Wri8JzUXxljwzLDM0nEW9F2g8r+UVXZ4e9ukh/4RK0PaxW/lNfaWd12PhJh55Unht8JqObG3BbGYOW9wW5Dt+qxZNSiFHB58mtzAi5Wklj1FSs0eFTCrtaqSqppIFjbaU0rZv2sKVkhYkqgTqDMqjzs5yZu0d0QEcCbbHpcL3tXYatYkVGNNY9YIrZTNC6YtrER/vKuhc14VystNYx2585+WLZcIgFxwi0/WWoTZwkTrKhaipknXvNiJQ3VtpW7deUsqzJVs1ESkU0YZSBkt6cHBEdJe51CtpUVUc0bKT19aKeSh6o8bbuExOz5tcUDJLLxq3nbdn1CYUl2S/IafbESIuLp1dyIqpKuONFjMKy6wQWumyGjZmxEdLyuArsu6sVohId8JbsnS4WohEiIhFalSHVlR1qeSfiYlHl/ts0K2sqqZaFZtebUsY9P6gfcZfbFt1vSQ9t3K2z1kciYcJPLfrRDS0a0ssbNh2TEbOO1nb+S2X7lv1Gi1KoPYzHJIuRMW9OktPB4RdykNUscaxt3TFsd/gtcDRPG202I6m90RW1Bb9KtSsE5ySNocPGVzrsfCSDTrnSeGxGjaB2j6tnvPitnEwpNDhkRRoIubpjq/nDLg9PFa2aZpmxMQm8XyW7Nhw4Y17po+WN5ll5f9BuMQN4adLB9xtstJG32w/GHUqoJMl10mlo6jqdTHPwsSYzZ25KTmFl1W7dg23PgSqixuAyHn2yEOEPJWRUVSyR4VOgV+tcNVTSQRxtiY5Zsy7QUbIWqVl6dTZNUiz2mwFmO4I6CEuNwlRTVKwqykZsN2W0yszriVj37T20vFz6g0CHTqVLpUenOOvPDIdE91IhER4vJ0l8pUVMyzMrGZfr9HdYlijXDhNS2dc14GTeYzVxVKFJmxgjOsbjF07pqL3SqpplhZsRqbLXpbavPdcSkrP4RWz/atW/lNfaWX12P/AKHQvPOk8Njnefm2RbmbWWNStmnUKpwpUs2iF6STejDSYl2pLGqahZo8Kmnums9NXUklMkbKzGA2a9rzDKKiDbVxwJNSoLZk5FeiaSej6i1EOkiHUOrhcZXoaxVXDIYFj1i8mrkTrij9nunWrw26bGpdGmvWfSJkivyh04OPRQYDAuW4Wrplp5KrkrI1XDGSao1roYo2emTbY4xtJbTlJzvsihUeHS50KZBlDIkPStz0uelkJadJcrFYlTMs2HCRi832O6UywKrYlI6rCISZmyfZrb/fCN5QVWm8pmUfaY+YuVHsKUHpJfQeWketUP3gPFwQqKis4uutdffJ/wAdRmTfY843Pts3MxqCtGtCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAzNk+zW3++EbygqtN5TMo+0x8xcqPYUoPSS+g8tI9aofvAeLghUVT7StvHbOeN2xMRIQKYUhvV2wucIfGUdqFwysefL7FkXGXRo4jmaxjRBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAbvkjbh3Zm1adNAMS3WoNEWntREtRF9VXY1xSKptrRD1iuhj9ot4H1FJj0UeSketUP3gPFwQHAtqbZlazogNVmjE3GuuI3ubZOFpblNfzZF2pejwSWHU0+b+ld4iN9sa3RMyPZkUgLc2U15WhIcZq9t1KHiBacSxjEQ/KHgrT6YpF3lOP1FsrKdsMsbGu7y1DmMnwRK3hYw8mXhG8tQ5jJ8ESYWGTLwjeWocxk+CJMLDJl4T63mqPMJPgiTCwyZeE+d5ahzGT4IkwsMmXhG8tQ5jJ8ESYWGTLwn1vNUeYSfBEmFhky8J87y1DmMnwRJhYZMvCN5ahzGT4IkwsMmXhG8tQ5jJ8ESYWGTLwjeWocxk+CJMLDJl4RvLUOYyfBEmFhky8I3lqHMZPgiTCwyZeEby1DmMnwRJhYZMvCfW81R5hJ8ESYWGTLwnzvLUOYyfBEmFhky8J9bzVHmEnwRJhYZMvCN5qjzCT4IkwsMmXhG81R5hJ8ESYWGTLwjeao8wk+CJMLDJl4RvNUeYSfBEmFhky8I3mqPMJPgiTCwyZeEbzVHmEnwRJhYZMvCN5qjzCT4IkwsMmXhPneWocxk+CJMLDJl4RvLUOYyfBEmFhky8I3lqHMZPgiTCwyZeEby1DmMnwRJhYZMvCfW81R5hJ8ESYWGTLwnzvLUOYyfBEmFhky8I3lqHMZPgiTCwyZeEby1DmMnwRJhYZMvCN5ahzGT4IkwsMmXhG8tQ5jJ8ESYWGTLwjeWocxk+CJMLDJl4RvLUOYyfBEmFhky8I3lqHMZPgiTCwyZeEby1DmMnwRJhYZMvCN5ahzGT4IkwsMmXhM5b2V933XIbapNuVKYRlpEm4xaflcVVLHI26plQ26rqGwxxsTy2U9l0co2CuO4dD91yWsW8Gxx1Nw28eMI8oy7Yv7luKamydrTvHXLBYVtq583rPlJLrOJoeOketUP3gPFwQHsQH5ONA8GkxwIfyFghTp0Yj8N7YfNWvB4L50FGBOE/u9sTmrHg8F96D7lpwje2JzVjweCdAy04RvbE5qx4PBOgZacI3tic1Y8HgnQMtOEb2xOaseDwToGWnCN7YnNWPB4J0DLThG9sPmrPyME6D5lpwje2JzVjweCdB9y04RvbE5qx4PBOgZacI3tic1Y8HgnQMtOEb2xOaseDwToGWnCN7YnNWPB4J0DLThG9sTmrHg8E6Blpwje2JzVjweCdAy04RvbE5qx4PBOgZacI3tic1Y8HgnQMtOEb2xOaseDwToGWnCN7YnNWPB4J0DLThG9sTmrHg8E6Blpwje2JzVjweCdAy04RvbE5qx4PBOgZacI3tic1Y8HgnQMtOEb2xOaseDwToGWnCN7YfNWfkYJ0HzLThG9sTmrHg8E6D7lpwje2JzVjweCdAy04RvbE5qx4PBOgZacI3th81Z+RgnQfMtOEb2xOaseDwToPuWnCN7YnNWPB4J0DLThG9sTmrHg8E6Blpwje2JzVjweCdAy04RvbD5qz8jBOg+ZacI3tic1Y8HgnQfctOEb2xOaseDwToGWnCN7YnNWPB4J0DLThG9sTmrHg8E6Blpwje2HzVn5GCdB8y04T9mWQaDS2AgP5BHpIV6NHQfqhUEB/9k=" width="22" /> R7 ENNSKÁ CYKLOSTEZKA (kompletní mapa v sekci ke stažení)<br />\n<br />\n<strong>(ve všech apartmánech je Vám k dispozici MTB mapa regionu SCHLADMING-DACHSTEIN 1:50.000, na kterou se odkazují i naše náhledy s čísly map)</strong></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num1.png" style="width: 20px; height: 20px;" />&nbsp;OKRUH KAIBLING - oranžová barva (mapa č. 06)</p>\n\n<p>délka 28,1 km, převýšení 834 m (15,3km asfalt; 12,3km šotolina; 0,5km ostatní)</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/kolo/hod1.png" class="full-width-img" style="width: 784px; height: 25px;" /></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num2.png" style="width: 20px; height: 20px;" />&nbsp;OKRUH STORM ALM - modrá barva (mapa č. 07)</p>\n\n<p>délka 42,1 km, převýšení 1320 m (10,2km asfalt; 31,4km šotolina; 0,5km ostatní)</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/kolo/hod2.png" class="full-width-img" style="width: 784px; height: 25px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/kolo/mapa1b2.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/kolo/mapa1a2.jpg" class="full-width-img" style="width: 400px; height: 420px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num3.png" style="width: 20px; height: 20px;" />&nbsp;OKRUH VIEHBERG (mapa č. 12)</p>\n\n<p>délka 41,6 km, převýšení 1009 m (21,2km asfalt; 20,4km šotolina)</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/kolo/hod3.png" class="full-width-img" style="width: 783px; height: 25px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/kolo/mapa3.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/kolo/map3.jpg" class="full-width-img" style="width: 400px; height: 463px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num4.png" style="width: 20px; height: 20px;" />&nbsp;POHODOVÝ UNTERTAL - zelená barva (mapa č. 18)</p>\n\n<p>délka 28,1 km, převýšení 96 m (22,3km asfalt; 3,2km šotolina) - (lanovkou na mezistanici PLANAI a z kopce dolů!)</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/kolo/hod4.png" class="full-width-img" style="width: 784px; height: 25px;" /></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num5.png" style="width: 20px; height: 20px;" />&nbsp;POHODOVÝ ROHRMOOS - fialová barva (mapa č. 19)</p>\n\n<p>délka 37,2 km, převýšení 372 m (29,5km asfalt; 4,3km šotolina; 3,4km singletrail) - (lanovkou na PLANAI a z kopce dolů!)</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/kolo/hod5.png" class="full-width-img" style="width: 784px; height: 25px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/kolo/mapa4b5.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/kolo/mapa4a5.jpg" class="full-width-img" style="width: 400px; height: 303px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num6.png" style="width: 20px; height: 20px;" />&nbsp;OKRUH AICH- modrá barva (mapa č. 09)</p>\n\n<p>délka 28,8 km, převýšení 816 m (26,1km asfalt; 1,9km šotolina; 0,8km ostatní)</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/kolo/hod6.png" class="full-width-img" style="width: 784px; height: 26px;" /></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num7.png" style="width: 20px; height: 20px;" />&nbsp;OKRUH STRUBSCHLUCHT - červená barva (mapa č. 10)</p>\n\n<p>délka 35,4 km, převýšení 1.271 m (31,9km asfalt; 1,8km šotolina; 1,7km ostatní)</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/kolo/hod7.png" class="full-width-img" style="width: 784px; height: 25px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/kolo/mapa6b7.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/kolo/mapa6a7.jpg" class="full-width-img" style="width: 400px; height: 364px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num8.png" style="width: 20px; height: 20px;" />&nbsp;OKRUH REITERALM- oranžová barva (mapa č. 03)</p>\n\n<p>délka 43,4 km, převýšení 1.270 m (22,6km asfalt; 20,8km šotolina)</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/kolo/hod8.png" class="full-width-img" style="width: 784px; height: 26px;" /></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num9.png" style="width: 20px; height: 20px;" />&nbsp;OKRUH ROHRMOOS - modrá barva (mapa č. 04)</p>\n\n<p>délka 41,9 km, převýšení 828 m (30,8km asfalt; 11,1km šotolina)</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/kolo/hod9.png" class="full-width-img" style="width: 784px; height: 26px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/kolo/mapa8b9.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/kolo/mapa8a9.jpg" class="full-width-img" style="width: 400px; height: 458px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num10.png" style="width: 20px; height: 20px;" />&nbsp;PANORAMATICKÝ OKRUH (mapa č. 05)</p>\n\n<p>délka 56,6 km, převýšení 1.311 m (31,7km asfalt; 22,3km šotolina, 2,6km ostatní)</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/kolo/hod10.png" class="full-width-img" style="width: 784px; height: 25px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/kolo/mapa10.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/kolo/map10.jpg" class="full-width-img" style="width: 400px; height: 296px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://www.alpineliving.cz/media/userfiles/Obrazky-mix/R7%20logo.jpg" style="width: 50px; height: 50px;" />R7 ENNSKÁ CYKLOSTEZKA (kompletní mapa v sekci ke stažení)<br />\n<br />\n<img alt="" src="http://www.alpineliving.cz/media/userfiles/kolo/R777.jpg" class="full-width-img" style="width: 770px; height: 23px;" /><br />\n<br />\nPáteřní ENNSKÁ CYKLOSTEZKA v úseku RADSTADT-PICHL-SCHLADMING-<strong>HAUS</strong>-AICH-OBLARN-TRAUTENFELS (75km) nabízí nenáročnou cyklistiku vhodnou i pro rodiny s dětmi. Poznejte romantické údolí řeky ENNS, navštivte malebné vesničky a ochutnejte místni gastronomii.<br />\n<br />\n&nbsp;<img alt="" src="http://www.alpineliving.cz/media/userfiles/kolo/r7.jpg" style="width: 200px; height: 150px;" />&nbsp;<img alt="" src="http://www.alpineliving.cz/media/userfiles/kolo/1.jpg" style="width: 200px; height: 150px;" /> <img alt="" src="http://www.alpineliving.cz/media/userfiles/kolo/2.jpg" style="width: 225px; height: 150px;" /> <img alt="" src="http://www.alpineliving.cz/media/userfiles/kolo/4.jpeg" style="width: 226px; height: 150px;" /></p>\n', '');
INSERT INTO `page_data` (`id`, `page_id`, `language_id`, `route_id`, `nazev`, `nadpis`, `uvodni_popis`, `popis`, `url`) VALUES
(31, 24, 1, 101, 'Koupání', 'Koupání - přehledová mapa cílů', '', '<p><img alt="" src="http://www.alpineliving.cz/media/userfiles/koupani/mapa_leto.jpg" class="full-width-img" style="width: 899px; height: 437px;" /></p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num1.png" style="line-height: 1.6em; width: 20px; height: 20px;" /><span style="line-height: 1.6em;">&nbsp;KOUPALIŠTĚ HAUS</span><br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num2.png" style="line-height: 1.6em; width: 20px; height: 20px;" /><span style="line-height: 1.6em;">&nbsp;PANORAMA BAD GRÖBMING</span><br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num3.png" style="line-height: 1.6em; width: 20px; height: 20px;" /><span style="line-height: 1.6em;">&nbsp;PŘÍRODNÍ KOUPALIŠTĚ AICH</span><br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num4.png" style="line-height: 1.6em; width: 20px; height: 20px;" /><span style="line-height: 1.6em;">&nbsp;LÁZNĚ SCHLADMING</span><br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num5.png" style="line-height: 20.7999992370605px; width: 20px; height: 20px;" /><span style="line-height: 20.7999992370605px;">&nbsp;RAMSAU BEACH</span><br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num6.png" style="line-height: 20.7999992370605px; width: 20px; height: 20px;" /><span style="line-height: 20.7999992370605px;">&nbsp;THERME AMADÉ ALTENMARKT</span></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num1.png" style="width: 20px; height: 20px;" />&nbsp;KOUPALIŠTĚ HAUS<br />\nVyhřívané koupaliště a beach-volejbalové hřiště (vzdálenost z apartmánu 500m, z centra obce podchodem pod hlavní cestou B320 a jste přímo u koupaliště)&nbsp;<a href="http://www.haus.at/de/infrastruktur/freizeit-sport.php">www.haus.at/de/infrastruktur/freizeit-sport.php</a></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/koupani/1.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/1.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/11.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/11.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/111.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/111.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/1111.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/1111.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/11111.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/11111.jpg" style="width: 175px; height: 131px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num2.png" style="width: 20px; height: 20px;" />&nbsp;PANORAMA BAD GRÖBMING<br />\nVzdálenost autem z HAUSU 12 km do obce Gröbming. (HAUS-AICH-GRÖBMING). V obci Gröbming je označen příjezd k PANORAMA BAD GRÖBMING. Moderní zařízení, otevřené v roce 2010, plavecký bazén, whirpooly, saunový svět, brouzdaliště, skokanská věž, vše s nádherným výhledem na vrchol STODERZINKEN.<a href="http://www.groebming.at/panoramabad/index.php">www.groebming.at/panoramabad/index.php</a></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/koupani/2.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/2.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/22.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/22.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/222.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/222.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/2222.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/2222.jpg" style="width: 175px; height: 73px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/22222.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/22222.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;</p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num3.png" style="width: 20px; height: 20px;" />&nbsp;PŘÍRODNÍ KOUPALIŠTĚ AICH<br />\nPřírodní koupaliště v obci AICH, z apartmánů pouze 5 min jízdy autem (HAUS - AICH). Beach-volejbalové hřiště, tenisové antukové kurty, stolní tenis, vodní trampolína, restaurace.<a href="http://www.aich.at/freizeit.php">www.aich.at/freizeit.php</a></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/koupani/3.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/3.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/33.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/33.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/333.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/333.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/3333.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/3333.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/33333.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/33333.jpg" style="width: 175px; height: 131px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num4.png" style="width: 20px; height: 20px;" />&nbsp;LÁZNĚ SCHLADMING<br />\nVzdálenost autem z HAUSU 5 km do SCHLADMINGU. (HAUS-SCHLADMING, sjezd z B320 SCHLADMING OST, odbočka vpravo, dále podjezdem). Zimní a letní lázně, whirpooly, saunový svět, brouzdaliště, tobogány, skluzavky, fitness-studio, vše s nádherným výhledem na okolní hory.&nbsp;<a href="http://www.erlebnisbad-schladming.at/">www.erlebnisbad-schladming.at/</a></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/koupani/4.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/4.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/44.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/44.jpg" style="width: 175px; height: 113px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/444.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/444.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/4444.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/4444.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/44444.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/44444.jpg" style="width: 175px; height: 116px;" /></a></p>\n\n<hr />\n<p style="line-height: 20.7999992370605px;"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num5.png" style="line-height: 20.7999992370605px; width: 20px; height: 20px;" /><span style="line-height: 20.7999992370605px;">&nbsp;RAMSAU BEACH</span><br />\nPřírodní koupaliště v obci RAMSAU, z apartmánů 10 min jízdy autem (HAUS - SCHLADMING-RAMSAU). Lanový park,&nbsp;vodní fotbal, 2x Beach-volejbalové hřiště, dětské hřiště, půjčovna Segway. <a href="http://www.beach.co.at">www.beach.co.at</a></p>\n\n<p style="line-height: 20.7999992370605px;"><a href="http://www.alpineliving.cz/media/userfiles/koupani/ramsau beach1.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/koupani/ramsau%20beach1.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://www.alpineliving.cz/media/userfiles/koupani/ramsau beach2.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/koupani/ramsau%20beach2.jpg" style="width: 175px; height: 79px;" /></a>&nbsp;<a href="http://www.alpineliving.cz/media/userfiles/koupani/ramsau beach3.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/koupani/ramsau%20beach3.jpg" style="width: 175px; height: 65px;" /></a>&nbsp;<a href="http://www.alpineliving.cz/media/userfiles/koupani/ramsau beach4.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/koupani/ramsau%20beach4.jpg" style="width: 175px; height: 124px;" /></a>&nbsp;<a href="http://www.alpineliving.cz/media/userfiles/koupani/ramsau beach5.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/koupani/ramsau%20beach5.jpg" style="width: 175px; height: 124px;" /></a></p>\n\n<hr style="line-height: 20.7999992370605px;" />\n<p style="line-height: 20.7999992370605px;"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num6.png" style="line-height: 20.7999992370605px; width: 20px; height: 20px;" /><span style="line-height: 20.7999992370605px;">&nbsp;THERME AMADÉ ALTENMARKT</span><br />\n<span style="line-height: 20.7999992370605px;">Vzdálenost 28 km . (HAUS - SCHLADMING - RADSTADT, ALTENMARKT). Termální lázně, bazény se slanou i sladkou vodou,&nbsp;</span><span style="line-height: 20.7999992370605px;">vnitřní a venkovní bazény, whirpooly, saunový svět, brouzdaliště, tobogány, skluzavky, fitness-studio, vše s nádherným&nbsp;</span><span style="line-height: 20.7999992370605px;">výhledem na okolní hory. <a href="http://www.thermeamade.at">www.thermeamade.at</a></span></p>\n\n<p style="line-height: 20.7999992370605px;"><a href="http://www.alpineliving.cz/media/userfiles/koupani/therme-amade1.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/koupani/therme-amade1.jpg" style="width: 175px; height: 93px;" /></a>&nbsp;<a href="http://www.alpineliving.cz/media/userfiles/koupani/therme-amade2.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/koupani/therme-amade2.jpg" style="width: 175px; height: 108px;" /></a>&nbsp;<a href="http://www.alpineliving.cz/media/userfiles/koupani/therme-amade3.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/koupani/therme-amade3.jpg" style="width: 175px; height: 114px;" /></a>&nbsp;<a href="http://www.alpineliving.cz/media/userfiles/koupani/therme-amade4.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/koupani/therme-amade4.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://www.alpineliving.cz/media/userfiles/koupani/therme-amade5.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/koupani/therme-amade5.jpg" style="width: 175px; height: 122px;" /></a></p>\n', ''),
(32, 25, 1, 102, 'Ke stažení', 'Nabídka v letních měsících', '', '<h2>Tipy na výlety, koupání, lázně</h2>\n\n<ul>\n<li>informace, zajimavé turistické cíle</li>\n<li>tipy na výlety</li>\n<li>informace, zajimavé turistické cíle</li>\n<li>tipy na výlety</li>\n</ul>\n', ''),
(33, 26, 1, 103, 'Tenis', 'TENISOVÝ AREÁL HAUS IM ENNSTAL', '', '<p><img alt="" src="http://www.alpineliving.cz/media/userfiles/koupani/mapa_tenis.jpg" class="full-width-img" style="width: 900px; height: 437px;" /></p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num1.png" style="width: 20px; height: 20px;" /> TENISOVÝ AREÁL HAUS IM ENNSTAL,Sportplatz, 8967 Haus im Ennstal</p>\n\n<p>Rezervace dvorců na telefonu: +43 3686 2560<br />\n<a href="mailto:tennis@sportunionhaus.at">tennis@sportunionhaus.at</a><br />\n<a href="http://www.geomix.at/verein/su-haus-im-ennstal-tennis/" target="_blank">www.sportunionhaus.at<br />\n<br />\n<img alt="" src="http://www.alpineliving.cz/media/userfiles/Obrazky-mix/1.jpeg" style="width: 300px; height: 200px;" /><img alt="" src="http://www.alpineliving.cz/media/userfiles/Obrazky-mix/2.jpeg" style="width: 300px; height: 200px;" /><img alt="" src="http://www.alpineliving.cz/media/userfiles/Obrazky-mix/3.JPG" style="width: 301px; height: 200px;" /></a><br />\n&nbsp;</p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num2.png" style="line-height: 20.7999992370605px; width: 20px; height: 20px;" /><span style="line-height: 20.7999992370605px;">&nbsp;TENISOVÝ DVORCE AICH, Freizeitseeanlage AICH</span></p>\n\n<p><span style="line-height: 20.7999992370605px;">tel. +43 3686 4305</span><br />\n<a href="http://www.aich.at">www.aich.at</a><br />\n<span id="cloak5682"><a href="mailto:freizeitsee.aich@gmx.at">freizeitsee.aich@gmx.at</a></span></p>\n\n<p><a href="http://www.alpineliving.cz/media/userfiles/koupani/33333.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/koupani/33333.jpg" style="width: 300px; height: 225px;" /></a></p>\n\n<p>&nbsp;</p>\n', ''),
(34, 1, 4, 104, 'Homepage', 'Luxury apartments directly on the slope', '', NULL, ''),
(35, 13, 4, 105, 'Apartments', 'EQUIPMENT OF THE APARTMENT', '', '<p>APARTMENT RAMSAU &ndash; for 4 persons<a href="http://www.alpineliving.cz/media/userfiles/Obrazky-mix/pudB9.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/Obrazky-mix/pubB9small.jpg" style="width: 250px; height: 216px; float: right;" /></a></p>\n\n<ul>\n	<li>large living room with kitchen and sofa bed for 2 persons, SAT TV</li>\n	<li>a fully equipped kitchen , coffee maker , toaster , kettle , dishwasher, oven , microwave oven</li>\n	<li>9 m<sup>2</sup> terrace with seating overlooking to the ski slope</li>\n	<li>bedroom with double bed, SAT TV</li>\n	<li>separate WC , bathroom with shower , private sauna, washing machine at the apartment</li>\n	<li>FREE Wi - Fi internet , iPAD available at the apartment</li>\n	<li>underground garage and a private heated ski room</li>\n	<li>100 % disabled access</li>\n</ul>\n\n<p>&nbsp;</p>\n\n<p>APARTMENT KAIBLING &ndash; for 4 persons<a href="http://www.alpineliving.cz/media/userfiles/Obrazky-mix/pudB10.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/Obrazky-mix/pubB10small.jpg" style="width: 250px; height: 216px; float: right;" /></a></p>\n\n<ul>\n	<li>large living room with kitchen and sofa bed for 2 persons, SAT TV</li>\n	<li>a fully equipped kitchen , coffee maker , toaster , kettle , dishwasher, oven , microwave oven</li>\n	<li>9 m<sup>2</sup> terrace with seating overlooking to the ski slope</li>\n	<li>bedroom with double bed, SAT TV</li>\n	<li>separate WC , bathroom with shower , private sauna, washing machine at the apartment</li>\n	<li>FREE Wi - Fi internet , iPAD available at the apartment</li>\n	<li>underground garage and a private heated ski room</li>\n	<li>100 % disabled access</li>\n</ul>\n\n<p>&nbsp;</p>\n\n<p>APARTMENT ENNSTALBLICK &ndash; for 4 persons<a href="http://alpineliving.cz/media/userfiles/Obrazky-mix/pudorysB15.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/Obrazky-mix/pudorysB15.jpg" style="width: 250px; height: 216px; float: right;" /></a></p>\n\n<ul>\n	<li>large living room with kitchen and sofa bed for 2 persons, SAT TV</li>\n	<li>a fully equipped kitchen , coffee maker , toaster , kettle , dishwasher, oven , microwave oven</li>\n	<li>9 m<sup>2</sup> terrace with seating overlooking to the ski slope</li>\n	<li>bedroom with double bed, SAT TV</li>\n	<li>separate WC , bathroom with shower , private infra sauna</li>\n	<li>FREE Wi - Fi internet , tablet available at the apartment</li>\n	<li>underground garage and a private heated ski room</li>\n	<li>100 % disabled access</li>\n</ul>\n\n<p>&nbsp;</p>\n\n<p style="line-height: 20.8px;">APARTMENT HOCHWURZEN&nbsp;&ndash; <span style="line-height: 20.8px;">for 4 persons</span><a href="http://www.alpineliving.cz/media/userfiles/Obrazky-mix/HOCHWURZEN.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/Obrazky-mix/HOCHWURZEN.jpg" style="width: 250px; height: 200px; float: right;" /></a></p>\n\n<ul style="line-height: 20.8px;">\n	<li>large living room with kitchen and sofa bed for 2 persons, SAT TV</li>\n	<li>a fully equipped kitchen , coffee maker , toaster , kettle , dishwasher, oven , microwave oven</li>\n	<li>12 m<sup>2</sup>&nbsp;terrace with seating overlooking to the DACHSTEIN GLETSCHER</li>\n	<li>bedroom with double bed, SAT TV</li>\n	<li>separate WC , bathroom with shower , private sauna</li>\n	<li>FREE Wi - Fi internet&nbsp;</li>\n	<li>underground garage and a private heated ski room</li>\n	<li>100 % disabled access</li>\n</ul>\n', ''),
(36, 14, 4, 106, 'Reservation', 'RESERVATION OVERVIEW', '', '<p>Apartments can be booked via:</p>\n\n<ol>\n	<li>By e-mail: <a href="mailto:rezervace@alpineliving.cz?subject=Rezervace%20apartm%C3%A1n%C5%AF">rezervace@alpineliving.cz</a>&nbsp;<br />\n	&nbsp;</li>\n	<li>By phone call: <strong>+420 605 238 595</strong><br />\n	&nbsp;</li>\n	<li><a href="" id="openForm">By reservation form</a></li>\n</ol>\n\n<p>&nbsp;</p>\n', ''),
(37, 19, 4, 107, 'Price list', 'Price list', '', '<!-- costs -->\n<section class="tariff">\n<div class="row fg-no-gutter">\n<div class="winter fg8">\n<h3>Winter season 2016/2017</h3>\n\n<div class="row fg-no-gutter">\n<article class="item rounded">\n<header class="rounded shadow">Off&nbsp;season</header>\n\n<section class="date">\n<p>26.11.2016 - 20.12.2016<br />\n06.03.2017 - 16.04.2017</p>\n</section>\n\n<footer class="cost rounded">\n<p>130 EUR / NIGHT</p>\n\n<p>(apartment for 4 persons)</p>\n</footer>\n</article>\n\n<article class="item rounded">\n<header class="rounded shadow">Main season</header>\n\n<section class="date">\n<p>21.12.2016 - 05.03.2017<br />\n&nbsp;</p>\n</section>\n\n<footer class="cost rounded">\n<p>160 EUR / NIGHT</p>\n\n<p>(apartment for 4 persons)</p>\n</footer>\n</article>\n</div>\n</div>\n\n<div class="summer fg4">\n<h3>Summer season 2016</h3>\n\n<article class="item rounded">\n<header class="rounded shadow">Summer 2016</header>\n\n<section class="date">\n<p>20.05.2016 - 16.10.2016<br />\n&nbsp;</p>\n</section>\n\n<footer class="cost rounded">\n<p>70 EUR / NIGHT</p>\n\n<p>(apartment for 4 persons)</p>\n</footer>\n</article>\n<img alt="" src="http://alpineliving.cz/media/userfiles/SOMMERCARD/sommercard2.jpg" style="width: 282px; height: 177px; margin-left: 5px; margin-top: 11px;" /></div>\n</div>\n\n<div class="surcharge">\n<p>&nbsp;</p>\n\n<h3><strong>SURCHARGES - REQUIRED</strong></h3>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">FINAL CLEANING INCLUDING BED LINEN, TOWELS FOR 4 PEOPLE &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong>65 EUR / APARTMENT</strong></span></p>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">TOURIST TAX FOR VILLAGE HAUS&nbsp;/ NIGHT &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong>1,50 EUR / PERSON / NIGHT</strong></span></p>\n&nbsp;\n\n<h3><strong>INCLUDES IN&nbsp;THE PRICE OF THE APARTMENT&nbsp;</strong></h3>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">SUMMER CARD&nbsp;&quot;SOMMERCARD&quot; (SEASON FROM&nbsp;20.MAY&nbsp;2016 TO 16.OCTOBER&nbsp;2016) FOR APARTMENTS&nbsp;RAMSAU AND KAIBLING</span></p>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">UNLIMITED&nbsp;Wi-Fi CONNECTION TO INTERNET IN ALL APARTMENTS</span></p>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">BABY COT&nbsp;(FOR BABY UNDER 3 YEARS OLD) - MUST BE ORDERED BY E-MAIL OR TELEPHONE!</span></p>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">UNLIMITED USE OF SAUNA OR INFRA SAUNA IN APARTMENTS</span></p>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">ONE PARKING SPACE IN GARAGE FOR CAR</span></p>\n&nbsp;\n\n<h3><strong>CANCEL FEES</strong></h3>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">RESERVATIONS MAY BE CANCELED FREE OF CHARGE UNTIL 90 DAYS BEFORE ARRIVAL</span></p>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px;">IN CASE OF CANCELLATION 89 TO 31 DAYS BEFORE ARRIVAL WILL BE CHARGED 50% OF THE TOTAL RESERVATION</span></p>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px; background-color: rgb(255, 221, 0);">IN CASE OF CANCELLATION 30 TO 7 DAYS BEFORE ARRIVAL WILL BE CHARGED 70% OF THE TOTAL RESERVATION</span></p>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><span style="color: rgb(0, 0, 128); line-height: 20.8px; background-color: rgb(255, 221, 0);">IN CASE OF CANCELLATION LESS THAN&nbsp;7 DAYS BEFORE ARRIVAL WILL BE CHARGED 90% OF THE TOTAL RESERVATION</span></p>\n\n<h3><br />\n<strong>GIFT CARDS</strong></h3>\n\n<p style="line-height: 20.8px; background: #ffdd00; padding:5px; border-radius:5px;"><font color="#000080">SALE IN THE FORM OF GIFT VOUCHERS - GIFT CARDS EXHIBITS AT THE SUM FROM 50 EUR.</font></p>\n\n<p style="line-height: 20.8px;"><span style="color: rgb(0, 0, 128);">(you can order gift cards on e-mail&nbsp;rezervace@alpineliving.cz)</span></p>\n\n<h3><br />\n<img alt="" src="http://www.alpineliving.cz/media/userfiles/prijezdove%20informace/DP_L.jpg" style="width: 350px; height: 250px; line-height: 1.2em; font-size: 13px;" /><span style="line-height: 1.2em; font-size: 13px;">&nbsp; &nbsp; &nbsp; &nbsp;</span><img alt="" src="http://www.alpineliving.cz/media/userfiles/prijezdove%20informace/DP_Z.jpg" style="width: 350px; height: 250px; line-height: 1.2em; font-size: 13px;" /></h3>\n</div>\n</section>\n<!-- end costs -->\n\n<p>&nbsp;</p>\n', ''),
(38, 17, 4, 108, 'Winter', 'HAUSER KAIBLING / SCHLADMING / RAMSAU / DACHSTEIN', '', '<ul>\n	<li>region belongs to the top 5 ski areas in Austria</li>\n	<li>organizer of the World Ski Championships in 1982 and 2013</li>\n	<li>organizer of traditional &quot;night&quot; Slalom FIS World Cup series</li>\n	<li>100% coverage of the snowmaking equipment allowing skiing from late November until late April</li>\n	<li>123 km of perfectly groomed slopes and 56 comfortable cabin or chair lifts</li>\n	<li>4,6 km long FIS slope of the Planai ending in the center of Schladming</li>\n	<li>ski pass AMADÉ is valid on incredibly large area - 860 km of slopes</li>\n	<li>free ride zones in HAUSER Kaibling and new snow park at the Planai</li>\n	<li>paradise for fans of white traces - 472 km of perfectly groomed classic and skating tracks</li>\n</ul>\n\n<section class="trackplans-widget">\n<h2>HAUSER KAIBLING &ndash; map</h2>\n<a href="http://alpine.dgsbeta.cz/media/userfiles/Obrazky-mix/Horni_mapa_vetsi__hauser_kaibling_m.jpg"><img alt="" src="http://alpine.dgsbeta.cz/media/userfiles/Obrazky-mix/hauser_kaibling_m.jpg" /></a>\n\n<h2>&nbsp;</h2>\n\n<h2>SCHLADMING / DACHSTEIN</h2>\n\n<p><a href="http://alpine.dgsbeta.cz/media/userfiles/Obrazky-mix/spodni-mapa_vetsi_strediska_celkova.jpg"><img alt="" src="http://alpine.dgsbeta.cz/media/userfiles/Obrazky-mix/spodni-mapa_mensi_strediska_celkova.jpg" style="width: 933px; height: 538px;" /></a></p>\n\n<h2>Other activities</h2>\n\n<div class="row fg-no-gutter">\n<div class="item"><img alt="" src="http://alpine.dgsbeta.cz/media/userfiles/Obrazky-mix/lazne.jpg" />\n<ul>\n	<li>AQUAPARK SCHLADMING &ndash; distance from apartment 5min <a href="http://www.erlebnisbad-schladming.at">http://www.erlebnisbad-schladming.at</a></li>\n</ul>\n</div>\n\n<div class="item"><img alt="" src="http://alpine.dgsbeta.cz/media/userfiles/Obrazky-mix/sanky_hochwurzen.jpg" />\n<ul>\n	<li>TOBOGGAN TRACK HOCHWURZEN &ndash; length of track 6,3km; distance from apartment 10 min<a href="http://www.nachtrodeln.at/"> </a><a href="http://www.nachtrodeln.at/">http://www.nachtrodeln.at/</a></li>\n</ul>\n</div>\n\n<div class="item"><img alt="" src="http://alpine.dgsbeta.cz/media/userfiles/Obrazky-mix/bezky_ramsau.jpg" />\n<ul>\n	<li>WHITE TRACES PARADISE Nr. 1 in Austria &ndash; distance form apartment 10min<a href="http://www.ramsau.com"> www.ramsau.com</a></li>\n	<li>LIST OF ALL WINTER ACTIVITIES IN REGION &nbsp;<a href="http://www.schladming-dachstein.at/">http://www.schladming-dachstein.at/</a></li>\n</ul>\n</div>\n</div>\n</section>\n', ''),
(39, 21, 4, 109, 'Tourism', 'Tourism - map of famous targets', '', '<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/prehledova-mapa-turistika.jpg" class="full-width-img" style="line-height: 1.6em; width: 900px; height: 437px;" /></p>\n\n<p>&nbsp;</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num1.png" style="width: 20px; height: 20px;" />&nbsp;WATERFALLS WILDE WASSER - trip for children<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num2.png" style="width: 20px; height: 20px;" />&nbsp;WATERFALLS WILDE WASSER - PREINTALERHÜTTE<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num3.png" style="width: 20px; height: 20px;" />&nbsp;Circuit around LAKE CONSTANCE - trip for children<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num4.png" style="width: 20px; height: 20px;" />&nbsp;STODERZINKEN 2048 masl<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num5.png" style="width: 20px; height: 20px;" />&nbsp;DUISITZKARSEE/OBERTAL- trip for children<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num6.png" style="width: 20px; height: 20px;" />&nbsp;DACHSTEIN GLACIER</p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num1.png" style="width: 20px; height: 20px;" />&nbsp;WATERFALLS WILDE WASSER - trip for children<br />\nDistance from Haus 15min by car to the parking place below the falls (name in origin HAUS-Schladming-Rohrmoos-Untertal). Hike around the waterfalls to the lake RIESACHSEE is suitable for families with children (length 1.9 km &ndash; 1,5 hours to get there).</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/hode1.png" class="full-width-img" style="width: 785px; height: 26px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/turistika/1.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/1.jpg" style="width: 175px; height: 116px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/11.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/11.jpg" style="width: 175px; height: 116px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/111.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/111.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/1111.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/1111.jpg" style="width: 175px; height: 116px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/11111.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/11111.jpg" style="width: 175px; height: 117px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num2.png" style="width: 20px; height: 20px;" />&nbsp;WATERFALLS WILDE WASSER - PREINTALERHÜTTE<br />\nDistance from Haus 15min by car to the parking place below the falls (HAUS-Schladming-Rohrmoos-Untertal). Hike goes around the waterfalls, to the lake RIESACHSEE and continues through KERSCHBAUERALM to the mountain hut PREINTALERHÜTTE (1661 masl; it is open from May to September, distance 5,7 km &ndash; 3hours to get there, vertical rise 662 meters)</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/hode2.png" class="full-width-img" style="width: 783px; height: 26px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/turistika/2.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/2.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/22.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/22.jpg" style="width: 175px; height: 233px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/222.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/222.jpg" style="width: 175px; height: 120px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/2222.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/2222.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/22222.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/22222.jpg" style="width: 175px; height: 117px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num3.png" style="width: 20px; height: 20px;" />&nbsp;Circuit around LAKE CONSTANCE - trip for children<br />\nDistance from Haus 10 min by car to the parking place below the lak (HAUS-Ruperting-Niederberg-SEEWIGTAL). Easy and very pleasant walk in the mountain landscape, the circuit around the lake is open year round! We recommend to visit the local restaurant (length of circuit 2,3 kilometers &ndash; 1 hour)</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/hode3.png" class="full-width-img" style="width: 783px; height: 25px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/turistika/3.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/3.jpg" style="width: 175px; height: 116px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/33.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/33.jpg" style="width: 175px; height: 105px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/333.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/333.jpg" style="width: 175px; height: 110px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/3333.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/333.jpg" style="width: 175px; height: 110px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/33333.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/33333.jpg" style="width: 175px; height: 117px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num4.png" style="width: 20px; height: 20px;" />&nbsp;STODERZINKEN 2048 masl<br />\nDistance from Haus 10min by car to the parking place in the village Assach. (Haus-Aich-Assach). By the forest path from the village Assach, trail No. 100; 679; 675; 676, we hike to the mountain hut STEINERHUTTE and to the wonderful place Stoderzinken with wonderful view 2048 masl (path is open from May to September, distance 8,9 km &ndash; 2,5 hours to get there, vertical rise 1369m)</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/hode4.png" class="full-width-img" style="width: 784px; height: 26px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/turistika/4.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/4.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/44.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/44.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/444.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/444.jpg" style="width: 175px; height: 98px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/4444.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/4444.jpg" style="width: 175px; height: 130px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/44444.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/44444.jpg" style="width: 175px; height: 131px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num5.png" style="width: 20px; height: 20px;" />&nbsp;DUISITZKARSEE/OBERTAL- trip for children<br />\nDistance from Haus 15min by car from the parking place by a nature trail No. 775 (HAUS-Schladming-Röhrmoos-Obertal). Initially the trail climbs steeply to the mountain hut SAGHUTTEN, continues to the wonderful views of the amazing lake DUISITZKARSEE. Trip is suitable for families with children. (totally 7,1 kilometers - 3 hours In total, vertical rise 608 m).</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/hode5.png" class="full-width-img" style="width: 783px; height: 26px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/turistika/5.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/5.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/55.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/55.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/555.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/555.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/5555.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/5555.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/55555.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/55555.jpg" style="width: 175px; height: 117px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num6.png" style="width: 20px; height: 20px;" />&nbsp;DACHSTEIN GLACIER&nbsp;<a href="http://www.derdachstein.at/dachstein/en/Home.html">http://www.derdachstein.at/dachstein/en/Home.html</a><br />\nDistance from Haus 15min by car to the parking place by the Dachstein cable car (HAUS-Schladming-RAMSAU).<br />\nDachstein Glacier is the most visited tourist destinations in Styria. A great experience is the funicular to the easternmost glacier in the Alps with an elevation of 1,000 meters where you will enjoy fantastic panoramic views of the surrounding mountains. Dachstein, it is a year-round paradise for skiers and especially for cross-country and also the starting point for hiking.<br />\nDachstein Sky Walk<br />\nThrilling - but safe! Underfoot: Hunerkogel, vertical cliff drops straight down. All around you is unparalleled mountain panorama. Welcome to the Dachstein Sky Walk, the most spectacular viewing platform in the Alps.<br />\nDachstein Ice Palace<br />\nVisit of Dachstein Ice Palace will take you deep into the heart of the Dachstein. Immerse yourself in the fascinating world of ice, light and sound. Let yourself be charmed by the shining purity of ice and mystical music.</p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/turistika/6.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/6.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/66.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/66.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/666.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/666.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/6666.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/6666.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/turistika/66666.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/turistika-male/66666.jpg" style="width: 175px; height: 117px;" /></a></p>\n', ''),
(40, 16, 4, 110, 'Summer', 'Nabídka v letních měsících', '', '<ul>\n	<li>velký obývací pokoj s kuchyní a rozkládací pohovkou pro 2 osoby, SAT TV</li>\n	<li>plně vybavená kuchyň, kávovar, myčka, trouba, mikrovlnná trouba</li>\n	<li>terasa 9m2 s posezením a výhledem na sjezdovku</li>\n	<li>velký obývací pokoj s kuchyní a rozkládací pohovkou pro 2 osoby, SAT TV</li>\n	<li>plně vybavená kuchyň, kávovar, myčka, trouba, mikrovlnná trouba</li>\n	<li>terasa 9m2 s posezením a výhledem na sjezdovku</li>\n</ul>\n\n<p><a class="special-offer" href="/pro-golfisty">Speciální nabídka pro golfisty</a> <!--<aside class="right fg4">\n					<img src="img/plan/apartment.jpg" alt="apartment">\n				</aside>--></p>\n\n<div class="shadow-corner-right">&nbsp;</div>\n<!-- end page content --><!-- track plans -->\n\n<section class="trackplans-widget">\n<h2>Sjezdovky</h2>\n\n<div class="wrapper row fg-no-gutter">\n<div class="item"><a href="./"><img alt="hp-01" src="media/img/plan/track-01-summer.jpg" /></a></div>\n\n<div class="item"><a href="./"><img alt="hp-02" src="media/img/plan/track-02-summer.jpg" /></a></div>\n</div>\n\n<div class="download row fg-no-gutter"><a class="map-dl" href="./">Turistické mapy ke stažení</a></div>\n</section>\n\n<div class="shadow-corner-right">&nbsp;</div>\n<!-- end track plans --><!-- costs -->\n\n<article class="triptips">\n<h2>Tipy na výlet, lázně</h2>\n\n<ul>\n	<li>informace, zajímavé turistické cíle</li>\n	<li>tipy na výlety</li>\n	<li>informace, zajímavé turistické cíle</li>\n	<li>tipy na výlety</li>\n</ul>\n</article>\n', ''),
(41, 22, 4, 111, 'Golf', 'Golf - map of famous targets', '', '<p><img alt="" src="http://alpineliving.cz/media/userfiles/golf/prehledova%20mapa%20golf.jpg" class="full-width-img" style="width: 900px; height: 437px;" /></p>\n\n<p>&nbsp;</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num1.png" style="width: 20px; height: 20px;" />&nbsp;Golf &amp; Country Club Dachstein Tauern<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num2.png" style="width: 20px; height: 20px;" />&nbsp;Golf Club Radstadt<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num3.png" style="width: 20px; height: 20px;" />&nbsp;Golf &amp; Country Club Schloss Pichlarn<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num4.png" style="width: 20px; height: 20px;" />&nbsp;Golf und Landclub ENNSTAL</p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num1.png" style="width: 20px; height: 20px;" />&nbsp;Golf &amp; Country Club Dachstein Tauern<br />\nChallenging 18-hole golf ground designed by Bernhard Langer. The ground is called &quot;pebble beach of Alps&quot;. Distance from apartments only 1km. <a href="http://www.schladming-golf.at/">http://www.schladming-golf.at/</a></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/golf/1.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/1.jpg" style="width: 175px; height: 93px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/11.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/11.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/111.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/111.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/1111.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/1111.jpg" style="width: 175px; height: 115px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/11111.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/11111.jpg" style="width: 175px; height: 131px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num2.png" style="width: 20px; height: 20px;" />&nbsp;Golf Club Radstadt<br />\nDistance from Haus 25 km to the village RADSTADT. (HAUS-Schladming-RADSTADT). In RADSTADT we continue to Obertauern and behind supermarket BILLA you are by the course. 18-hole championship golf ground, regularly awarded as the best ground in Austria, with technical rarity - JET BIRDIE cable car that will take you from hole No. 11 to hole No. 12 - one of the highest holes located in Austria! <a href="http://www.radstadtgolf.at/">http://www.radstadtgolf.at/</a></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/golf/2.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/2.jpg" style="width: 175px; height: 116px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/22.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/22.jpg" style="width: 175px; height: 116px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/222.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/222.jpg" style="width: 175px; height: 115px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/2222.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/2222.jpg" style="width: 175px; height: 116px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/22222.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/22222.jpg" style="width: 175px; height: 131px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num3.png" style="width: 20px; height: 20px;" />&nbsp;Golf &amp; Country Club Schloss Pichlarn<br />\nBeautiful, gently located in nature, 18-hole ground, from our apartments 33 km (HAUS, direction Liezen, at the intersection Trautenfels right, direction Donnersbach). Every year you can compete with players of Real Madrid in their summer training! <a href="http://www.golfpichlarn.at/">http://www.golfpichlarn.at/</a></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/golf/3.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/3.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/33.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/33.jpg" style="width: 175px; height: 88px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/333.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/333.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/3333.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/3333.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/33333.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/33333.jpg" style="width: 175px; height: 116px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num4.png" style="width: 20px; height: 20px;" />&nbsp;Golf und Landclub ENNSTAL<br />\nDistance from Haus 37 km (Liezen direction, exit at the village of Weissenbach). 18-hole flat ground, surrounded by stunning alpine scenery and very extensive flora and fauna. <a href="http://www.glcennstal.at/">http://www.glcennstal.at/</a></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/golf/4.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/4.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/44.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/44.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/444.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/444.jpg" style="width: 175px; height: 86px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/4444.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/4444.jpg" style="width: 175px; height: 120px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/golf/44444.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/golf-male/44444.jpg" style="width: 175px; height: 116px;" /></a></p>\n', '');
INSERT INTO `page_data` (`id`, `page_id`, `language_id`, `route_id`, `nazev`, `nadpis`, `uvodni_popis`, `popis`, `url`) VALUES
(42, 23, 4, 112, 'Cycling', 'Bike - overview map - cycling / mtb', '', '<p><img alt="" class="full-width-img" src="http://www.alpineliving.cz/media/userfiles/Obrazky-mix/mtb.jpg" style="width: 899px; height: 437px;" /></p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num1.png" style="width: 20px; height: 20px;" /> CIRCUIT Kaibling - orange color (Map No. 06)<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num2.png" style="width: 20px; height: 20px;" />&nbsp;STORM CIRCUIT ALM - blue color (Map No. 07)<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num3.png" style="width: 20px; height: 20px;" />&nbsp;CIRCUIT Viehberg (Map No. 12)<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num4.png" style="width: 20px; height: 20px;" />&nbsp;Relaxing Untertal - green color (Map No. 18)<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num5.png" style="width: 20px; height: 20px;" />&nbsp;Relaxing Röhrmoos - violet color (Map No. 19)<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num6.png" style="width: 20px; height: 20px;" />&nbsp;CIRCUIT AICH - blue color (Map No. 09)<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num7.png" style="width: 20px; height: 20px;" />&nbsp;CIRCUIT STRUBSCHLUCHT &ndash; red color (Map No. 10)<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num8.png" style="width: 20px; height: 20px;" />&nbsp;CIRCUIT REITERALM - orange color (Map No. 03)<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num9.png" style="width: 20px; height: 20px;" />&nbsp;CIRCUIT Röhrmoos - blue color (Map No. 04)<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num10.png" style="width: 20px; height: 20px;" />&nbsp;PANORAMIC CIRCUIT (Map No. 05)<br />\n<img alt="" height="22" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCAFoAWgDAREAAhEBAxEB/8QAHQABAAIDAQEBAQAAAAAAAAAAAAgJBQYHBAIDAf/EAGMQAAEDAgMBBgsRCgsHAwUAAAIAAwQFBgEHEggTFSIyUlQJERQ3QUJidZOysxcYISMxNTZWcnN0gpKUlaLSFjM4Q1FVV2HC0xkkNFNjcXaBkaG0g6OkscPi8ETB0SZl4ePx/8QAHQEBAAEFAQEBAAAAAAAAAAAAAAYCAwQFBwgBCf/EAEkRAAIBAgIGBQgGCQIFBQEAAAACAwQFEhMBBiIyUnIRFDRCUxUWMTNikqKyBxcjNVTSITZDVXOCk8LwJOIlQVGDoWFkcbHBgf/aAAwDAQACEQMRAD8As7pNHgb1Q/4lG+8B+KHkoD17zweZRvBCgP7vTB5lG8EKA+d6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQDemBzKN4IUA3pgcyjeCFAN6YHMo3ghQH93ng8yjeCFAN54PMo3ghQH9pHrVD94DxcEByPaH2iaVkXRB4AVG4Zg49RwMC6WHS5Z8kf+axZplhUjd5vMdqj4pG9GggddO1Lmddrr2Mi6JcNky/k8HSwGA8ngrT6amVu8clqNYblUN6zDymtebJfPttrHztxW8x+I1/lOt8Zj+ebFfPttrHztxMx+IeU63xmHmxXz7bax87cTMfiHlOt8Zh5sV8+22sfO3EzH4irynW+Mw82K+fbbWPnbiZj8Q8p1vjMPNivn221j524mY/EU+U63xmHmxXz7bax87cTMfiHlOt8Zh5sV8+22r/ADtxMx+IeU63xm94ebFfPttq/wA7cTOkHlOt8ZveHmxXz7bav87cTOkHlOt8ZveHmxXz7bav87cTOkHlOt8ZveHmxXz7bav87cTOkHlOt8ZveHmxXz7bav8AO3EzpB5TrfGb3h5sV8+22r/O3EzpB5TrfGb3h5sV8+22r/O3EzpB5TrfGb3h5sV8+22r/O3EzpB5TrfGYebJfPttq3ztxM6Qq8p1vjMPNkvn221b524mbIPKdb4zDzZL59ttW+duJnSDynW+Mw82S+fbbVvnbiZsg8p1vjMPNkvn221b524mbIPKdb4zDzZL59ttW+duJnSDynW+Mw82K+fbbV/nbiZ0g8p1vjMPNivn221f524mdIU+U63xmHmxXz7bav8AO3EzpB5TrfGb3h5sV8+22r/O3EzpB5TrfGb3h5sV8+22r/O3EzpB5TrfGb3h5sV8+22r/O3EzpB5TrfGb3h5sV8+22r/ADtxM6QeU63xm94ebFfPttq/ztxM6QeU63xm94ebFfPttq/ztxM6QeU63xm94ebFfPttrHztxMx+IeU63xmHmxXz7bax87cTMfiHlOt8Zh5sV8+22sfO3EzH4h5TrfGYebFfPttrHztxMx+IeU63xmHmxXz7bax87cTMfiHlOt8Zh5sV8+22sfO3EzH4h5TrfGYebFfPttrHztxMx+IeU63xmHmxXz7bax87cTMfiHlOt8ZveNhtjaezMtR5ko10zJDYY9PqeYW7gXc6SVxKiVe8Z1PrBcKdtmTETo2bNpWm54UtyJLbbptzRQ1SIYljodH+cb6fY/V2FuIKhZuY6vZb3HdUw6dmRe6d2WUSk8dI9aofvAeLggKstqe7HbvzzuiQb2LzMZ/qJjUXBFtvg8H6xfGUdqNOKVjgWsNQ1RcpPZ2TlCxiOBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAdAyAut2zM4bVqQPYtNYzAZe0lxm3C0kP1lfp2wyKbqzT6ae4RP7RbgpIehjx0j1qh+8B4uCAqKzk6612d8n/AChKMyb7HnG6dtm5jUFaNaEAQBAEAQBAfbLLshzQ0BuOF2ojqJCpdDNum92xkBmLeQi5SbRqL7JDqF51rcWy+M5pFX1hkbum3gs1wqdpImNcu6ya9YdUKn1+kyaTMH8XIb06vclxS+Krbo0eyxgVNJPRyZc64TCKgxQgCAIAgCAIAgCAIAgCAIAgCAIDN2Xa797XRT6HHeCO9Mc3MXHOKPB1fsrVXW4La6OasdcSxriNlbaJrlVx0iNhaRsJuFSyttSj1CVAm5uWdDnRXSYfjvThFxoxLSQkOrgkJKGR62V8qK6WqVlb2TpWn6PZ9DdHWYzy+Z/ZP6ZLI+kB+0q/Oi5fuif3R9Xs34uMeZ/ZP6ZLI+kB+0nnRcv3RP7o+r2b8XGPM/sn9MlkfSA/aTzouX7on90fV7N+LjHmf2T+mSyPpAftJ50XL90T+6Pq9m/FxjzP7J/TJZH0gP2k86Ll+6J/dH1ezfi4x5n9k/pksj6QH7SedFy/dE/uj6vZvxcY8z+yf0yWR9ID9pPOi5fuif3R9Xs34uMeZ/ZP6ZLI+kB+0nnRcv3RP7o+r2b8XGPM/sn9MlkfSA/aTzouX7on90fV7N+LjHmf2T+mSyPpAftJ50XL90T+6Pq9m/FxjzP7J/TJZH0gP2l986Ll+6p/dH1ezfi4zw5gZbjY9NoNRj1yn3BT6w265GlU0tTZCGnhau24y29j1h8tSTRNTtDJFvKxEb/q7NYsvMkWTHwmmKWkRMzZPs1t/vhG8oKrTeUzKPtMfMXKj2FKD0kvoPLSPWqH7wHi4IVFRWcXXWuvvk/46jMm+x5xufbZuZjUFaNaEAQBAEAQEsdk/Zvta97KqN63u05IgMukLDO6k2zubY6nHC08Iv8AtWzpqdWXMkOiavWSmqqZqqr3TdD2pMkMsAJqz7Q6teDg4ORITbOBF745wle6zFHuqbRr7aKLR/pocX8po91dELu2oagoNv0yjNcuSRSnP+nh9VWWrm7qmoqNcqlvURqvxH71nbEtfNDLypUi/wCzwerAsF1JIg4CTZO6eCQ6uE18ok01ayR4ZFLsusdNXUjRV0O13f8AO6RLWsOchAEB6YtLmzm9ceG/IHi6mWiLxVWuhi8kMsi4lUyTNi3LIHU1b1VcHuYTpfspgbhLy0dS37NvdPYzlfeUgtIWpWyLve79lVLHJwlS26tbdhb3TF161aza7wtVikzKW4XFGYwTer3OrjKllZd4szU09P61WUxioMcIAgCAIAgCAIAgN/yD68Fr/CS8mSh2uP3BV8pKtVfv2m5jr+RuS9i39JzIqVx2pS61UPu1q7XVE1gXD0jJLSK4zrfrDdbVJRQUNS0a5KbKnqqkgik0SM6946f52HKb9H9A+ZCudeeusX42T3jY9Ug8Medhym/R/QPmQp566xfjZPeHVIPDHnYcpv0f0D5kKeeusX42T3h1SDwx52HKb9H9A+ZCnnrrF+Nk94dUg8Medhym/R/QPmQp566xfjZPeHVIPDHnYcpv0f0D5kKeeusX42T3h1SDwx52HKb9H9A+ZCnnrrF+Nk94dUg8Medhym/R/QPmQp566xfjZPeHVIPDHnYcpv0f0D5kKeeusX42T3h1SDwx52HKb9H9A+ZCnnrrF+Nk94dUg8Mxt1bM+VUW16u+xYNCbeahPOA4MQdQkLZcJbS2a4awTV0MclbJ0My94sSUcGBtki/XPwdcle98ryja9B2P9YrvzL8pwf6QfVUnKxoi6KcZMzZPs1t/vhG8oKrTeUzKPtMfMXKj2FKD0kvoPLSPWqH7wHi4IVFRWcXXWuvvk/46jMm+x5xufbZuZjUFaNaEAQBAEAQFmLGWFQqeyhAs60p8WNOm0pocZTham3N00m9hqHlaiHp90pBgbKwqd20UDtZ1pKZvSpAfMrJO8co3G/ujo7kSK6e5NTGiFyO6XJEh7bglwS4S00kMke8cgr7VV2/167JoysGoCAIAgCAmbkztr2nZ1j0O3qzbUiKVNjBGJ+BgBi5pH75px08IuMXdEtrFVxqqqx1C260UlPBHBLHhwnfrO2q8sLzkMw4lxswpjpYCDFQAmMSx/Jqxw0/5rNSpjb9Ckupr9b6psCSbXtbJ2PHHAMOxgskkREbbyzFtobIatTdY824nZIPi02Wo4gD6pFydXF6X61rqx1w4TnuttZT9W6t+0IFLSnIQgCAIAgCAIAgCA3/IPrwWv8JLyZKHa4/cFXykq1V+/abmJJ7MH8izK/t1WP8AUEvOX0gdpov4EZ6yod2TmY6jeV6UTL+3ZleuCos0ulRB1HIeLg9yI8oi5KgVttlVdqhaSjTE7GbJJoj0YmId310TKlQZhMWlaj1TZH/1FSe3DV7kR1Lvts+hyokTFcKjC3Cq4jRyXdV3FMxlT0R62rmqTcC8qQdsk6QiM5k93jj752wj3SwL39EVbSRtNbpc3D3W2WK4bskjYXXCS+p9Si1iFHmwpDcyG+AuNSGHBMDEuyJCuCz08lLJpimXCy93Sb1W0No6dB6FiF4IAgOBZ7bZVk5KyXqXqxr9xAPCp8Ex0sFyXXO19zxl1jVr6O7nrAuidvsouJu9ymmqbhFT7PpY4JTeifSt9P4/YzI0/V/6eYW7CPxh0rp830MxZP2VW2Z7S7JrtF54lJT5I7R1l58QXioEw2akwOp+lzODIAeVp7Ye6FcT1j1QuerTaOtrijbdZd03FPWJU/pQ3m8vYjW/gD/kyUbtPb6fmX5jKk3GIK1z8HXJXvfK8o2vW1j/AFiu/Mvynnr6QfVUnKxoi6KcZMzZPs1t/vhG8oKrTeUzKPtMfMXKj2FKD0kvoPLSPWqH7wHi4IVFRmcfXWuzvk/5RRmTfY86XPts3MxpytGqCAIAgCAICSWw2dzVPN6NEg1eZHoUGO7LnQsHi3Ex06BHTxdWsh+SSz6PpzMJONVNNVJWYVfYXeJC7YWUF75zNW9TbYZiOU6ETsiRjIk4N6nS0iPyR1fKWfUxSTbKkz1jttXdFjhp93RvEZy2GM2BL1tgF3Q1Bta7qcpBvNO5cK+8fzzjObH5sgfSDadTlKfNS5cPxH5ubD2bI49LCiwz7rCoNfaTqko807lwr7w84/m5+Yon0gx9pOqyjzUunCvvHnkbE+bzGA6LbZe96qUbg/KcFOqy8Jb06q3Rf2fxKfl5y3OP2pj9JRP3y+dWk4SnzXu3h/Ev5jrOzPsfXHRMwma7flKCDCpg4PxIvVLTvVD/AGurQRcEeN7rT3SyqekZWxSEiserc0FTnVq/oXd5jC7dWcMqq31DtClSjZhUYd0kuMOYjrkuDxfQ5IdL+8iVFZLtZamPrXc3adaSFt3e5iKhEREREWoi4RES1hzvS2JsTH8QBAEAQBAEAQBAEBv+QfXgtf4SXkyUO1x+4KvlJVqr9+03MST2YP5FmV/bqsf6gl5y+kDtNF/AjPWNDuyczESOiO5mT6xmdT7MB1xuk0eM3JcZ7V2S6OrdPihpEfjcpdu+iS0RU9re5No+0kbD/Kpp7rKzPg7qkQV3s0IPsoCR2yLtWTMkK+3RK5IelWXMc0uNkWrqIy/Gt9zyhXIte9SItY6brVIuGpX4vZY29FW9XbC26WkUuqRK3To9Qp8puZBkhg61IZLUBiXbCS8VVNLLSStBOuFlJiraG0dOg9SxC8Q92zNsQLBZkWTZMzBy5DEgn1BktXUGHIEv53xV6F+j7UBrmy3S5r9j3V4v9pHK6uytiPeK43HjkPOOuni44ZEROOFqIi5RL1sqLGuFd0irMfKqPhs2WuYFUyvvqj3NSHSbmU98XNOrSLods2XckPBWmu9sgvFDLRzriVlL8MjQyKylzlyTGqhY1TlMFqZfprrrZdzi0RCvz5t8WmG6wxt3ZF+YnjtiibSQbrn4OuSve+V5Rter7H+sV35l+U8+/SD6qk5WNEXRTjJmbJ9mtv8AfCN5QVWm8pmUfaY+YuVHsKUHpJfQeWketUP3gPFwQqKis4uutdffJ/x1GZN9jzjc+2zczGoK0a0IAgCAIAgJ/dD9sjCjZf1e5Hg0vVWTuIY/0TX/AHES3dCmGPEdf1Qpcukafi/tMFmBt7VC0b0rNEiWjGlR6fJOMLz0smyc0lp1adPBVt63C2HCWKzW1qWoaDRDu+0YIeiOVfEuFZEMh7moGP8A01T11uEw/PWT8P8AF/tP2HokE/VwrDjEPc1Qv3Kdd08I89W8D4v9p+n8JDK9oLP0rj+5Tr3slXns34f4v9p+g9EhcwIenYA6e201j/8AQquu+yVee3/t/i/2nq/hHmfaM79JYfu0677JX56R+F8RtuV22rjmtfFMtqnWRIaelnjicjGaJCwGHGMuB2uCuxVWY2FVNnb9ZvKFSsCRbxKbUs8nRV5teZZSsvM4ak+W6O02tEVQiSHNRatRemN6uUJfVIeUo/UxYZDhWslDpo65m7sm0cTWIRQIAgCAIAgCAIAgCA3/ACD68Fr/AAkvJkodrj9wVfKSrVX79puYknswfyLMr+3VY/1BLzl9IHaaL+BGesqHdk5mIzbVWzHf+bmdF7XRQqXg5S4caMLROHpKUQMNiTbQ9sXGXXtTdcrRZLNSUNTJ9o3w7XeNRWUcs0rOpCd5l2LIcYfaNl5oibcbcHSQkPakvQ6Osi5kbbJoGU27K/KurZuVaqUujG3vhDprtSbZc4O7i2Q6mxLlaS1fFWlvN6prHBHPV+rZlXlxGRDC0zYVNNJshIhIeEPBJbzFiXEYxYF0NStXHOo9yQpFXZkW9CIOp6e4Wp5p4uMQ8kP2l5c+mGmoo5oJUhwytvN3WX8xJ7U7ti0M2ySe2gKjXqPk9dU62ahGpdYYhE41KlFpEBHjaS7UtOrT3S4zqpDSVN6po65MUbNu6DcVOllibShTHKmPzpT0iQ6b0h1wnHHHC1EZFxiJfoPFFHHGsca4VUgfSzNtG3M5U1YspZWYLuIM0VuoN0tofxj7pCRFp7kdP1loXvcC3ZbQvrGVm5VL2iBmizTS9SkRjHWMsdmW/M2rTqlx29SeqKfD4Le6FuZSi7YWuVpUHvGuFosdXHRVcmGRvh5jNho5ZlxKWrxxdHJtsX2jZe3hHW24PCAupuEJLxIunRpv3Svi/wBxNP2P8pDCufg65K975XlG16jsf6xXfmX5Tz/9IPqqTlY0RdFOMmZsn2a2/wB8I3lBVabymZR9pj5i5UewpQekl9B5aR61Q/eA8XBCoqKzi661198n/HUZk32PONz7bNzMagrRrQgCAIDJ0m16zXj002kT6gRFp0xYzj3iiq9Ghm3VMiKmnm9XGzGUmZV3pBJsZVpV2KRlpHqimut6vlCqst+EyvJtbutC3ulpth0On5PZUUimTZTUSHR4I4SZDhaQEvVcLp+6xxUiRVjTCd6pYo7fSrG2yqqct2idlij5zRca9Q9wgXViHTGUPoMzB9UcHNPZ7v8A/HSxaimWZcSmivVhiua5sWy/zFeV1WrV7Krsqj1yA9TajGLS7HeHSXuu6Huh4JLS6dDRthY4zU00tHJlTrhYxSoMYIAgCAnxsH5OuW/a0q96lHxbnVfDcoIuYcIYw/jPjl/kOHKW5o48K42OwapW3q8HW5N5t3lNXz12qZVubRNEZpUoioVuPExPZZMtMonOC8JaeNpHi8khVM1ThlXCYN1v7Q3SNY22I97+47ltHZUtZ85TiNI0yao1pnUx3Vp19MeL0+SQl2VlTR5ybJKLzQLdqL7Le3lIy2t0Pm8agAuVus02j4Y/i2tUhwfFFa9aJ23iC02p9S/r5FU2ST0OedgwW4XkwT3a7pELT4yudR9ozm1L4Zjld+7FuZFkxSlx4LVyRR4Rb0kRvD/seMXxdSx3pJFI9War3Cl0Y1XMX2ThUiO7FecYfaNl4C0k24OkhLuhWERRlZWwsfCFIQBAEAQBAb/kH14LX+El5MlDtcfuCr5SVaq/ftNzEk9mD+RZlf26rH+oJecvpA7TRfwIz1lQ7snMx2vX+tcrNoRD2xtjkMyGZl6WXDBu6gHdJlPZHSNREe2H+l8b3S9Aaga/6bUy2y5t9j3W4f8Ab8poq6hztuLeOU9DzyjuWPfFUveZCODQ40J+n/xpshcfdIh1CI9zp4RfFU6+lK/0LUUdrjbFKzK3KpgWyB1kaViI92cK6q1wdP8AHn/KEu60PZIeVTSyb7GYyrzUr+Tt4Rbit6YUeU1wXWS+9yA7Ztwe2ElgXqy0d9o2o6xcSt8PtKVQzPTtiQ6xtMbYFbz6jxaRCYcodssiDj8EXNRSH+U4XbCJcUfjKD6oag0urDNUSfaSt3uFTNqq56jZ3VI76V1U1ZNS08s6zm1sBx6da8YJFQp9ddnPReKUgWxISFvlF6YPydK88V13p7Jr+0tc2FZI1VW4cRIEj01FDhQ0XZV2O6pnNUN/blYfpNoxHdBboJNvTTEuE23yR5RfF9zJdd9fqbV+Hq1E2ZO3w+0Y9FQtUNik3Szyi0aBbtIh0umw2YFOhti1HjMhpbAB4oiK8Z1dXNXTNUTtidvTpJciaF0dCnjvL2I1v4A/5MlkWnt9PzL8wk3GIK1z8HXJXvfK8o2vW1j/AFiu/Mvynnr6QfVUnKxoi6KcZMzZPs1t/vhG8oKrTeUzKPtMfMXKj2FKD0kvoPLSPWqH7wHi4IVFRmcfXWuzvk/5RRmTfY86XPts3MxpytGqCA27LHKyv5uXK1RaBHwcdx4T0hzULTDfKMldiiaRsKmyt9vnuUuVET6yp2MbHy8jNvVhgLnqw8IpM1vDBkS7lvi/K1LdR0kcZ1236tUdFoxS/aN7Rv1czqy3y7ZGLLuSk00WuCMWOY46f6gBXmlSP0sbmW5W+jXC0iqfVjZ+2JmRVypVu19moVHASPcNzNssRHjFhqFEmSRsKnyku1FXPlwSYmNnvOz6ZftsVCgVZrF6nTg0PNgWnH1dXq/14K46Ky4WM6ppo6qJoJd1jgUL7qNk59pibJk3Zla4Wnqk8NUukFjyh7Zr/wA4PbYejFT8pFk6zYWwyNmQfEv+03jNTJu0dpKz48oH2cZBta4Fdh6TIeT0+UPc/wDJXpIkmU2VwttLeoMXusVx5nZV3DlLcbtHr8QmTw4TMhvhNSG+UBLQyxNG2Fjitwt89tlypVNRVo1YQG7ZM5cyM1Mx6PbzOGODD72qS4PaMDwjL5KvxJmSKptrVRNcKuOAsezxzGhZFZPyZcIWmJDLAwaVH7XdNOlv0PyDhhq+Kt5M+TGdrudYlpoWZeVSEOzts3VfPmtPVaqvPRLcBzE5VQLhOynO2bDV23KJaeGnaZsTHLbLZJbvJny7KfMWP21QafZ1BptEp+G4QobODEdsj6ZaRw/zW+0LhXCdphhSnjWKPdU5PtIbRcjIOPSyats63vhgYhIKTuTTRD2C4JY4rHnnye6R+9XnySq/Z4sRwWL0Rmsi+3jIsyE4z0/TMG5hiXS7nHSSw+veyRFddHxbUJ2rLXbSy/zCkNwpLsi2ai5jpFmqYCLZl3Lo46flaVlRVUcn6CUUOstDWNhxZbe0ZbPTZktjOqC7MxEaXcOj0iqMYeieOng4OD2w/wCaqmgWZTIutjpromLTsycRWve1l1fL25p1BrkXGLUIrmlwe1LkkJdsJLQujRthY4fVUstHK0Eq7SmEVBihAEAQBAb/AJB9eC1/hJeTJQ7XH7gq+UlWqv37TcxJPZg/kmZX9uKz5cl5y+kHtFF/AjPWVD6H5jta5WbcID5babbb0C3gI+iWkRVel2bTi0sW+grT25dmQcsa4V7282A25VX9MiKPGhyS4XB/oy+qS9j/AEa64teafyXVetjXe4l/MRG40mU2am6RNXczRjR+pAb1krlLUs6swqbbFNPCPu5bo/KLisNDxnNPbe5UZ1hvkGrtvkrpdrDurxMZMELVEmWpcBlvl3Rsq7OpttUGN1PT4YaeFxnS7ZwuURLwNeLtU3utkrattpv/AB/6E5hiWFMCmyNtiyOgBwbHkiK0bPpbTiYyOg+lSXDD3l7Ea38Af8mS29p7fT8y/MY8m4xBWufg65K975XlG162sf6xXfmX5Tz19IPqqTlY0RdFOMmZsn2a2/3wjeUFVpvKZlH2mPmLlR7ClB6SX0HlpHrVD94DxcEKiorOLrrXX3yf8dRmTfY843Pts3MxqCtGtPfb9BmXRXINJpzRPTpz4sNN8oiLSq9GhmbCpehhkqJVij3mLR8rsvLb2ccsMW3nGWAiMYyapUXMOE6fbFj+rsDgpAiLDGd7oKOnstJy7zEKs+9rm5MzalNptBlvUS1OE2DTXpb0ofymXG4XJWqmqWk2VOYXjWSeuZooGwxkfSLUWouESwSGsTH6HbZoyKzdFzOh08IzQQ2i7o+EX1RH5S2tCm8x0zUymxNJU/ymQ2p9pG48t87qZGtudpj0yIPVkFzHUxIJzHVpPD3On0e1VVRUNHJsmRf75PQ16x07bu8SGyZzkoGftmHLjNg3IAdxqFMe4WLRF4wl2CWbFKsykvttygu8GNf5lI8XdW61sW5nCdNFyo5bV14nhprmOOPU5fjBbLtSHteUPG5SwnZqVvZIhUyy6s1eKPagfu8JISXBsTaky34zdUpUnD0t4OkL8R3pfUMVmfZ1CkvZaO+UnErf+CJrWxhAtq7p0C9Llco9DPRvVVWRHRIIi07m5q4p8XgrX9Uwt9oxz/RqvHDOy1cmFO6xom0vkhbOSUumwaRcb1XqTxF1XFkCIkwOkdJaR5Ss1EKw7ppr5aqa16VSKTExJvYXydws6yX7vqDGA1auiIsavVaiDxflFwsfcis+kjwriYnWqtt6rTdZfek+U4/tI3PN2h9oKl2BQXscafAk9RA5jxN1/wDUO+5ERIfilyliVDZ8yxqR28zNebpHQwbq7P5iZ7zluZF5ZEeGAU63qHF9TtsftGRY/wB5EttswodLbTBbKThjVSvk9qu56jnTTb0myXMYEOSQtUsMSFluMXBJvAeVp7blLS9ZbNxnIfOGdrgtW7bK932Sb+elgxM+slZTVLJuQ+/GCo0qR2Cc06h+UPTH4y2syZ0eydOutIt2oGVOZSrN5k47zjToYsvNkQuNuDpIS5JKPdBwJlZWwsfC+AmFsW7SMtisxsv7kmHIiSvQpcqQfCac/miIu1Lte64PbLa0k/7NjpmrN7bH1Gpbe3W/tOi7dGUMe6rCG8YTABWKHh6e4PquxMeMJcrSXSIfjcpXqyPEmNTda1W1aqm6yu9H8pXwtIcZCAIAgCA3/IPrwWv8JLyZKHa4/cFXykq1V+/abmJJ7MH8kzK/txWfLkvOX0g9oov4EZ6yofQ/MdrXKzbhAajmhmlb2UNpyq9cc4IkRofS29Xpj58hse2JSWy2Stv1WtJRR4m/8L/8mJPOlOuZIVQ7QG0JX8/rscqNSdOLR2CIafSWy9LjN8rujLti/ZXuPVbVaj1Yo1hgXFI283eYhFVVPUv06d05WpqYYQGStu5qpZtch1miTXqbVIrguMSGS4QF/wCdqsOso4LhA1NUrijbulaO0bYlLSNlna2pee1NapFU3Gm3lGa1PRenpblaeM414xD2q8Wa6ahVOrkmmpptqmbvcPssTCirlqNlt4kMfZXITdaAh9MPeXsRrfwB/wAmS29p7fT8y/MY8m4xBWufg65K975XlG162sf6xXfmX5Tz19IPqqTlY0RdFOMmZsn2a2/3wjeUFVpvKZlH2mPmLlR7ClB6SX0HlpHrVD94DxcEKiorOLrrXX3yf8dRmTfY843Pts3MxqCtGtJQbAdiM3DmdUrhkt62aFFEmdX8+7qES+SLn1VsaNMUmIn2p9KstW07dxfmN66ILmc7CbotjQnja6pb3wnafUINRCyPyhMviir1bL+z0G21wr2jWOiTvbTEJVqDlYQFnmyHZmFg5CUc5A7hIqW6VWTg52N04v8AuxBSGmTLiO76u0vVLbHi3m2iuzNS8McwMxriuHp46ahNcdZ1cbc9Wlv6oitHI+KRmOMXCp65VyS8TG47L2Z72V2bdJk4uY4UyoGMCaOPbNuFwS+KWklcgly5DZWCva31q8LbLE5Nr6w2L4yNrj2A4dV0dvfNhzpcXc+E5/u9S3NSmZGdV1io1q7fJ/1XaK+8oc47gyauUKpRJHTZdxEJcFz71IDkkPK5JdqtJDM0O6cftlzntkuOJtksZy7zKs3aZsKS0INS23Q3KfR5X31kv/jkuD/zwW8R0qFO1UdfS3qm2f5lMdmrs4WXmbXKTUrgwqj05psIIvRMOmT2nUQ4ukLZdLtuFj0h4X618lp45GxMWq6y0dwkWSfF0qbLnJU6lYmTVckWtBJ2bBg7nGZY4zIadOofcjwv7lXJiWPZMu5PJS0MjU67SqRE2ALbOfmhWq3Kac1w4OOAOODxicLhLW0a4mxaTnmqMDSVck78JvvRCbrmjQ7ateHuhMy3jnS228C4Qt8FvVp7XURF7oR/Ir1c+zh0G21wqHyo6aPvbRCFmiz5JaGoElwuSLJEtThY5Top5WbZVieWwhe9SO0ptm1eHKjlTTxkwnZDJNjiyZYkTY6uSWOr4y3NI7Yctjr2qtTNkNSzrp2fQcR20clJVi37LuenQTxt2rHuzjzQ+lsSS4wFp4urjd101g1UOmPTi0bpFtZ7U9LU9ZjXYb5iOCwSDH70+oSKTUIs2G6ceVGeF1p5stJAYlqEhJfV04WxFyJ2jdXXeUtwtmdFzSymgPvmMiPWqUIvFp9XE29Jeh/X01JtH2iHoqJlrqNW095So+oRxizpDA8Vpwmx+KSjbbx51l0ZcjKfgqC2EAQBAb/kH14LX+El5MlDtcfuCr5SVaq/ftNzEk9mD+SZlf24rPlyXnL6Qe0UX8CM9ZUPofmO1rlZtzRM4s47byRtJ6u3DLwb7WNDbIeqJTnIbH9rtVLNX9Xa3WOr0U1EvM3dUwaioSnTE5U9nhnlcGe14OVmtu4txW9TcKntl6XFb5I91yiXuPVvVqj1Zo+rUy7XebvMQqpqZKiTExztS4xDc8qMorkznuuPQbchHIcMh3eUX3mK32zjhdqPjKO3y/UOr9I1XWyYeFe83sqZEEDz6ehFOl7SWyDcOQoR6lHN24LZcERcqTLWnqZ3kuD2okXFJRHVLXyh1nxQN9nKvdbvL7JmVVC9NtbynAV1A1Z7KPWJtt1aHVKXKcg1CI6LrEhktLgGPFISWPU00VZE0E64lbZYr0acLYizbZO2xIWdDLVt3ITdNvBoOAXFaniPbN8k+UPye58ca86gS6vtprqLap2+H/aS6jrtFRsPvEnlxE3ph7y9iNb+AP8AkyW3tPb6fmX5jHk3GIK1z8HXJXvfK8o2vW1j/WK78y/KeevpB9VScrGiLopxkzNk+zW3++EbygqtN5TMo+0x8xcqPYUoPSS+g8tI9aofvAeLghUVFZxdda6++T/jqMyb7HnG59tm5mNQVo1pO7oc8cQs+7X9OGop7TertuC3/wBy3NFusdc1M0f6aVvaOC7a816XtFXCDhYk1GZitMjj2o7g2XjESw6v1pD9aWZrnIun2ThawiJnUNmeyKLmHnFRKNXjwwpx63MWCLTu5COoQ+MsmnRZJMLEgsNLFWVyJPuk+9p68o+XORtfJgxjSJMbe6G2JaeEeGn0Pcj0/wDBbqofLjOuXyoWht8mleHCpVoo4cDP624TbgmJaSEtQoVLpwsrFwdysMVHLWqNTAFyO9SXReHH1MRJnHUpPp3D0dNtUbYuH+0p7UYPN7HfNiCoMws+oDb7+LO7xH2wHp6RM9PBFZtG32pMNVHVbguJicA592/AzDnWdXjxoFWbfFuLjM4LU1svvZtn6nc9LlLb5y4sLHVdF1gWpakn2W7vtG8lX6c7FqbrzwBFgETcs3h6QBpHUXT6fq4dIlexaDa6ZI8LM26phLIzLsq9XZEK1a3T6k5HDA3WoJYY6MOnp1KhHRt0xKWupKr9FNIrYT1XhmPatg4R/ukrkKj7v09y6rc06/6l9kdV3iqprKal9e2hTVZG0/lXFEsfu0pZ6f5tzUrPWIuIwNN7tq/tlMeG1xlQ6+21hdkfAjx06iacER91jp9Bfesx8Ra84bbiw5pou07tCZfTco61RYdYiV2fVY24xo8P03SRcVzHk6eN/crNRMmDCam+3ei00UkKviZiu9aI4wF8YoLYtnGM7ByJslmQ0TLwU1vU24PSxw7Kk0Pq1PRVp2bfD08JVhdEoZ9zVaQDQR23Zbrgst8UNThcEVHX3jgVU2KeRvaMYqDFCAIAgN/yD68Fr/CS8mSh2uP3BV8pKtVfv2m5iSezB/JMyv7cVny5Lzl9IPaKL+BGesqH0PzGy54Z7W3kTarlVrknApR4F1FTW8fTpR8ke55RdqtBq3qxXazVeRTLs95u6pfqalKZcTFTeb2cVx52XhIr1wyt0IuCxFb+8xQ7Vtsf2u2XuSw2Ci1dolpKJeZu83tEKnnaobFIaSpIYx0zInIG48+rsbptIaxj01oh6uqjjfpMdv8AaLkiodrPrRR6tUrT1Lfad1e8xm01M9Q3Qpazk7kzbeSNqM0K3o2Ijxn5jvCelHyzL/wV4fv+sVZrJVtVVbcq/wDJSZwQJTphQ3GqUuJWabJgT47cyHJAmnY7w6hMS7UhUepaiWklWeFsLKZDaNDaOjSVt7WmxdKyxkSLpsth6daePpkmHxnIBftB3Xar17qL9IcV50LQXRsM/dbi/wBxE623tFtx7pE1d1NGein1CVSZ0ebCfdizGHBcaeZLSQEPbCSszQx1EbRSriVj6ull2lLJNkHbJazQCPaN5Ptx7qAdMSZxW548nuXfGXkTXz6Pms+K5WxcUHe0cP8AtJZQ12d9k+8SevL2H1v4A/5Mlxq09vp+ZfmNxJuMQTrn4OuSve+V5RtetrH+sV35l+U89/SD6qk5WNEXRTjJmbJ9mtv98I3lBVabymZR9pj5i5UewpQekl9B5aR61Q/eA8XBCoqMzj6612d8n/KKMyb7HnG59tm5mNOVo1pL3od93NQ7oui3HncAcnRmpbAEXqk2RC50v16TH5K2tDp2mU6RqZU4ZZoG720eDogtiv02/KPdbTfTh1OKMR1wcOK+0Rcb3QEPgyVNcmFlYt640rLPHUrusRRWsOdn3FlPwZDciO6ceQ2WptxstJCXckvvtFSaWjbEu8Zi5L6uO8dx3+rlQrG4cFvq6S49o9zqJXNLs28xkTVc9Vhz5GYwitGKbTlbZ79/ZiW/QWR1FMlttudy3q1EXydSuxrikVTZW2masq44lLMto26RsLIy6preOl/qHGGxj+Q3fShL4urV8Vb+ZsuNjuF5qOp2+ST2cJVIo2efT96bUpVHqEedCkOxZkdwXmnmS0kBDxSFfV04SqN2jZZE2WUlNQdsyh3LS4MfMyx4tyzoeI4t1FllvpkQ9vpLil7n0P1Cthoq1w/aKdCh1ngmRVuEOJl7x0DaeztqN1bPFIrdoNYs0C4Hii1N9wfTo/8ARckdRCQkX2lkVM2KHFGbi+3OSa1rNSr9nJvHBNja+wsrO6mNSHtxh1cSp56j0jqL73q+MIj8ZYNI+GQhurFZ1W4Krtstskq9tvKx7MLLBusU5vF+p2+4UkWw9EnGS9B4cMOVh0hL4uK2VXHmR4joetFuaso8yPeQriWiOJBAEAQG35TZdVDNTMCk27T2sSKS7qdcxw4LDQ8Jwy7kR+tpHtldiTMbCbO20UlwqVgT/FLNc4rsjZQ5MVecxhi2MKDhEhjh/OYjubeH+PSW+lbLjO53KfRQUMknCpU444TjhGZaiItREo4ee204to/iFIQBAEBv+QfXgtf4SXkyUO1x+4KvlJVqr9+03MST2YP5FmV/bqsf6gl5y+kDtNF/AjPWVDuyczHPtsTZBk5zPFdttzHvumYYFoqfId1R5DY/zer7259UvdcIpJqDr6li0eTq5fsW73/NebiUx62hzttN4rXrFHn27VpVNqkR6n1CI4TT8WQ3pcAh7UhXr2mqYqyJZ6ZsUbbpE9KMrYWOvbNuy/XtoCuC6IuUu1YzmmXViH/dtco/FUB1u1zotV4OKdt1f7mM6jo2qm9ktSy9y8oOVtqw7dt6CEGmxB6WkeM4XbGZdsRcpeJLxeKy91TVlY2JmJlFEsK4UNkWjMsID5cbB5smnW8HGzHSQkOoSFXEdo9OJNP6S1p0YiAu1tsPnBdmXjlzA1RNJOzqHH/FcpxgeT/R/J5K9UaifSQs2hbbeW2u6/FzEYrbdh+1iIN8XgkvSK7W0pHSVGyrsY1nNCZT7ouXqqg2q2YvsadTcmbpL8X2zY/0nyeUuJ67fSDSWZJKCjwyT7vEq8xuqS3tK2KTdLHLqjjFsmrsBr3NunuhhuhERacGy4xFwiXkq3PplucLt3pF+YlT7mnSQZrn4OuSve+V5RtesbH+sV35l+U8+fSD6qk5WNEXRTjJmbJ9mtv98I3lBVabymZR9pj5i5UewpQekl9B5aR61Q/eA8XBCoqMzj6612d8n/KKMyb7HnG59tm5mNOVo1pteVl+yMsr/otyRumeMF8ScbEtOtsuC4PxhIldifLkxGwt9Y1DVx1K90szvO2Le2j8oxaB0HYNUYGRCmD6JMudqX92PoFh/WpCyrMh3SpggvVHh7rbpWXmVljXsqLkfo1eiEy6GPpMjDDptSG+1MMezgo9IjRthY4bX0E9tlyp1NVVo1oQH6w4cipTGYsVhyRKdIW22Wx1EZckRX3RtbpUiNI2FV2iw7ZG2aXMqac5c1xtB9005rS1HxHp9RNY9r7su2/wW8pqfL2m3js+rtk8nx58/rG+E47t25zMXPXYtkUmUMiJS3N2nE1j0xKT2rfxR+sSxayXE2WpGtbbnomkWkibd3uYictYc7CAICTtD2z4NBtGLbzOW9DOA0Aa45YDuTruAjwyb06dRadWpbFavCuHCT+LWdIYlgWmXCRzuCrYVy4qlVGo7cEZck5Ix2eC21qLVpHuRWCzYmxEGmkzJWlXZLJdl/PmHnTZQwZ7gfdLT2cGp8c/xw8XdfjdnulvoJlmXoO4WK7JdIML+sXe/McC2lNjaoUydMuaw4hz6e+eLsmkND6bH7pse2HueNgsGopP04oyI3zVh1ZqmiXZ4fykSZEV+G8TEho47w8Em3B0kK1u7vHOXRo2wsp8L4Um55dZPXVmlVGYdApEiQBlpKY42Qxmu6M9OkVdSJpN02tDa6u4SYYl/m7pYtkFs90bISgvFg+E6tvj/HKmQ6emOHo6R5IrfQwrCp2i02iG0xfo3u8xETbHz/DNO6G7eocjdLZpLhemDxZcjikfuR4o+6Ilq6ubMbCpzrWa7rXS9Wgb7NfiYjisAg4QBAEAQG/5B9eC1/hJeTJQ7XH7gq+UlWqv37TcxJPZg/kmZX9uKz5cl5y+kHtFF/AjPWVD6H5jta5Wbc5DnZsu2RnpKhza7DOLU4xjqqEHS28+0P4twu2HxV0TV3XW66txtDStiRu627ob/qpq56OKo/Sx0q17XpVmUGDRKLCbp9KhhuTEdkeCI/8AnZUMr6+puU7VVU2J2MxE0RrhUyi1xkBAEAQBAcNq2xvltWs0m71kUkd0++O0sREYj7+r76Qfs8Ul1CD6Qb3T2ryYsn6OLvaF4TUaaCBpc3CdyHAWxEQHARHgiI9hcxZ9LacWk2fQYe8vYjW/gD/kyW1tPb6fmX5i3JuMQVrn4OuSve+V5RtetrH+sV35l+U89fSD6qk5WNEXRTjJmbJ9mtv98I3lBVabymZR9pj5i5UewpQekl9B5aR61Q/eA8XBCoqKzi661198n/HUZk32PONz7bNzMagrRrQgO57N207U8lqkNOqGDlStN9z06Lq4UcsfxjX7Q9ss2nqGh2W3SXWO/SWtsmTajYniL2Xe0bahBgUC5aaWHCH8awXjAS23THMp1rpobxB3ZFOJ3b0PK1qm+Ttv3HUKIJfiZDIy2x9zwmy+URLF00KNusRWo1OpJGxRSMvxGEpfQ4obUsSqV8PyovbNxaaLR4/GJwvFVK0K95jEj1LTp25vhO8Zb7P9g5LROrKfBawlNDqcqtScwcew/XqL0B+LpWakMce6SyhtNDa1xov8zHGNo/bRp9Hp8q3LCl9WVY/S36q395jD225l2x91xRWJPVKuyhGr1rPHGrQUTYm4iCTzxyHnHXTxccMtRERaiIlpjkulsTYmP4gCAIAgCAy9p3ZV7Hrker0Oe9T6gwWoHmS+qXKHuVWjsrYlMqmqZaOTNgbCxOrJzbntu5YLEC9P/p+siOAnM6XTiPFyuU3/AFFwe6W5jq1bZY6xbNaqaoVY6rZb4TtUqm5Y5m6X3mbdrxu4ffMdycMv7+MsnCkhJmS31202FjyQ9nnK+gkUlq0KOz6Hom40JYDh8ZUZMXCW0tFvj2lhU8l1595a5S0vqd6s05gWR9Lp9L0uF/UIN+ovrTRx94onu1vt64WkXlUhjn7tiV7Ndp6kUMHretwtQuNif8YlD/SEPFHuR+US1c1S0mypzO8ayS1+KKDZT5iPCwCFBAEAQBAEBv8AkH14LX+El5MlDtcfuCr5SVaq/ftNzHSsqdoK0sparmNRrkKpx5x3hV5LYx6W+8JNk+WktTYkK5JrNqtcL/1SpocLKsUa7yqep6eoSDM0PxG++fQyw53W/oWX+7UK+rm/cKe+pm9fg/zQPPoZYc7rf0LL/dp9XN+4U99R1+D/ADQPPoZYc7rf0LL/AHafVzfuFPfUdfg/zQfXn0MsOdVr6El/u1T9XN+4U99Sjr8H+aB59DLDnVa+hJf7tPq5v3CnvqOvwf5oHn0MsOdVr6El/u0+rm/cKe+o6/B/mgefQyw51WvoSX+7T6ub9wp76jr8H+aB59DLDnVa+hJf7tPq5v3CnvqOvwf5oHn0MsOdVr6El/u0+rm/cKe+o6/B/mgefQyw51WvoSX+7T6ub9wp76jr8H+aDHXJti5a1C3apDYk1onn4jrIDvJL4xAQj+LWzt/0f3ynqopXWPDoZW31Pj1sLaG0L/8ARwe4o7sXZ7yXafaNlwafL1NuDpIfTG12mw6VbWC6svEvynC/pC9XScrGgLo5xkzNk+zW3++EbygqtN5TMo+0x8xcqPYUoPSS+g8tI9aofvAeLghUVGZx9da7O+T/AJRRmTfY86XPts3MxpytGqCAIDJW5dNYtGot1CiVOVSpYeo7EdIC+qq1Zl2lMinqZaVsyBsLHdLd2680KG0DUp+m1ocMOl050PSfymyFZmiskXeJVDrbcI1wthY9czb7zNlCQtMUOL0+2ZhuFiPynCTrkhd0633Bu6vunH78zhvPMuUT1xV+ZPDVqGPq3NgPctjwRWO80km8R2sudZXN9vJi+U05WDVBAEAQBAEAQBAEGE+2ZDsctTTptl3JaUKl0su6ehytT3h0nNkkPF4TpKvExc0zSt3mPGXCLUXGVBZP6gCAIAgCAIAgPfb9en2xWItUpr3Us2MW6NPbmJaS9yXBWJWUcFwgamqVxRtvGXSVU1DOtTA2F13Tei2jMwyLUVcbIi/+3xP3KhvmLYfBb+pJ+Yl/nvf/AB/hX8o88ZmD+fWvo2J+5TzFsPgt/Uk/MPPe/wDj/Cv5R54zMH8+tfRsT9ynmLYfBb+pJ+Yee9/8f4V/KPPGZg/n1r6NifuU8xbD4Lf1JPzDz3v/AI/wr+UeeMzB/PrX0bE/cp5i2HwW/qSfmHnvf/H+FfyjzxmYP59a+jYn7lPMWw+C39ST8w897/4/wr+UeeMzB/PrX0bE/cp5i2HwW/qSfmHnvf8Ax/hX8o88ZmD+fWvo2J+5TzFsPgt/Uk/MPPe/+P8ACv5R54zMH8+tfRsT9ynmLYfBb+pJ+Yee9/8AH+FfyjzxmYP59a+jYn7lPMWw+C39ST8w897/AOP8K/lHnjMwfz619GxP3KeYth8Fv6kn5h573/x/hX8o88ZmD+fWvo2J+5TzFsPgt/Uk/MPPe/8Aj/Cv5TXL0zIuPMHqPf8AqIzuo9YsaWGmtGrTq4gjyRW+tVht9lxdRjw4t7aZvmNFdL3XXjD1yTFh3dlV+U1lb80RmbJ9mtv98I3lBVabymZR9pj5i5UewpQekl9B5aR61Q/eA8XBCoqKzi661198n/HUZk32PONz7bNzMagrRrQgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgMzZPs1t/vhG8oKrTeUzKPtMfMXKj2FKD0kvoPLSPWqH7wHi4IVFRWcXXWuvvk/46jMm+x5xufbZuZjUFaNaEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEBmbJ9mtv8AfCN5QVWm8pmUfaY+YuVHsKUHpJfQeWketUP3gPFwQqKis4uutdffJ/x1GZN9jzjc+2zczGoK0a0IAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIDM2T7Nbf74RvKCq03lMyj7THzFyo9hSg9JL6Dy0j1qh+8B4uCFRUZnH11rs75P+UUZk32PONz7bNzMacrRrQgxBCnpCDpCFQQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEBmbJ9mtv98I3lBVabymZR9pj5i5UewpQekl9B5aR61Q/eA8XBCoqKzi661198n/HUZk32PONz7bNzMagrRrSZ2xhkjZOZGXFRqNyUFiqS2qgTQOuEQ4iOgeDwSW3pIo5I8TKdQ1YtlJWUbPOmJsRIPzpuU/tOieEc+0szq8XCS/yBbfBUedNyn9p0Twjn2k6vFwjyBbfBU/GRsk5TPtE2VoRxwLstuujj/jgSdWi4SnTq/bW/ZGi3hsD5f1mG5jQ3qhbsvpcAhexktYe6E/RL5SstRxtumpqdU6CRfssS6SF+cmR9x5J14YFbaF6I/q6mqEfVuL4/sl3K1MsLQ7xzK52qe1yZcv8rHPlYNKEBLXZu2SLQzhyyj3HWalWos05LrRNQX2Rb0iXB4zRF9ZbSnpo5I8THRLHq/SXGkz5WbF/6HVv4PDLr893P86jfuFf6lGSHzOoOJvh/KP4PDLr893P86jfuE6lGPM6g4m+H8o/g8Muvz3c/wA6jfuE6lGPM6g4m+H8o/g8Muvz3c/zqN+4TqUY8zqDib4fykQto7LCl5QZnSbbo0iZKgtR2nhcnOCTmoh7kRH6q1tREscmFTnF7oo7dWNBFunMVjGhCAIAgCAIAgCAIAgCAIAgCAIAgCAID3yLeqUWixau9Cebpcpwm2Jm5+lmQ8YdXKVXQ28X2p5ljWVl2W7x4FSWAgCAIAgCAIDM2T7Nbf74RvKCq03lMyj7THzFyo9hSg9JL6Dy0j1qh+8B4uCFRUVnF11rr75P+OozJvsecbn22bmY1BWjWsWFdD260dW76ueTbW7ovVnZdUOwNzEpS9RbAnZG2Vt55bRJT0dyPXN0aMmy0ww42H+0WC1ZGpCn1st0baUbF7pkrZ22sr7lnsxDqMyjm7jpFyox9DfT7ohIsB+MqtFXCxk0+s9tqGwYsPMd7ZdbkNiYFgbZYdMSwx9DHBZhKtGnFtKcu2kMt42Z2UtepzrQuzY7BTILmGHCbfbwxIel7r0Rx/USsTJmIaS80S11FJHp3vSpVMo2efAgJ27IGdljWLk1FpdfuaDSqiMx9wmJBFq0kXBxW4pZUWPDpY61q1caOlocuaRVbEx3Dzz2Vft3pfyy/wDhZ2dHxEq8tW7xlHnnsq/bvS/ll/8ACZ0fEPLVu8ZT3UDP7L26arGplIuuBUKhILQ1HZIsSPH8nqL4ssbbKsXYbpRVD5cMqsx0VXjblYe2tUotS2gKz1K7g9uEdhh3T2piPCH6y0NXpxSnCdaGV7k2E4WsIioQBAEAQBAEAQBAEAQBAEAQBAEAQBAWH7H1r0q8tmhqlVqAxUqfJmSRcZeHUJcL/Iv1reUyq0WFjtGrkEdRaVjlXEu0cQ2gti6q2EMmuWeL1aoIanDhcaTGH/qCsSakZdqMit41ZelxT0u0vykXuKtcQE/qAIAgCAIDM2T7Nbf74RvKCq03lMyj7THzFyo9hSg9JL6Dy0j1qh+8B4uCFRUZnH11rs75P+UUZk32PONz7bNzMacrRrWLCuh7daOrd9XPJtrd0XqzsuqHYG5iUpeotgTspfuD19qXwl3xiUXbePNNT6+TmPAqDFLTNk24pF07P9pTJr+MiU207ENwvVxwaeNsfqiKkVK2KFTvtgn01FthZv8AOg6vVRFymSxLi4tHgX+Cv6fQb+TcYpnrDYM1icDX3sX3BHTydSi7HmmX1jHkXwshAEAQoJrbAOUeIDPzAqDfCLVDpol/vnP2cPjLb0cX7TSdU1PtuFWrpOVSU2bF/wAfLHL6t3LJHB3qFjEmmcceluruPoNt/GLHDBbCR8tcRPrhWLQ0z1Dd0qOrFWlV6rTKnNd3aZMdcfec5ThFqIlG2bE2I87TSNNI0km8x5FQWQgCAIAgCAIAgCAIAgCAIAgCAIAgCAso2GPwfqf8Nk+UW+o/VHcdVvu1f5iRCzSXkZNoLY3o2ZQSK1a+DNDuX0TINOmNKLuulxS7oVgzUyybSkIvGrcNdilg2ZPmIB3dZtZsSuSKRXqe9TagwWkm3h43dCXbD3S0ro0bYWOQVNLLRyNFOuFjDKgxQgCAIDM2T7Nbf74RvKCq03lMyj7THzFyo9hSg9JL6Dy0j1qh+8B4uCFRUVnF11rr75P+OozJvsecbn22bmY1BWjWsWFdD260dW76ueTbW7ovVnZdUOwNzEpS9RbAnZS/cPsgqXwl3xiUXbePM9T6+TmMfxlQY+8WrbLtrSrLyKtSmz2sY8vcHJDoF6o7q6TmGBfr0kKkkCYI1U9BWOmalt8Ubek3DMy4mLQy/uKsSMfSYcF10vk4qt2wq2kz62bRT00krd1SnsiJwiIi1EXCIlGDzg28fxCkIAgPbQ6PIuCtU+lw2sXpkx9uM02PGIyLSKrVcTYS9DC00qxrvMW+5e2hGsOyqNb8URFmnxgZ4PZLDDhY/wB+PTUlTRhXCejaSmWlgSBe6RR6IZmDg1CoFmR38d0eLfCU2HJHULfT+Nq/wWurpO4c/wBca3CsdIvMxCNag5YEAQBAEAQBAEAQBAEAQH3HjnMkMstDqcdcFsR5REhUiNIyqp2fzm2bHta/4lv7Sy+qTEm82bl4Z/fOcZse1r/iW/tJ1SYebNy8Mec4zY9rX/Et/aTqkw82bl4Y85xmx7Wv+Jb+0nVJh5s3Lwx5zjNj2tf8S39pOqTDzZuXhn5FseZsCXsYMvcvt/aXzqsvCPNq6eGTh2T7FrWXWT0OjXBDxg1EJT7hM4kJdISPg8VbimRo48LHVLBSS0dCsU67R2lZJJAgNBzTybtrOChHT6/CwcMcMdwmN49J+OXKEv8A29RWZY1kXCxq66201yjwTqV255bM1zZJyzkvN76W+R6WapHHgjyRcHtC+qtLNA0Jxe7WGe1ti3o+I4+sQjYQBAZmyfZrb/fCN5QVWm8pmUfaY+YuVHsKUHpJfQeWketUP3gPFwQqKis4uutdffJ/x1GZN9jzjc+2zczGoK0a1iwroe3Wjq3fVzyba3dF6s7Lqh2BuYlLjh08FsCdkWZnQ/rJmzX5Dlaq4k84RkIk322PuVgtRxsQWTVGjkkaRmbaM9Y+xFl7ZlWj1N0JtckMHujbc8xJrDH9YYD0iVSUsa7RmUmrFBTPmYcTe0d4qNRg0SA5KnSmKfEaHhPyDwbbDD9eOPoYLM6eglLOsa9LbOgg1tf7UVNvWjnZNpSxmU3FzAqhUA9EHtBahab5WGoRLV2dK09TUYtmM5ZrJfo6iPqdM2Je8xEVaw5uEAQBASN2FbBwurOHfh4MDh0CNjJHp4CQ4vnwG/k8IvdCKz6NMUmIm2qVHnV2a37MsjW8O1FTe0pen3d52XTVAPdIrcrqSNpLUO5NDuYkPclpIvjKO1D4pGPPt8qut3CV/wCU5msY0IQBAEAQBAEAQBAEAQBAZG2/ZFS/hbXlBVabxlUnr4+ZS59Sg9LBAEAQBAEAQBAEAQGPqNNi1aC9Dmx2pcR8MQdYeDA2zHH1cCHH1cE06MRbdFdcLeghdtB7DpMjJruXje6BhqddohY+j/sS/ZWpmpO9GczvGqu9PQ+7+UhrKivwZDkaS0ceQ0WlxtwdJCXJIVq22TmL6GjbCx+S+FJmbJ9mtv8AfCN5QVWm8pmUfaY+YuVHsKUHpJfQeWketUP3gPFwQqKis4uutdffJ/x1GZN9jzjc+2zczGoK0atiwroe3Wjq3fVzyba3dF6s7Pqh2BuYlMtgTs5sW0RlmMgmCvejC8JaSb6pH0MVZzo+I0zXi3q2HTMpsdCzAtu6cRwpNep9QxLijHkiRY/3dNVaHVt1jMirIJvVvoYzNSpkWrRTizYrUyOfGafbwMC+LirhkuivowsQ22w9mWhUy0ZF7WnACnPQSEqhEjj6W40Rad0Ee1IcccNXc+5WrqadcOYhzfWSxwrA1XTLhZd4hKtQcrCAIAgLFtg2yBt7KAq04GAyK1KN3V/RB6WP+Ykt7RphjxHadVKTJoc7jO05qXUNj5d3FXMSxEoUJ1xvEeXp4P1uksqRsK6WJNXz9VppJuFSoN57GQ846ZaiMiIlGDzk2nEzMx8IUhAEAQBAEAQBAEAQBAEBkbb9kVL+FteUFVpvGVSevj5lLn1KD0sEAQBAEAQBAEAQBAEBh7kual2hR36pWJrNPgMDqcfeLSIqltKr+ljGmmjp0zJWwqVqbUuctt5u3iMi3aE1BZjamyqxDpfm90QjwdPJ1cLxVo6iVZG2TiWsFzguU+KCPd73EcSWERUzNk+zW3++EbygqtN5TMo+0x8xcqPYUoPSS+g8tI9aofvAeLghUVGZx9da7O+T/lFGZN9jzjc+2zczGnK0a1iwroe3Wjq3fVzyba3dF6s7Lqh2BuYlKXqLYE7KX7g9fal8Jd8YlF33jzPU+vk5jzwZ0ilym5UN9yLIaLU28yWkhJU6NndLaO0LYo2LZMg71kZiZP2vcEv+Wyoul8uU4BE2RfGxHV/epJC+KNdJ6FtdS1ZRRztvMpm8y6cNWy9uWIY4EL9NkBiOPvZKt91jJrUzKaRfZYp5UXPN7bwQpCA/WDBfqU6PDjgT0h9wWWmx4xERaRFfejEXETMkWNe8XCZe2oxYdkUO3o+nEKbDajahHTgZCOGovjFqx/vUoRcK6FPR9HAtLBHAvdU4Tt73TjRMmY1Max9Mq9QbYLpY+juYCThfWER+MsGsbDHhIprdPk0GDiYrsWkOLhAEAQBAEAQBAEAQBAEAQHtocgItap77uO5thJBwi5IiQqtd4yKd1WWNm4i07z0OVft1p31/sqQZ8fEd88t23xlHnocq/brTvr/ZTPj4h5btvjKPPQ5V+3WnfX+ymfHxDy3bfGUeehyr9utO+v8AZTPj4h5btvjKfB7UmVTQ4kV607T3OvH9lOsRcRS18tq/tlPP57PKX26Q/AvfYTrMXEWvOC1+Mpu1hZkW5mXTZE+2qo3VobDu4OPNiQ6T0iWnhYYdghVxHWTaU21LWQVseZA2JTalcM0IAgOUZ2bQ1sZJ0zp1J7qyrOBqYpcch3Zz9Zckf1rHlnWPQaC53imtidMm9wldGcOetz501kpdYlYswWy/i1NZL0hgf2i7olpZZmm3jjVyu9TdJMUjbPCc9WMaMIDM2T7Nbf74RvKCq03lMyj7THzFyo9hSg9JL6Dy0j1qh+8B4uCFRUVnF11rr75P+OozJvsecbn22bmY1BWjWsWFdD260dW76ueTbW7ovVnZdUOwNzEpS9RbAnZS/cPsgqXwl3xiUXbePNNT6+TmPAqDFLNdiOSUnZ1t4C/EPy28PDmX7S39J6lTuurGnFa4/wCb5jsV1ejbFX+CPeISzG3SRz+qb/4KaFFDzS28EKQgOobMVtfdTnpaUUgxcYZljLc09qLfpnjCKyadcUykgsEWfcYlLYFIj0AQI6IlcXVV42pQhwx0woLssulj2zp6f+j9ZaiubaVTkuuc2KWKDhXERGWrOchAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBBhLAuh29aq4u/JeQZW7ovVnYtTuwNzErlsCfH4vPBHaJxwsAAcOniWPqYIUadOH9LERdoLbdh24cmgWETdRqI4Ytu1fjR2S/o/5wu64vulrZqrDsxnPbzrQlPigo9puIg1WK1NuKqSqlU5T02dJcJx2Q8WpwyWp0ti2mOUSzSVEjSSNiY8ioLYQBAZmyfZrb/fCN5QVWm8pmUfaY+YuVHsKUHpJfQeWketUP3gPFwQqKis4uutdffJ/x1GZN9jzjc+2zczGoK0a1iwroe3Wjq3fVzyba3dF6s7Lqh2BuYlKXqLYE7KX7h9kFS+Eu+MSi7bx5pqfXycx4FQYpZDsGSDeyDZAi1CzUpLY/qHgl+1it7R+qO3ap6f+GLzMd9rkfqujz4+GPSxdYcD/ABHFZundJdJoxIylNE6KUGdIikWomHCbIvclpUVPM76MLMp+CFsICUXQ+aE1PzYq9Rc6eqn00iD3Tjgj4q2NCv2jMT7U6HFVyO3dUsOW6OxFYm2vUMZu0HXMMDxIGGWGhHV6nSaH9paGr0/anDdaHzLk3s4ThSwiIhAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAWBdDs61dxd+S8gyt5R+rOxandgbmJB35mPb2WVDOq3DUG4EQcODq4RuFyRH1SxWW7rGuJiYVdbBRR5k7YSvbaA2trgzcedplKJ6hWxhjpwjiWl6RhynSw8UeD7paWapaTZU49eNYpbh9lFsx/McDWEQ4IAgCAIDM2T7Nbf74RvKCq03lMyj7THzFyo9hSg9JL6Dy0j1qh+8B4uCFRUVnF11rr75P+OozJvsecbn22bmY1BWjWsWFdD260VW76ueTbW7ovVnY9T+wNzEosSH8q2BO9OkpiuD19qXwl3xiUXbePNdT6+TmPAqDFLB+h7VpqXlPWKb+Oh1UzL3Jthp8XFbuib7M7HqfMrUDR8LEpi6RD0lsCdFRWeNnSLDzXuajyMOni1NcdbLlAfpgl8khUbmXDIynnq7UzUtZJE3EaOrBpwgJq9Dkp4YjfE8hw3QSisCXc+mEXiitrQrvHUtS02Zn5SbPofrW2Om9JU5tJy8Zue96uYniYYVAxHUXaio5UeuY8/Xx8y5TN7RzRY5oQgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgJL7PG0rR8isoK3D6nOo3DMqhOxoXFAR3Jsd0MuTqEvkrY086wx+0TyzXuK00LLvPiOIZiZnXJmpXzq1xVJ2c7jj6Wz6jTA8kA4uA/8AhLDeVpGxMROtuFTcJMydjV1aNeEAQBAEAQGZsn2a2/3wjeUFVpvKZlH2mPmLlR7ClB6SX0HlpHrVD94DxcEKiorOLrrXX3yf8dRmTfY843Pts3MxqCtGtPbBr1SpbO5Q6jKhtkWrSy+TY/VVeh2XdLyVEsa4Y2wno+66vfnuofO3PtKrG/EXOt1PiMYsiIi1EWoi7ZWjFP4gPXT6xPpIuDCnyYYnxup3Sb1fJVehmXdLiTTR+rbCer7sK9+eqj87c+0qsb8Re65U+I3vGPmTpFQkE/KfckPFxnHnCIi+MSpZsRju7SNikbEfkqCkID10+tVGliQwp8mGJ8bqd0m9XyVXoZl3S5FNNH6tsJ6vuwr356qPztz7SqxvxF7rVT4je8Y1552Q8Trpm444WonHC1EStGLiZmxMfCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAzNk+zW3++EbygqtN5TMo+0x8xcqPYUoPSS+g8tI9aofvAeLghUVFZxdda6++T/jqMyb7HnG59tm5mNQVo1oQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQGZsn2a2/wB8I3lBVabymZR9pj5i5UewpQekl9B5aR61Q/eA8XBCoqMzj6612d8n/KKMyb7HnG59tm5mNOVo1oQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQGZsn2a2/3wjeUFVpvKZlH2mPmLlR7ClB6SX0HlpHrVD94DxcEKiorOLrrXX3yf8AHUZk32PONz7bNzMagrRrQgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgMzZPs1t/vhG8oKrTeUzKPtMfMXKj2FKD0kvoPLSPWqH7wHi4IVFRWcXXWuvvk/wCOozJvsecbn22bmY1BWjWhAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQYggCAIAgMzZPs1t/vhG8oKrTeUzKPtMfMXKj2FKD0kvoPLSPWqH7wHi4IVFRWcXXWuvvk/wCOozJvsecbn22bmY1BWjWnaNm3Z4DPyTXmzrBUrewGC1A1um6bpun7tZ9PTZy4sRJLJZ1vDyLmYcJ3X+Dlje3Rz5kP2lkdRXiJb5lr4xx7aO2X2shqDSai3Xiq3Vz5MbmTG56NI6tXGWDUQ5LL7RoL1YFtMCyrJi2jb7E2GX75y/pdxs3ODLs+JhJCKUbilj2urUszqezixGdQ6q9cpVqczeI82vZL9ezEptpyDKDIl1BunuOEP3oic3MuCtfCmcyqQ1KVmq1pG3sWH4jrG0NsrY5E2pT6zjXsKt1VMGLuPU+56eARauN3KvVEOSyklu+ry2qmz8zFtdB6Nn7ZMxzysqRX8Lgwpe4zDh7juG6atLbZatWr+kV6KjzI1bEfLNYPK0DS5mHC2E5Vm1lvLylv6p21NPqgopCTcjTpF4CHUJD8pYLLhbCxobpQNbaloGO03LsYFb2UEi+MLlweFmmBUeo+puNqAS06tXdLMnpMlcWIlD6r4aHrmZ3cRyHJjJ6rZ0XbhRKYQR2m291kynOKyH5Vbp4WqG5SL222y3SfIiJdReh42oEUQk3PUnZOnhEINiPT9ys7qabuI6Mmp1Mq7cjYiO20VsyVPIpyJUG5e+1vzHNxbmadLjR9LVoMe6HVwu5WtmhaFiJXqwSWtVkRsUZvWTexUxmxltR7qcuY6eU8XcepxjatGh1xvjau5WctDiVdOI2Fr1aW5Ui1OZhxYvmN3/g5Y3t0c+ZD9pVdRXiNp5lr4xzTP3ZDayXsQrjbuJyqEMltjcCY3PDhdtxlh1NOsKqxprvq2ttps/MxHiyB2Sn87rOeuEq8NJZGWUYG9w19PSIkRfWWRFR5iY8RYs2rnlSBp2kwmsbRez3JyCqlGYOojVItTaccCRue56XAIdQ/WH5Sw548l+gxL3ZPJDR7WLEa1kvln5rmYEG2Rm73lJFwuqNz1adI6uKqoIc1sJq7bR+UKlabFhxGz7RWz7jkLUKPFxq+FW6vaNzVuW56NJaVTPHkthNte7J5Jy2zMWI4+rBFyRez3smM54WRIr7lwnSyanOQ9xFjdNWltstXG/pFsYqNZEV8RM7Lq+t0gafMw7WE6a90ORrcy3K8z1d1E/7le6l7Rv8AzLXxvhOI567K9yZJwm6o9JZrFDNzcurI4kJNEXFEx7X3SwZoWh9JF7rq9Pa0zd6M4qsYipLmztgV+6LVpFXfuoIbk6I3Ixj9S6tGsdWnVqW36j7R0Sj1R61BHO0mHFtEcM1LBkZX5g1m2JLvVB050QF7Tp1tkIkJfGEhWpZcLMpDblRabdVNT6e6dm2f9khrOyxSuJy4TpZDKcjbiLG6cXTwtWrulsoqTMRZMRILNq8t2gaZpMO0dKLocrOktN5nq+B/9yudS9okHmWvjfCR9z22ca/kZMYcmut1KjyiIWKhHHSOrkkPaktbNC0LYWIjd7HPacLadpW7x0axNi7C+sroV3x7owZwkxDk9SlG4pDq4OrV3KzNNHhTHiNtQ6sdepVqlm3vZOEZa2O7mHf1HtkH+pXJ7+4btp1aO6WLBHnPhIjS0rVVStNxNhOj7RWzO/kLFpErGr4VZie4bfTFrc9BDp+0qp4cll0G/vdha0xrJmYsRkdnvZUdzztWbW8a5vS3HllGFsmN018ES1fWV+GkzEx4iuzWDytE0rSYcJgrDyAYvTPSr5fDWiabp5yW+rxa1bpuRaeKqKeHOZvZMeCzrUXNqHM3e8d7/g5Y3t0c+ZD9pZXUV4iV+Za+MaFnZsYsZR5d1C527kcqRRSbHqco256tRCPG1LFqafJXFiNZc9V1t9I1Tm4sJr2zrsn1DOqmuVyfOKjUEXSabdANTkgh42keSPF1K5DS5i4m3TW2bV+S6LnM2GM7vK6HhajkcgjXNU25HS4JOA2Qjj7lX9NEmHZYmDam0jLhWRiJWdmS1ZyRuveip4i/HfDdYk4B6Tb4fskPbCtU6NG2Wxz67WqW0y5cm0rbpz5UGkMzZPs1t/vhG8oKrTeUzKPtMfMXKj2FKD0kvoPLSPWqH7wHi4IVFRWcXXWuvvk/46jMm+x5xufbZuZjUFaNaTM6HB65X571D8Z5bqj9VpOkal+vm5VND2tcybtt7P25YFNuWq0+E0Mbc48eW423hqjNkWkRLlLXTySLI2FjB1lramG4yJFIyrs/KcKuC+Lhuxltis1yoVZlotTbcySTwiXc6lZ0uzbxD5qyeoXDLIzFoeQc5im5C2Y9IPBtrCnNYYkXY6f/APVJdGn9Gg7pZHVbXCzcJFvOnLvCw9siz6rHa3OnV+rQ5g6eLg7uwi8PyuF/tFqoky6voIRe6Pqt5gnXdkZfexHR+iH9amgd+R8g8vtf6Yzf64fd68y//p7+h+FueRtRx/JWX8f9y0s2D1ClvUzsMnN/apoG39ZTFUpttX/ThweZdAYj7zfbNl6YyXjLX1yYXxGDrdSrNFHWRnacyy3XY8nl+W2GvIis6v8AUsSJ9OKxf9v+04l0OeRCCXerJYhjPIIxhytzw3TV9YhVui0/ZMpEdS9K5sy6d7ZNa2yo97WbnOxcwTZzNFPcTp0hhwhbZIRHU3we21ai7rUsOVpIZ8TFGtGirhrFmVmwG47RW0nl5mfkdLosCt4yq+71M9hH6ifb6TgkJFwib08rtlerJYpl2DYXW9UNdbWgWT7TTh7rbxEuk5mXbQaezApty1WnwWtWiPHluNthqLVwREuUS16yyYd45xHW1MK5ccjKpZDd1Znxtk+VVG5shuoja4SMJgnjuu6dTiWrVytS3dfp0rE+H/P0nbmkfyNm9O1l/wBpWxW8xLouaD1JVrhqVSi6tW4ypbjjer3JEtEzs28xw+SuqZlwyyMy8xYVl84GSuyFCnOkIPt0jq3EtOn06SWoBL4zoj/cpA2nK0Iv/TD/APe0dgt2jybYtDtw4veNc6IBao1nKalV1pvBx6kTx1O8lh3DSX18GVg3Bd1ixrZFnW5Zl7rfMRq2K/whaD70/wCTVug9b/KQDVv70jOqdEZ9frN+DveMrdb65eUlmum7EQ6WEctLFOh9dZOo9+3/ACTKkVN6hTsmp3YW5v7VInXtnHfVCzfuEYF0VYRjVuS2xH6rcJvSL5aW9PF09qtJDNJiXaIFX3GtjuEqxyNssxODaTeGZsz3E9UGwbfOntOELg8V0iH/AD1LbV+zGdVuunMtMjScJWvY1undt50GitloOozmIurTq063BHV9ZaumTMlVThUEPWJVi4mwlrNy3RHti67Et5vEW8anKfZBv+iaiuF425rd6W+1VfZb+09CTTrStBDxNh+EhZ0QK1MKTmzS6020LbdWp44GX8460RCX1NyWkqtHRO2k5trjT4amOXiX5Tu+wL1j3e+j/iit1T+oUkGpnYW5iJlZz3va0M26pJZuapuR4dVe/ibkkiZJsXS9L0lwdOlaeGaRZFxEFq7tWU9dI2iRtluImptbU+PcmzdXpbjeomGmJzXclrD9kiWwr1+zOoXtFqLTI/s4jA7DNbwuDInGnuHuhwJb8Yh5IlwsB+sr0P2kCmt1RmzKBk4WOA7KlnEG1VUWSY9LpBzHCEu04WkfGWDb12mYidnpui/MvCzHddvGjt17JIKmyG6lSqk2WLg9oJETRfWxFVVy7KsTHWqPOtrPo7rGQ2MmRtnZxjT3w0iTkqYf6xwxx6X+QrMX7OBSjVNMq24/aYjnsZVAqxtNyp7mOopLEx3UXdFqWFb/AEsQ/V5868tK3exG87eF83Fal828xRa5UKWy7TyNxuHJNsSLWXCx0q3VyyLLhU3GttVPTyx6IZGUipWsyLruKnuQqpctVqEM9OqPKlOONl8UiWEzs28c8kr6uZWR5GZeYsdt50srtkmLNpw4NSqdbHVgeh+PxY3TV8vHprf1WnLibCdroNHULOrJ3VxEEslc4qxaebVGrVTuCaMF6YO+Tkh1xwTaIvTNQ9stVSzZb7bbJya3XOeGtSeSTvbR2nbVzbsXNG0LfG3K2zVKpDmkRC22YkDRBwuMPKEVVWOkjKyEt1nuVDXUyrBJiZWIhLAOZmZsn2a2/wB8I3lBVabymZR9pj5i5UewpQekl9B5aR61Q/eA8XBCoqKzi661198n/HUZk32PONz7bNzMagrRrSZnQ4PXK/PeofjPLdUfqtJ0jUv183Kp2HM6/wDIyg3tPg3jApb1xN4N9UHIp26mWpsSHhaeFwdKuM8GL9JK6+qs8M7LVKuZykStqq58t7mn0E8vI8OO000Yy+o4nU+otXB1cHhLU1OBn2N057rDPb5svqGH2sKkuLdN0NjmI6wWh5m392DH8mI4ah/5LczacuLEvdOgUP3EvKeDM8Y2aOV+W2YEMcHDplTp1SIuyIE82Lw/FLxUk0faxyr/AJiLFdhuVthrV3lwt+Y17oiHWqoHfkfIPLCr/Spb1v8Au1eZf/09/Q+esjUu/b/kmVlwaOmFf/6W9TOwyc39qn3Z0IM7Nmu6rLfPByq0d6VS28HMfRFxhwiil7nSLY/FJWNP+opVb/n+UyqTDcrfUUbbysy/lM3eb+MnYtlGQ4gQ2wAYiXYxFsR/9lcrdOjTB08peb7h0q3AV+ZZ5l1nKe7Y1wUV3AX2OC4y597fb7Zsu5WphmaFsSnG6GsmoZ1ngLGsss6LF2lraepj0eOco2v43Q5+kiw5WI8oe6wW7Vo6lTtVBdqO9RaYWXa7ysRJ2otlg8p8HLlt9w5VrvO9Jxlz0XIRFxcNXbB+TFaeop8lvZIHrBq91H/U025w8JHFYxA1LPL1/A2lf2Qb/wBMKkNf6pju+n7j/wC3/aVtWbQTuq7KPRg1ap0tqNqEdWnUQjqWlg0ZkyqcOhizpVi4iw3bS6vh5FYUekQ3pZypcaMTMZonC3IPTOKPdNgtlW6W0quE7VrFoeO1tFFo4VMreFIdzF2TJEZ2O7jNdoDbu5yBxbPB9psS9HV3TavVmjMibSZDRtV2bLkXawEN9iv8IWg+9P8Ak1g0G/8AynLdW/vSM6p0Rn1+s34O94yt1vrl5SWa6bsRDpYRy0sS6H11kqj37f8AJMqQ03qFOyandhbm/tUzUGnbPv3fvuhhQCurq4yc6pMtXVOvhcfg6tSoiyOlcsztKWPrO1hzMXxGp7eFMu+Tl03Igy2PuXYfAp0Vtkhfxx7UsS1aSDAu5H4yxK/Q+zwmHrWlS1H0xtsd4jzsR2v90OfVMlEPpNKYfmF6HTEi07mPlNXxV8oF22cgOrUHWLlHi7u0SG2j63VmNpDK3GNBmSKZSyF916KwZDhi67pcEiHuWxWTp0t1pdBOr9JKtwo8C7KtiPz6IXbG+GXlCrjbWpynztycc5Lbg/aEVj167SsNcIMyjWXhYzewH1j3u+j/APyFbCn9Qo1M7C3MQLzI64lzd8pPliUfTeU5Xde2zczFkOfvT86tX9Xq7zNfsLeV/qWO3VWn/gzYvD/tOHdDpuHVjeNDLHDARxYmNj+XjCXiiqKLTii0qQ3UubC80H8x0XJCy95tp7OOonh0tzJjc+Tpk+nfsilMuWsmn2iQW+lwXuqk5fiPo5bWbuyhezm6boTmNWkjh+Qgkuvt4eIqJtGZSLpLyOtxtdQvOfvDeLLzYoF0nNzeboBaS7p3i+MrlVpwwYS3S6ep2HF7JGTYQ6/kfvfI8UVjUHpblIDqr95K3MTBzpvHKe2axT2swokCRUXGMSjFKhbviLer8unlLMkeJWwyHULnU22Fl69hIsbUF75PXHYcGPYEOnx6wM8DdKJB3Atx0OauFp5Wla6p0xsy4CBX2rtUtJholXFi4SUuYn4I9S/sqP8Ap8Fn1vqGJ233N/2/7SLOzNsy2ZnLZT9Vr1YqdPqATCjtswZLADjhpHtTbItXCWNBSRyJiY55YbJSXKBnnZlbEfltU7MNs5HWlSKpQqhVpkiZN6mMak60Y4DuZFwdDY8LgqxVwrDhwl2/2GmtkCywsxGVYJATM2T7Nbf74RvKCq03lMyj7THzFyo9hSg9JL6Dy0j1qh+8B4uCFRUVnF11rr75P+OozJvsecbn22bmY1BWjWkzOhweuV+e9Q/GeW6o/VaTpGpfr5uVTle2NSJ8vaJul1iFJebIYmkm2iIS/izS1k2hs1tk1GtELyXORlXh+U4dKpsyCIlIivRxLik82QqzpXiIg0ci7ylmdmBgeyFEHH1Mbbc8mS31T6huU7pbtGKyJyHJ9he72rxy9uPL2pOYkMfU9Gwxx4rLvGEfcnwvjKmkfMiw8JG9U6lZoZqGT/MW8Z/oh/WqoPfkf9O8sav9MZtNcPu3RzL/AHGR6Hx1kah37f8AJMrNg9Qpb1M7DJzf2qcz2ZMwsLY2nr6td53TDrdUnYNDjxd3becIfq6lg26XZy9JqbVWdWvs8DbsjMSMz+pbNG2dL0iR/vQU98h+MWr/AN1frP0wdHKTm6Jl22dV4WIz7Btl2reca8GLgodKrj7BxnGMKhFbfJsS3TVp1D6Har5RojQ/pXvHO9U6amqnmWePQ276f5jmO0NQp+V20XUztyCVDFt9iTTBgN7kOA7mPEEe61LXaMcU+FeI1d+gkobmzQLh3cOEm3nw+7M2Y7jfqreDcx2jCbzZYaek6Qj0x+UtxXYctjp1xZmtEjS72Aq3H1Foe8cEUs9vX8DaV/ZBv/TCt/X+qY7vp+4/+3/aQy2MrX+6PPuhuYiRM08XJpEI+ppHg6vjEsCi0beI5Zq7T9YuUfs7RM3P7agp+Q9XpVPl0R6ruT2Df6bL4t6BEtPo6sFlzVSxNhY6peL3HaWjWRcWI2XJzNKm582A7V2YGMJh5x2I9EdcFwh7Ho4j+XAlkeuixL3jLtVyS7U7SquHukKdmKhu2ztaDSH2xbcgvzGCEe106hWroN/+U5daIer33J4WY6D0Q2nSp9etDqaK9I0x3tWLQYl0uEqK1WaVcPCSTXJJJFiwLiIgPUOox2yN2BJbbHhERNEIisLToZVxMcvyZV2sJYP0PrrJ1Hv2/wCSZUhpvUKde1O7C3N/apBjNB44+bl3OgWlxuuyyEu63clHodllOX3TT/xCbmYsN2gicn7KtZeMtTpUph08ceyXAW8r9zTzHYrpiazM3snHuh1WvjhGuyvuYFwiahtljh6BcYi/ZXyi0YYukiupdPtTT/ym55k7cdJy8vmr207a8qoHTndwKU3KAcDLpcnpKjry4sOE3Vx1ohoahqdo8WE3faFhMZm7NdblRWsHBepwVNj0en0tOAueLqVytTpiNrctC3C1SMneXEapsC9Y93vo/wCKKvU/qFNNqZ2FuYhlVstrhvnOOs0ym0ma65Kq7uG6YMFpASdL0wi7UVpIY2aRTn1VRT1VwkREZsTE6NrmqMWvs21iE6fpkkI0Frui1iXitktnXv8AZnVL4y0tokRuHCRP2FrjwoefUOIXT0VWC/D9HtS0i8PkvrKxQ6elmX2Tm2q9Rk3ONeLEpOKuxRseHmZdQ44NvPxsHxP3qIIj9ZZc2xFIddkTRT6aip9n5VOF7BtUG4cp7tt58dQhKPgl2wuhpLxVap/tKXCQ7VCbOgnibi+Yy219LKy9lmkUPVpcknBp5fEb3QvJf5qi4NsqpsNYdPVbNlcqkftg/r+R+98jxRVFD6W5SDaqfeS/zG69EHpsudf1slGiPyBGmlqJsCLT6YStVmhs03WuMTyTxYV7pE+RR58NsnX4Ullse2caIRWD0MpzpoZF2mUs8jR3cx9k9uLAwF6VU7U3JkB7LvU2nT8v0FIKzRmwNhO7Uv8ArLMqr3oyCOzRZlUuHPO14jUWQPUMwZcvU2Q7gDRai1cni6fdEtdRaG0y4jklnpJ5LhHGq7rbX8pKLoifW3trvsXkTVdf3ToWuPY4+YgItYceMzZPs1t/vhG8oKrTeUzKPtMfMXKj2FKD0kvoPLSPWqH7wHi4IVFRmcfXWuzvk/5RRmTfY86XPts3MxpytGqO7bLO0JSchpNxOVamzqkNTBjBvqPc+Bue6atWou7Wxp6lYVwsSqw3aG0vI8q4sRIT+EWs/wBq9b+U19pZHXY+EmnnnSeGxxPaf2nKHntblHp1LpM+muQZJPkcwm9JCQ6eDpJYNTMszLh7pGb9fYLpAsUSMuFjYrf2zaNR8mGbJO3JrkpullT+qhfb3PUQkOrT8ZXZqpZImQzKXWaCnoVpGjbFhwnDsjc1XcnMwoNxYMuSooCTMiK2WknQIeL4qs002Tp6SJWuv8m1a1K7veOobS+1NSs9bOp1Gg0OZS3Is4ZZOSHRISw3NwelwfdKqqnWfDhJHetYYrpTZCRsu10no2btrGk5H2HKoM6hTKm87OOZusd0RERJtselwve1kw1axosbFuxX+K1U7RPGzYmxHD6jfb/mnTbwpeuHIKrnVIwlxmyJ4nBElrYXyWVlI5U1mZWNVxbO1iJN5obctGzBy4rluNWxPhyqlFKNuxvtk23iXbLOqalZkaNSc1WtkNVSSQZbYmUj5klnHU8kryCtwWsJjLje4y4ZY6ReD8mrtSVmnqMlvZITbLk9snz0JmMbc+VVVajS6jTKm3Oaw1CLsAHTax7ktS2XW4d46do1qtr6OmVWxcpH7aS2tns5Kf8Ac/RIL1Kt3WJulILDd5WI8XUI8ER7nUS1lRUNNs90id71k8pJ1aBcKfER1WKQVSWFd2zaNV8kXrFG3ZzclyjDTOqyfb3PULWDerTyfQWyqapaiNlU6O2s8DW/qeW2LDh+E5rsy55UjImvVeq1CkSqtImRxjNdTuiGgdWotWr3IqimqFhViPWK5xWuVpXXFsmM2jM5Gs8L9arsWE/TorUJuM3HkOCZDpIiIuD7pYsz5kjMU3y6Ldp1ljXCqqbpszbUsPIm3avSKjRZVVbmS8JLRR3RHQWnSQlq9yKzqerWNMLGfYr5Hao3jkXFiMXRc/KHSdpaZmS3RpjFJkmTu97ZCTomTQi4Wri8JzUXxljwzLDM0nEW9F2g8r+UVXZ4e9ukh/4RK0PaxW/lNfaWd12PhJh55Unht8JqObG3BbGYOW9wW5Dt+qxZNSiFHB58mtzAi5Wklj1FSs0eFTCrtaqSqppIFjbaU0rZv2sKVkhYkqgTqDMqjzs5yZu0d0QEcCbbHpcL3tXYatYkVGNNY9YIrZTNC6YtrER/vKuhc14VystNYx2585+WLZcIgFxwi0/WWoTZwkTrKhaipknXvNiJQ3VtpW7deUsqzJVs1ESkU0YZSBkt6cHBEdJe51CtpUVUc0bKT19aKeSh6o8bbuExOz5tcUDJLLxq3nbdn1CYUl2S/IafbESIuLp1dyIqpKuONFjMKy6wQWumyGjZmxEdLyuArsu6sVohId8JbsnS4WohEiIhFalSHVlR1qeSfiYlHl/ts0K2sqqZaFZtebUsY9P6gfcZfbFt1vSQ9t3K2z1kciYcJPLfrRDS0a0ssbNh2TEbOO1nb+S2X7lv1Gi1KoPYzHJIuRMW9OktPB4RdykNUscaxt3TFsd/gtcDRPG202I6m90RW1Bb9KtSsE5ySNocPGVzrsfCSDTrnSeGxGjaB2j6tnvPitnEwpNDhkRRoIubpjq/nDLg9PFa2aZpmxMQm8XyW7Nhw4Y17po+WN5ll5f9BuMQN4adLB9xtstJG32w/GHUqoJMl10mlo6jqdTHPwsSYzZ25KTmFl1W7dg23PgSqixuAyHn2yEOEPJWRUVSyR4VOgV+tcNVTSQRxtiY5Zsy7QUbIWqVl6dTZNUiz2mwFmO4I6CEuNwlRTVKwqykZsN2W0yszriVj37T20vFz6g0CHTqVLpUenOOvPDIdE91IhER4vJ0l8pUVMyzMrGZfr9HdYlijXDhNS2dc14GTeYzVxVKFJmxgjOsbjF07pqL3SqpplhZsRqbLXpbavPdcSkrP4RWz/atW/lNfaWX12P/AKHQvPOk8Njnefm2RbmbWWNStmnUKpwpUs2iF6STejDSYl2pLGqahZo8Kmnums9NXUklMkbKzGA2a9rzDKKiDbVxwJNSoLZk5FeiaSej6i1EOkiHUOrhcZXoaxVXDIYFj1i8mrkTrij9nunWrw26bGpdGmvWfSJkivyh04OPRQYDAuW4Wrplp5KrkrI1XDGSao1roYo2emTbY4xtJbTlJzvsihUeHS50KZBlDIkPStz0uelkJadJcrFYlTMs2HCRi832O6UywKrYlI6rCISZmyfZrb/fCN5QVWm8pmUfaY+YuVHsKUHpJfQeWketUP3gPFwQqKis4uutdffJ/wAdRmTfY843Pts3MxqCtGtCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAzNk+zW3++EbygqtN5TMo+0x8xcqPYUoPSS+g8tI9aofvAeLghUVT7StvHbOeN2xMRIQKYUhvV2wucIfGUdqFwysefL7FkXGXRo4jmaxjRBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAbvkjbh3Zm1adNAMS3WoNEWntREtRF9VXY1xSKptrRD1iuhj9ot4H1FJj0UeSketUP3gPFwQHAtqbZlazogNVmjE3GuuI3ubZOFpblNfzZF2pejwSWHU0+b+ld4iN9sa3RMyPZkUgLc2U15WhIcZq9t1KHiBacSxjEQ/KHgrT6YpF3lOP1FsrKdsMsbGu7y1DmMnwRK3hYw8mXhG8tQ5jJ8ESYWGTLwjeWocxk+CJMLDJl4T63mqPMJPgiTCwyZeE+d5ahzGT4IkwsMmXhG8tQ5jJ8ESYWGTLwn1vNUeYSfBEmFhky8J87y1DmMnwRJhYZMvCN5ahzGT4IkwsMmXhG8tQ5jJ8ESYWGTLwjeWocxk+CJMLDJl4RvLUOYyfBEmFhky8I3lqHMZPgiTCwyZeEby1DmMnwRJhYZMvCfW81R5hJ8ESYWGTLwnzvLUOYyfBEmFhky8J9bzVHmEnwRJhYZMvCN5qjzCT4IkwsMmXhG81R5hJ8ESYWGTLwjeao8wk+CJMLDJl4RvNUeYSfBEmFhky8I3mqPMJPgiTCwyZeEbzVHmEnwRJhYZMvCN5qjzCT4IkwsMmXhPneWocxk+CJMLDJl4RvLUOYyfBEmFhky8I3lqHMZPgiTCwyZeEby1DmMnwRJhYZMvCfW81R5hJ8ESYWGTLwnzvLUOYyfBEmFhky8I3lqHMZPgiTCwyZeEby1DmMnwRJhYZMvCN5ahzGT4IkwsMmXhG8tQ5jJ8ESYWGTLwjeWocxk+CJMLDJl4RvLUOYyfBEmFhky8I3lqHMZPgiTCwyZeEby1DmMnwRJhYZMvCN5ahzGT4IkwsMmXhM5b2V933XIbapNuVKYRlpEm4xaflcVVLHI26plQ26rqGwxxsTy2U9l0co2CuO4dD91yWsW8Gxx1Nw28eMI8oy7Yv7luKamydrTvHXLBYVtq583rPlJLrOJoeOketUP3gPFwQHsQH5ONA8GkxwIfyFghTp0Yj8N7YfNWvB4L50FGBOE/u9sTmrHg8F96D7lpwje2JzVjweCdAy04RvbE5qx4PBOgZacI3tic1Y8HgnQMtOEb2xOaseDwToGWnCN7YnNWPB4J0DLThG9sPmrPyME6D5lpwje2JzVjweCdB9y04RvbE5qx4PBOgZacI3tic1Y8HgnQMtOEb2xOaseDwToGWnCN7YnNWPB4J0DLThG9sTmrHg8E6Blpwje2JzVjweCdAy04RvbE5qx4PBOgZacI3tic1Y8HgnQMtOEb2xOaseDwToGWnCN7YnNWPB4J0DLThG9sTmrHg8E6Blpwje2JzVjweCdAy04RvbE5qx4PBOgZacI3tic1Y8HgnQMtOEb2xOaseDwToGWnCN7YfNWfkYJ0HzLThG9sTmrHg8E6D7lpwje2JzVjweCdAy04RvbE5qx4PBOgZacI3th81Z+RgnQfMtOEb2xOaseDwToPuWnCN7YnNWPB4J0DLThG9sTmrHg8E6Blpwje2JzVjweCdAy04RvbD5qz8jBOg+ZacI3tic1Y8HgnQfctOEb2xOaseDwToGWnCN7YnNWPB4J0DLThG9sTmrHg8E6Blpwje2HzVn5GCdB8y04T9mWQaDS2AgP5BHpIV6NHQfqhUEB/9k=" width="22" /> R7 Enns BICYCLE TRAIL<br />\n<br />\n<strong>(at all apartments are available MTB maps of Schladming-Dachstein region 1:50.000, on which are referred our Numbers on map)</strong></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num1.png" style="width: 20px; height: 20px;" />&nbsp;CIRCUIT Kaibling - orange color (Map No. 06)</p>\n\n<p>Length of 28.1 km, vertical rise 834 m (15.3 km asphalt; 12.3 km gravel; 0.5 kilometers others)</p>\n\n<p><img alt="" class="full-width-img" src="http://alpineliving.cz/media/userfiles/kolo/hode1.png" style="width: 784px; height: 25px;" /></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num2.png" style="width: 20px; height: 20px;" />&nbsp;STORM CIRCUIT ALM - blue color (Map No. 07)</p>\n\n<p>Length of 42.1 km, vertical rise 1320 m (10.2 km asphalt; 31.4 km gravel; 0.5 kilometers others)</p>\n\n<p><img alt="" class="full-width-img" src="http://alpineliving.cz/media/userfiles/kolo/hode2.png" style="width: 784px; height: 25px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/kolo/mapa1b2.jpg"><img alt="" class="full-width-img" src="http://alpineliving.cz/media/userfiles/kolo/mapa1a2.jpg" style="width: 400px; height: 420px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num3.png" style="width: 20px; height: 20px;" />&nbsp;CIRCUIT Viehberg (Map No. 12)</p>\n\n<p>Length of 41.6 km, vertical rise 1009 m (21.2 km asphalt; 20.4 km gravel)</p>\n\n<p><img alt="" class="full-width-img" src="http://alpineliving.cz/media/userfiles/kolo/hode3.png" style="width: 783px; height: 25px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/kolo/mapa3.jpg"><img alt="" class="full-width-img" src="http://alpineliving.cz/media/userfiles/kolo/map3.jpg" style="width: 400px; height: 463px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num4.png" style="width: 20px; height: 20px;" />&nbsp;Relaxing Untertal - green color (Map No. 18)</p>\n\n<p>Length of 28.1 km, vertical rise 96 m (22.3 km asphalt; 3.2 km gravel) - (by cable car to the PLANAI halfway station and directly down the hill!)</p>\n\n<p><img alt="" class="full-width-img" src="http://alpineliving.cz/media/userfiles/kolo/hode4.png" style="width: 784px; height: 25px;" /></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num5.png" style="width: 20px; height: 20px;" />&nbsp;Relaxing Röhrmoos - violet color (Map No. 19)</p>\n\n<p>Length of 37.2 km, vertical rise 372 m (29.5 km asphalt; 4.3 km gravel, 3.4 km single track) - (by cable car to the PLANAI and directly down the hill!)</p>\n\n<p><img alt="" class="full-width-img" src="http://alpineliving.cz/media/userfiles/kolo/hode5.png" style="width: 784px; height: 25px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/kolo/mapa4b5.jpg"><img alt="" class="full-width-img" src="http://alpineliving.cz/media/userfiles/kolo/mapa4a5.jpg" style="width: 400px; height: 303px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num6.png" style="width: 20px; height: 20px;" />&nbsp;CIRCUIT AICH - blue color (Map No. 09)</p>\n\n<p>Length of 28.8 km, vertical rise 816 m (26.1 km asphalt, gravel 1.9 km, 0.8 km of other)</p>\n\n<p><img alt="" class="full-width-img" src="http://alpineliving.cz/media/userfiles/kolo/hode6.png" style="width: 784px; height: 26px;" /></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num7.png" style="width: 20px; height: 20px;" />&nbsp;CIRCUIT STRUBSCHLUCHT &ndash; red color (Map No. 10)</p>\n\n<p>Length of 35.4 km, vertical rise 1.271 m (31.9 km asphalt; 1.8 km gravel; 1.7 kilometers others)</p>\n\n<p><img alt="" class="full-width-img" src="http://alpineliving.cz/media/userfiles/kolo/hode7.png" style="width: 784px; height: 25px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/kolo/mapa6b7.jpg"><img alt="" class="full-width-img" src="http://alpineliving.cz/media/userfiles/kolo/mapa6a7.jpg" style="width: 400px; height: 364px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num8.png" style="width: 20px; height: 20px;" />&nbsp;CIRCUIT REITERALM - orange color (Map No. 03)</p>\n\n<p>Length of 43.4 km, vertical rise 1.270 m (22.6 km asphalt; 20.8 km gravel)</p>\n\n<p><img alt="" class="full-width-img" src="http://alpineliving.cz/media/userfiles/kolo/hode8.png" style="width: 784px; height: 26px;" /></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num9.png" style="width: 20px; height: 20px;" />&nbsp;CIRCUIT Röhrmoos - blue color (Map No. 04)</p>\n\n<p>Length of 41.9 km, vertical rise 828 m (30.8 km asphalt; 11.1 km gravel)</p>\n\n<p><img alt="" class="full-width-img" src="http://alpineliving.cz/media/userfiles/kolo/hode9.png" style="width: 784px; height: 26px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/kolo/mapa8b9.jpg"><img alt="" class="full-width-img" src="http://alpineliving.cz/media/userfiles/kolo/mapa8a9.jpg" style="width: 400px; height: 458px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num10.png" style="width: 20px; height: 20px;" />&nbsp;PANORAMIC CIRCUIT (Map No. 05)</p>\n\n<p>Length of 56.6 km, vertical rise 1.311 m (31.7 km asphalt, gravel 22.3 km, 2.6 km others)</p>\n\n<p><img alt="" class="full-width-img" src="http://alpineliving.cz/media/userfiles/kolo/hode10.png" style="width: 784px; height: 25px;" /></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/kolo/mapa10.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/kolo/map10.jpg" class="full-width-img" style="width: 400px; height: 296px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://www.alpineliving.cz/media/userfiles/Obrazky-mix/R7%20logo.jpg" style="width: 50px; height: 50px;" />R7 Enns BICYCLE TRAIL<br />\n<br />\n<img alt="" class="full-width-img" src="http://alpineliving.cz/media/userfiles/kolo/R777e.png" style="width: 784px; height: 25px;" /><br />\n<br />\nFor a relaxing biking recommend R7 bike path, which passes through the village HAUS. Section Radstadt-Schladming-Haus-Oblarn-Irdning offers undemanding cycling suitable for families with children. The trail is very well marked, we recommend visiting of the local pubs and restaurants where you can taste specific local cuisine.<br />\n<br />\n&nbsp;<img alt="" src="http://www.alpineliving.cz/media/userfiles/kolo/r7.jpg" style="width: 200px; height: 150px;" />&nbsp;<img alt="" src="http://www.alpineliving.cz/media/userfiles/kolo/1.jpg" style="width: 200px; height: 150px;" /> <img alt="" src="http://www.alpineliving.cz/media/userfiles/kolo/2.jpg" style="width: 225px; height: 150px;" /> <img alt="" src="http://www.alpineliving.cz/media/userfiles/kolo/4.jpeg" style="width: 226px; height: 150px;" /></p>\n', '');
INSERT INTO `page_data` (`id`, `page_id`, `language_id`, `route_id`, `nazev`, `nadpis`, `uvodni_popis`, `popis`, `url`) VALUES
(43, 24, 4, 113, 'Swimming', 'Swimming - map of famous targets', '', '<p><img alt="" src="http://alpineliving.cz/media/userfiles/koupani/prehledova%20mapa%20koupani.jpg" class="full-width-img" style="width: 900px; height: 437px;" /></p>\n\n<p>&nbsp;</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num1.png" style="width: 20px; height: 20px;" />&nbsp;SWIMMING POOL HAUS<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num2.png" style="width: 20px; height: 20px;" />&nbsp;PANORAMA BAD GRÖBMING<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num3.png" style="width: 20px; height: 20px;" />&nbsp;NATURAL SWIMMING POOL AICH<br />\n<img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num4.png" style="width: 20px; height: 20px;" />&nbsp;SPA SCHLADMING</p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num1.png" style="width: 20px; height: 20px;" />&nbsp;SWIMMING POOL HAUS<br />\nHeated swimming pool and beach-volleyball court (the distance from the apartment 500m from the town center by underpass under the main road B320 and you are right by the swimming pool) <a href="http://www.haus.at/de/infrastruktur/freizeit-sport.php">http://www.haus.at/de/infrastruktur/freizeit-sport.php</a></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/koupani/1.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/1.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/11.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/11.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/111.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/111.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/1111.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/1111.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/11111.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/11111.jpg" style="width: 175px; height: 131px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num2.png" style="width: 20px; height: 20px;" />&nbsp;PANORAMA BAD GRÖBMING<br />\nDistance from Haus 12 km to the village Gröbming. (HAUS-AICH-Grobming). In Gröbming is arrival marked the PANORAMA BAD Gröbming. Modern facility, opened in 2010, swimming pool, whirlpool, sauna world, wading pool, jumping tower, all with spectacular views to the top STODERZINKEN.<a href="http://www.groebming.at/panoramabad/index.php">http://www.groebming.at/panoramabad/index.php</a></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/koupani/2.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/2.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/22.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/22.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/222.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/222.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/2222.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/2222.jpg" style="width: 175px; height: 73px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/22222.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/22222.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;</p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num3.png" style="width: 20px; height: 20px;" />&nbsp;NATURAL SWIMMING POOL AICH<br />\nNatural swimming pool in the village of Aich. From apartment only 5 min by car (HAUS - AICH). Beach-volleyball court, clay tennis courts, table tennis, water trampoline, restaurant.<a href="http://www.aich.at/freizeit.php">http://www.aich.at/freizeit.php</a></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/koupani/3.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/3.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/33.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/33.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/333.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/333.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/3333.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/3333.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/33333.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/33333.jpg" style="width: 175px; height: 131px;" /></a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num4.png" style="width: 20px; height: 20px;" />&nbsp;SPA SCHLADMING<br />\nDistance from Haus to Schladming 5 km. (HAUS-Schladming, exit from B320 SCHLADMING OST, turn right, then underpass). Winter and summer baths, whirlpools, sauna world, wading pool, water toboggans, water slides, fitness-studio, all with spectacular views to the surrounding mountains.<a href="http://www.erlebnisbad-schladming.at/">http://www.erlebnisbad-schladming.at/</a></p>\n\n<p><a href="http://alpineliving.cz/media/userfiles/koupani/4.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/4.jpg" style="width: 175px; height: 131px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/44.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/44.jpg" style="width: 175px; height: 113px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/444.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/444.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/4444.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/4444.jpg" style="width: 175px; height: 117px;" /></a>&nbsp;<a href="http://alpineliving.cz/media/userfiles/koupani/44444.jpg"><img alt="" src="http://alpineliving.cz/media/userfiles/koupani-male/44444.jpg" style="width: 175px; height: 116px;" /></a></p>\n', ''),
(44, 26, 4, 114, 'Tennis', 'TENNIS COURTS HAUS IM ENNSTAL', '', '<p><img alt="" src="http://www.alpineliving.cz/media/userfiles/Obrazky-mix/tenis.jpg" class="full-width-img" style="width: 900px; height: 437px;" /></p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num1.png" style="width: 20px; height: 20px;" /> TENNIS COURTS HAUS IM ENNSTAL,Sportplatz, 8967 Haus im Ennstal</p>\n\n<p>Phone: +43 3686 2560<br />\n<a href="mailto:tennis@sportunionhaus.at">tennis@sportunionhaus.at</a><br />\n<a href="http://www.sportunionhaus.at" target="_blank">www.sportunionhaus.at<br />\n<img alt="" src="http://www.alpineliving.cz/media/userfiles/Obrazky-mix/1.jpeg" style="width: 300px; height: 200px;" /><img alt="" src="http://www.alpineliving.cz/media/userfiles/Obrazky-mix/2.jpeg" style="width: 300px; height: 200px;" /><img alt="" src="http://www.alpineliving.cz/media/userfiles/Obrazky-mix/3.JPG" style="width: 301px; height: 200px;" /></a><br />\n&nbsp;</p>\n', ''),
(45, 4, 4, 115, 'Photo Gallery', 'Photo Gallery', '', NULL, ''),
(46, 18, 4, 125, 'Web cameras', 'Web cameras', '', '<p><strong>Next web cameras</strong></p>\n\n<p><a href="http://www.hauser-kaibling.at/de/wetter-schnee/webcams/"><img alt="" src="http://alpine.dgsbeta.cz/media/userfiles/Obrazky-mix/cam7.jpg" style="width: 400px; height: 300px;" /></a><br />\n<a href="http://www.hauser-kaibling.at/de/wetter-schnee/webcams/">More..</a></p>\n\n<p>View on the pistes: <a href="http://www.planai.at/winter/en/Home.html">PLANAI TOP STATION, DACHSTEIN a SCHLADMING-DACHSTEIN</a></p>\n', ''),
(47, 20, 4, 126, 'Weather', 'Weather', '', '<h2>WEATHER ON THE PISTES IN HAUSER KAIBLING ( 2015 masl)</h2>\n<link href="http://www.snow-forecast.com/stylesheets/feed.css" media="screen" rel="stylesheet" type="text/css" />\n<div id="wf-weatherfeed"><iframe allowtransparency="true" frameborder="0" height="272" marginheight="0" marginwidth="0" scrolling="no" src="http://www.snow-forecast.com/resorts/Haus/forecasts/feed/top/m" style="overflow:hidden;border:none;" width="469">&amp;amp;amp;amp;amp;amp;amp;amp;amp;lt;p&amp;amp;amp;amp;amp;amp;amp;amp;amp;gt;Your browser does not support iframes.&amp;amp;amp;amp;amp;amp;amp;amp;amp;lt;/p&amp;amp;amp;amp;amp;amp;amp;amp;amp;gt;</iframe>\n<div id="wf-link"><a href="http://www.snow-forecast.com/"><img alt="Snow Forecast" src="http://www.snow-forecast.com/images/feed/snowlogo-150.png" /></a>\n<p id="cmt">View detailed snow forecast for <a href="http://www.snow-forecast.com/resorts/Haus?utm_source=embeddable&amp;utm_medium=widget&amp;utm_campaign=Haus">Hauser Kaibling</a> at:<br />\n<a href="http://www.snow-forecast.com/resorts/Haus?utm_source=embeddable&amp;utm_medium=widget&amp;utm_campaign=Haus"><strong>snow-forecast.com</strong></a></p>\n\n<div style="clear: both;">&nbsp;</div>\n</div>\n</div>\n\n<h2>WEATHER IN HAUS VILLAGE (752 masl)</h2>\n<link href="http://www.snow-forecast.com/stylesheets/feed.css" media="screen" rel="stylesheet" type="text/css" />\n<div id="wf-weatherfeed"><iframe allowtransparency="true" frameborder="0" height="272" marginheight="0" marginwidth="0" scrolling="no" src="http://www.snow-forecast.com/resorts/Haus/forecasts/feed/bot/m" style="overflow:hidden;border:none;" width="469">&amp;amp;amp;amp;amp;amp;amp;lt;p&amp;amp;amp;amp;amp;amp;amp;gt;Your browser does not support iframes.&amp;amp;amp;amp;amp;amp;amp;lt;/p&amp;amp;amp;amp;amp;amp;amp;gt;</iframe>\n<div id="wf-link"><a href="http://www.snow-forecast.com/"><img alt="Snow Forecast" src="http://www.snow-forecast.com/images/feed/snowlogo-150.png" /></a>\n<p id="cmt">View detailed snow forecast for <a href="http://www.snow-forecast.com/resorts/Haus?utm_source=embeddable&amp;utm_medium=widget&amp;utm_campaign=Haus">Hauser Kaibling</a> at:<br />\n<a href="http://www.snow-forecast.com/resorts/Haus?utm_source=embeddable&amp;utm_medium=widget&amp;utm_campaign=Haus"><strong>snow-forecast.com</strong></a></p>\n\n<div style="clear: both;">&nbsp;</div>\n</div>\n</div>\n', ''),
(48, 25, 4, 127, 'Downloads', 'Downloads', '', '<p>Arrival information for apartment ALPINE LIVING-RAMSAU B9&nbsp;<a href="http://www.alpineliving.cz/media/userfiles/prijezdove%20informace/information%20apartment%20RAMSAU.pdf"><img alt="" src="http://alpineliving.cz/media/userfiles/Obrazky-mix/PDF%20LOGO%20small.png" style="width: 40px; height: 42px;" /></a></p>\n\n<p>Arrival information for apartment ALPINE LIVING-KABLING B10&nbsp;<a href="http://www.alpineliving.cz/media/userfiles/prijezdove%20informace/information%20apartment%20KAIBLING.pdf"><img alt="" src="http://alpineliving.cz/media/userfiles/Obrazky-mix/PDF%20LOGO%20small.png" style="width: 40px; height: 42px;" /></a></p>\n\n<p>Arrival information for apartment ALPINE LIVING-ENNSTALBLICK B15&nbsp;<a href="http://www.alpineliving.cz/media/userfiles/prijezdove%20informace/information%20apartment%20ENNSTALBLICK.pdf"><img alt="" src="http://alpineliving.cz/media/userfiles/Obrazky-mix/PDF%20LOGO%20small.png" style="width: 40px; height: 42px;" /></a></p>\n\n<p>____________________________________________________________________________________________</p>\n\n<p>ENNS BICYCLE TRAIL R7&nbsp; <a href="http://www.alpineliving.cz/media/userfiles/kolo/ennsradweg_2014.pdf"><img alt="" src="http://alpineliving.cz/media/userfiles/Obrazky-mix/PDF%20LOGO%20small.png" style="width: 40px; height: 42px;" /></a></p>\n\n<p>WATERFALLS WILDE WASSER&nbsp; <a href="http://www.alpineliving.cz/media/userfiles/turistika/wildewasser_prospekt_neu.pdf"><img alt="" src="http://alpineliving.cz/media/userfiles/Obrazky-mix/PDF%20LOGO%20small.png" style="width: 40px; height: 42px;" /></a></p>\n\n<p>TOURIST CIRCUIT&nbsp;SCHAFSINN &nbsp;&nbsp;<a href="http://www.alpineliving.cz/media/userfiles/prijezdove%20informace/Schafsinn%20rundweg.pdf"><img alt="" src="http://alpineliving.cz/media/userfiles/Obrazky-mix/PDF%20LOGO%20small.png" style="width: 40px; height: 42px;" /></a></p>\n', ''),
(49, 11, 4, 128, 'Contacts', 'Contacts', '', '<ul>\n	<li>ALPINE LIVING**** APARTMENTS &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; NEED HELP?<br />\n	Klosterhügel 74 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Sales Manager: Bc. Adéla Běťáková<br />\n	Haus im Ennstal, Austria<img alt="" src="http://alpineliving.cz/media/userfiles/Obrazky-mix/fotos.jpg" style="float: right; position: relative; right: 500px; width: 82px; height: 100px;" /></li>\n	<li>phone: + 420&nbsp;605 238 595</li>\n	<li>e-mail:&nbsp; <a href="mailto:rezervace@alpineliving.cz">rezervace@alpineliving.cz</a></li>\n	<li>GPS:&nbsp; <span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:black">47.408361, 13.767178</span></li>\n</ul>\n', ''),
(50, 27, 1, 132, 'Rafty - kajak', 'Rafty - kajak', '', '<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num1.png" style="line-height: 20.7999992370605px; width: 20px; height: 20px;" /><span style="line-height: 20.7999992370605px;">&nbsp;</span>RAFTING NA ŘECE ENNS<br />\n<br />\n<a href="http://www.alpineliving.cz/media/userfiles/raft/raft3.jpg" style="line-height: 1.6em;"><img alt="" src="http://www.alpineliving.cz/media/userfiles/raft/raft3.jpg" style="width: 300px; height: 131px;" /></a>&nbsp;<a href="http://www.alpineliving.cz/media/userfiles/raft/raft2.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/raft/raft2.jpg" class="full-width-img" style="width: 397px; height: 131px;" /></a>&nbsp;<a href="http://www.alpineliving.cz/media/userfiles/raft/raft4.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/raft/raft4.jpg" style="width: 197px; height: 131px;" /></a></p>\n\n<p>Poznejte krásy i záludnosti&nbsp;divoké řeky ENNS. Rafting probíhá v úseku MANDLING - PICHL - SCHLADMING - HAUS. Doporučujeme&nbsp;jak pro děti i dospělé. Rezervace a další informace :&nbsp;<a href="http://www.bac.at/rafting/rafting_schladming/rafting_enns/">http://www.bac.at/rafting/rafting_schladming/rafting_enns/</a></p>\n\n<hr />\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/turistika/num2.png" style="line-height: 20.7999992370605px; width: 20px; height: 20px;" /><span style="line-height: 20.7999992370605px;">&nbsp;KAJAK NA ŘECE ENNS - PŮJČOVNA KAJAKŮ, KURZY S INSTRUKTOREM</span></p>\n\n<p><span style="line-height: 20.7999992370605px;"><a href="http://www.alpineliving.cz/media/userfiles/raft/kajak1.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/raft/kajak1.jpg" style="width: 200px; height: 133px;" /></a>&nbsp;<a href="http://www.alpineliving.cz/media/userfiles/raft/kayjak2.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/raft/kayjak2.jpg" style="width: 236px; height: 133px;" /></a>&nbsp;<a href="http://www.alpineliving.cz/media/userfiles/raft/kajak4.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/raft/kajak4.jpg" style="width: 177px; height: 133px;" /></a>&nbsp;<a href="http://www.alpineliving.cz/media/userfiles/raft/kajak5.jpg"><img alt="" src="http://www.alpineliving.cz/media/userfiles/raft/kajak5.jpg" style="width: 177px; height: 133px;" /></a>&nbsp;</span></p>\n\n<p>Výborně vybavenou půjčovnu kajaků naleznete přímo v HAUSu na adrese:</p>\n\n<p><span class="textklein">Helmut Knauß<br />\nMarktstraße 213/9<br />\n8967 Haus im Ennstal<br />\n<strong>Tel.: 0650 / 42 14 383 </strong><br />\ninfo@kajaktiv.at<br />\n<a href="http://www.kajaktiv.at">www.kajaktiv.at</a><br />\n<br />\nJe možné také objednat průvodce, nebo pro úplné začátečníky krátké či vícedenní kurzy.</span></p>\n', ''),
(52, 29, 1, 134, 'Prohlídka', 'Prohlídka', '', '<p><iframe frameborder="0" height="510" src="http://www.alpineliving.cz/media/userfiles/vp/VirtualniProhlidkaAlpineLiving.html" style="border:0" width="700"></iframe></p>\n', ''),
(53, 30, 1, 136, 'SOMMERCARD', 'SOMMERCARD - BONUSOVÁ LETNÍ KARTA', '', '<p>Naši klienti mají v letní sezoně (od 20.5.2016 do 16.10.2016) k dispozici ZDARMA letní kartu SOMMERCARD.<br />\n<strong>Karta SOMMERCARD Vás opravňuje k bezplatnému užívání lanovek, skibusů, vstupů do přírodních parků, bazénů nebo přírodních koupališť&nbsp;apod.&nbsp;(karta je zatím k dispozici pro klienty apartmánů RAMSAU a KAIBLING)</strong></p>\n\n<p>Seznam více jak&nbsp;100 prázdninových cílů naleznete zde: (po kliknutí na kartu SOMMERCARD)</p>\n\n<p><a href="http://www.sommercard.info/en/angebote?type%5B%5D=inc&amp;cat%5B%5D=8&amp;cat%5B%5D=9&amp;cat%5B%5D=10&amp;cat%5B%5D=11&amp;cat%5B%5D=12&amp;cat%5B%5D=13&amp;cat%5B%5D=14&amp;cat%5B%5D=15&amp;cat%5B%5D=16&amp;cat%5B%5D=17&amp;from=&amp;to=&amp;query=search+term"><img alt="" src="http://www.alpineliving.cz/media/userfiles/SOMMERCARD/sommercard%20logo.jpg" style="width: 345px; height: 218px;" /></a><br />\n&nbsp;</p>\n', '');

-- --------------------------------------------------------

--
-- Struktura tabulky `page_photos`
--

CREATE TABLE IF NOT EXISTS `page_photos` (
  `id` int(11) NOT NULL,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `page_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `page_photo_data`
--

CREATE TABLE IF NOT EXISTS `page_photo_data` (
  `id` int(11) NOT NULL,
  `page_photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL,
  `poradi` int(11) NOT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `class` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_categories`
--

CREATE TABLE IF NOT EXISTS `product_categories` (
  `id` int(11) NOT NULL,
  `poradi` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `class` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `priorita` int(11) NOT NULL,
  `show_prod` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_categories_products`
--

CREATE TABLE IF NOT EXISTS `product_categories_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_category_data`
--

CREATE TABLE IF NOT EXISTS `product_category_data` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `product_category_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nazev_full` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `nazev_full2` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `nadpis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `zobrazit` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_category_photos`
--

CREATE TABLE IF NOT EXISTS `product_category_photos` (
  `id` int(11) NOT NULL,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `product_category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_category_photo_data`
--

CREATE TABLE IF NOT EXISTS `product_category_photo_data` (
  `id` int(11) NOT NULL,
  `product_category_photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_data`
--

CREATE TABLE IF NOT EXISTS `product_data` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `parametry` text COLLATE utf8_czech_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_files`
--

CREATE TABLE IF NOT EXISTS `product_files` (
  `id` int(11) NOT NULL,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `file_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_file_data`
--

CREATE TABLE IF NOT EXISTS `product_file_data` (
  `id` int(11) NOT NULL,
  `product_file_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_photos`
--

CREATE TABLE IF NOT EXISTS `product_photos` (
  `id` int(11) NOT NULL,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_photo_data`
--

CREATE TABLE IF NOT EXISTS `product_photo_data` (
  `id` int(11) NOT NULL,
  `product_photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `references`
--

CREATE TABLE IF NOT EXISTS `references` (
  `id` int(11) NOT NULL,
  `poradi` int(11) NOT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

--
-- Vypisuji data pro tabulku `references`
--

INSERT INTO `references` (`id`, `poradi`, `photo_src`, `available_languages`) VALUES
(1, 4, '', 5),
(2, 8, '', 5),
(3, 5, '', 5),
(4, 6, '', 5),
(5, 3, '', 5),
(6, 9, '', 5),
(7, 7, '', 5),
(8, 2, '', 5),
(9, 1, '', 5);

-- --------------------------------------------------------

--
-- Struktura tabulky `reference_data`
--

CREATE TABLE IF NOT EXISTS `reference_data` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `reference_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `on_homepage` tinyint(4) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

--
-- Vypisuji data pro tabulku `reference_data`
--

INSERT INTO `reference_data` (`id`, `language_id`, `reference_id`, `route_id`, `nazev`, `uvodni_popis`, `popis`, `on_homepage`) VALUES
(1, 1, 1, 78, 'Apartmány Exterier', '', NULL, 0),
(2, 0, 0, 0, '', NULL, NULL, 0),
(3, 1, 2, 80, 'APARTMÁN KAIBLING', '', NULL, 0),
(4, 1, 3, 85, 'Obec HAUS', '', NULL, 0),
(5, 1, 4, 86, 'Lyžování HAUSER KAIBLING', '', NULL, 0),
(6, 1, 5, 88, 'Úvodní ', '', NULL, 1),
(7, 1, 6, 94, 'APARTMÁN RAMSAU', '', NULL, 0),
(8, 1, 7, 95, 'APARTMÁN ENNSTALBLICK', '', NULL, 0),
(9, 1, 8, 96, 'LÉTO HAUS - SCHLADMING', '', NULL, 0),
(10, 1, 9, 97, 'DACHSTEIN', '', NULL, 0),
(11, 4, 6, 116, 'APARTMENT RAMSAU', '', NULL, 0),
(12, 4, 2, 117, 'APARTMENT KAIBLING', '', NULL, 0),
(13, 4, 7, 118, 'APARTMENT ENNSTALBLICK', '', NULL, 0),
(14, 4, 4, 119, 'Skiing HAUSER KAIBLING', '', NULL, 0),
(15, 4, 3, 120, 'HAUS village', '', NULL, 0),
(16, 4, 1, 121, 'Apartments Exterier', '', NULL, 0),
(17, 4, 8, 122, 'SUMMER HAUS - SCHLADMING', '', NULL, 0),
(18, 4, 9, 123, 'DACHSTEIN', '', NULL, 0),
(19, 4, 5, 124, 'Main', '', NULL, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `reference_photos`
--

CREATE TABLE IF NOT EXISTS `reference_photos` (
  `id` int(11) NOT NULL,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `reference_id` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=157 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

--
-- Vypisuji data pro tabulku `reference_photos`
--

INSERT INTO `reference_photos` (`id`, `poradi`, `zobrazit`, `photo_src`, `ext`, `reference_id`) VALUES
(29, 8, 1, '15', 'jpg', 1),
(30, 9, 1, '14', 'jpg', 1),
(26, 6, 1, '17', 'jpg', 1),
(27, 4, 1, '11_1', 'jpg', 1),
(28, 7, 1, '16', 'jpg', 1),
(22, 3, 1, '10', 'jpg', 1),
(23, 5, 1, '12_1', 'jpg', 1),
(13, 5, 1, '1', 'jpg', 2),
(14, 6, 1, '3', 'jpg', 2),
(15, 7, 1, '2', 'jpg', 2),
(16, 8, 1, '4', 'jpg', 2),
(17, 9, 1, '5', 'jpg', 2),
(18, 10, 1, '6', 'jpg', 2),
(19, 11, 1, '7', 'jpg', 2),
(152, 17, 1, 'dsc_0803', 'jpg', 3),
(151, 16, 1, 'dsc_0664', 'jpg', 3),
(35, 5, 1, '24', 'jpg', 3),
(144, 9, 1, 'dsc_0660', 'jpg', 3),
(96, 2, 1, '1459180_10152049748383394_1196730067_n', 'jpg', 4),
(95, 1, 1, '75967_10152310591073394_1977448993_n', 'jpg', 4),
(47, 4, 1, '1', 'jpg', 5),
(48, 3, 1, '2', 'jpg', 5),
(49, 5, 1, '3', 'jpg', 5),
(50, 2, 1, '13', 'jpg', 5),
(51, 6, 1, '5', 'jpg', 5),
(52, 1, 1, '14', 'jpg', 5),
(53, 1, 0, 'obyvaci-pokoj', 'jpg', 7),
(54, 2, 1, 'obyvaci-pokoj-apartman-ennstalblick', 'jpg', 7),
(55, 3, 1, 'koupelna-apartman-ennstalblick', 'jpg', 7),
(56, 4, 1, 'kuchyne-apartman-ennstalblick', 'jpg', 7),
(57, 5, 1, 'apartman-ennstalblick', 'jpg', 7),
(58, 6, 1, 'apartman-ennstalblick_1', 'jpg', 7),
(59, 7, 1, 'apartman-ennstalblick_2', 'jpg', 7),
(60, 1, 1, 'apartman-ramsau', 'jpg', 6),
(61, 2, 1, 'apartman-ramsau_1', 'jpg', 6),
(62, 3, 1, 'apartman-ramsau_2', 'jpg', 6),
(63, 4, 1, 'apartman-ramsau_3', 'jpg', 6),
(64, 5, 1, 'apartman-ramsau_4', 'jpg', 6),
(65, 6, 1, 'apartman-ramsau_5', 'jpg', 6),
(66, 7, 1, 'apartman-ramsau_6', 'jpg', 6),
(67, 8, 1, 'apartman-ramsau_7', 'jpg', 6),
(68, 1, 1, 'radfahren-dsc6841-original-139047', 'jpg', 8),
(69, 2, 1, 'leto-turistika', 'jpg', 8),
(70, 3, 1, 'leto-turistika_1', 'jpg', 8),
(72, 4, 1, 'leto-turistika_2', 'jpg', 8),
(73, 5, 1, 'leto-turistika_3', 'jpg', 8),
(74, 6, 1, 'leto-turistika_4', 'jpg', 8),
(75, 7, 1, 'leto-cykloturistika', 'jpg', 8),
(76, 8, 1, 'leto-cykloturistika_1', 'jpg', 8),
(77, 9, 1, 'leto-cykloturistika_2', 'jpg', 8),
(78, 10, 1, 'leto-cykloturistika_3', 'jpg', 8),
(79, 11, 1, 'leto-cykloturistika_4', 'jpg', 8),
(80, 12, 1, 'leto-cykloturistika_5', 'jpg', 8),
(81, 13, 1, 'leto-cykloturistika_6', 'jpg', 8),
(82, 1, 1, 'ledovec-dachstein', 'jpg', 9),
(83, 2, 1, 'ledovec-dachstein_1', 'jpg', 9),
(84, 3, 1, 'ledovec-dachstein_2', 'jpg', 9),
(85, 4, 1, 'ledovec-dachstein_3', 'jpg', 9),
(86, 5, 1, 'ledovec-dachstein_4', 'jpg', 9),
(87, 6, 1, 'ledovec-dachstein_5', 'jpg', 9),
(88, 7, 1, 'ledovec-dachstein_6', 'jpg', 9),
(89, 8, 1, 'ledovec-dachstein_7', 'jpg', 9),
(90, 9, 1, 'ledovec-dachstein_8', 'jpg', 9),
(91, 10, 1, 'ledovec-dachstein_9', 'jpg', 9),
(92, 11, 1, 'ledovec-dachstein_10', 'jpg', 9),
(93, 12, 1, 'ledovec-dachstein_11', 'jpg', 9),
(94, 13, 1, 'ledovec-dachstein_12', 'jpg', 9),
(98, 4, 1, '1455023_10152049748338394_476627559_n', 'jpg', 4),
(154, 19, 1, '793864_10152476135988394_287804570252473796_o', 'jpg', 3),
(145, 10, 1, 'dsc_0661', 'jpg', 3),
(101, 7, 1, '1414969_10152214233638394_1307600964_o', 'jpg', 4),
(153, 18, 1, 'dsc_0612', 'jpg', 3),
(103, 9, 1, '1385300_10152324831133394_612857134_n', 'jpg', 4),
(104, 10, 1, '1380265_10152324831228394_814783433_n', 'jpg', 4),
(105, 11, 1, '1173800_10152324830968394_418711594_n', 'jpg', 4),
(106, 12, 1, '1015751_10152235797393394_373191221_o', 'jpg', 4),
(107, 13, 1, '823453_10152047956258394_912608360_o', 'jpg', 4),
(108, 14, 1, '551406_10152049748468394_1175555449_n', 'jpg', 4),
(109, 15, 1, '1599795_10152205141008394_180426333_o', 'jpg', 4),
(146, 11, 1, 'dsc_0665', 'jpg', 3),
(111, 17, 1, '1549404_10152194895658394_236221469_n', 'jpg', 4),
(112, 18, 1, '1548144_10152268787638394_1805621711_o', 'jpg', 4),
(113, 19, 1, '1528644_10152194895473394_238888898_n', 'jpg', 4),
(114, 20, 1, '1523921_10152167663473394_1954625678_o', 'jpg', 4),
(115, 21, 1, '1522230_10152173966678394_233231698_n', 'jpg', 4),
(116, 22, 1, '1486825_10152049748693394_1450828400_n', 'jpg', 4),
(117, 23, 1, '1484164_10152049748513394_1172522424_n', 'jpg', 4),
(118, 24, 1, '1479525_10152056872678394_89852042_n', 'jpg', 4),
(119, 25, 1, '1479063_10152049748068394_942267244_n', 'jpg', 4),
(150, 15, 1, 'dsc_0805', 'jpg', 3),
(149, 14, 1, 'dsc_0811', 'jpg', 3),
(122, 28, 1, '1464049_10152049748048394_508419655_n', 'jpg', 4),
(148, 13, 1, 'dsc_0812', 'jpg', 3),
(124, 30, 1, '1978470_10152346247123394_677991656_o', 'jpg', 4),
(125, 31, 1, '1966258_10152291781153394_1978115638_o', 'jpg', 4),
(126, 32, 1, '1959223_10152324830428394_1929165706_n', 'jpg', 4),
(127, 33, 1, '1939876_10152310591278394_102375334_n', 'jpg', 4),
(128, 34, 1, '1932339_10152324830838394_1547372565_n', 'jpg', 4),
(129, 35, 1, '1922391_10152310590668394_208764547_n', 'jpg', 4),
(130, 36, 1, '1909141_10152343800148394_2047616867_o', 'jpg', 4),
(131, 37, 1, '1900717_10152327016588394_347812767_o', 'jpg', 4),
(132, 38, 1, '1888882_10152225102803394_722834787_o', 'jpg', 4),
(133, 39, 1, '1799977_10152233939448394_547599433_o', 'jpg', 4),
(134, 40, 1, '1779700_10152310590823394_1472209672_n', 'jpg', 4),
(135, 41, 1, '1654470_10152310591058394_1213871133_n', 'jpg', 4),
(136, 42, 1, '1796772_10152320252308394_14167900_o', 'jpg', 4),
(147, 12, 1, 'dsc_0808', 'jpg', 3),
(138, 44, 1, '10003953_10152314774578394_369758900_n', 'jpg', 4),
(139, 45, 1, '10003928_10152310590163394_537769322_n', 'jpg', 4),
(140, 46, 1, '10003834_10152338349158394_1025277976_o', 'jpg', 4),
(141, 47, 1, '1982318_10152324830408394_858467547_n', 'jpg', 4),
(142, 48, 1, '1979953_10152307943768394_1490385823_o', 'jpg', 4),
(143, 8, 1, 'loznice', 'jpg', 7),
(155, 20, 1, '1801165_10152533397053394_3811199087470408213_o', 'jpg', 3),
(156, 1, 1, '10369072_10152619346068394_7248242583379622654_o', 'jpg', 3);

-- --------------------------------------------------------

--
-- Struktura tabulky `reference_photo_data`
--

CREATE TABLE IF NOT EXISTS `reference_photo_data` (
  `id` int(11) NOT NULL,
  `reference_photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=158 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

--
-- Vypisuji data pro tabulku `reference_photo_data`
--

INSERT INTO `reference_photo_data` (`id`, `reference_photo_id`, `language_id`, `nazev`, `popis`) VALUES
(29, 29, 0, '15', ''),
(30, 30, 0, '14', ''),
(26, 26, 0, '17', ''),
(27, 27, 0, '11', ''),
(28, 28, 0, '16', ''),
(22, 22, 0, '10', ''),
(23, 23, 0, '12', ''),
(13, 13, 0, '1', ''),
(14, 14, 0, '3', ''),
(15, 15, 0, '2', ''),
(16, 16, 0, '4', ''),
(17, 17, 0, '5', ''),
(18, 18, 0, '6', ''),
(19, 19, 0, '7', ''),
(153, 152, 0, 'DSC_0803', ''),
(152, 151, 0, 'DSC_0664', ''),
(35, 35, 0, '24', ''),
(145, 144, 0, 'DSC_0660', ''),
(97, 96, 0, '1459180_10152049748383394_1196730067_n', ''),
(96, 95, 0, '75967_10152310591073394_1977448993_n', ''),
(47, 47, 0, '1', ''),
(48, 48, 0, '2', ''),
(49, 49, 0, '3', ''),
(50, 50, 0, '13', ''),
(51, 51, 0, '5', ''),
(52, 52, 0, '14', ''),
(53, 53, 0, 'Obývací pokoj', 'APARTMÁN ENNSTALBLICK'),
(54, 54, 0, 'Obývací pokoj APARTMÁN ENNSTALBLICK', ''),
(55, 53, 1, 'Obývací pokoj APARTMÁN ENNSTALBLICK', ''),
(56, 55, 0, 'Koupelna APARTMÁN ENNSTALBLICK', ''),
(57, 56, 0, 'Kuchyně APARTMÁN ENNSTALBLICK', ''),
(58, 57, 0, 'APARTMÁN ENNSTALBLICK', ''),
(59, 58, 0, 'APARTMÁN ENNSTALBLICK', ''),
(60, 59, 0, 'APARTMÁN ENNSTALBLICK', ''),
(61, 60, 0, 'APARTMÁN RAMSAU', ''),
(62, 61, 0, 'APARTMÁN RAMSAU', ''),
(63, 62, 0, 'APARTMÁN RAMSAU', ''),
(64, 63, 0, 'APARTMÁN RAMSAU', ''),
(65, 64, 0, 'APARTMÁN RAMSAU', ''),
(66, 65, 0, 'APARTMÁN RAMSAU', ''),
(67, 66, 0, 'APARTMÁN RAMSAU', ''),
(68, 67, 0, 'APARTMÁN RAMSAU', ''),
(69, 68, 0, 'radfahren-dsc6841-original-139047', ''),
(70, 69, 0, 'Léto turistika', 'Léto turistika'),
(71, 70, 0, 'Léto turistika', 'Léto turistika'),
(73, 72, 0, 'Léto turistika', 'Léto turistika'),
(74, 73, 0, 'Léto turistika', 'Léto turistika'),
(75, 74, 0, 'Léto turistika', 'Léto turistika'),
(76, 75, 0, 'Léto cykloturistika', 'Léto cykloturistika'),
(77, 76, 0, 'Léto cykloturistika', 'Léto cykloturistika'),
(78, 77, 0, 'Léto cykloturistika', 'Léto cykloturistika'),
(79, 78, 0, 'Léto cykloturistika', 'Léto cykloturistika'),
(80, 79, 0, 'Léto cykloturistika', 'Léto cykloturistika'),
(81, 80, 0, 'Léto cykloturistika', 'Léto cykloturistika'),
(82, 81, 0, 'Léto cykloturistika', 'Léto cykloturistika'),
(83, 82, 0, 'Ledovec DACHSTEIN', 'Ledovec DACHSTEIN'),
(84, 83, 0, 'Ledovec DACHSTEIN', 'Ledovec DACHSTEIN'),
(85, 84, 0, 'Ledovec DACHSTEIN', 'Ledovec DACHSTEIN'),
(86, 85, 0, 'Ledovec DACHSTEIN', 'Ledovec DACHSTEIN'),
(87, 86, 0, 'Ledovec DACHSTEIN', 'Ledovec DACHSTEIN'),
(88, 87, 0, 'Ledovec DACHSTEIN', 'Ledovec DACHSTEIN'),
(89, 88, 0, 'Ledovec DACHSTEIN', 'Ledovec DACHSTEIN'),
(90, 89, 0, 'Ledovec DACHSTEIN', 'Ledovec DACHSTEIN'),
(91, 90, 0, 'Ledovec DACHSTEIN', 'Ledovec DACHSTEIN'),
(92, 91, 0, 'Ledovec DACHSTEIN', 'Ledovec DACHSTEIN'),
(93, 92, 0, 'Ledovec DACHSTEIN', 'Ledovec DACHSTEIN'),
(94, 93, 0, 'Ledovec DACHSTEIN', 'Ledovec DACHSTEIN'),
(95, 94, 0, 'Ledovec DACHSTEIN', 'Ledovec DACHSTEIN'),
(99, 98, 0, '1455023_10152049748338394_476627559_n', ''),
(155, 154, 0, '793864_10152476135988394_287804570252473796_o', ''),
(146, 145, 0, 'DSC_0661', ''),
(102, 101, 0, '1414969_10152214233638394_1307600964_o', ''),
(154, 153, 0, 'DSC_0612', ''),
(104, 103, 0, '1385300_10152324831133394_612857134_n', ''),
(105, 104, 0, '1380265_10152324831228394_814783433_n', ''),
(106, 105, 0, '1173800_10152324830968394_418711594_n', ''),
(107, 106, 0, '1015751_10152235797393394_373191221_o', ''),
(108, 107, 0, '823453_10152047956258394_912608360_o', ''),
(109, 108, 0, '551406_10152049748468394_1175555449_n', ''),
(110, 109, 0, '1599795_10152205141008394_180426333_o', ''),
(147, 146, 0, 'DSC_0665', ''),
(112, 111, 0, '1549404_10152194895658394_236221469_n', ''),
(113, 112, 0, '1548144_10152268787638394_1805621711_o', ''),
(114, 113, 0, '1528644_10152194895473394_238888898_n', ''),
(115, 114, 0, '1523921_10152167663473394_1954625678_o', ''),
(116, 115, 0, '1522230_10152173966678394_233231698_n', ''),
(117, 116, 0, '1486825_10152049748693394_1450828400_n', ''),
(118, 117, 0, '1484164_10152049748513394_1172522424_n', ''),
(119, 118, 0, '1479525_10152056872678394_89852042_n', ''),
(120, 119, 0, '1479063_10152049748068394_942267244_n', ''),
(151, 150, 0, 'DSC_0805', ''),
(150, 149, 0, 'DSC_0811', ''),
(123, 122, 0, '1464049_10152049748048394_508419655_n', ''),
(149, 148, 0, 'DSC_0812', ''),
(125, 124, 0, '1978470_10152346247123394_677991656_o', ''),
(126, 125, 0, '1966258_10152291781153394_1978115638_o', ''),
(127, 126, 0, '1959223_10152324830428394_1929165706_n', ''),
(128, 127, 0, '1939876_10152310591278394_102375334_n', ''),
(129, 128, 0, '1932339_10152324830838394_1547372565_n', ''),
(130, 129, 0, '1922391_10152310590668394_208764547_n', ''),
(131, 130, 0, '1909141_10152343800148394_2047616867_o', ''),
(132, 131, 0, '1900717_10152327016588394_347812767_o', ''),
(133, 132, 0, '1888882_10152225102803394_722834787_o', ''),
(134, 133, 0, '1799977_10152233939448394_547599433_o', ''),
(135, 134, 0, '1779700_10152310590823394_1472209672_n', ''),
(136, 135, 0, '1654470_10152310591058394_1213871133_n', ''),
(137, 136, 0, '1796772_10152320252308394_14167900_o', ''),
(148, 147, 0, 'DSC_0808', ''),
(139, 138, 0, '10003953_10152314774578394_369758900_n', ''),
(140, 139, 0, '10003928_10152310590163394_537769322_n', ''),
(141, 140, 0, '10003834_10152338349158394_1025277976_o', ''),
(142, 141, 0, '1982318_10152324830408394_858467547_n', ''),
(143, 142, 0, '1979953_10152307943768394_1490385823_o', ''),
(144, 143, 0, 'Ložnice', ''),
(156, 155, 0, '1801165_10152533397053394_3811199087470408213_o', ''),
(157, 156, 0, '10369072_10152619346068394_7248242583379622654_o', '');

-- --------------------------------------------------------

--
-- Struktura tabulky `reservations`
--

CREATE TABLE IF NOT EXISTS `reservations` (
  `id` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `termin_od` date NOT NULL,
  `termin_do` date NOT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `price` float NOT NULL,
  `discount` int(11) NOT NULL DEFAULT '0',
  `currency_id` int(11) NOT NULL,
  `prevodem` tinyint(4) NOT NULL DEFAULT '0',
  `note` tinytext COLLATE utf8_czech_ci,
  `apartman_id` int(11) NOT NULL,
  `customers_count` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `splatnost` int(11) NOT NULL,
  `splatnost_zaloha` int(11) NOT NULL DEFAULT '0',
  `splatnost_at` date NOT NULL,
  `splatnost_zaloha_at` date NOT NULL,
  `free` tinyint(4) NOT NULL DEFAULT '0',
  `tax_free` tinyint(4) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `paid` tinyint(4) NOT NULL DEFAULT '0',
  `keys` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=184 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `reservations`
--

INSERT INTO `reservations` (`id`, `code`, `termin_od`, `termin_do`, `email`, `phone`, `price`, `discount`, `currency_id`, `prevodem`, `note`, `apartman_id`, `customers_count`, `created_at`, `splatnost`, `splatnost_zaloha`, `splatnost_at`, `splatnost_zaloha_at`, `free`, `tax_free`, `status`, `paid`, `keys`) VALUES
(4, 1001, '2015-05-08', '2015-05-12', 'mohr@fototrend.cz', '', 0, 0, 1, 0, 'MOHR-Doktorka - zdarma - neposílat klíče', 5, 0, '2015-04-23 00:00:00', 15, 0, '2015-05-08', '2015-04-23', 1, 0, 0, 0, 0),
(3, 1000, '2015-04-30', '2015-05-03', 'mildgret@seznam.cz', '777 162 067', 77, 0, 1, 0, '', 5, 0, '2015-04-23 00:00:00', 15, 0, '2015-05-08', '2015-04-23', 0, 0, 0, 0, 0),
(5, 1002, '2015-06-11', '2015-06-14', 'mohr@fototrend.cz', '', 0, 0, 1, 0, 'MOHR brácha - zdarma, neposílat klíče', 5, 0, '2015-04-23 00:00:00', 15, 0, '2015-05-08', '2015-04-23', 1, 0, 0, 0, 0),
(6, 1003, '2015-07-11', '2015-07-18', 'marcel.holes@saphir.cz', '731596320', 16610, 0, 2, 1, '', 3, 0, '2015-04-23 00:00:00', 10, 0, '2015-05-03', '2015-04-23', 0, 0, 0, 0, 0),
(7, 1004, '2015-07-11', '2015-07-18', 'vasicek@rall.cz', '737211069', 16899, 0, 2, 1, '', 4, 0, '2015-04-23 00:00:00', 10, 0, '2015-05-03', '2015-04-23', 0, 0, 0, 0, 0),
(8, 1005, '2015-07-22', '2015-07-26', 'kamenikova.petra@seznam.cz', '', 10203, 5, 2, 1, '', 3, 0, '2015-04-23 00:00:00', 3, 0, '2015-04-26', '2015-04-23', 0, 0, 0, 0, 0),
(9, 1006, '2015-07-22', '2015-07-26', 'kamenikova.petra@seznam.cz', '', 10203, 5, 2, 1, '', 4, 0, '2015-04-23 00:00:00', 3, 0, '2015-04-26', '2015-04-23', 0, 0, 0, 0, 0),
(62, 1049, '2015-08-27', '2015-08-30', 'mohr@fototrend.cz', '', 287, 0, 1, 0, '\n', 5, 0, '2015-06-28 00:00:00', 15, 0, '2015-07-13', '2015-06-28', 0, 0, 0, 0, 0),
(11, 1008, '2015-08-01', '2015-08-08', 'michal.vymola@avex.cz', '608600300', 16321.2, 0, 2, 1, '', 3, 0, '2015-04-23 00:00:00', 15, 0, '2015-05-08', '2015-04-23', 0, 0, 0, 0, 0),
(12, 1009, '2015-08-01', '2015-08-08', 'michal.vymola@avex.cz', '', 583, 0, 1, 0, '', 4, 0, '2015-04-23 00:00:00', 15, 0, '2015-05-08', '2015-04-23', 0, 0, 0, 0, 0),
(13, 1010, '2015-07-29', '2015-08-10', 'iva@vanecek.cz', '603297749', 28188, 0, 2, 1, 'Plaská 17, 323 00 Plzeň', 5, 0, '2015-04-23 00:00:00', 15, 0, '2015-05-08', '2015-04-23', 0, 0, 0, 0, 0),
(14, 1011, '2015-06-17', '2015-06-21', 'michal.vymola@avex.cz', '608600300', 0, 0, 2, 1, 'TEAM BUILDING AVEX\n\nDědy\nGusta\nHradil\n', 3, 0, '2015-04-23 00:00:00', 15, 0, '2015-05-08', '2015-04-23', 1, 0, 0, 0, 0),
(15, 1012, '2015-06-17', '2015-06-21', 'michal.vymola@avex.cz', '608600300', 9928, 0, 2, 1, 'TEAM BUILDING AVEX\n\nMichal+ Míša\nIvan + Béďa', 4, 0, '2015-04-23 00:00:00', 15, 0, '2015-05-08', '2015-04-23', 0, 0, 0, 0, 0),
(16, 1013, '2015-12-16', '2015-12-20', 'Jaromir.Palecek@ucb.com', '', 15538, 0, 2, 1, '', 3, 0, '2015-04-23 00:00:00', 15, 0, '2015-05-08', '2015-04-23', 0, 0, 0, 0, 0),
(17, 1014, '2015-12-16', '2015-12-20', 'Jaromir.Palecek@ucb.com', '', 15208, 0, 2, 1, '', 4, 0, '2015-04-23 00:00:00', 15, 0, '2015-05-08', '2015-04-23', 0, 0, 0, 0, 0),
(18, 1015, '2015-12-16', '2015-12-20', 'Jaromir.Palecek@ucb.com', '', 15538, 0, 2, 1, '', 5, 0, '2015-04-23 00:00:00', 15, 0, '2015-05-08', '2015-04-23', 0, 0, 0, 0, 0),
(19, 1016, '2015-12-20', '2015-12-27', 'oskera@telest.cz', '', 26235, 25, 2, 1, '100 EUR/NOC\n+ 65 EUR úklid\n+ pobytová taxa', 3, 0, '2015-04-23 00:00:00', 15, 0, '2015-05-08', '2015-04-23', 0, 0, 0, 0, 0),
(20, 1017, '2015-12-20', '2015-12-27', 'oskera@telest.cz', '', 26813, 25, 2, 1, '100 EUR/NOC\n+ 65 EUR úklid\n+ pobytová taxa', 4, 0, '2015-04-23 00:00:00', 15, 0, '2015-05-08', '2015-04-23', 0, 0, 0, 0, 0),
(21, 1018, '2015-12-28', '2016-01-02', 'zelik@oxalis.cz', '603517011', 24957, 0, 2, 1, '', 3, 0, '2015-04-23 00:00:00', 30, 0, '2015-05-23', '2015-04-23', 0, 0, 3, 0, 0),
(22, 1019, '2016-01-03', '2016-01-09', 'dedek@euzl.cz', '', 908, 0, 1, 0, '', 3, 0, '2015-04-23 00:00:00', 15, 0, '2015-05-08', '2015-04-23', 0, 0, 0, 0, 0),
(23, 1020, '2016-01-03', '2016-01-09', 'michal.vymola@avex.cz', '', 908, 0, 1, 0, '', 4, 0, '2015-04-23 00:00:00', 15, 0, '2015-05-08', '2015-04-23', 0, 0, 0, 0, 0),
(24, 1021, '2016-01-04', '2016-01-10', 'vidla@volny.cz', '', 24393, 0, 2, 1, '', 5, 0, '2015-04-23 00:00:00', 15, 0, '2015-05-08', '2015-04-23', 0, 0, 0, 0, 0),
(25, 1022, '2016-01-25', '2016-01-28', 'misa.jurena@gmail.com ', '', 11647, 25, 2, 1, '', 4, 0, '2015-04-23 00:00:00', 15, 0, '2015-05-08', '2015-04-23', 0, 0, 0, 0, 0),
(26, 1023, '2016-01-23', '2016-01-30', 'mrazek.milan@seznam.cz ', '775248851', 32038, 0, 2, 1, 'vyúčtování zasláno 26.11.2015', 5, 0, '2015-04-23 00:00:00', 5, 0, '2015-04-28', '2015-04-23', 0, 0, 0, 0, 0),
(142, 1122, '2016-01-31', '2016-02-06', 'ROMANA@EXPECTA.CZ', '', 25383, 15, 2, 1, '', 3, 0, '2016-01-21 00:00:00', 15, 0, '2016-02-05', '2016-01-21', 0, 0, 0, 0, 0),
(28, 1025, '2016-02-07', '2016-02-14', 'dagmar.prchalova@hotmail.com', '', 1143, 10, 1, 1, '', 3, 0, '2015-04-23 00:00:00', 5, 0, '2015-04-28', '2015-04-23', 0, 0, 0, 0, 0),
(29, 1026, '2016-02-07', '2016-02-14', 'dagmar.prchalova@hotmail.com', '608323038', 1101, 10, 1, 1, 'POSTÝLKA !!!', 4, 0, '2015-04-23 00:00:00', 5, 0, '2015-04-28', '2015-04-23', 0, 0, 0, 0, 0),
(143, 1123, '2016-02-16', '2016-02-20', 'betakova.a@seznam.cz', '605238595', 20323, 0, 2, 0, '', 3, 0, '2016-01-22 00:00:00', 15, 1, '2016-02-06', '2016-01-23', 0, 0, 4, 0, 0),
(33, 1030, '2016-02-27', '2016-03-03', 'bryska@pesak.cz', '', 21450, 15, 2, 1, 'slíbeno 135 EUR /apartmán', 3, 0, '2015-04-23 00:00:00', 15, 0, '2015-05-08', '2015-04-23', 0, 0, 0, 0, 0),
(34, 1031, '2016-02-27', '2016-03-03', 'bryska@pesak.cz', '', 21450, 15, 2, 0, '', 4, 0, '2015-04-23 00:00:00', 15, 0, '2015-05-08', '2015-04-23', 0, 0, 0, 0, 0),
(35, 1032, '2016-03-31', '2016-04-03', 'MarekZabojnik@seznam.cz', '', 11688, 10, 2, 0, '', 3, 0, '2015-04-23 00:00:00', 15, 1, '2015-05-08', '2015-04-24', 0, 0, 0, 1, 0),
(36, 1033, '2016-03-31', '2016-04-03', 'MarekZabojnik@seznam.cz', '', 11688, 10, 2, 0, '', 4, 0, '2015-04-23 00:00:00', 15, 1, '2015-05-08', '2015-04-24', 0, 0, 0, 1, 0),
(37, 1034, '2016-03-28', '2016-04-02', 'MarekZabojnik@seznam.cz', '', 18288, 10, 2, 0, '', 5, 0, '2015-04-23 00:00:00', 15, 1, '2015-05-08', '2015-04-24', 0, 0, 0, 1, 0),
(77, 1062, '2015-12-09', '2015-12-13', 'ski@avex.cz', '', 14603, 0, 2, 0, '', 4, 0, '2015-08-20 00:00:00', 15, 0, '2015-09-04', '2015-08-20', 0, 0, 0, 0, 0),
(46, 1040, '2015-06-10', '2015-06-14', 'radim.korinek@bikomatic.cz', '608 775 880', 9488, 10, 2, 1, 'SK BIKE PRO HANÁ, o.s.\nOstružinová 147/2\nOlomouc 779 00\n\nJan Reiner   ml. 29.1. 1985\nJan Reiner   st. 3. 2. 1960\n\nJiří Buksa 8.4. 1965\nDomnik Buksa 7.11. 1990\n', 3, 0, '2015-04-29 00:00:00', 15, 0, '2015-05-14', '2015-04-29', 0, 0, 0, 0, 0),
(76, 1061, '2015-12-09', '2015-12-13', 'ski@avex.cz', '', 14603, 0, 2, 0, '', 3, 0, '2015-08-20 00:00:00', 15, 0, '2015-09-04', '2015-08-20', 0, 0, 0, 0, 0),
(47, 1041, '2015-06-10', '2015-06-14', 'radim.korinek@bikomatic.cz', '608 775 880', 9322.5, 10, 2, 1, 'SPECIALIZED CONCEPT STORE\nBikomatic company s.r.o.\nGebauerova 1087/1\n702 00 Ostrava - Přívoz\nradim.korinek@bikomatic.cz\n+420 608 775 880\n', 4, 0, '2015-04-29 00:00:00', 15, 0, '2015-05-14', '2015-04-29', 0, 0, 0, 0, 0),
(56, 1043, '2015-06-17', '2015-06-21', 'michal.vymola@avex.cz', '608600300', 10588, 0, 2, 0, 'TEAM BUILDING AVEX', 5, 0, '2015-05-16 00:00:00', 15, 0, '2015-05-31', '2015-05-16', 0, 0, 0, 0, 0),
(73, 1058, '2016-01-11', '2016-01-14', 'betakova.e@seznam.cz', '605238595', 13338, 0, 2, 1, '', 4, 0, '2015-08-14 00:00:00', 15, 0, '2015-08-29', '2015-08-14', 0, 0, 4, 0, 0),
(57, 1044, '2015-07-27', '2015-08-01', 'rakovsky@rakovsky.com', '777 176 211', 12169, 0, 2, 1, '', 3, 0, '2015-05-22 00:00:00', 15, 0, '2015-06-06', '2015-05-22', 0, 0, 0, 0, 0),
(58, 1045, '2015-07-27', '2015-08-01', 'rakovsky@rakovsky.com', '777 176 211', 12169, 0, 2, 1, '', 4, 0, '2015-05-22 00:00:00', 15, 0, '2015-06-06', '2015-05-22', 0, 0, 0, 0, 0),
(59, 1046, '2015-08-08', '2015-08-15', 'mouseoleum@centrum.cz', '', 520, 15, 1, 1, 'Paša, Šeda a spol.', 3, 0, '2015-06-02 00:00:00', 15, 0, '2015-06-17', '2015-06-02', 0, 0, 0, 0, 0),
(60, 1047, '2016-01-31', '2016-02-07', 'Beata@gistrova.cz', '', 29604, 15, 2, 1, '', 4, 0, '2015-06-06 00:00:00', 15, 0, '2015-06-21', '2015-06-06', 0, 0, 0, 0, 0),
(81, 1066, '2016-01-10', '2016-01-14', 'kmalosik@seznam.cz', '', 17188, 0, 2, 1, '', 5, 0, '2015-11-21 00:00:00', 7, 0, '2015-11-28', '2015-11-21', 0, 0, 3, 0, 0),
(63, 1050, '2015-08-15', '2015-08-23', '8honza6@gmail.com', '', 17188, 0, 2, 0, '', 3, 0, '2015-06-30 00:00:00', 15, 0, '2015-07-15', '2015-06-30', 0, 0, 0, 0, 0),
(64, 1051, '2016-01-14', '2016-01-17', 'Ganapathi@seznam.cz', '', 11234, 15, 2, 1, 'Je to dárek pro Honzu Lutonského od manželky.\n\n3noci x 80 EUR            240 EUR \nÚklid   65 EUR \n \nCELKEM 305 EUR = 8540 Kč \n\nvyúčtování zasláno 26.11.2015\n', 4, 0, '2015-06-30 00:00:00', 15, 0, '2015-07-15', '2015-06-30', 0, 0, 4, 0, 0),
(66, 1052, '2015-07-19', '2015-07-26', 'polak.karel@gmail.com', '722100000', 16610, 0, 2, 1, '', 5, 0, '2015-07-16 00:00:00', 1, 0, '2015-07-17', '2015-07-16', 0, 0, 0, 0, 0),
(89, 1073, '2016-01-25', '2016-01-28', '', '', 11647, 25, 2, 1, '', 3, 0, '2015-09-09 00:00:00', 15, 0, '2015-09-24', '2015-09-09', 0, 0, 0, 0, 0),
(70, 1055, '2016-03-23', '2016-03-28', 'Michal.vymola@avex.cz', '608600300', 0, 0, 1, 0, 'Michal vymola s rodinou', 3, 0, '2015-08-09 00:00:00', 15, 0, '2015-08-24', '2015-08-09', 1, 0, 0, 1, 1),
(71, 1056, '2016-03-23', '2016-03-28', 'Michal.vymola@avex', '608600300', 0, 0, 1, 0, 'Vymola s family', 4, 0, '2015-08-09 00:00:00', 15, 0, '2015-08-24', '2015-08-09', 1, 0, 0, 1, 1),
(72, 1057, '2016-03-23', '2016-03-28', 'Michal.vymola@avex.cz', '608600300', 19869, 0, 2, 0, 'Pasta&masarik', 5, 0, '2015-08-09 00:00:00', 15, 1, '2015-08-24', '2015-08-10', 0, 0, 0, 1, 1),
(74, 1059, '2015-12-30', '2016-01-02', '', '', 557, 0, 1, 1, 'Rezervace pro Balaze Totha, zaslat zálohovku \nv půlce září. Zasláno, uhrazeno na účet Výmola\n\nMichal', 5, 0, '2015-08-17 00:00:00', 15, 0, '2015-09-01', '2015-08-17', 0, 0, 0, 0, 0),
(75, 1060, '2016-01-11', '2016-01-14', 'betakova.e@seznam.cz', '605238595', 12967, 0, 2, 0, '', 3, 0, '2015-08-18 00:00:00', 15, 0, '2015-09-02', '2015-08-18', 0, 0, 4, 0, 0),
(79, 1064, '2015-12-13', '2015-12-16', 'betakova.a@seznam.cz', '605238595', 2613, 0, 2, 0, '', 4, 0, '2015-08-21 00:00:00', 15, 0, '2015-09-05', '2015-08-21', 0, 1, 0, 0, 0),
(119, 1099, '2017-02-11', '2017-02-18', 'jitka.zamorska@seznam.cz', '', 2847, 0, 2, 0, '', 4, 0, '2015-11-06 00:00:00', 15, 0, '2015-11-21', '2015-11-06', 0, 0, 0, 0, 0),
(139, 1119, '2016-01-21', '2016-01-24', '', '', 10657, 25, 2, 1, '', 4, 0, '2016-01-02 00:00:00', 5, 0, '2016-01-07', '2016-01-02', 0, 0, 0, 0, 0),
(88, 1072, '2015-12-30', '2016-01-03', 'bryška marcel', '734 551 431', 17870, 13, 2, 0, 'Cena 140 EUR/noc', 4, 0, '2015-09-07 00:00:00', 15, 0, '2015-09-22', '2015-09-07', 0, 0, 4, 0, 0),
(84, 1069, '2016-02-20', '2016-02-27', 'mohr@fototrend.cz', '+420 731 127 121', 1245, 0, 1, 0, '\n\n   Karty předám', 5, 0, '2015-09-02 00:00:00', 15, 0, '2015-09-17', '2015-09-02', 0, 0, 0, 0, 0),
(117, 1097, '2016-02-20', '2016-02-27', 'mohr', '', 33647, 0, 2, 0, '  zaplatí hotově', 4, 0, '2015-11-01 00:00:00', 15, 1, '2015-11-16', '2015-11-02', 0, 0, 0, 0, 0),
(118, 1098, '2017-02-11', '2017-02-18', 'jitka.zamorska@seznam.cz', '725284192', 2847, 0, 2, 0, '', 3, 0, '2015-11-06 00:00:00', 15, 0, '2015-11-21', '2015-11-06', 0, 0, 0, 0, 0),
(87, 1071, '2015-09-04', '2015-09-07', '', '', 287, 0, 1, 0, '', 3, 0, '2015-09-03 00:00:00', 15, 0, '2015-09-18', '2015-09-03', 0, 0, 1, 0, 0),
(90, 1074, '2016-03-06', '2016-03-10', '', '', 667, 0, 1, 0, '', 4, 0, '2015-09-11 00:00:00', 15, 0, '2015-09-26', '2015-09-11', 0, 0, 0, 0, 0),
(91, 1075, '2015-09-25', '2015-09-28', 'michaela@salvetova.cz', '777233448', 0, 0, 2, 1, NULL, 3, 0, '0000-00-00 00:00:00', 7, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0),
(92, 1076, '2015-12-27', '2015-12-30', 'michaeltiki@seznam.cz', '+420724203857', 15813, 0, 2, 1, '', 4, 0, '2015-09-23 00:00:00', 30, 0, '2015-10-23', '2015-09-23', 0, 0, 0, 0, 0),
(93, 1077, '2016-01-31', '2016-02-04', 'densti@icloud.com', '777 036 445', 16638, 20, 2, 1, 'Ck maxim - provize CK 20%', 5, 0, '2015-09-15 00:00:00', 15, 0, '2015-09-30', '2015-09-15', 0, 0, 0, 0, 0),
(94, 1078, '2016-01-28', '2016-01-31', '', '', 13173, 20, 2, 1, 'Jitka Králová\nreferent krizového řízení  a podatelny\n\nMěstská část Praha 21\nÚřad městské části\nStaroklánovická 260\n190 16 Praha 9 – Újezd nad Lesy\n\nTel.            +420 281 012 926\nFax:           +420 281 971 531\ne-mail:       jitka.k', 3, 0, '2015-09-16 00:00:00', 15, 0, '2015-10-01', '2015-09-16', 0, 0, 0, 0, 0),
(95, 1079, '2016-01-28', '2016-01-31', '', '', 13173, 20, 2, 1, 'Jitka Králová\nreferent krizového řízení  a podatelny\nmobil 777 709 607.\nMěstská část Praha 21\nÚřad městské části\nStaroklánovická 260\n190 16 Praha 9 – Újezd nad Lesy\n\nTel.            +420 281 012 926\nFax:           +420 281 971 531\ne-m', 4, 0, '2015-09-16 00:00:00', 15, 0, '2015-10-01', '2015-09-16', 0, 0, 0, 0, 0),
(96, 1080, '2015-09-25', '2015-09-28', 'kamenikova.petra@seznam.cz', '', 8264, 0, 2, 0, '', 4, 0, '2015-09-19 00:00:00', 5, 0, '2015-09-24', '2015-09-19', 0, 0, 0, 0, 0),
(97, 1081, '2015-11-13', '2015-11-17', '', '', 0, 0, 1, 0, '', 4, 0, '2015-09-21 00:00:00', 15, 0, '2015-10-06', '2015-09-21', 1, 0, 0, 0, 0),
(98, 1082, '2016-03-06', '2016-03-10', '', '', 503, 20, 1, 0, '', 3, 0, '2015-09-23 00:00:00', 15, 0, '2015-10-08', '2015-09-23', 0, 0, 0, 0, 0),
(99, 1083, '2016-03-06', '2016-03-10', '', '', 16528, 0, 2, 0, '', 5, 0, '2015-09-23 00:00:00', 15, 0, '2015-10-08', '2015-09-23', 0, 0, 0, 0, 0),
(100, 1084, '2016-02-04', '2016-02-07', '', '', 15318, 0, 2, 1, 'Fakturováno na CK MAXIM, č. faktury 16004', 5, 0, '2015-09-25 00:00:00', 15, 0, '2015-10-10', '2015-09-25', 0, 0, 4, 0, 0),
(101, 1085, '2015-12-09', '2015-12-13', '', '', 14878, 0, 2, 0, '', 5, 0, '2015-09-29 00:00:00', 15, 1, '2015-10-14', '2015-09-30', 0, 0, 0, 0, 0),
(102, 1086, '2016-03-11', '2016-03-15', 'autoradiaservis@gmail.com', '777016416', 15428, 10, 2, 1, 'Vyúčtování zasláno 3.3.2016', 4, 0, '2015-10-02 00:00:00', 5, 0, '2015-10-07', '2015-10-02', 0, 0, 0, 0, 0),
(103, 1087, '2015-10-08', '2015-10-12', 'mallatova@seznam.cz', '', 361, 0, 1, 0, 'Ahoj,je to kolegyně z práce. Zaplatí pouze úklid. Karty predam. \nZdravím Míša ', 5, 0, '2015-10-03 00:00:00', 15, 0, '2015-10-18', '2015-10-03', 0, 0, 0, 0, 0),
(104, 1088, '2016-01-18', '2016-01-22', 'michal.kadlecek@volny.cz', '606271536', 16858, 0, 2, 1, 'Adresa: Českolipská 350\n        47141  DUBÁ\n', 5, 0, '2015-10-03 00:00:00', 15, 0, '2015-10-18', '2015-10-03', 0, 0, 1, 0, 0),
(105, 1089, '2016-01-17', '2016-01-21', '', '', 15043, 15, 2, 1, '', 3, 0, '2015-10-03 00:00:00', 5, 0, '2015-10-08', '2015-10-03', 0, 0, 0, 0, 0),
(106, 1090, '2016-01-17', '2016-01-21', '', '', 547, 15, 1, 0, '550 EUR zaplaceno hotově 25.1.2016', 4, 0, '2015-10-03 00:00:00', 5, 0, '2015-10-08', '2015-10-03', 0, 0, 0, 0, 0),
(107, 1091, '2016-01-13', '2016-01-17', 'kamenikova.petra@seznam.cz', '', 13613, 25, 2, 1, '', 3, 0, '2015-10-05 00:00:00', 15, 0, '2015-10-20', '2015-10-05', 0, 0, 0, 0, 0),
(108, 1092, '2015-12-26', '2015-12-30', 'mohr@fototrend.cz', '731127121', 721, 0, 1, 0, '\n    Ahoj Michale,\n\n    pojedou s námi děti.Zaplatím úklid.\n\n    Dík  MM', 5, 0, '2015-10-06 00:00:00', 15, 0, '2015-10-21', '2015-10-06', 0, 0, 0, 0, 0),
(109, 1093, '2016-07-30', '2016-08-06', '', '', 2847, 0, 2, 0, '', 4, 0, '2015-10-13 00:00:00', 15, 0, '2015-10-28', '2015-10-13', 0, 0, 0, 0, 0),
(110, 1094, '2016-07-30', '2016-08-06', '', '', 2847, 0, 2, 0, '', 5, 0, '2015-10-13 00:00:00', 15, 0, '2015-10-28', '2015-10-13', 0, 0, 0, 0, 0),
(114, 1095, '2016-03-10', '2016-03-13', '', '', 12018, 10, 2, 1, '', 3, 0, '2015-10-27 00:00:00', 15, 1, '2015-11-11', '2015-10-28', 0, 0, 4, 0, 0),
(115, 1096, '2016-03-10', '2016-03-13', '', '', 13090, 0, 2, 1, '', 5, 0, '2015-10-27 00:00:00', 15, 1, '2015-11-11', '2015-10-28', 0, 0, 4, 0, 0),
(120, 1100, '2017-02-11', '2017-02-18', 'jitka.zamorska@seznam.cz', '', 2847, 0, 2, 0, '', 5, 0, '2015-11-06 00:00:00', 15, 0, '2015-11-21', '2015-11-06', 0, 0, 0, 0, 0),
(121, 1101, '2016-03-16', '2016-03-20', 'stanzacha@gmail.com', '724444584', 16858, 0, 2, 0, '', 3, 0, '2015-11-19 00:00:00', 6, 1, '2015-11-25', '2015-11-20', 0, 0, 0, 1, 0),
(122, 1102, '2016-03-16', '2016-03-20', '', '', 16858, 0, 2, 0, '', 4, 0, '2015-11-19 00:00:00', 6, 0, '2015-11-25', '2015-11-19', 0, 0, 0, 1, 0),
(123, 1103, '2015-11-26', '2015-12-03', '', '', 0, 0, 1, 0, '', 5, 0, '2015-11-23 00:00:00', 15, 0, '2015-12-08', '2015-11-23', 1, 0, 0, 0, 0),
(124, 1104, '2016-01-14', '2016-01-18', '', '', 16693, 0, 2, 0, 'zaplaceno hotově 13.1.2016 -převzal Michal', 5, 0, '2015-11-23 00:00:00', 5, 1, '2015-11-28', '2015-11-24', 0, 0, 0, 0, 0),
(125, 1105, '2016-03-03', '2016-03-06', '', '', 557, 0, 1, 0, '', 3, 0, '2015-11-30 00:00:00', 15, 0, '2015-12-15', '2015-11-30', 0, 0, 0, 0, 0),
(126, 1106, '2016-03-03', '2016-03-06', '', '', 557, 0, 1, 0, '', 4, 0, '2015-11-30 00:00:00', 15, 0, '2015-12-15', '2015-11-30', 0, 0, 0, 0, 0),
(156, 1136, '2016-02-21', '2016-02-24', 'b.stablova@gmail.com', '776 640 526', 15813, 0, 2, 1, '130 AKCE LAST MINUTE, fakturováno 16005 a 16006', 3, 0, '2016-02-10 00:00:00', 15, 0, '2016-02-25', '2016-02-10', 0, 0, 4, 0, 0),
(128, 1108, '2016-08-06', '2016-08-13', '', '', 93, 0, 1, 0, '', 3, 0, '2015-12-04 00:00:00', 15, 0, '2015-12-19', '2015-12-04', 0, 0, 0, 0, 0),
(129, 1109, '2016-08-06', '2016-08-13', '', '', 93, 0, 1, 0, '', 5, 0, '2015-12-04 00:00:00', 15, 0, '2015-12-19', '2015-12-04', 0, 0, 0, 0, 0),
(130, 1110, '2016-08-06', '2016-08-13', '', '', 93, 0, 1, 0, '', 4, 0, '2015-12-04 00:00:00', 15, 0, '2015-12-19', '2015-12-04', 0, 0, 0, 0, 0),
(132, 1112, '2016-02-14', '2016-02-18', '', '', 20323, 0, 2, 1, 'Fakturováno na CK 28.12.2015\nfaktura číslo 15015', 5, 0, '2015-12-08 00:00:00', 15, 0, '2015-12-23', '2015-12-08', 0, 0, 0, 0, 0),
(133, 1113, '2016-02-27', '2016-03-02', 'kamenikova.petra@seznam.cz', '', 18728, 10, 2, 0, '', 5, 0, '2015-12-08 00:00:00', 15, 0, '2015-12-23', '2015-12-08', 0, 0, 0, 0, 0),
(168, 1147, '2016-12-27', '2017-01-01', 'ivodusik@seznam.cz', '608710850', 2544, 0, 2, 0, '', 3, 0, '2016-03-11 00:00:00', 15, 0, '2016-03-26', '2016-03-11', 0, 0, 0, 0, 0),
(135, 1115, '2016-12-14', '2016-12-18', '', '', 81, 0, 1, 0, '', 3, 0, '2015-12-22 00:00:00', 15, 0, '2016-01-06', '2015-12-22', 0, 0, 0, 0, 0),
(136, 1116, '2016-12-14', '2016-12-18', '', '', 81, 0, 1, 0, '', 4, 0, '2015-12-22 00:00:00', 15, 0, '2016-01-06', '2015-12-22', 0, 0, 0, 0, 0),
(137, 1117, '2016-12-14', '2016-12-18', '', '', 81, 0, 1, 0, '', 5, 0, '2015-12-22 00:00:00', 15, 0, '2016-01-06', '2015-12-22', 0, 0, 0, 0, 0),
(138, 1118, '2016-01-21', '2016-01-24', '', '', 10657, 25, 2, 1, '', 3, 0, '2016-01-02 00:00:00', 5, 0, '2016-01-07', '2016-01-02', 0, 0, 0, 0, 0),
(176, 1155, '2016-04-07', '2016-04-10', '', '', 12925, 0, 2, 0, '', 5, 0, '2016-03-22 00:00:00', 15, 7, '2016-04-06', '2016-03-29', 0, 0, 0, 0, 0),
(141, 1121, '2016-03-20', '2016-03-23', 'barahora@seznam.cz', '605 92 80 18', 467, 0, 1, 0, '', 4, 0, '2016-01-13 00:00:00', 15, 0, '2016-01-28', '2016-01-13', 0, 0, 0, 1, 1),
(144, 1124, '2017-01-02', '2017-01-07', 'kamenikova.petra@seznam.cz', '', 2544, 0, 2, 0, '', 3, 0, '2016-01-25 00:00:00', 15, 0, '2016-02-09', '2016-01-25', 0, 0, 0, 0, 0),
(145, 1125, '2017-01-02', '2017-01-07', 'kamenikova.petra@seznam.cz', '', 2544, 0, 2, 0, '', 4, 0, '2016-01-25 00:00:00', 15, 0, '2016-02-09', '2016-01-25', 0, 0, 0, 0, 0),
(146, 1126, '2016-02-07', '2016-02-14', 'salamon.noemi@gmail.com', '0036/30-9190828', 1234, 0, 1, 1, '', 5, 0, '2016-01-26 00:00:00', 3, 0, '2016-01-29', '2016-01-26', 0, 0, 0, 0, 0),
(147, 1127, '2016-06-30', '2016-07-06', 'havrlantovam@csob.cz', '737 201 724', 485, 0, 1, 1, '', 3, 0, '2016-01-29 00:00:00', 15, 0, '2016-02-13', '2016-01-29', 0, 0, 0, 0, 0),
(148, 1128, '2016-08-13', '2016-08-19', 'ihanelova@csob.cz', '603 800 460', 2943, 0, 2, 1, 'Střední 208, Dubí 3, 417 03\n\nskupina Miloš Mohr\n603 800 460', 3, 0, '2016-02-01 00:00:00', 15, 0, '2016-02-16', '2016-02-01', 0, 0, 0, 0, 0),
(149, 1129, '2016-08-13', '2016-08-19', 'ihanelova@csob.cz', '', 3438, 0, 2, 1, 'U Zámecké zahrady 1667/5 41501 Teplice\n\nSkupina Miloš MOHR', 4, 0, '2016-02-01 00:00:00', 15, 0, '2016-02-16', '2016-02-01', 0, 0, 0, 0, 0),
(150, 1130, '2016-08-13', '2016-08-19', 'ihanelova@csob.cz', '', 3438, 0, 2, 1, ' Fučíkova 194, 417 42 Krupka\n\nSkupina Miloš MOHR', 5, 0, '2016-02-01 00:00:00', 15, 0, '2016-02-16', '2016-02-01', 0, 0, 0, 0, 0),
(151, 1131, '2016-07-20', '2016-07-24', '', '', 81, 0, 1, 0, '', 3, 0, '2016-02-04 00:00:00', 15, 0, '2016-02-19', '2016-02-04', 0, 0, 0, 0, 0),
(152, 1132, '2016-07-20', '2016-07-24', '', '', 81, 0, 1, 0, '', 4, 0, '2016-02-04 00:00:00', 15, 0, '2016-02-19', '2016-02-04', 0, 0, 0, 0, 0),
(153, 1133, '2016-07-20', '2016-07-24', '', '', 81, 0, 1, 0, '', 5, 0, '2016-02-04 00:00:00', 15, 0, '2016-02-19', '2016-02-04', 0, 0, 0, 0, 0),
(154, 1134, '2016-02-14', '2016-02-17', '', '', 557, 0, 1, 0, '', 4, 0, '2016-02-04 00:00:00', 15, 0, '2016-02-19', '2016-02-04', 0, 0, 0, 0, 0),
(155, 1135, '2016-02-17', '2016-02-20', '', '', 566, 0, 1, 0, 'Bryška - 130 EUR / NOC + 65 EUR úklid\nZAPLACENO HOTOVĚ V EURECH.', 4, 0, '2016-02-09 00:00:00', 15, 0, '2016-02-24', '2016-02-09', 0, 0, 3, 0, 0),
(157, 1137, '2016-03-20', '2016-03-23', 'svozilek.filip@centrum.cz', '777223446', 12802, 5, 2, 0, '', 3, 0, '2016-02-12 00:00:00', 8, 1, '2016-02-20', '2016-02-13', 0, 0, 0, 1, 0),
(159, 1138, '2016-07-25', '2016-07-30', 'mallatova@seznam. Cz', '', 435, 0, 1, 0, 'Karty neposilat  pouze úklid. Rodina', 5, 0, '2016-02-14 00:00:00', 15, 0, '2016-02-29', '2016-02-14', 0, 0, 0, 0, 0),
(162, 1141, '2016-07-24', '2016-07-30', '', '', 14245, 0, 2, 0, '', 3, 0, '2016-02-15 00:00:00', 15, 0, '2016-03-01', '2016-02-15', 0, 0, 0, 0, 0),
(161, 1140, '2016-07-24', '2016-07-30', '', '', 14245, 0, 2, 0, '', 4, 0, '2016-02-15 00:00:00', 15, 0, '2016-03-01', '2016-02-15', 0, 0, 0, 0, 0),
(163, 1142, '2016-04-07', '2016-04-10', '', '', 11317, 10, 2, 0, '', 3, 0, '2016-02-23 00:00:00', 15, 1, '2016-03-09', '2016-02-24', 0, 0, 0, 0, 0),
(164, 1143, '2016-04-07', '2016-04-10', '', '', 466, 0, 1, 0, '', 4, 0, '2016-02-23 00:00:00', 15, 0, '2016-03-09', '2016-02-23', 0, 0, 0, 0, 0),
(165, 1144, '2016-03-02', '2016-03-06', 'mohr@fototrend.cz', '', 739, 0, 1, 0, 'rodina,karty vyřidím sám\núklid u Balasze.', 5, 0, '2016-02-24 00:00:00', 15, 0, '2016-03-10', '2016-02-24', 0, 0, 0, 0, 0),
(166, 1145, '2016-07-31', '2016-08-06', 'mrakota@seznam.cz', '602 431 341', 527, 0, 1, 0, 'Břetislav Dvořák - skupina Miloš!\n\n        Wolkerova 688/15\n\n        41002 Lovosice\n\n        r.č. 701216/2663\n\n        mail -  mrakota@seznam.cz\n\n        tel.  602 431 341\n\n\n        Šárka Mrvíková\n\n        r.č.795630/2651\n\n        adresa stejná', 3, 0, '2016-02-24 00:00:00', 15, 0, '2016-03-10', '2016-02-24', 0, 0, 0, 0, 0),
(169, 1148, '2016-12-27', '2017-01-01', 'ivodusik@seznam.cz', '608710850', 2544, 0, 2, 0, '', 4, 0, '2016-03-11 00:00:00', 15, 0, '2016-03-26', '2016-03-11', 0, 0, 0, 0, 0),
(170, 1149, '2016-12-27', '2017-01-01', 'ivodusik@seznam.cz', '608710850', 2544, 0, 2, 0, '', 5, 0, '2016-03-11 00:00:00', 15, 0, '2016-03-26', '2016-03-11', 0, 0, 0, 0, 0),
(171, 1150, '2017-03-18', '2017-03-25', 'ivodusik@seznam.cz', '608710850', 2847, 0, 2, 0, '', 3, 0, '2016-03-11 00:00:00', 15, 0, '2016-03-26', '2016-03-11', 0, 0, 0, 0, 0),
(172, 1151, '2017-03-18', '2017-03-25', 'ivodusik@seznam.cz', '608710850', 2847, 0, 2, 0, '', 4, 0, '2016-03-11 00:00:00', 15, 0, '2016-03-26', '2016-03-11', 0, 0, 0, 0, 0),
(173, 1152, '2017-03-18', '2017-03-25', 'ivodusik@seznam.cz', '608710850', 2847, 0, 2, 0, '', 5, 0, '2016-03-11 00:00:00', 15, 0, '2016-03-26', '2016-03-11', 0, 0, 0, 0, 0),
(174, 1153, '2016-07-14', '2016-07-20', '', '', 509, 0, 1, 0, '', 3, 0, '2016-03-11 00:00:00', 15, 0, '2016-03-26', '2016-03-11', 0, 0, 0, 0, 0),
(175, 1154, '2018-05-20', '2018-05-24', 'ondrej.brabec@dgstudio.cz', '731826584', 81, 0, 1, 0, 'Pouze test nebrat ohled ', 3, 0, '2016-03-12 00:00:00', 15, 0, '2016-03-27', '2016-03-12', 0, 0, 0, 0, 0),
(178, 1156, '2016-08-06', '2016-08-13', '', '', 555, 0, 1, 0, '', 6, 0, '2016-03-30 00:00:00', 15, 7, '2016-04-14', '2016-04-06', 0, 0, 0, 0, 0),
(179, 1157, '2017-02-12', '2017-02-18', '', '', 1025, 0, 1, 0, '+ potřebuje ještě jeden apartmán ve stejném termínu.', 6, 0, '2016-04-04 00:00:00', 15, 7, '2016-04-19', '2016-04-11', 0, 0, 0, 0, 0),
(180, 1158, '2016-04-17', '2016-04-21', 'ivo.dedek@seznam.cz', '+420 724 310 076', 10890, 0, 2, 1, 'JUVACLUBS\nChemická 953\n14800 Praha 4\nKunratice', 3, 0, '2016-04-05 00:00:00', 7, 1, '2016-04-12', '2016-04-06', 0, 0, 0, 0, 1),
(181, 1159, '2016-07-02', '2016-07-06', 'vymolovi@volny.cz', '737801454', 9378, 10, 2, 1, '', 4, 0, '2016-04-14 00:00:00', 7, 0, '2016-06-02', '2016-04-20', 0, 0, 0, 0, 0),
(182, 1160, '2016-06-09', '2016-06-12', 'mohr@fototrend.cz', '', 289, 0, 1, 0, '  rodina,pokud bude úklid - beru,jinak zajistím\n\n   včetně karet', 5, 0, '2016-04-07 00:00:00', 15, 7, '2016-04-22', '2016-04-14', 0, 0, 0, 0, 0),
(183, 1161, '2016-07-09', '2016-07-15', 'mailto:jana.sirotkova@babor-cz.eu', '734 473 299  ', 13833, 0, 2, 0, 'potřebují uskladnit kola od 1.7 !!! poslat karty dříve!', 4, 0, '2016-04-13 00:00:00', 15, 7, '2016-06-09', '2016-04-20', 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `reservation_prices`
--

CREATE TABLE IF NOT EXISTS `reservation_prices` (
  `id` int(11) NOT NULL,
  `nazev` varchar(255) NOT NULL,
  `termin_od` date NOT NULL,
  `termin_do` date NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0',
  `price` float NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `poradi` tinyint(4) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `reservation_prices`
--

INSERT INTO `reservation_prices` (`id`, `nazev`, `termin_od`, `termin_do`, `shop_id`, `price`, `zobrazit`, `poradi`) VALUES
(1, 'START & FINÁLE', '2015-11-28', '2015-12-19', 0, 115, 1, 1),
(2, 'HLAVNÍ SEZONA', '2015-12-20', '2016-01-03', 0, 160, 1, 2),
(3, 'VEDLEJŠÍ SEZONA', '2016-01-04', '2016-01-25', 0, 130, 1, 3),
(4, 'HLAVNÍ SEZONA', '2016-01-26', '2016-03-05', 0, 160, 1, 4),
(5, 'VEDLEJŠÍ SEZONA', '2016-03-06', '2016-04-08', 0, 130, 1, 5),
(6, 'START & FINÁLE', '2016-04-09', '2016-04-17', 0, 115, 1, 6),
(7, 'LETNÍ SEZONA', '2015-04-20', '2015-11-27', 3, 90, 1, 7),
(8, 'LETNÍ SEZONA 2016', '2016-04-18', '2016-11-25', 0, 30, 1, 8),
(9, 'HLAVNÍ SEZONA', '2016-12-21', '2017-03-05', 0, 4560, 1, 9),
(10, 'VEDLEJŠÍ SEZONA', '2016-11-25', '2016-12-20', 0, 130, 1, 10),
(11, 'VEDLEJŠÍ SEZONA', '2017-03-06', '2017-04-16', 0, 130, 1, 11),
(13, 'LETNÍ SEZONA 2016', '2016-04-18', '2016-11-25', 3, 300, 1, 12);

-- --------------------------------------------------------

--
-- Struktura tabulky `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`) VALUES
(1, 'login', 'Login privileges, granted after account confirmation'),
(2, 'admin', 'Administrative user, has access to everything.'),
(3, 'global_admin', 'Global administrator.');

-- --------------------------------------------------------

--
-- Struktura tabulky `roles_users`
--

CREATE TABLE IF NOT EXISTS `roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `roles_users`
--

INSERT INTO `roles_users` (`user_id`, `role_id`) VALUES
(2, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(2, 2),
(5, 2),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(5, 3),
(8, 3);

-- --------------------------------------------------------

--
-- Struktura tabulky `routes`
--

CREATE TABLE IF NOT EXISTS `routes` (
  `id` int(11) NOT NULL,
  `nazev_seo` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `module_id` int(11) NOT NULL,
  `module_action` varchar(64) COLLATE utf8_czech_ci NOT NULL DEFAULT 'index',
  `param_id1` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `baselang_route_id` int(11) NOT NULL,
  `parent_route_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `read_only` tinyint(4) NOT NULL DEFAULT '0',
  `internal` tinyint(4) NOT NULL DEFAULT '0',
  `searcheable` tinyint(4) NOT NULL DEFAULT '1',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `nazev_seo_old` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=138 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

--
-- Vypisuji data pro tabulku `routes`
--

INSERT INTO `routes` (`id`, `nazev_seo`, `module_id`, `module_action`, `param_id1`, `language_id`, `baselang_route_id`, `parent_route_id`, `title`, `description`, `keywords`, `read_only`, `internal`, `searcheable`, `deleted`, `nazev_seo_old`, `created_date`, `updated_date`, `deleted_date`, `zobrazit`) VALUES
(1, 'index', 1, 'index', '', 1, 0, 0, 'Úvodní stránka', '', '', 1, 1, 1, 0, '', NULL, '2016-04-17 13:57:05', NULL, 1),
(81, 'rezervace', 1, 'reservation', NULL, 1, 0, 0, 'Rezervace', '', '', 0, 0, 1, 0, NULL, '2013-10-21 14:23:31', '2015-10-04 21:16:13', NULL, 0),
(82, 'pro-golfisty', 1, 'detail', NULL, 1, 0, 0, 'Pro golfisty', '', '', 0, 0, 1, 0, NULL, '2013-10-22 15:10:40', '2013-10-22 15:44:52', NULL, 1),
(83, 'leto', 1, 'detail', NULL, 1, 0, 0, 'Léto', '', '', 0, 0, 1, 0, NULL, '2013-10-22 15:24:03', '2014-05-14 20:47:20', NULL, 0),
(84, 'tipy-na-vylety', 1, 'detail', NULL, 1, 0, 0, 'Tipy na výlety', '', '', 0, 0, 1, 0, NULL, '2013-10-22 16:05:43', '2016-04-17 16:33:21', NULL, 1),
(37, 'fotogalerie', 5, 'index', NULL, 1, 0, 0, 'Fotogalerie', '', '', 0, 0, 1, 0, NULL, '2013-09-12 12:23:42', NULL, NULL, 1),
(42, 'prohlaseni-o-pristupnosti', 1, 'detail', NULL, 1, 0, 0, 'Prohlášení o přístupnosti', '', '', 0, 0, 1, 0, NULL, '2013-09-12 14:08:05', NULL, NULL, 1),
(43, 'ochrana-osobnich-udaju', 1, 'detail', NULL, 1, 0, 0, 'Ochrana osobních údajů', '', '', 0, 0, 1, 0, NULL, '2013-09-12 14:12:05', '2013-10-22 16:09:49', NULL, 1),
(45, 'mapa-stranek', 1, 'sitemap', NULL, 1, 0, 0, 'Mapa stránek', '', '', 0, 0, 1, 0, NULL, '2013-09-12 14:42:07', NULL, NULL, 1),
(46, 'vyhledavani', 10, 'index', NULL, 1, 0, 0, 'Vyhledávání', '', '', 0, 0, 1, 0, NULL, '2013-09-12 16:58:02', NULL, NULL, 0),
(65, 'kontakty', 1, 'contact', NULL, 1, 0, 0, 'Kontaktujte nás', '', '', 0, 0, 1, 0, NULL, '2013-09-26 11:17:57', '2016-04-18 06:47:26', NULL, 1),
(66, 'novinky', 3, 'index', NULL, 1, 0, 0, 'Novinky', 'Novinky', '', 0, 0, 1, 0, NULL, '2013-09-26 12:55:21', NULL, NULL, 1),
(78, 'exterier', 5, 'detail', NULL, 1, 0, 0, 'Apartmány Exterier', '', '', 0, 0, 1, 0, NULL, '2013-10-20 20:37:15', '2013-11-10 22:52:39', NULL, 1),
(79, 'pujcovna', 21, 'index', NULL, 1, 0, 0, 'Půjčovna', '', '', 0, 0, 1, 0, NULL, '2013-10-21 09:44:49', '2016-04-20 21:05:29', NULL, 1),
(80, 'apartman-kaibling', 5, 'detail', NULL, 1, 0, 0, 'APARTMÁN KAIBLING', 'APARTMÁN KAIBLING', 'APARTMÁN KAIBLING', 0, 0, 1, 0, NULL, '2013-10-21 09:56:37', '2014-03-26 10:52:08', NULL, 1),
(85, 'obec-haus', 5, 'detail', NULL, 1, 0, 0, 'Obec HAUS', '', '', 0, 0, 1, 0, NULL, '2013-10-28 21:06:50', '2013-11-10 22:41:14', NULL, 1),
(86, 'lyzovani-hauser-kaibling', 5, 'detail', NULL, 1, 0, 0, 'Lyžování HAUSER KAIBLING', 'Lyžování HAUSER KAIBLING', 'Lyžování HAUSER KAIBLING', 0, 0, 1, 0, NULL, '2013-10-28 21:11:14', '2014-05-15 09:20:23', NULL, 1),
(87, 'video-tipy', 1, 'camera', NULL, 1, 0, 0, 'Video tipy', '', '', 0, 0, 1, 0, NULL, '2013-10-29 10:08:42', '2016-04-18 06:59:34', NULL, 1),
(88, 'uvodni', 5, 'detail', NULL, 1, 0, 0, 'Úvodní', '', '', 0, 0, 1, 0, NULL, '2013-10-30 13:25:03', NULL, NULL, 1),
(89, 'cenik', 1, 'detail', NULL, 1, 0, 0, 'O nás', '', '', 0, 0, 1, 0, NULL, '2013-11-07 14:01:11', '2016-04-17 16:34:18', NULL, 1),
(90, 'pocasi', 1, 'detail', NULL, 1, 0, 0, 'Počasí', '', '', 0, 0, 1, 0, NULL, '2013-11-07 14:27:17', '2014-04-15 17:14:28', NULL, 0),
(91, 'ramsau-b9-deleted-10-03-2014073620', 21, 'detail', NULL, 1, 0, 0, 'RAMSAU', '', '', 0, 0, 1, 1, 'ramsau-b9', '2014-03-10 07:36:11', '2016-04-24 19:32:08', '2014-03-10 07:36:20', 1),
(92, 'kaibling-b10-deleted-10-03-2014073651', 21, 'detail', NULL, 1, 0, 0, 'KAIBLING', '', '', 0, 0, 1, 1, 'kaibling-b10', '2014-03-10 07:36:42', '2016-04-21 14:48:55', '2014-03-10 07:36:51', 1),
(93, 'ennstalblick', 21, 'detail', NULL, 1, 0, 0, 'ENNSTALBLICK', '', '', 0, 0, 1, 0, NULL, '2014-03-10 09:07:29', '2016-04-24 19:31:57', NULL, 1),
(136, 'sommercard', 1, 'detail', NULL, 1, 0, 0, 'SOMMERCARD - BONUSOVÁ LETNÍ KARTA', 'SOMMERCARD - BONUSOVÁ LETNÍ KARTA', 'SOMMERCARD - BONUSOVÁ LETNÍ KARTA', 0, 0, 1, 0, NULL, '2016-03-30 20:03:16', '2016-03-30 20:28:04', NULL, 1),
(94, 'apartman-ramsau', 5, 'detail', NULL, 1, 0, 0, 'APARTMÁN RAMSAU', 'APARTMÁN RAMSAU', 'APARTMÁN RAMSAU', 0, 0, 1, 0, NULL, '2014-03-26 10:50:38', NULL, NULL, 1),
(95, 'apartman-ennstalblick', 5, 'detail', NULL, 1, 0, 0, 'APARTMÁN ENNSTALBLICK', 'APARTMÁN ENNSTALBLICK', 'APARTMÁN ENNSTALBLICK', 0, 0, 1, 0, NULL, '2014-03-26 10:53:52', NULL, NULL, 1),
(96, 'leto-haus---schladming', 5, 'detail', NULL, 1, 0, 0, 'LÉTO HAUS - SCHLADMING', 'LÉTO HAUS - SCHLADMING', 'LÉTO HAUS - SCHLADMING', 0, 0, 1, 0, NULL, '2014-03-26 11:10:50', '2014-03-26 11:12:42', NULL, 1),
(97, 'dachstein', 5, 'detail', NULL, 1, 0, 0, 'DACHSTEIN', 'DACHSTEIN', 'DACHSTEIN', 0, 0, 1, 0, NULL, '2014-03-26 11:14:36', '2014-03-26 11:15:11', NULL, 1),
(98, 'turistika', 1, 'detail', NULL, 1, 0, 0, 'Turistika', '', '', 0, 0, 1, 0, NULL, '2014-04-03 12:18:25', '2016-03-31 09:29:06', NULL, 1),
(99, 'golf', 1, 'detail', NULL, 1, 0, 0, 'Golf', '', '', 0, 0, 1, 0, NULL, '2014-04-03 12:18:39', '2016-03-31 09:31:39', NULL, 1),
(100, 'kolo', 1, 'detail', NULL, 1, 0, 0, 'Kolo', '', '', 0, 0, 1, 0, NULL, '2014-04-03 12:18:50', '2016-03-31 09:35:03', NULL, 1),
(101, 'koupani', 1, 'detail', NULL, 1, 0, 0, 'Koupání', '', '', 0, 0, 1, 0, NULL, '2014-04-03 12:19:06', '2016-03-31 09:36:11', NULL, 1),
(102, 'ke-stazeni', 1, 'detail', NULL, 1, 0, 0, 'Ke stažení', '', '', 0, 0, 1, 0, NULL, '2014-04-14 08:45:29', '2016-04-18 06:26:27', NULL, 1),
(103, 'tenis', 1, 'detail', NULL, 1, 0, 0, 'Tenis', '', '', 0, 0, 1, 0, NULL, '2014-05-09 18:50:22', '2016-03-31 09:37:04', NULL, 1),
(104, 'homepage', 1, 'index', NULL, 4, 1, 0, 'Homepage', '', '', 0, 0, 1, 0, NULL, '2014-05-15 07:38:24', '2014-05-15 20:32:02', NULL, 1),
(105, 'apartments', 1, 'detail', NULL, 4, 79, 0, 'Apartments', '', '', 0, 0, 1, 0, NULL, '2014-05-15 07:52:28', '2016-04-01 18:32:07', NULL, 1),
(106, 'reservation', 1, 'reservation', NULL, 4, 81, 0, 'Reservation', '', '', 0, 0, 1, 0, NULL, '2014-05-15 08:00:46', '2016-04-06 09:39:12', NULL, 1),
(107, 'price-list', 1, 'detail', NULL, 4, 89, 0, 'Price list', '', '', 0, 0, 1, 0, NULL, '2014-05-15 08:09:45', '2016-04-01 19:27:38', NULL, 1),
(108, 'winter', 1, 'detail', NULL, 4, 84, 0, 'Winter', '', '', 0, 0, 1, 0, NULL, '2014-05-15 08:20:55', '2014-05-15 08:21:49', NULL, 1),
(109, 'tourism', 1, 'detail', NULL, 4, 98, 0, 'Tourism', '', '', 0, 0, 1, 0, NULL, '2014-05-15 08:34:37', '2016-03-31 09:44:48', NULL, 1),
(110, 'summer', 1, 'detail', NULL, 4, 83, 0, 'Summer', '', '', 0, 0, 1, 0, NULL, '2014-05-15 08:36:26', NULL, NULL, 1),
(111, 'golf-en', 1, 'detail', NULL, 4, 99, 0, 'Golf', '', '', 0, 0, 1, 0, NULL, '2014-05-15 08:41:40', '2016-03-31 09:45:30', NULL, 1),
(112, 'cycling', 1, 'detail', NULL, 4, 100, 0, 'Cycling', '', '', 0, 0, 1, 0, NULL, '2014-05-15 08:58:43', '2016-03-31 09:49:59', NULL, 1),
(113, 'swimming', 1, 'detail', NULL, 4, 101, 0, 'Swimming', '', '', 0, 0, 1, 0, NULL, '2014-05-15 09:08:30', '2016-03-31 09:50:36', NULL, 1),
(114, 'tennis', 1, 'detail', NULL, 4, 103, 0, 'Tennis', '', '', 0, 0, 1, 0, NULL, '2014-05-15 09:12:20', '2016-03-31 09:51:09', NULL, 1),
(115, 'photo-gallery', 5, 'index', NULL, 4, 37, 0, 'Photo Gallery', '', '', 0, 0, 1, 0, NULL, '2014-05-15 09:14:37', NULL, NULL, 1),
(116, 'apartment-ramsau', 5, 'detail', NULL, 4, 94, 0, 'APARTMENT RAMSAU', 'APARTMENT RAMSAU', 'APARTMENT RAMSAU', 0, 0, 1, 0, NULL, '2014-05-15 09:17:30', NULL, NULL, 1),
(117, 'apartment-kaibling', 5, 'detail', NULL, 4, 80, 0, 'APARTMENT KAIBLING', 'APARTMENT KAIBLING', 'APARTMENT KAIBLING', 0, 0, 1, 0, NULL, '2014-05-15 09:18:25', NULL, NULL, 1),
(118, 'apartment-ennstalblick', 5, 'detail', NULL, 4, 95, 0, 'APARTMENT ENNSTALBLICK', 'APARTMENT ENNSTALBLICK', 'APARTMENT ENNSTALBLICK', 0, 0, 1, 0, NULL, '2014-05-15 09:18:58', NULL, NULL, 1),
(119, 'skiing-hauser-kaibling', 5, 'detail', NULL, 4, 86, 0, 'Skiing HAUSER KAIBLING', 'Skiing HAUSER KAIBLING', 'Skiing HAUSER KAIBLING', 0, 0, 1, 0, NULL, '2014-05-15 09:21:08', NULL, NULL, 1),
(120, 'haus-village', 5, 'detail', NULL, 4, 85, 0, 'HAUS village', '', '', 0, 0, 1, 0, NULL, '2014-05-15 09:21:53', NULL, NULL, 1),
(121, 'exterier-en', 5, 'detail', NULL, 4, 78, 0, 'Apartments Exterier', '', '', 0, 0, 1, 0, NULL, '2014-05-15 09:22:27', NULL, NULL, 1),
(122, 'summer-haus---schladming', 5, 'detail', NULL, 4, 96, 0, 'SUMMER HAUS - SCHLADMING', 'SUMMER HAUS - SCHLADMING', 'SUMMER HAUS - SCHLADMING', 0, 0, 1, 0, NULL, '2014-05-15 09:23:12', NULL, NULL, 1),
(123, 'dachstein-en', 5, 'detail', NULL, 4, 97, 0, 'DACHSTEIN', 'DACHSTEIN', 'DACHSTEIN', 0, 0, 1, 0, NULL, '2014-05-15 09:23:27', NULL, NULL, 1),
(124, 'main', 5, 'detail', NULL, 4, 88, 0, 'Main', '', '', 0, 0, 1, 0, NULL, '2014-05-15 09:25:22', '2014-05-15 09:27:42', NULL, 0),
(125, 'webcameras', 1, 'camera', NULL, 4, 87, 0, 'Web cameras', '', '', 0, 0, 1, 0, NULL, '2014-05-15 09:30:26', NULL, NULL, 1),
(126, 'weather', 1, 'detail', NULL, 4, 90, 0, 'Weather', '', '', 0, 0, 1, 0, NULL, '2014-05-15 09:33:18', NULL, NULL, 1),
(127, 'downloads', 1, 'detail', NULL, 4, 102, 0, 'Downloads', '', '', 0, 0, 1, 0, NULL, '2014-05-15 09:36:04', '2014-10-07 16:05:05', NULL, 1),
(128, 'contacts', 1, 'contact', NULL, 4, 65, 0, 'Contacts', '', '', 0, 0, 1, 0, NULL, '2014-05-15 09:40:06', '2016-04-01 18:35:01', NULL, 1),
(129, 'ennstalblick-en', 21, 'detail', NULL, 4, 93, 0, 'ENNSTALBLICK', '', '', 0, 0, 1, 0, NULL, '2014-05-15 23:10:42', '2014-05-15 23:28:24', NULL, 0),
(130, 'kaibling-b10-deleted-10-03-2014073651-en', 21, 'detail', NULL, 4, 92, 0, 'KAIBLING B10', '', '', 0, 0, 1, 0, NULL, '2014-05-15 23:26:52', '2014-05-15 23:28:14', NULL, 0),
(131, 'ramsau-b9-deleted-10-03-2014073620-en', 21, 'detail', NULL, 4, 91, 0, 'RAMSAU B9', '', '', 0, 0, 1, 0, NULL, '2014-05-15 23:27:15', '2014-05-15 23:28:01', NULL, 0),
(132, 'rafty-kajak', 1, 'detail', NULL, 1, 0, 0, 'Rafty - kajak', 'Rafty - kajak', 'rafty - kajak', 0, 0, 1, 0, NULL, '2015-05-14 10:38:02', '2016-03-31 09:38:15', NULL, 1),
(133, 'prohlidka-deleted-30-05-2015(14:22:05)', 1, 'detail', NULL, 1, 0, 0, 'Prohlídka', '', '', 0, 0, 1, 1, 'prohlidka', '2015-05-27 16:36:04', '2015-05-28 10:26:08', '2015-05-30 14:22:05', 0),
(134, 'prohlidka-kaibling', 1, 'detail', NULL, 1, 0, 0, 'Prohlídka', '', '', 0, 0, 1, 0, NULL, '2015-05-30 14:17:55', '2015-05-30 14:18:35', NULL, 1),
(135, 'hochwurzen', 21, 'detail', NULL, 1, 0, 0, 'HOCHWURZEN', '', '', 0, 0, 1, 0, NULL, '2016-03-23 21:59:41', '2016-04-24 19:32:05', NULL, 1),
(137, 'hochwurzen-en', 21, 'detail', NULL, 4, 135, 0, 'HOCHWURZEN', '', '', 0, 0, 1, 0, NULL, '2016-04-04 09:14:16', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL,
  `module_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `submodule_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value_subcode_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `value_subcode_2` varchar(255) CHARACTER SET utf8 NOT NULL,
  `poradi` int(11) NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

--
-- Vypisuji data pro tabulku `settings`
--

INSERT INTO `settings` (`id`, `module_code`, `submodule_code`, `value_code`, `value_subcode_1`, `value_subcode_2`, `poradi`, `value`, `description`) VALUES
(1, 'article', 'item', 'photo', 't1', 'resize', 1, '110,83,Image::WIDTH', ''),
(2, 'article', 'item', 'photo', 't1', 'crop', 2, '110,83', ''),
(3, 'article', 'gallery', 'photo', 't1', 'resize', 3, '214,157,Image::WIDTH', ''),
(4, 'referencex', 'gallery', 'photo', 't1', 'resize', 4, '1024,768,Image::WIDTH', ''),
(5, 'reference', 'gallery', 'photo', 't2', 'resize', 5, '470,200,Image::WIDTH', ''),
(6, 'reference', 'gallery', 'photo', 't2', 'crop', 6, '470,200', ''),
(7, 'catalog', 'category', 'photo', 't1', 'resize', 7, '1024,768,Image::WIDTH', ''),
(8, 'catalog', 'category', 'photo', 't2', 'resize', 8, '215,185,Image::WIDTH', ''),
(9, 'catalog', 'category', 'photo', 't3', 'resize', 9, '186,87,Image::INVERSE', ''),
(10, 'catalog', 'category', 'photo', 't3', 'crop', 10, '186,87', ''),
(11, 'catalog', 'item', 'photo', 't1', 'resize', 11, '461,296,Image::WIDTH', ''),
(12, 'catalog', 'item', 'photo', 't1', 'crop', 12, '461,296', ''),
(13, 'catalog', 'item', 'photo', 't2', 'resize', 13, '204,157,Image::INVERSE', ''),
(14, 'catalog', 'item', 'photo', 't2', 'crop', 14, '204,157', ''),
(15, 'article', 'gallery', 'photo', 't1', 'crop', 15, '214,157', NULL),
(16, 'article', 'item', 'photo', 't2', 'resize', 16, '460,295,Image::WIDTH', NULL),
(17, 'article', 'item', 'photo', 't2', 'crop', 17, '460,295', NULL),
(18, 'article', 'item', 'photo', 't3', 'resize', 18, '214,156,Image::WIDTH', NULL),
(19, 'article', 'item', 'photo', 't3', 'crop', 19, '214,156', NULL),
(20, 'shop', 'item', 'photo', 't1', 'resize', 20, '237,225,Image::WIDTH', NULL),
(21, 'shop', 'item', 'photo', 't1', 'crop', 21, '237,225', NULL),
(22, 'reference', 'gallery', 'photo', 't1', 'resize', 22, '443,155,Image::WIDTH', NULL),
(23, 'reference', 'gallery', 'photo', 't1', 'crop', 23, '443,155', NULL),
(24, 'reference', 'gallery', 'photo', 't3', 'resize', 24, '900,570,Image::WIDTH', NULL),
(25, 'reference', 'gallery', 'photo', 't3', 'crop', 25, '900,570', NULL),
(26, 'page', 'item', 'photo', 't1', 'resize', 26, '279,238,Image::WIDTH', NULL),
(27, 'page', 'item', 'photo', 't1', 'crop', 27, '279,238', NULL),
(28, 'page', 'unrelated', 'photo', 't1', 'resize', 28, '279,238,Image::WIDTH', ''),
(29, 'page', 'unrelated', 'photo', 't1', 'crop', 28, '279,238', ''),
(30, 'page', 'item', 'photo', 't3', 'resize', 0, '933,378', 'Obrázek na stránku před textem se stínem'),
(31, 'page', 'item', 'photo', 't3', 'resize', 29, '933,378', NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `shops`
--

CREATE TABLE IF NOT EXISTS `shops` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `poradi` int(11) NOT NULL,
  `shop_category_id` int(11) NOT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `zobrazit_rotator` tinyint(4) NOT NULL DEFAULT '0',
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `priplatek_sluzby` float NOT NULL,
  `priplatek_taxa` float NOT NULL,
  `poplatek_energie` float NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

--
-- Vypisuji data pro tabulku `shops`
--

INSERT INTO `shops` (`id`, `date`, `poradi`, `shop_category_id`, `photo_src`, `zobrazit_rotator`, `available_languages`, `priplatek_sluzby`, `priplatek_taxa`, `poplatek_energie`) VALUES
(4, '0000-00-00', 3, 0, '', 0, 5, 65, 1.5, 4),
(3, '0000-00-00', 1, 0, '', 0, 5, 65, 1.5, 4),
(5, '0000-00-00', 4, 0, '', 0, 5, 65, 1.5, 4),
(6, '0000-00-00', 2, 0, '', 0, 5, 65, 1.5, 4);

-- --------------------------------------------------------

--
-- Struktura tabulky `shop_data`
--

CREATE TABLE IF NOT EXISTS `shop_data` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nadpis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `technicky_popis` text COLLATE utf8_czech_ci,
  `rozmery` text COLLATE utf8_czech_ci,
  `motor` text COLLATE utf8_czech_ci,
  `shop_category_id` int(11) NOT NULL DEFAULT '0',
  `zobrazit` tinyint(4) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

--
-- Vypisuji data pro tabulku `shop_data`
--

INSERT INTO `shop_data` (`id`, `language_id`, `shop_id`, `route_id`, `nazev`, `nadpis`, `technicky_popis`, `rozmery`, `motor`, `shop_category_id`, `zobrazit`) VALUES
(4, 1, 4, 92, 'APARTMÁN KAIBLING - pro 4 osoby', 'KAIBLING', '', '', '', 0, 1),
(3, 1, 3, 91, 'APARTMÁN RAMSAU - pro 4 osoby', 'RAMSAU', NULL, '', '', 0, 1),
(5, 1, 5, 93, 'APARTMÁN ENNSTALBLICK - pro 4 osoby', 'ENNSTALBLICK', NULL, '', '', 0, 1),
(6, 4, 5, 129, 'APARTMENT ENNSTALBLICK B15 - for 4 persons', 'ENNSTALBLICK', NULL, NULL, NULL, 0, 0),
(7, 4, 4, 130, 'APARTMENT KAIBLING B10 - for 4 persons', 'KAIBLING B10', NULL, NULL, NULL, 0, 0),
(8, 4, 3, 131, 'APARTMENT RAMSAU B9 - for 4 persons', 'RAMSAU B9', NULL, NULL, NULL, 0, 0),
(9, 1, 6, 135, 'Apartmán HOCHWURZEN - pro 4 osoby', 'HOCHWURZEN', NULL, '', '', 0, 1),
(10, 4, 6, 137, 'Apartment HOCHWURZEN - for 4 persons', 'HOCHWURZEN', '', '', '', 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `shop_photos`
--

CREATE TABLE IF NOT EXISTS `shop_photos` (
  `id` int(11) NOT NULL,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `shop_id` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

--
-- Vypisuji data pro tabulku `shop_photos`
--

INSERT INTO `shop_photos` (`id`, `poradi`, `zobrazit`, `photo_src`, `ext`, `shop_id`) VALUES
(1, 1, 1, 'fairline-targa-50-gran-turismo', 'jpg', 2),
(2, 2, 1, 'boat-1_7071', 'jpg', 2),
(3, 3, 1, 'man_in_the_boat', 'jpg', 2),
(4, 4, 1, 'guitar-boat', 'jpg', 2),
(5, 5, 1, 'brosen_motor_boat', 'jpg', 2),
(6, 6, 1, 'boat-show', 'jpg', 2),
(7, 1, 1, 'guitar-boat', 'jpg', 1),
(8, 2, 1, 'fairline-targa-50-gran-turismo', 'jpg', 1),
(9, 3, 1, 'boat-1_7071', 'jpg', 1),
(10, 4, 1, 'man_in_the_boat', 'jpg', 1),
(11, 5, 1, 'brosen_motor_boat', 'jpg', 1),
(12, 6, 1, 'boat-show', 'jpg', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `shop_photo_data`
--

CREATE TABLE IF NOT EXISTS `shop_photo_data` (
  `id` int(11) NOT NULL,
  `shop_photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

--
-- Vypisuji data pro tabulku `shop_photo_data`
--

INSERT INTO `shop_photo_data` (`id`, `shop_photo_id`, `language_id`, `nazev`, `popis`) VALUES
(1, 1, 0, 'Fairline-Targa-50-GRAN-TURISMO', ''),
(2, 2, 0, 'boat-1_7071', ''),
(3, 3, 0, 'man_in_the_boat', ''),
(4, 4, 0, 'guitar-boat', ''),
(5, 5, 0, 'Brosen_motor_boat', ''),
(6, 6, 0, 'boat-show', ''),
(7, 7, 0, 'guitar-boat', ''),
(8, 8, 0, 'Fairline-Targa-50-GRAN-TURISMO', ''),
(9, 9, 0, 'boat-1_7071', ''),
(10, 10, 0, 'man_in_the_boat', ''),
(11, 11, 0, 'Brosen_motor_boat', ''),
(12, 12, 0, 'boat-show', '');

-- --------------------------------------------------------

--
-- Struktura tabulky `static_content`
--

CREATE TABLE IF NOT EXISTS `static_content` (
  `id` int(11) NOT NULL,
  `kod` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

--
-- Vypisuji data pro tabulku `static_content`
--

INSERT INTO `static_content` (`id`, `kod`, `available_languages`) VALUES
(1, 'homepage-info-1', 5),
(3, 'cenik', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `static_content_data`
--

CREATE TABLE IF NOT EXISTS `static_content_data` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `static_content_id` int(11) NOT NULL,
  `popis` text COLLATE utf8_czech_ci
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

--
-- Vypisuji data pro tabulku `static_content_data`
--

INSERT INTO `static_content_data` (`id`, `language_id`, `static_content_id`, `popis`) VALUES
(1, 1, 1, '<p>Apartmány se nachází jen dvě minuty chůze od centra obce Haus, kde naleznete nepřeberné množství restaurací, hospůdek a pizzérií. Apartmány jsou zařízené v moderním alpském stylu ve standartu **** s veškerým vybavením. Velký obývací pokoj, plně vybavená kuchyňka, terasa s posezením, koupelna se sprchovým koutem a vanou atd.</p>\n\n<p><br />\nApartmány se nachází jen dvě minuty chůze od centra obce Haus, kde naleznete nepřeberné množství restaurací, hospůdek a pizzérií. Apartmány jsou zařízené v moderním alpském stylu ve standartu **** s veškerým vybavením.</p>\n'),
(3, 1, 3, NULL),
(4, 4, 1, '<p><span style="line-height: 1.6em;">LUXURY SKI-OUT / SKI-IN APARTMENTS AT HAUS / SCHLADMING</span></p>\n\n<p>Brand new apartments are located right on the ski slope in HAUSER Kaibling, just 2 minutes<br />\nwalk from the village center where you will find many restaurants, pubs and pizzerias.<br />\nApartments&nbsp; are furnished in a modern Alpine style in the **** standard with all facilities.</p>\n\n<p><img alt="" src="http://alpineliving.cz/media/userfiles/Obrazky-mix/ikonye.jpg" class="full-width-img" style="width: 600px; height: 88px;" /></p>\n');

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL,
  `email` varchar(127) NOT NULL,
  `username` varchar(32) NOT NULL DEFAULT '',
  `password` char(50) NOT NULL,
  `logins` int(10) unsigned NOT NULL DEFAULT '0',
  `last_login` int(10) unsigned DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `logins`, `last_login`, `created_by`) VALUES
(5, 'info.vzak@gmail.com', 'hana', 'e67fcbdb1f94a7645d31ff55cbdcee5925b5ef1381e5b6f2e4', 263, 1424350793, 5),
(7, 'info@dgstudio.cz', 'dgstudio', '58c6017b8b5254f2ea76e007d4d946486959cfb632b2037824', 112, 1461518801, 5),
(8, 'petr@ohnut.cz', 'ohnut', 'fe8d7663f44a5cb475227cdb896e644bfbfa6ef050e46ab563', 8, 1384941392, 8),
(10, 'michal.vymola@avex.cz', 'avex', '2bcfb8844df1f81b7d7ab7e7fa766cf70da596b923c265e050', 53, 1399653513, 7),
(11, 'ski@avex.cz', 'admin', '5f0dcc6621fa5f5f57005d597c8e48535d103b7914335b0c00', 673, 1460625945, 10),
(12, 'mohr@fototrend.cz', 'mohr', 'b414d3270f4b6ab2fb7641c0547695042d80fcda8e175cca34', 28, 1460224606, 11),
(13, 'tk.kudela@gmail.com', 'kudela', 'ffa014d7edf234ef4d83345ca55f46ed98820edea8db9aea7a', 6, 1459775142, 11),
(14, 'daniel.has@dgstudio.cz', 'vyvoj', '8dab915a1add4d5c0a8969cf7bb0905274d72997c3afc6e85f', 5, 1460958836, 7);

-- --------------------------------------------------------

--
-- Struktura tabulky `user_admin_prefernces`
--

CREATE TABLE IF NOT EXISTS `user_admin_prefernces` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `admin_preference_id` int(11) DEFAULT NULL,
  `value` varchar(10) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `user_log`
--

CREATE TABLE IF NOT EXISTS `user_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `datetime_start` datetime NOT NULL,
  `datetime_end` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `user_rights`
--

CREATE TABLE IF NOT EXISTS `user_rights` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `module_name` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `permission` tinyint(11) NOT NULL DEFAULT '0',
  `readonly` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktura tabulky `user_tokens`
--

CREATE TABLE IF NOT EXISTS `user_tokens` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(32) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `admin_preferences`
--
ALTER TABLE `admin_preferences`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `admin_settings`
--
ALTER TABLE `admin_settings`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `admin_structure`
--
ALTER TABLE `admin_structure`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Klíče pro tabulku `admin_structure_data`
--
ALTER TABLE `admin_structure_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `admin_module_id` (`admin_structure_id`);

--
-- Klíče pro tabulku `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `article_data`
--
ALTER TABLE `article_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`,`article_id`),
  ADD KEY `article_id` (`article_id`),
  ADD KEY `route_id` (`route_id`),
  ADD KEY `zobrazit` (`zobrazit`);

--
-- Klíče pro tabulku `article_photos`
--
ALTER TABLE `article_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `zobrazit` (`zobrazit`),
  ADD KEY `article_id` (`article_id`);

--
-- Klíče pro tabulku `article_photo_data`
--
ALTER TABLE `article_photo_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_photo_id` (`article_photo_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Klíče pro tabulku `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reservation_id` (`reservation_id`);

--
-- Klíče pro tabulku `email_queue`
--
ALTER TABLE `email_queue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email_queue_body_id` (`email_queue_body_id`),
  ADD KEY `queue_sent` (`queue_sent`),
  ADD KEY `queue_errors_count` (`queue_errors_count`);

--
-- Klíče pro tabulku `email_queue_bodies`
--
ALTER TABLE `email_queue_bodies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email_type_id` (`queue_email_type_id`),
  ADD KEY `queue_newsletter_id` (`queue_newsletter_id`);

--
-- Klíče pro tabulku `email_receivers`
--
ALTER TABLE `email_receivers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Klíče pro tabulku `email_settings`
--
ALTER TABLE `email_settings`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `email_types`
--
ALTER TABLE `email_types`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `email_types_receivers`
--
ALTER TABLE `email_types_receivers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email_type_id` (`email_type_id`,`email_receiver_id`);

--
-- Klíče pro tabulku `language_strings`
--
ALTER TABLE `language_strings`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `language_string_data`
--
ALTER TABLE `language_string_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`);

--
-- Klíče pro tabulku `manufacturers`
--
ALTER TABLE `manufacturers`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `manufacturer_data`
--
ALTER TABLE `manufacturer_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `zobrazit` (`zobrazit`),
  ADD KEY `manufacturer_id` (`manufacturer_id`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `route_id` (`route_id`);

--
-- Klíče pro tabulku `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kod` (`kod`);

--
-- Klíče pro tabulku `module_actions`
--
ALTER TABLE `module_actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_id` (`module_id`,`povoleno`);

--
-- Klíče pro tabulku `owner_data`
--
ALTER TABLE `owner_data`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_category_id` (`page_category_id`),
  ADD KEY `show_in_menu` (`show_in_menu`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Klíče pro tabulku `page_categories`
--
ALTER TABLE `page_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `code` (`code`);

--
-- Klíče pro tabulku `page_data`
--
ALTER TABLE `page_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `page_id` (`page_id`),
  ADD KEY `route_id` (`route_id`);

--
-- Klíče pro tabulku `page_photos`
--
ALTER TABLE `page_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `zobrazit` (`zobrazit`),
  ADD KEY `page_id` (`page_id`);

--
-- Klíče pro tabulku `page_photo_data`
--
ALTER TABLE `page_photo_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_photo_id` (`page_photo_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Klíče pro tabulku `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `product_categories_products`
--
ALTER TABLE `product_categories_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_category_id` (`product_category_id`);

--
-- Klíče pro tabulku `product_category_data`
--
ALTER TABLE `product_category_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`,`product_category_id`),
  ADD KEY `route_id` (`route_id`),
  ADD KEY `zobrazit` (`zobrazit`),
  ADD KEY `product_category_id` (`product_category_id`);

--
-- Klíče pro tabulku `product_category_photos`
--
ALTER TABLE `product_category_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `zobrazit` (`zobrazit`),
  ADD KEY `product_category_id` (`product_category_id`);

--
-- Klíče pro tabulku `product_category_photo_data`
--
ALTER TABLE `product_category_photo_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `product_category_photo_id` (`product_category_photo_id`);

--
-- Klíče pro tabulku `product_data`
--
ALTER TABLE `product_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`,`product_id`),
  ADD KEY `route_id` (`route_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Klíče pro tabulku `product_files`
--
ALTER TABLE `product_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `zobrazit` (`zobrazit`),
  ADD KEY `product_id` (`product_id`);

--
-- Klíče pro tabulku `product_file_data`
--
ALTER TABLE `product_file_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_file_id` (`product_file_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Klíče pro tabulku `product_photos`
--
ALTER TABLE `product_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `zobrazit` (`zobrazit`),
  ADD KEY `product_id` (`product_id`);

--
-- Klíče pro tabulku `product_photo_data`
--
ALTER TABLE `product_photo_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `product_photo_id` (`product_photo_id`);

--
-- Klíče pro tabulku `references`
--
ALTER TABLE `references`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `reference_data`
--
ALTER TABLE `reference_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`,`reference_id`),
  ADD KEY `route_id` (`route_id`),
  ADD KEY `reference_id` (`reference_id`);

--
-- Klíče pro tabulku `reference_photos`
--
ALTER TABLE `reference_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `zobrazit` (`zobrazit`),
  ADD KEY `reference_id` (`reference_id`);

--
-- Klíče pro tabulku `reference_photo_data`
--
ALTER TABLE `reference_photo_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `reference_photo_id` (`reference_photo_id`);

--
-- Klíče pro tabulku `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dat` (`termin_od`,`termin_do`) USING BTREE,
  ADD KEY `apartman_id` (`apartman_id`);

--
-- Klíče pro tabulku `reservation_prices`
--
ALTER TABLE `reservation_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Date` (`termin_od`,`termin_do`),
  ADD KEY `shop_id` (`shop_id`);

--
-- Klíče pro tabulku `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uniq_name` (`name`);

--
-- Klíče pro tabulku `roles_users`
--
ALTER TABLE `roles_users`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `fk_role_id` (`role_id`);

--
-- Klíče pro tabulku `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nazev_seo` (`nazev_seo`),
  ADD KEY `page_type_id` (`module_id`),
  ADD KEY `zobrazit` (`zobrazit`),
  ADD KEY `baselang_route_id` (`baselang_route_id`),
  ADD KEY `parent_id` (`parent_route_id`);

--
-- Klíče pro tabulku `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_code` (`module_code`),
  ADD KEY `submodule_code` (`submodule_code`),
  ADD KEY `value_code` (`value_code`);

--
-- Klíče pro tabulku `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `shop_data`
--
ALTER TABLE `shop_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`,`shop_id`),
  ADD KEY `article_id` (`shop_id`),
  ADD KEY `route_id` (`route_id`),
  ADD KEY `zobrazit` (`zobrazit`);

--
-- Klíče pro tabulku `shop_photos`
--
ALTER TABLE `shop_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `zobrazit` (`zobrazit`),
  ADD KEY `article_id` (`shop_id`);

--
-- Klíče pro tabulku `shop_photo_data`
--
ALTER TABLE `shop_photo_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_photo_id` (`shop_photo_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Klíče pro tabulku `static_content`
--
ALTER TABLE `static_content`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `static_content_data`
--
ALTER TABLE `static_content_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `static_page_id` (`static_content_id`);

--
-- Klíče pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uniq_username` (`username`),
  ADD UNIQUE KEY `uniq_email` (`email`);

--
-- Klíče pro tabulku `user_admin_prefernces`
--
ALTER TABLE `user_admin_prefernces`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `user_log`
--
ALTER TABLE `user_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Klíče pro tabulku `user_rights`
--
ALTER TABLE `user_rights`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `module_name` (`module_name`);

--
-- Klíče pro tabulku `user_tokens`
--
ALTER TABLE `user_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uniq_token` (`token`),
  ADD KEY `fk_user_id` (`user_id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `admin_settings`
--
ALTER TABLE `admin_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pro tabulku `admin_structure`
--
ALTER TABLE `admin_structure`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT pro tabulku `admin_structure_data`
--
ALTER TABLE `admin_structure_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT pro tabulku `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `article_data`
--
ALTER TABLE `article_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `article_photos`
--
ALTER TABLE `article_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `article_photo_data`
--
ALTER TABLE `article_photo_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pro tabulku `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=368;
--
-- AUTO_INCREMENT pro tabulku `email_queue`
--
ALTER TABLE `email_queue`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pro tabulku `email_queue_bodies`
--
ALTER TABLE `email_queue_bodies`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pro tabulku `email_receivers`
--
ALTER TABLE `email_receivers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `email_types`
--
ALTER TABLE `email_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pro tabulku `email_types_receivers`
--
ALTER TABLE `email_types_receivers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `language_strings`
--
ALTER TABLE `language_strings`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `manufacturers`
--
ALTER TABLE `manufacturers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `manufacturer_data`
--
ALTER TABLE `manufacturer_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT pro tabulku `module_actions`
--
ALTER TABLE `module_actions`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `owner_data`
--
ALTER TABLE `owner_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pro tabulku `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT pro tabulku `page_categories`
--
ALTER TABLE `page_categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pro tabulku `page_data`
--
ALTER TABLE `page_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT pro tabulku `page_photos`
--
ALTER TABLE `page_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `page_photo_data`
--
ALTER TABLE `page_photo_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `product_categories_products`
--
ALTER TABLE `product_categories_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `product_category_data`
--
ALTER TABLE `product_category_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `product_category_photos`
--
ALTER TABLE `product_category_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `product_category_photo_data`
--
ALTER TABLE `product_category_photo_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `product_data`
--
ALTER TABLE `product_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `product_files`
--
ALTER TABLE `product_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `product_file_data`
--
ALTER TABLE `product_file_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `product_photos`
--
ALTER TABLE `product_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `product_photo_data`
--
ALTER TABLE `product_photo_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `references`
--
ALTER TABLE `references`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pro tabulku `reference_data`
--
ALTER TABLE `reference_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT pro tabulku `reference_photos`
--
ALTER TABLE `reference_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=157;
--
-- AUTO_INCREMENT pro tabulku `reference_photo_data`
--
ALTER TABLE `reference_photo_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=158;
--
-- AUTO_INCREMENT pro tabulku `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=184;
--
-- AUTO_INCREMENT pro tabulku `reservation_prices`
--
ALTER TABLE `reservation_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pro tabulku `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pro tabulku `routes`
--
ALTER TABLE `routes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=138;
--
-- AUTO_INCREMENT pro tabulku `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT pro tabulku `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pro tabulku `shop_data`
--
ALTER TABLE `shop_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pro tabulku `shop_photos`
--
ALTER TABLE `shop_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pro tabulku `shop_photo_data`
--
ALTER TABLE `shop_photo_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pro tabulku `static_content`
--
ALTER TABLE `static_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pro tabulku `static_content_data`
--
ALTER TABLE `static_content_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pro tabulku `user_admin_prefernces`
--
ALTER TABLE `user_admin_prefernces`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `user_log`
--
ALTER TABLE `user_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `user_rights`
--
ALTER TABLE `user_rights`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `user_tokens`
--
ALTER TABLE `user_tokens`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `user_tokens`
--
ALTER TABLE `user_tokens`
  ADD CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
