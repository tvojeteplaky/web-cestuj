<?php
return array(
	'alpha'         => 'Pole :field must contain only letters',
	'alpha_dash'    => 'Pole :field must contain only letters and dashes',
	'alpha_numeric' => 'Pole :field must contain only letters and numbers',
	'color'         => 'Pole :field must be a color',
	'credit_card'   => 'Pole :field must be a credit card number',
	'date'          => 'Pole :field must be a date',
	'decimal'       => 'Pole :field must be a decimal with :param1 places',
	'digit'         => 'Pole :field must be a digit',
	'email'         => 'Pole :field must be a email address',
	'email_domain'  => 'Pole :field must contain a valid email domain',
	'exact_length'  => 'Pole :field must be exactly :param1 characters long',
	'in_array'      => 'Pole :field must be one of the available options',
	'ip'            => 'Pole :field must be an ip address',
	'matches'       => 'Pole :field must be the same as :param1',
	'min_length'    => 'Položka nemá požadovaný počet :param1 znaků',
	'max_length'    => 'Pole :field must be less than :param1 characters long',
	'phone'         => 'Pole :field must be a phone number',
	'not_empty'     => 'Položka musí být vyplněna.',
	'range'         => 'Pole :field must be within the range of :param1 to :param2',
	'regex'         => 'Pole :field does not match the required format',
	'url'           => 'Pole :field must be a url',
	'not_unique_nazev_seo'=>"Zvolený název seo již existuje.",
  'not_unique_code'=>"Zvolený kód produktu již existuje."
);
?>