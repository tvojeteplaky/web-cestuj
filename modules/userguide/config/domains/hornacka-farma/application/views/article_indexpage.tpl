{foreach name=art from=$articles key=key item=item}
<div class="item">
	<article>
		<div class="content">
			<div class="heading">
				<a href="{$url_base}{$item.nazev_seo}" title="{$item.nazev}">
					<small><time pubdate="pubdate">{$item.date|date_format:'%d. %m. %Y'}</time></small> | <h1>{$item.nazev}</h1>
				</a>
			</div>
			<div class="text">
				{$item.uvodni_popis}
			</div>
			<div class="more">
				<a href="{$url_base}{$item.nazev_seo}" title="více »">více</a>
			</div>
		</div>

		<div class="image">
			{if $item.photo}
				<a href="{$url_base}{$item.nazev_seo}" title="{$item.nazev}">
					<img src="{$item.photo}" alt="Náhled">
				</a>
			{/if}
		</div>
	</article>
	<div class="clear"></div>
</div>
{/foreach}