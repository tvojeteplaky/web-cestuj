{* widget (boxik) s nastroji pro uzivatele vkladany do beznych stranek *}
{* promenna $user je namapovana na db *}
{if !$logged_in}
	<a href="{$url_base}prihlaseni-uzivatele" title="Přihlášení uživatele">Přihlášení</a> | <a href="{$url_base}registrace-uzivatele" title="Registrace nového uživatele">Registrace</a>
{else}
	<a href="{$url_base}ucet-uzivatele" title="Nastavení osobních údajů"><strong>{$user.nazev}</strong></a> | <a href="{$url_base}odhlaseni-uzivatele" title="Odhlášení">Odhlášení</a>
{/if}