{* sablona pro registraci uzivatele *}
{assign var='included' value=$included|default:false}
{assign var='editmode' value=$editmode|default:false}

{if !$included}
<section>
<div id="registration">
<h1>{if $editmode}Změna registračních údajů{else}Registrační formulář{/if}</h1>
<p>Položky označené * jsou povinné.</p>
<form action="" method="post" id="registrationForm">
{/if}
  {if $registration_error_change}
  <div class="caption captError">{$registration_error_change}</div>
  {/if}

  <h2>Přihlašovací údaje</h2>
  <table>
    <tr>
      <th>
		  <label for="username">Uživatelské jméno</label>
  		  <span class="description">*</span>
	  </th>
      <td>
		  <input type="text" class="inputbox-standard-text" name="user-registration[username]" id="user-registration-username" value="{$data.username}" />{if $registration_errors.username}<span class="error">{$registration_errors.username}</span>{/if}
	  </td>
    </tr>
    <tr>
      <th>
		  <label for="password">Heslo</label>
		  <span class="description">*</span>
	  </th>
      <td>
		  <input type="password" name="user-registration[password]" id="user-registration-password" />{if $registration_errors.password}<span class="error">{$registration_errors.password}</span>{/if}
	  </td>
	</tr>
    <tr>
      <th>
		  <label for="password-confirm">Potvrzení hesla</label>
		  <span class="description">*</span>
	  </th>
      <td>
		  <input type="password" name="user-registration[password_confirm]" id="user-registration-password-confirm" />{if $registration_errors.password_confirm}<span class="error">{$registration_errors.password_confirm}</span>{/if}
	  </td>
	</tr>
    <tr>
      <th>
		  <label for="myFirstOption">Výběr distribučního místa</label>
		  <span class="description">*</span>
	  </th>
	  <td>
		<select name="user-registration[place_id]" id="myFirstOption">
			<option>Vyberte distribuční místo</option>
            {foreach from=$product_places  key=key item=item name=places}
			<option value="{$item.id}" {if $item.id==$place_id or $data.place_id==$item.id}selected="selected"{/if}>{$item.mesto}</option>
            {/foreach}
        </select>

		{if $registration_errors.place_id}
			<span class="error">{$registration_errors.place_id}</span>
		{/if}
	  </td>
    </tr>
	{* if !$editmode}
	<tr>
		<th>&nbsp;</th>
		<td>
			<select name="user-registration[distributionplace_id]" id="mySecondSelect">
				<option>Vyberte distribuční místo</option>
			</select>
		</td>
	</tr>
	{/*}
	</table>

	<h2>Kontaktní informace</h2>
	<table>
		<tr>
			<th>
				<label for="nazev">Jméno a příjmení, firma *</label>
			</th>
			<td>
				<input type="text" name="user-registration[nazev]" id="user-registration.nazev" value="{$data.nazev}" />{if $registration_errors.nazev}<span class="error">{$registration_errors.nazev}</span>{/if}
			</td>
		</tr>
    <tr>
      <th><label for="ulice">Ulice a číslo popisné *</label></th>
      <td><input type="text" name="user-registration[ulice]" id="user-registration-ulice" value="{$data.ulice}" />{if $registration_errors.ulice}<span class="error">{$registration_errors.ulice}</span>{/if}</td>
    </tr>
    <tr>
      <th><label for="mesto">Město *</label></th>
      <td><input type="text" name="user-registration[mesto]" id="user-registration-mesto" value="{$data.mesto}" />{if $registration_errors.mesto}<span class="error">{$registration_errors.mesto}</span>{/if}</td>
    </tr>
    <tr>
      <th><label for="psc">PSČ *</label></th>
      <td><input type="text" name="user-registration[psc]" id="user-registration-psc" value="{$data.psc}" />{if $registration_errors.psc}<span class="error">{$registration_errors.psc}</span>{/if}</td>
    </tr>
    <tr>
      <th><label for="email">E-mail *</label></th>
      <td><input type="text" name="user-registration[email]" id="user-registration-email" value="{$data.email}" />{if $registration_errors.email}<span class="error">{$registration_errors.email}</span>{/if}</td>
    </tr>
    <tr>
      <th><label for="telefon">Telefon *</label></th>
      <td><input type="text" name="user-registration[telefon]" id="user-registration-telefon" value="{$data.telefon}" />{if $registration_errors.telefon}<span class="error">{$registration_errors.telefon}</span>{/if}</td>
    </tr>
    <tr>
      <th><label for="ic">IČ</label></th>
      <td><input type="text" name="user-registration[ic]" id="user-registration-ic" value="{$data.ic}" />{if $registration_errors.ic}<span class="error">{$registration_errors.ic}</span>{/if}</td>
    </tr>
    <tr>
      <th><label for="dic">DIČ</label></th>
      <td><input type="text" name="user-registration[dic]" id="user-registration-dic" value="{$data.dic}" />{if $registration_errors.dic}<span class="error">{$registration_errors.dic}</span>{/if}</td>
    </tr>
	<tr>
		<td colspan="2" class="t-right">
			{if $editmode}
				<a class="button cancel" href="/ucet-uzivatele" >Storno</a>
			{/if}
			<input type="submit" name="next_step" title="Odeslat registraci" value="{if $editmode}Uložit{else}Registrovat{/if}" class="button success" />
		</td>
	</tr>
  </table>

  {if !$included}<input type="hidden" name="action" value="user-registration" />{/if}
{if !$included}
</form>
</div>
</section>
{/if}