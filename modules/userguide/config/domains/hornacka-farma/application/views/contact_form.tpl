{* sablona kontaktniho formulare *}{* soucast kontroleru contactform *}
<div id="FormSection">
<a name="form" id="form"></a>

{if $contact_form_message}<div class="msg strong green">{$contact_form_message}</div>{/if}
{if $contact_form_error}<div class="msg strong red">{$contact_form_error}</div>{/if}

{if $recommend_form_message}<div class="msg strong green">{$recommend_form_message}</div>{/if}
{if $recommend_form_error}<div class="msg strong red">{$recommend_form_error}</div>{/if}

  <div id="TabsF" class="box">
    <ul><li>
      <a href="#TabsF-1" class="bookmark bookmark1">{translate str="Kontaktní formulář"}</a></li><li>
      <a href="#TabsF-2" class="bookmark bookmark2">{translate str="Odeslat odkaz"}</a></li>
    </ul>
    <div class="content">
      <div id="TabsF-1">
        <form action="{$urlform}#form" method="post" class="presentationForm">
          <input type="hidden" name="action" value="dotaz" />
          <input type="hidden" name="contact_location" value="{$current_url}" />
          <input type="hidden" name="nazev_produktu" value="{$subject_name}" />

          <table border="0" summary="kontaktní formulář">
            <tr>
              <td colspan="2">{translate str="Zaujaly vás informace na této stránce a chcete se"}
                <span class="bold">{translate str="dozvědět více"}?
                </span>. {translate str="Neváhejte a vyplňte následující kontaktní formulář. S odpovědí se Vám ozveme co nejdříve."}</td>
            </tr>
            <tr>
              <td class="col1">
                <label for="dotaz_jmeno">{translate str="Jméno a příjmení"} (
                  <span class="red">*
                  </span>){if isset($error.dotaz_jmeno)}
                  <span>{$error.dotaz_jmeno}
                  </span>{/if}
                </label>
                <input type="text" id="dotaz_jmeno" name="dotaz_jmeno" value="{$form.dotaz_jmeno}" /></td>                <td>
                <label for="dotaz_telefon">{translate str="Telefon"} (
                  <span class="red">*
                  </span>){if isset($error.dotaz_telefon)}
                  <span>{$error.dotaz_telefon}
                  </span>{/if}
                </label>
                <input type="text" id="dotaz_telefon" name="dotaz_telefon" value="{$form.dotaz_telefon}"/></td>
            </tr>
            <tr>                <td>
                <label for="dotaz_firma">{translate str="Firma"}{if isset($error.dotaz_firma)}
                  <span>{$error.dotaz_firma}
                  </span>{/if}
                </label>
                <input type="text" id="dotaz_firma" name="dotaz_firma" value="{$form.dotaz_firma}" /></td>                <td>
                <label for="dotaz_email">E-mail (
                  <span class="red">*
                  </span>){if isset($error.dotaz_email)}
                  <span>{$error.dotaz_email}
                  </span>{/if}
                </label>
                <input type="text" id="dotaz_email" name="dotaz_email" value="{$form.dotaz_email}" /></td>
            </tr>
            <tr>
              <td colspan="2">
                <label for="text_dotazu">{translate str="Text zprávy"} (
                  <span class="red">*
                  </span>){if isset($error.text_dotazu)}
                  <span>{$error.text_dotazu}
                  </span>{/if}
                </label>
<textarea id="text_dotazu" name="text_dotazu" rows="6" cols="74">{$form.text_dotazu}</textarea></td>
            </tr>
            <tr>
              <td colspan="2">
                <div class="botRow">{translate str="Pro odeslání dotazu vyplňte prosím následující kód"} (
                  <span class="red">*
                  </span>):   <br />
                  <span class="captcha">{$captcha}
                  </span>
                  <input type="text" id="dotaz_captcha" name="dotaz_captcha" class="kod" />{if isset($error.dotaz_captcha)}
                  <span class="red"> {$error.dotaz_captcha}
                  </span>{/if}
                  <span class="info">
                    {*<img src="{$resources_path}img/ico_info.gif" alt="info" title="info" border="0" height="16" width="11" />*}{translate str="Položky označené"} (
                    <span class="red">*
                    </span>)
                    <span class="bold">{translate str="jsou povinné"}
                    </span>.
                  </span>
                </div>
                <input type="submit" value="{translate str="odeslat"}" title="odeslat" name="odeslat" class="submit" /></td>
            </tr>
          </table>
        </form>
      </div>
      <div id="TabsF-2" >
        <form action="{$urlform}#form" method="post" class="presentationForm">
          <input type="hidden" name="action" value="doporucit" />
          <input type="hidden" name="recommend_location" value="{$current_url}" />
          <table border="0" summary="kontaktní formulář">
            <tr>
              <td class="col1">
                <label for="jmeno">{translate str="Vaše jméno"} (
                  <span class="red">*
                  </span>){if isset($error.jmeno)}
                  <span>{$error.jmeno}
                  </span>{/if}
                </label>
                <input type="text" id="jmeno" name="jmeno" value="{$form.jmeno}" /></td>                <td>
                <label for="email">{translate str="Váš e-mail"} (
                  <span class="red">*
                  </span>){if isset($error.email)}
                  <span>{$error.email}
                  </span>{/if}
                </label>
                <input type="text" id="email" name="email" value="{$form.email}" /></td>
            </tr>
            <tr>
              <td class="col1">
                <label for="jmeno2">{translate str="Jméno příjemce"} (
                  <span class="red">*
                  </span>){if isset($error.jmeno2)}
                  <span>{$error.jmeno2}
                  </span>{/if}
                </label>
                <input type="text" id="jmeno2" name="jmeno2" value="{$form.jmeno2}" /></td>                <td>
                <label for="email2">{translate str="E-mail příjemce"} (
                  <span class="red">*
                  </span>){if isset($error.email2)}
                  <span>{$error.email2}
                  </span>{/if}
                </label>
                <input type="text" id="email2" name="email2" value="{$form.email2}" /></td>
            </tr>
            <tr>
              <td colspan="2">
                <label for="zprava">{translate str="Text zprávy"} (
                  <span class="red">*
                  </span>){if isset($error.zprava)}
                  <span>{$error.zprava}
                  </span>{/if}
                </label>
<textarea id="zprava" name="zprava" rows="6" cols="74">{$form.zprava}</textarea></td>
            </tr>
            <tr>
              <td colspan="2">
                <div class="botRow">{translate str="Pro odeslání zprávy vyplňte prosím následující kód"} (
                  <span class="red">*
                  </span>):      <br />
                  <span class="captcha">{$captcha}</span>
                  <input type="text" id="kod" name="kod" class="kod" />{if isset($error.kod)}
                  <span class="red"> {$error.kod}
                  </span>{/if}
                  <span class="info">
                    {*<img src="{$resources_path}img/ico_info.gif" alt="info" title="info" border="0" height="16" width="11" />*}{translate str="Položky označené"} (*)
                    <span class="bold">{translate str="jsou povinné"}
                    </span>.
                  </span>
                </div>
                <input type="submit" value="{translate str="odeslat"}" title="odeslat" name="odeslat" class="submit" /></td>
            </tr>
          </table>
        </form>
      </div>
    </div>
    <div class="foot">
    </div>
  </div>
</div>