{* vypis kategorie produktu - vnejsi obalujici sablona *}
<section>
	<div id="categories">
		<section>
			<h1>{$page.nadpis}</h1>
			<p>
				{$page.popis}
			</p>
		</section>

		<div class="products-box">
			{* vnitni sablona - vypis produktu*}
			{$product_categories_items}
		</div>
		<div class="clear"></div>
	</div>
</section>
<div class="clear"></div>