<?xml version="1.0" encoding="UTF-8"?>
<dat:dataPack id="ob001" ico="27706371" application="Farma obchod" version="2.0" note="Import objednávky" xmlns:dat="http://www.stormware.cz/schema/version_2/data.xsd" xmlns:ord="http://www.stormware.cz/schema/version_2/order.xsd" xmlns:typ="http://www.stormware.cz/schema/version_2/type.xsd">
  <dat:dataPackItem id="NAB001" version="2.0">
    <!-- Import nabídky, textová a skladová položka-->
    <ord:order version="2.0">
      <ord:orderHeader>
        <!--cislo objednavky-->
        <ord:number>
          <typ:numberRequested>{$order_id}</typ:numberRequested> 
        </ord:number>
        <!--nechat stejne-->
        <ord:orderType>receivedOrder</ord:orderType>
        <!--datum objednavky-->
        <ord:date>{$xml.order_date|date_format:"%Y-%m-%d"}</ord:date>
        <!--nechat stejne-->
        <ord:text>Objednávka z webu</ord:text>
        <ord:intNote>{$xml.order_shopper_note}</ord:intNote>
        <!--kod zakaznika-->
        <ord:partnerIdentity>
          <typ:address>
          <typ:company>{$xml.order_shopper_code}</typ:company>
          <typ:name>{$xml.order_shopper_name}</typ:name>
          <typ:city>{$xml.order_shopper_city}</typ:city>
          <typ:street>{$xml.order_shopper_street}</typ:street>
          <typ:zip>{$xml.order_shopper_zip}</typ:zip>
          <typ:ico>{$xml.order_shopper_ic}</typ:ico>
          <typ:dic>{$xml.order_shopper_dic}</typ:dic>
          </typ:address>
        </ord:partnerIdentity>
       <!--poznamka objednavky od zakaznika pri odesilani objednavky-->
        <ord:note>{$xml.order_shopper_email};{$xml.order_shopper_phone};{$xml.order_shopper_note}</ord:note> 
	<ord:paymentType><typ:ids>hotově</typ:ids></ord:paymentType>
      </ord:orderHeader>
      <ord:orderDetail>
        <!--jednotlive polozky objednavky-->
        {foreach item=item key=k name=iter from=$products} 
        <ord:orderItem>
          <!--nazev produktu-->
          <ord:text>{$item.nazev}</ord:text>
          <!--pocet jednotek-->
          <ord:quantity>{$item.mnozstvi}</ord:quantity>
          <!--merna jednotka-->
          <ord:unit>{$item.jednotka}</ord:unit>
          <!--cena s dph (true)/cena bez dph(false)-->
          <ord:payVAT>true</ord:payVAT>
          <!--dan (none, low, high)-->
          <ord:rateVAT>{if $item.tax_code == "higher_vat"}high{elseif $item.tax_code == "lower_vat"}low{else}none{/if}</ord:rateVAT>
          <!--jednotkova cena za polozku v Kc-->
          <ord:homeCurrency>
            <typ:unitPrice>{$item.cena_s_dph}</typ:unitPrice>
          </ord:homeCurrency>
          <!--kod produktu-->
          <ord:code>{$item.code}</ord:code>
          <ord:stockItem>
            <typ:stockItem>
              <typ:ids>{$item.code}</typ:ids>
              </typ:stockItem>
            </ord:stockItem>
        </ord:orderItem>
        {/foreach}
      </ord:orderDetail>
      <ord:orderSummary>
        <ord:roundingDocument>up2one</ord:roundingDocument>
      </ord:orderSummary>
    </ord:order>
  </dat:dataPackItem>
</dat:dataPack>