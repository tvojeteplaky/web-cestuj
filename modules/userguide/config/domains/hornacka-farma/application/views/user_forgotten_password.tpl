{* sablona pro zapomenuté heslo *}

<div class="product">
  {if $message=="regok"}
  <div class="caption captOk">Změna registračních údajů byla provedena</div>
  {elseif $message=="branchok"}
  <div class="caption captOk">Údaje o pobočce byly uloženy</div>
  {elseif $message=="branchdeleted"}
  <div class="caption">Zvolená pobočka byla smazána</div>
  {/if}
 
  <h2>Obnovení hesla</h2>
  {if !$message}
  <h3>Po zadání registračního mailu Vám bude zasláno nové heslo</h3>
  <div class="registration">

<form action="" method="post">
    <input type="hidden" name="action" value="forgotten-password" />   
    <label for="dic">Registrační email:</label>
    <input type="text" class="inputbox-standard-text" name="registration_mail" id="user-registration-username" value="" />{if $registration_errors.username}<span class="error">{$registration_errors.username}</span>{/if}
    <input type="submit" name="next_step" value="Odeslat" class="right submit b140" />
</form>
  {else}
  <h3>{$message}</h3>
  {/if}

  </div>
</div>