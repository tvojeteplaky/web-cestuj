{* sablona pro zobrazeni tabulky s prehledem udaju v poslednim kroku objednavky *}
<h2 class="caption">Fakturační údaje</h2>
<table summary="Fakturační údaje">
  <tr class="sectiontableentry2">
    <td class="order-col-default1">Jméno a příjmení, firma</td>
    <td>{$billing_data.nazev}</td>
  </tr>
  <tr class="sectiontableentry2">
    <td>Ulice a číslo popisné</td>
    <td>{$billing_data.ulice}</td>
  </tr>
  <tr class="sectiontableentry2">
    <td>Město</td>
    <td>{$billing_data.mesto}</td>
  </tr>
  <tr class="sectiontableentry2">
    <td>PSČ</td>
    <td>{$billing_data.psc}</td>
  </tr>
  <tr class="sectiontableentry2">
    <td>E-mail</td>
    <td>{$billing_data.email}</td>
  </tr>
  <tr class="sectiontableentry2">
    <td>Telefon</td>
    <td>{$billing_data.telefon}</td>
  </tr>
  <tr class="sectiontableentry2">
    <td>IČ</td>
    <td>{$billing_data.ic}</td>
  </tr>
  <tr class="sectiontableentry2">
    <td>DIČ</td>
    <td>{$billing_data.dic}</td>
  </tr>
</table>

<h2 class="caption">Dodací adresa</h2>
<table summary="Dodací adresa">
  {if !$branch_data || !$branch_data.id}
  <tr class="sectiontableentry2">
    <td class="order-col-default1">Stejná jako fakturační</td>
    <td></td>
  </tr>
  {else}
  <tr class="sectiontableentry2">
    <td class="order-col-default1">Jméno a příjmení, firma</td>
    <td>{$branch_data.nazev}</td>
  </tr>
  <tr class="sectiontableentry2">
    <td>Ulice a číslo popisné</td>
    <td>{$branch_data.ulice}</td>
  </tr>
  <tr class="sectiontableentry2">
    <td>Město</td>
    <td>{$branch_data.mesto}</td>
  </tr>
  <tr class="sectiontableentry2">
    <td>PSČ</td>
    <td>{$branch_data.psc}</td>
  </tr>
  <tr class="sectiontableentry2">
    <td>E-mail</td>
    <td>{$branch_data.email}</td>
  </tr>
  <tr class="sectiontableentry2">
    <td>Telefon</td>
    <td>{$branch_data.telefon}</td>
  </tr>
  {/if}
  
</table>

<h2 class="caption">Zvolený způsob dopravy a platby</h2>
<table summary="Způsob dopravy a platby">
  <tr class="sectiontableentry2">
    <td class="order-col-default1">Doprava</td>
    <td>{$platba}</td>
  </tr>
  <tr class="sectiontableentry2">
    <td>Platba</td>
    <td>{$doprava}</td>
  </tr>
</table>