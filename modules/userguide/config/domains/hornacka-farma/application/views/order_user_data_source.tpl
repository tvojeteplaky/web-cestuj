{* sablona formulare sdruzujici tabulky pro vyber zpusobu zadani dat o uzivateli v objednavce (prihlaseni,registrace s prihlasenim,pouhe zadani dat) *}
{* tato sablona je soucasti objednavky (krok 2-3.) - nadrazena sablona: order_container.tpl *}


<div class="caption">Pro pokračování v objednávce zvolte prosím jeden ze způsobů zadání osobních údajů:</div>
<form action="" method="post" class="accordion ac2">
  <h2 class="caption"><input type="radio" name="action" class="chbradio" value="user-login" {if $action=="user-login"}checked="checked" {/if}/>Přihlášení uživatele</h2>
  <div class="chbox user-login">
  {$user_login}
  </div>
  
  <h2 class="caption"><input type="radio" name="action" class="chbradio" value="user-registration" {if $action=="user-registration"}checked="checked" {/if}/>Registrace uživatele</h2>
  <div class="chbox user-registration">
  {$user_registration}
  </div>
  
  <h2 class="caption"><input type="radio" name="action" class="chbradio" value="order-unregistered-user" {if $action=="order-unregistered-user"}checked="checked" {/if}/>Nákup bez registrace</h2>
  <div class="chbox order-unregistered-user">
  {$order_unregistered_user}
  </div>
</form>

{literal}
  <script type="text/javascript">
    $(document).ready(function() {
    var val;
      $('.chbradio').checkBox();
      var sel=$('.chbradio[checked=checked]').attr("value");
      $("div."+sel).show();
      
      // accordion neni funkcni nefunguje 
      // $(".accordion").accordion();
      $('.chbradio').click(function(){
        val=$(this).attr("value");
        $(".chbox").slideUp();
        $("div."+val).slideDown();
      });

    });
  </script>
{/literal}