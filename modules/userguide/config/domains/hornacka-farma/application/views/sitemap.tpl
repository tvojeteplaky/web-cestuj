{* sablona mapy stranek *}

<ul>
{foreach name=lnk from=$links key=key item=link}
  <li>
    <a href="./{$link.nazev_seo}">{$link.nazev}</a>
    {* vylistovani pripojenych produktu *}
    {if ($link.products|@count >0 || $link.sublinks|@count >0)}
    <ul>
      {foreach from=$link.products key=pkey item=pitem}
      <li><a href="{$url_base}{$pitem.nazev_seo}" title="{$pitem.nazev}">{$pitem.nazev}</a></li>
      {/foreach}

      {if $link.sublinks|@count >0}    
      {foreach from=$link.sublinks key=skey item=sitem}
      <li><a href="{$url_base}{$sitem.nazev_seo}" title="{$sitem.nazev}">{$sitem.nazev}</a></li>
      {/foreach}
        
      {/if}
    </ul>
    {/if}
  </li>
{/foreach}
  <li><a href="{$url_base}{language cz="clanky" en="articles" lng=$language}">{language cz="Novinky" en="Articles" lng=$language}</a></li>
</ul>