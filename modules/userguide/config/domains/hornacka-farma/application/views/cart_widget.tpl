{* sablona nahledoveho kosiku vlozeneho do stranek *}
<div class="inner">
	<section>
		<a href="{$url_base}nakupni-kosik" title="Nákupní košík">&nbsp;</a>

		<h1>Nákupní košík</h1>

		<div class="row">
			<span class="name">Položek:</span>
			<span class="value">{$cart_items}</span>
		</div>

		<div class="row">
			<span class="name">Cena:</span>
			<span class="value">{$parameters.cena_celkem_s_dph|currency:'cz'},- Kč</span>
		</div>
	</section>
</div>