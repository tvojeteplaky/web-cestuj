{* sablona pro reklamační formulář *}
<p>Byl vyplněn reklamační formulář s těmito údaji:</p>
<table>
  <tr>
    <td>Jméno</td>
    <td>{$data.nazev}</td>
  </tr>
  <tr>
    <td>Telefon</td>
    <td>{$data.telefon}</td>
  </tr>
  <tr>
    <td>E-mail</td>
    <td>{$data.email}</td>
  </tr>
  <tr>
    <td>Firma</td>
    <td>{$data.nazev}</td>
  </tr>
  <tr>
    <td>Kód reklamované objednávky:</td>
    <td>{$data.order_id}</td>
  </tr>
  <tr>
    <td>Text reklamace:</td>
    <td>{$data.text}</td>
  </tr>
</table>