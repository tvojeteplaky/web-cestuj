{* sablona ktera slouzi k "obaleni" prislusnych sekci a doplneni kroku objednavky a tlacitek sekce *}
{assign var='order_step' value=$order_step|default:0}

<div id="shopping-cart">
  {* readonly kosik *}
  <h1>Nákupní košík</h1>
  {$cart_detail_table}

  {* obsah podle kroku objednavky *}
  {$content}
</div>