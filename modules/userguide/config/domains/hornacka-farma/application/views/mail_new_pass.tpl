{* sablona pro email s novým heslem *}
<p>Došlo k resetování hesla pro email {$email}. Zasíláme Vám nové přihlašovací údaje:</p>
<table>
  <tr>
    <td>Login:</td>
    <td><strong>{$login}</strong></td>
  </tr>
  <tr>
    <td>Nové heslo:</td>
    <td><strong>{$new_password}</strong></td>
  </tr>
</table>

<p><em>Pokud jste tuto akci neprovedli, neprodleně nás prosím kontaktujte.</em></p>