{* sablona pro potvrzeni objednavky *}
{* tato sablona je soucasti objednavky (krok 4. - potvrzeni objednavky) - nadrazena sablona: order_container.tpl *}

{$shopping_cart}

{$order_price_summary_table}

{$order_summary_table}
<form action="" method="post">
<h2 class="caption vertical-separator-50">Poznámka</h2>
<table summary="Poznámka zákazníka">
  <tr class="sectiontableentry2">
    <td colspan="2"><textarea name="poznamka" rows="6" cols="91">{$poznamka}</textarea></td>
  </tr>
  
</table>

<div class="order-bottom-buttons vertical-separator-50">
  <a href="dodaci-udaje" title="zpět na dodací údaje" class="left b110">Zpět</a>
  <input type="submit" name="next_step" value="Závazně objednat" class="right b140" />
</div>
</form>