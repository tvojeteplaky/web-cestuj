{* sablona pro výpis objednávek *}
{if $user_message}
  <p class="cart-message cmtype-added">{$user_message}</p>
   <div class="overall"></div>
  {literal}
  <script type="text/javascript">
  $(document).ready(function() {
  $(".cart-message").effect("bounce", { times:1 }, 200);
  });
  </script>
  {/literal}
  {/if}


<div class="cright">
<div class="product" >

<h2>{translate str="Seznam objednávek"}</h2>

{if (count($nove_objednavky) == 0 && count($zpracovavane_objednavky) == 0 && count($vyrizene_objednavky) == 0 && count($dodane_objednavky) == 0)}
Nemáte žádnou objednávku
{/if}


<!--  seznam novych objednavek -->
{if count($nove_objednavky) > 0}
    <p><img src="/media/img/new_orders_h2.png" alt="Seznam nových objednávek"/></p>
<table border="0" class="order_list" >
  {*<tr>
    <th>Číslo objednávky</th>
    <th>Datum</th>
    <th>Cena</th>
  </tr>*}
  {foreach key=k item=order name=iter from=$nove_objednavky}  		           
  <tr {if $smarty.foreach.iter.iteration mod 2 == 0}class="sectiontableentry2"{else}class="sectiontableentry1"{/if}>            
    <td class="td-order-name">Objednávka číslo {$order.order_code}</td>           
    <td class="td-order-date">{$order.order_date|date}</td>
    <td class="td-order-price">{$order.order_total|currency:'cz'} Kč</td>
    <td><a href="{$urlbase}{translate str="nahled-objednavky"}?id={$order.order_code}" title="Zobrazení náhledu objednávky"><img src="/media/img/show_order.png" alt="Zobrazit náhled objednávky"/></a></td>
    <td><a href="{$urlbase}{translate str="detail-objednavky"}?id={$order.order_code}" title="Editace otevřené objednávky"><img src="/media/img/edit_order.png" alt="Editace otevřené objednávky"/></a></td>
    <td><a href="{$urlbase}?order_delete=1&amp;id={$order.order_code}" title="Smazání otevřené objednávky"><img src="/media/img/remove_from_cart.png" alt="Smazání otevřené objednávky"/></a></td>          
  </tr>
  {/foreach}
</table>
{/if}

<!--  seznam zpracovavanych objednavek -->
{if count($zpracovavane_objednavky) > 0}
     <p><img src="/media/img/handled_orders_h2.png" alt="Seznam vyřizovaných objednávek"/></p>
<table border="0" class="order_list" >
  {*<tr>
    <th>Číslo objednávky</th>
    <th>Datum</th>
    <th>Cena</th>
  </tr>*}
  {foreach key=k item=order name=iter from=$zpracovavane_objednavky}  		           
  <tr {if $smarty.foreach.iter.iteration mod 2 == 0}class="sectiontableentry2"{else}class="sectiontableentry1"{/if}>            
    <td class="td-order-name">Objednávka číslo {$order.order_code}</td>           
    <td class="td-order-date">{$order.order_date|date}</td>
    <td class="td-order-price-2">{$order.order_total|currency:'cz'} Kč</td>
    <td><a href="{$urlbase}{translate str="nahled-objednavky"}?id={$order.order_code}" title="Zobrazení náhledu objednávky"><img src="/media/img/show_order.png" alt="Zobrazit náhled objednávky"/></a></td>
          
  </tr>
  {/foreach}
</table>
{/if}

<!--  seznam stornovanych objednavek -->
{if count($dodane_objednavky) > 0}
        <p><img src="/media/img/supplied_orders_h2.png" alt="Seznam dodaných objednávek"/></p>
<table border="0" class="order_list" >
  {*<tr>
    <th>Číslo objednávky</th>
    <th>Datum</th>
    <th>Cena</th>
  </tr>*}
  {foreach key=k item=order name=iter from=$dodane_objednavky}  		           
  <tr {if $smarty.foreach.iter.iteration mod 2 == 0}class="sectiontableentry2"{else}class="sectiontableentry1"{/if}>            
    <td class="td-order-name">Objednávka číslo {$order.order_code}</td>           
    <td class="td-order-date">{$order.order_date|date}</td>
    <td class="td-order-price-2">{$order.order_total|currency:'cz'} Kč</td>
    <td><a href="{$urlbase}{translate str="nahled-objednavky"}?id={$order.order_code}" title="Zobrazení náhledu objednávky"><img src="/media/img/show_order.png" alt="Zobrazit náhled objednávky"/></a></td>
          
  </tr>
    {/foreach}
</table>
{/if}   
      <div class="order-col-celkem-2">
                                            <a href="/ucet-uzivatele" title="Zpět" onclick="" id="basket-back" style="color:white;margin-right: 735px;">Zpět</a>

                             
                      </div>
     
 <div class="clear"></div>
</div>
</div>
