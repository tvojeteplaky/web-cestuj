{* sablona drobitkove navigace *}
<div id="breadcrumb">
	<ul>
		{foreach name=breadcr from=$data key=key item=item}
		<li {if $smarty.foreach.breadcr.first}id="first-navigation-li"{/if}>
			{if !$smarty.foreach.breadcr.last}<a href="{$url_base}{$item.nazev_seo}" title="{$item.nazev}">{/if}
				{$item.nazev}
			{if !$smarty.foreach.breadcr.last}</a>{/if}
		</li>
		{/foreach}
	</ul>
</div>