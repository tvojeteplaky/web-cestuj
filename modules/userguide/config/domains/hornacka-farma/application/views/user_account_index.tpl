{* sablona vstupni registracni stranky uzivatele *}
<div class="product">
  {if $message=="regok"}
  <div class="caption captOk">Změna registračních údajů byla provedena</div>
  {elseif $message=="branchok"}
  <div class="caption captOk">Údaje o pobočce byly uloženy</div>
  {elseif $message=="branchdeleted"}
  <div class="caption">Zvolená pobočka byla smazána</div>
  {elseif $message=="compok"}
  <div class="caption captOk">Reklamační formulář byl úspěšně odeslán</div>
  {/if}
    {if $orders}
    <div class="order-bottom-buttons" style="float:right;">
          <a class="left b140" href="{$url_base}prehled-objednavek" style="background: url('/media/img/basket_submit_big.png') no-repeat;height:37px;color:white;width:269px;padding-top:9px;margin-right:5px;">Zobrazit seznam Vašich objednávek</a>
          <a class="right b140" href="{$url_base}reklamace" style="background: url('/media/img/basket_submit_big.png') no-repeat;height:37px;color:white;width:269px;padding-top:9px;">Reklamační formulář</a>
      </div>

  {/if}
<div id="account-content">
    <h2 style="float:left;">Účet uživatele</h2>
   <div class="clear"></div>
  <div class="zakladni-udaje-uzivatele-1">
        <p class="basic-info-hidden-1 hidden"><a class="right " style="margin-right:20px;" href="{$url_base}nastaveni-uctu?mode=edit">Upravit registrační údaje</a></p>
  <h3>Vaše odběrové město: <a href="{$url_base}{$place.nazev_seo}">{$place.nazev}</a></h3>
  </div>

  {* prehled zakladnich udaju uzivatele *}

  <div class="zakladni-udaje-uzivatele">
    <h3>Základní údaje</h3>
     <table>
       <tr><td colspan="3" class="title">{$shopper.nazev}</td></tr>
       <tr><td class="col1">{$shopper.ulice}</td>     {if $shopper.ic}<td>IČ:</td><td class="bold">{$shopper.ic}</td>{else}<td colspan="2"></td>{/if}</tr>
       <tr><td>{$shopper.psc}, {$shopper.mesto}</td>  {if $shopper.dic}<td>DIČ</td><td class="bold">{$shopper.dic}</td>{else}<td colspan="2"></td>{/if}</tr>
       <tr><td colspan="3">&nbsp;</td></tr>
     </table>
     <table>
       <tr><td>Tel:</td><td class="bold">{$shopper.telefon}</td></tr>
       <tr><td>E-mail:</td><td>{$shopper.email}</td></tr>
     </table>

   </div>


   {* seznam dodacích adres *}
   {*<h3>Seznam dodacích adres</h3>
   {if $branches@count >0}
   <div class="order-section">
     <table>
       {foreach name=br from=$branches key=key item=item}
         <tr class="sectiontableentry1"><td>{$item.nazev}</td><td>{$item.ulice}</td><td>{$item.psc}, {$item.mesto}</td><td><a href="pobocka-uzivatele?brid={$item.id}">Upravit</a></td> <td><a href="?delbranch={$item.id}" onclick="javascript:return confirm('Opravdu smazat pobočku {$item.nazev}?');">Smazat</a></td></tr>
       {/foreach}
     </table>
   </div>
   {else}
   Nebyla vložena žádná pobočka.
   {/if}

   <p><a class="right" href="{$url_base}pobocka-uzivatele">Vložit novou pobočku</a></p>*}

</div>
</div>