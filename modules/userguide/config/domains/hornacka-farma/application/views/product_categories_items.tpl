{* vnitrni vypis kategorii *}

{foreach name=product from=$categories key=key item=item}
<div class="category">
	<div class="info">
		<div class="name">
			<h3>
				<a href="{$url_base}{$item.nazev_seo}" title="{$item.nazev}">
					{$item.nazev}
				</a>
			</h3>
		</div>
	</div>
	<div class="image">
		<a href="{$url_base}{$item.nazev_seo}" title="{$item.nazev}">
			<img src="{$media_path}img/categories/{$item.id}.jpg" alt="{$item.nazev}" />
		</a>
	</div>
</div>
{/foreach}

	{*
	<p>{$item.vyrobce}</p>
    <p class="content"> </p>
      {if $item.cena_s_dph && $item.cena_s_dph>0}

      <div class="product-price">
        <p class="price-dph"  {if !$logged_in}style="width:70px;" {/if}>{$item.cena_s_dph|currency:'cz'} <span id="currency">Kč</span>{if !$logged_in}           <p class="price-dph-unlogged" style="margin-top:10px;margin-left:0px;padding-left:0px;margin-bottom:10px;"><span>Pro nakupování se musíte <a href="/prihlaseni-uzivatele" title="Přihlášení uživatele">příhlásit</a></span></p>{else}{if $item.pocet_na_sklade<=0}<span id="skladem" slyle="float:right;">Není skladem</span>{/if}{/if}</p>
      </div>

      {*<p class="price-no-dph">bez DPH {$item.cena_bez_dph|currency:'cz'},- Kč / ks</p>
        {if $item.pocet_na_sklade>0}
        {if !$stop_trading}
        {if $logged_in}
          <div class="product-basket">
            <form action="{$url_self}" method="post">
            <p class="number"><input type="text" name="product_quantity" id="ks{$item.id}" value="1" /> <span id="product-unit">{$item.jednotka}</span></p>
            <p class="button"><input type="submit" class="submit" value="koupit" /><input type="hidden" name="product_id" value="{$item.id}" /></p>
            </form>
          </div>
        {else}

        {/if}
        {/if}
        {else}
           <p class="price-dph" style="margin-top:0px;"></p>
        {/if}
      {else}
      <div class="product-price">
      <p class="price-dph" style="width:100px;{if $logged_in}margin-bottom:24px;{/if}"><span>Cena není uvedena</span></p>
      </div>
      {/if}
      <div class="clear"></div>
    </div>
    {if $smarty.foreach.product.iteration mod 3 == 0}<div class="clear"></div>{/if}
	*}