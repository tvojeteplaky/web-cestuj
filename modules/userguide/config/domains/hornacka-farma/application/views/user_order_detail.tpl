{* sablona pro detail objednávky *}
  {* zpravy o úpravě *}
  {if $smarty.get.remove_item || $smarty.post.product_quantity}
<p class="cart-message cmtype-{$cart_message_type}">Otevřená objednávka byla upravena</p>
      <div class="overall"></div>
    {literal}
    <script type="text/javascript">
    $(document).ready(function() {
  $(".cart-message").effect("bounce", { times:2 }, 200, function(){setTimeout(function(){ $(".cart-message").fadeOut();$(".overall").fadeOut(); }, 1000)});

  });
  </script>
  {/literal}
  {/if}

            {if $open_order_total>0}  
            <h1 id="h1-basket-order">Objednávka #{$order.id}</h1>
                    {if $order.open==1} <form action="" method="post" class="quantityForm" id="quantityForm">{/if}
                        
                        {*Editace distribučního místa*}
                        
                        {*<br/>
                        <h3>Při objednávce jste vybrali toto distribuční místo</h3>
                        <select name="distributionplace_id">
                              {foreach from=$product_places  key=key item=item name=places}
                                    
                                   <option value="{$item.id}" {if $item.id==$order.distributionplace_id}selected="selected"{/if}>{$item.mesto}, {$item.ulice} </option>
                            
                              {/foreach}
                        </select>*} 
                        {*toto se pak smaže až se odkryje selectbox nahoře*}
                        <input type="hidden" name="distributionplace_id" value="{$distributionplace_id}" />
                      <table summary="nákupní košík" border="0" id="open-order-table">  
                        {*<tr>    
                          <th class="order-col-nazev">Název produktu</th>         
                          <th>Množství</th>  	   	
                          <th class="txtRight">Cena s DPH za ks</th>  	
                          <th class="txtRight">Cena s DPH</th>  	
                          <th class="order-col-smazat">&nbsp;</th>  
                        </tr>*}
                        <input type="hidden" name="order_id" value="{$order.id}" />
                        {foreach key=k item=product name=prod from=$order_cart_products}     
                        <tr {if $smarty.foreach.prod.iteration mod 2 == 0}class="sectiontableentry2"{else}class="sectiontableentry1"{/if}>	
                          <td class="order-col-nazev"><a href="{$product.nazev_seo}" title="Detailu produktu {$product.nazev}" class="basket-item">{$smarty.foreach.prod.index+1}. {$product.nazev}</a></td>	
                          <td class="order-col-mnozstvi">
                            {if $order.open==1}
                                                             <input type="text" title="Obnovit množství v košíku" class="inputbox-amount" size="4" maxlength="4" name="product_quantity[{$product.product_id}]" value="{$product.units|units:'0'}" />
                                                             
                            {else}               
                                {$product.units|units:'0'}          
                                {$product.jednotka}
                            {/if}
                              
                          </td>	
                         {* <td class="txtRight bold">{$product.cena_s_dph|currency:'cz'} Kč</td>*}	
                          <td class="order-col-cena">{$product.total_price_with_tax|currency:'cz'} Kč</td>	
                          {if $order.open==1}
                              <td class="order-col-smazat">
              
                               <a href="?remove_item={$product.product_id}&id={$order.id}&shopper_id={$order.order_shopper_id}" title="Smazat produkt {$product.nazev}"><img src="{$media_path}img/remove_from_cart.png" alt="Vyjmout zboží z košíku" /></a></td>
            
                              </td>  
                          {/if} 
                        </tr>
                        
                        {/foreach}
                         <tr><td colspan="{if $order.open==1}4{else}3{/if}" >&nbsp;</td></tr>                 
                        <tr><td colspan="{if $order.open==1}4{else}3{/if}" class="td-solid">&nbsp;</td></tr>
                        {* mezisoucty *}
                  
                      </table>
                      <div class="order-col-celkem"> Cena celkem: {$open_order_total|currency:'cz'},- Kč </div>
                      
                        <div class="clear"></div>
                     {if $order.open==1} <div class="order-col-celkem-2">
                                            <a href="/prehled-objednavek" title="Zpět" onclick="" id="basket-back" style="color:white;">Zpět</a>
                                             <a href="#" title="Uložit změny" onclick="{literal}javascript:document.getElementById('quantityForm').submit();{/literal}" id="basket-reload-2" style="color:white;">Potvrdit změny</a>
                             
                      </div>
                      
                      </form>{/if}
                {else}
                  <br/>
                  <h3>Nemáte žádnou otevřenou objednávku.</h3>
                {/if}

     <div class="clear"></div>
           
            