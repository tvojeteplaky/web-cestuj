{* sablona kontaktniho formulare *}{* soucast kontroleru contactform *}

{if $contact_form_message}<div class="msg strong green">{$contact_form_message}</div>{/if}
{if $contact_form_error}<div class="msg strong red">{$contact_form_error}</div>{/if}
{* sablona kontaktniho formulare *}{* soucast kontroleru contactform *}
{if $contact_form_message}<div class="msg strong green">{$contact_form_message}</div>{/if}
{if $contact_form_error}<div class="msg strong red">{$contact_form_error}</div>{/if}

{if $recommend_form_message}<div class="msg strong green">{$recommend_form_message}</div>{/if}
{if $recommend_form_error}<div class="msg strong red">{$recommend_form_error}</div>{/if}

<form action="{$urlform}#form" method="post" class="presentationForm">
	<input type="hidden" name="action" value="dotaz" />
	<input type="hidden" name="subject" value="" />
    <input type="hidden" name="contact_location" value="{$current_url}" />
    <input type="hidden" name="nazev_produktu" value="{$subject_name}" />

	<table>
		<tr>
			<td>
				<input data-default-value="Jméno a příjmení *" class="default-value" type="text" id="dotaz_jmeno" name="dotaz_jmeno" value="{if isset($form.dotaz_jmeno)}{$form.dotaz_jmeno}{else}Jméno a příjmení *{/if}" />
			</td>
			<td rowspan="4">
				<textarea data-default-value="Zpráva *" class="default-value" id="text_dotazu" name="text_dotazu" rows="9" cols="{if $mode == 'full-page'}83{else}58{/if}">{if isset($form.text_dotazu)}{$form.text_dotazu}{else}Zpráva *{/if}</textarea>
			</td>
		</tr>
		<tr>
			<td>
				<input data-default-value="Telefon *" class="default-value" type="text" id="dotaz_telefon" name="dotaz_telefon" value="{if isset($form.dotaz_telefon)}{$form.dotaz_telefon}{else}Telefon *{/if}"/>
			</td>
		</tr>
		<tr>
			<td>
				<input data-default-value="Firma" class="default-value" type="text" id="dotaz_firma" name="dotaz_firma" value="{if isset($form.dotaz_firma)}{$form.dotaz_firma}{else}Firma{/if}" />
			</td>
		</tr>
		<tr>
			<td>
				<input data-default-value="E-mail *" class="default-value" type="text" id="dotaz_email" name="dotaz_email" value="{if isset($form.dotaz_email)}{$form.dotaz_email}{else}E-mail *{/if}" />
			</td>
		</tr>
		<tr>
			<td colspan="2" class="t-right">
				<input type="submit" value="{translate str="odeslat"}" title="odeslat" name="odeslat" class="button success" />
			</td>
		</tr>
	</table>
</form>

<p>
	Položky označené * jsou povinné.
</p>