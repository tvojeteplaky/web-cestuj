{* sablona pro vyber dodaci adresy *}
{* tato sablona je soucasti objednavky (krok 3. - dodaci udaje) - nadrazena sablona: order_container.tpl *}
{if $message=="regok"}<div class="caption captOk">Registrace byla úspěšná, můžete pokračovat v objednávce</div>{/if}
{if $message=="loginok"}<div class="caption captOk">Přihlášení proběhlo úspěšně, můžete pokračovat v objednávce</div>{/if}
<form action="" method="post" class="accordion ac2">
  <h2 class="caption">Fakturační údaje</h2>
  <table summary="Fakturační údaje">
    <tr class="sectiontableentry2">
      <td class="order-col-default1">Jméno a příjmení, firma</td>
      <td>{$data.nazev}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td>Ulice a číslo popisné</td>
      <td>{$data.ulice}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td>Město</td>
      <td>{$data.mesto}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td>PSČ</td>
      <td>{$data.psc}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td>E-mail</td>
      <td>{$data.email}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td>Telefon</td>
      <td>{$data.telefon}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td>IČ</td>
      <td>{$data.ic}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td>DIČ</td>
      <td>{$data.dic}</td>
    </tr>
  </table>
  
  <h2 class="caption vertical-separator-50">Údaje pro dopravu</h2>
  <table summary="Údaje pro dopravu">
    
    {foreach name=br from=$branches key=key item=item}
      <tr class="sectiontableentry2">
        <td colspan="2"><input type="radio" name="branch_id" class="chbradio" value="{$item.id}" {if $item.checked}checked="checked" {/if}/>{$item.nazev}</td>
      </tr>
    {/foreach}

  </table>
  
  <div class="order-bottom-buttons vertical-separator-50">
    <a href="doprava-a-platba" title="zpět na dopravu a platbu" class="left b110">Zpět</a>
    <input type="submit" name="next_step" value="Další krok" class="right b110" />
  </div>
</form>

{literal}
  <script type="text/javascript">
    $(document).ready(function() {
    var val;
      $('.chbradio').checkBox();
    });
  </script>
{/literal}