{* sablona formulare sdruzujici tabulky pro vyber dopravy a platby *}
{* tato sablona je soucasti objednavky (krok 2. - doprava a platba) - nadrazena sablona: order_container.tpl *}

<form action="" method="post" id="ship-paym-form" class="jquery_css ajax-form">
  {* vlozene sablony s dopravou a platbou: *}
  
  {$order_shipping}
  
  <div class="vertical-separator-20"></div>
  
  {$order_payment}
  
  <div class="vertical-separator-20"></div>
  
  {* tabulka s cenovymi soucty *}
  {$order_price_summary_table}

  <div class="order-bottom-buttons vertical-separator-50">
    <a href="nakupni-kosik" title="pokračovat v nákupu" class="left b140">Zpět na košík</a>
    <input type="submit" name="next_step" value="Další krok" class="right b110" />
  </div>
</form>

{literal}
  <script type="text/javascript">
    $(document).ready(function() {
      $('.chbradio').checkBox(); 
      
      $(".ajaxradio").click(function(){$('#payment-table').showLoading(); AjaxSubmitForm(); });
      
      function AjaxSubmitForm(){
        $.ajax({
               type: "POST",
               data: $(".ajax-form").serialize(),
               //url: "url::base().url::current().Kohana::config('config.url_suffix').",
               success: function(response){
               var json = eval("(" + response + ")");
               //$('#page-title').html(json.page_title);
               //$('#page-description').html(json.page_description);
               //$('#page-keywords').html(json.page_keywords);
               $('#main-content').html(json.main_content);
               $('#cart-widget').html(json.cart_widget);
               $('#user-widget').html(json.user_widget);
               $('#payment-table').hideLoading();
               },
               error: function(XMLHttpRequest, textStatus, errorThrown){
               alert("Chyba: "+textStatus);
               }
       });
     }
    
   });
  </script>
{/literal}

