{* sablona detailu kosiku - podle nastaveni - moznost zobrazeni v editovatelnem, nebo v read-only modu *}
{* tato sablona je soucasti objednavky (krok 1. - kosik) - nadrazena sablona: order_container.tpl *}

{assign var='read_only' value=$read_only|default:false}

  {*<h2>Nákupní košík</h2>*}
  {if $cart_products && count($cart_products)>0}
    <table>
      {*<tr>
        <th class="order-col-nazev">Název produktu</th>
        <th>Množství</th>
        <th class="txtRight">Cena s DPH za ks</th>
        <th class="txtRight">Cena s DPH</th>
        <th class="order-col-smazat">&nbsp;</th>
      </tr>*}
      {foreach key=k item=product name=prod from=$cart_products}
      <tr {if $smarty.foreach.prod.iteration mod 2 == 0}class="sectiontableentry2"{else}class="sectiontableentry1"{/if}>
        <td class="order-col-nazev"><a href="{$product.nazev_seo}" title="Detailu produktu {$product.nazev}" class="basket-item">{$smarty.foreach.prod.index+1}. {$product.nazev}</a></td>
        <td class="order-col-mnozstvi">
          {if !$read_only}
          <input type="text" title="Obnovit množství v košíku" class="t-center" size="3" maxlength="6" name="product_quantity[{$product.id}]" value="{$product.mnozstvi}" />
          {else}
            {$product.mnozstvi}
          {/if}
          {$product.jednotka}
        </td>
       {* <td class="txtRight bold">{$product.cena_s_dph|currency:'cz'} Kč</td>*}
        <td class="order-col-cena"><strong>{$product.cena_celkem_s_dph|currency:'cz'} Kč</strong></td>
        <td class="order-col-smazat">
			{if !$read_only}
			<a href="?remove_item={$product.id}" title="Smazat produkt {$product.nazev}"><img src="{$media_path}img/remove_from_cart.png" alt="Vyjmout zboží z košíku" /></a>
			{else}
			&nbsp;
			{/if}
         </td>
      </tr>

      {/foreach}
    </table>
    <div class="order-col-celkem">
		{if !$read_only}
          {*<a href="#" onclick="{literal}javascript:jQuery('form[class=\'quantityForm\']').submit(); return false;{/literal}">
            <img style="vertical-align:middle;" src="{$media_path}img/ico_refresh.gif" alt="upravit" title="upravit" /></a>*}
          <a href="#" class="button success" title="Upravit množství v košíku" onclick="{literal}javascript:jQuery('form[class=\'quantityForm\']').submit(); return false;{/literal}">upravit</a>
          {/if}
		  <span class="total-price">
		  Cena celkem: {$price_total_products|currency:'cz'},- Kč
		  </span>
    </div>


  {else}
  <strong>Košík neobsahuje žádné zboží.</strong>
  {/if}
