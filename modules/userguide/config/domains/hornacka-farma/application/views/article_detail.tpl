<div id="news-detail">
	<section>
		<h1>{$title}</h1>
		{if $back_link_url}
		<div class="back-link">
			<a href="{$url_base}{$back_link_url}" class="link" title="Zpět na seznam novinek">{$back_link_text}</a>
		</div>
		{/if}
		
		{*
		{if $photo}
		<img class="news-img" src="{$photo}" alt="{$title}" />
		{/if}
		*}

		{$description}

		{$gallery}


	</section>
</div>