{* sablona s prihlasovacim formularem pro uzivatele *}
{assign var='included' value=false}

{if !$included}
<div id="login">
	<section>
		<h1>Přihlášení uživatele</h1>

		<div class="message">
			<p>
				Před prvním nákupem se prosím
				<a href="/registrace-uzivatele" title="Registrace uživatele">zaregistrujte</a>.
			</p>
		</div>

		<form action="" method="post" id="registrationForm">
{/if}
			{if isset($registration_error) && $registration_error}
			<div class="caption captError">
				{$registration_error}
			</div>
			{/if}

			<table>
				<tr>
					<td>
						<label for="username">Uživatelské jméno</label>
					</td>
					<td>
						<input type="text" name="username" id="username" />
						{if $login_errors.username}
							<span class="error">{$login_errors.username}</span>
						{/if}
					</td>
				</tr>
				<tr>
					<td>
						<label for="password">Heslo</label>
					</td>
					<td>
						<input type="password" name="password" id="password" />
						{if $login_errors.password}
							<span class="error">{$login_errors.password}</span>
						{/if}
					</td>
				</tr>
				<tr>
					<td colspan="2" class="t-right">
						<input type="submit" name="next_step" value="Přihlásit se" class="button success" />
						{if !$included}
						<input type="hidden" name="action" value="user-login" />
						{/if}
					</td>
				</tr>
			</table>
{if !$included}
		</form>
	</section>
</div>
{/if}