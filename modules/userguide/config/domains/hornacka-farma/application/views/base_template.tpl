{*
   CMS system Hana ver. 2 (C) Pavel Herink 2010
   zakladni spolecna sablona layoutu stranek

   automaticky generovane promenne spolecne pro vsechny sablony:
   $media_path - zaklad cesty do adresare "media/" se styly, obrazky, skripty a jinymi zdroji
   $url_base - zakladni url
*}
<!DOCTYPE html>
<html lang="cs">
<head>
	<meta charset="UTF-8" />

	<meta name="keywords" content="{$keywords}" />
	<meta name="description" content="{$description}" />
	<meta name="robots" content="index,follow" />
	<meta name="author" content="all: dg studio; www.dgstudio.cz; info@dgstudio.cz" />

	<link rel="stylesheet" href="{$media_path}css/style.css"  media="screen" />
	<link rel="stylesheet" href="{$media_path}css/colorbox.css"  media="screen" />
	<link rel="stylesheet" href="{$media_path}css/print.css" media="print" />

	<!--[if IE]>
	<script src="{$media_path}js/html5shiv.js"></script>
	<![endif]-->
	<script src="{$media_path}js/jquery-1.9.1.min.js"></script>
	<script src="{$media_path}js/jquery.jcarousel.min.js"></script>
	<script src="{$media_path}js/jquery.colorbox-min.js"></script>
	<script src="{$media_path}js/jquery.colorbox-cs.js"></script>
	<script src="{$media_path}js/jquery.default-values.js"></script>
	<script src="{$media_path}js/main.js"></script>

	<title>{if $title}{$title} - {/if}Název prezentace</title>
</head>

<body>

<div id="user-widget">
	<div class="inner">
		<div class="widget">
			{$user_widget}
		</div>
	</div>
</div>

<div id="page" >
	{* header *}
	<header>
		<div id="header">
			<div class="inner">
				<div class="logo">
					<a href="{$url_base}" title="Horácká farma">
						Horácká farma
						<span></span>
					</a>
				</div>

				<div class="facebook">
					<a href="#" rel="nofollow" title="Najdete nás na Facebooku">
						Najdete nás na facebooku
						<span></span>
					</a>
				</div>

				{* hlavni navigace *}
				<div id="nav">
					<nav>
						{$nav}
					</nav>
				</div>

				<div class="cart-widget">
					{$cart_widget}
				</div>

				<div class="image"></div>
				<div class="clear"></div>
			</div>
		</div>
	</header>

	{* content *}
	<div id="content">
		<div class="inner">
			{* drobitkova navigace *}
			{if !$is_indexpage}
				{$breadcrumb}
			{/if}

			<div class="clear"></div>

			{* hlavni obsah v zavislosti na typu stranky *}
			<div id="main-content">
				{* produktova subnavigace *}
				{$subnav}

				{$main_content}
				<div class="clear"></div>
			</div>
		</div>
	</div>

	{* footer *}
	<footer>
		<div id="footer">
			<div class="inner">
				<div class="text">
					<p>
						<a href="http://www.ippi.cz/klavesove-zkratky/">Klávesové zkratky  na tomto webu - rozšířené</a> | <a href="/prohlaseni-o-pristupnosti" title="Prohlášení o přístupnosti">Prohlášení o přístupnosti</a> | <a href="/ochrana-osobnich-udaju" title="Ochrana osobních údajů">Ochrana osobních údajů</a> | <a href="/mapa-stranek" title="Mapa stránek" accesskey="3">Mapa stránek</a><br />
						Realizace:  <a href="http://www.dgstudio.cz/" title="tvorba www stránek – dg studio">tvorba www stránek</a> | <a href="http://www.virtualni-prohlidky.eu/" title="virtuální prohlídky">virtuální prohlídky</a> | <a href="http://www.web-na-splatky.cz/" title="web na splátky">web na splátky</a> / dg studio
					</p>
				</div>
				<div class="signature">
					<a href="http://dgstudio.cz" title="Tvorba www stránek">
						<img src="{$media_path}img/signature.png" alt="DG studio">
					</a>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</footer>
</div>

{if $ga_script}
	{$ga_script}
{/if}
</body>

</html>