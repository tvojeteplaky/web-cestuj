{* sablona pro generovani seznamu měst a výdejních míst *}

<div class="news-item" style="margin-left:110px;">
  {* detail clanku / kategorie *}
  <h2>{$title}</h2>
  <div class="distribution-place-box">
  {if $description}<div class="distribution-place-item" style="height:auto;">
  {if $photo}<img class="news-img" src="{$photo}" alt="{$title}" />{/if}

  <p>{$description}</p>

  </div>
  {/if}

  {* seznam clanku *}
  {foreach name=art from=$articles key=key item=item}
    <div class="distribution-place-item" style="height:auto;">
     {* <div class="date" style="float:right;">{$item.date|date}</div> *}
      <h3><a href="{$url_base}{$item.nazev_seo}" title="{$item.nazev}">{$item.nazev}</a></h3>
      {if $item.photo}<a href="{$url_base}{$item.nazev_seo}" title="{$item.nazev}">  <img src="{$item.photo}" alt="{$item.nazev}" /></a>{/if}
      <div class="content">

        {$item.uvodni_popis}
        <a href="{$url_base}{$item.nazev_seo}" title="Zobrazit více">Zobrazit více</a>
       <div class="clear"></div>
       <br/>
       <h3>Distibuční místa pro město {$item.nazev} </h3>
        {foreach name=art from=$item.distributionplaces key=key item=distributionplace}
          <h4>{$distributionplace.ulice_cp}</h4>
        {/foreach}
      </div>

      <div class="clear"></div>
    </div>
  {/foreach}
  </div>
  <div class="correct right pagination">{$pagination}</div>

  {$gallery}
  <div class="clear"></div>
  {if $back_link_url}<div class="footer"><a href="{$url_base}{$back_link_url}" class="link" title="Zpět na seznam výrobců">{$back_link_text}</a></div>{/if}
</div>
<div class="clear"></div>








