{* sablona kontaktniho formulare *}{* soucast kontroleru contactform *}
<div id="FormSection"> 
<a name="form2" id="form2"></a>     
   
{if $recommend_form_message}<div class="msg strong green">{$recommend_form_message}</div>{/if}             
{if $recommend_form_error}<div class="msg strong red">{$recommend_form_error}</div>{/if}        
            
  <div id="TabsF" class="box">                
    <div class="content">                        
      <div id="TabsF-2" >                 
        <form action="{$urlform}?tab=poslat_odkaz#form2" method="post" class="presentationForm">                       
          <input type="hidden" name="action" value="doporucit" />                       
          <input type="hidden" name="recommend_location" value="{$current_url}" />                            
          <table border="0" summary="kontaktní formulář">                           
            <tr>                               
              <td class="col1">                
                <label for="jmeno">{translate str="Vaše jméno"} (                   
                  <span class="red">*                   
                  </span>){if isset($error.jmeno)}                   
                  <span>{$error.jmeno}                   
                  </span>{/if}                 
                </label>                
                <input type="text" id="jmeno" name="jmeno" value="{$form.jmeno}" /></td>                <td>                
                <label for="email">{translate str="Váš e-mail"} (                   
                  <span class="red">*                   
                  </span>){if isset($error.email)}                   
                  <span>{$error.email}                   
                  </span>{/if}                 
                </label>                
                <input type="text" id="email" name="email" value="{$form.email}" /></td>                                
            </tr>                           
            <tr>                               
              <td class="col1">                
                <label for="jmeno2">{translate str="Jméno příjemce"} (                   
                  <span class="red">*                   
                  </span>){if isset($error.jmeno2)}                   
                  <span>{$error.jmeno2}                   
                  </span>{/if}                 
                </label>                
                <input type="text" id="jmeno2" name="jmeno2" value="{$form.jmeno2}" /></td>                <td>                
                <label for="email2">{translate str="E-mail příjemce"} (                   
                  <span class="red">*                   
                  </span>){if isset($error.email2)}                   
                  <span>{$error.email2}                   
                  </span>{/if}                 
                </label>                
                <input type="text" id="email2" name="email2" value="{$form.email2}" /></td>                                
            </tr>                           
            <tr>                               
              <td colspan="2">                
                <label for="zprava">{translate str="Text zprávy"} (                   
                  <span class="red">*                   
                  </span>){if isset($error.zprava)}                   
                  <span>{$error.zprava}                   
                  </span>{/if}                 
                </label>
<textarea id="zprava" name="zprava" rows="6" cols="74">{$form.zprava}</textarea></td>                                
            </tr>                           
            <tr>                               
              <td colspan="2">                
                <div class="botRow">{translate str="Pro odeslání zprávy vyplňte prosím následující kód"} (                   
                  <span class="red">*                   
                  </span>):      <br />              
                  <span class="captcha">{$captcha}</span>                     
                  <input type="text" id="kod" name="kod" class="kod" />{if isset($error.kod)}                    
                  <span class="red"> {$error.kod}                   
                  </span>{/if}                                            
                  <span class="info">                    
                    {*<img src="{$resources_path}img/ico_info.gif" alt="info" title="info" border="0" height="16" width="11" />*}{translate str="Položky označené"} (*)                      
                    <span class="bold">{translate str="jsou povinné"}                    
                    </span>.                   
                  </span>                                         
                </div>                  
                <input type="submit" value="{translate str="odeslat"}" title="odeslat" name="odeslat" class="submit" /></td>                           
            </tr>                       
          </table>                   
        </form>             
      </div>         
    </div>         
    <div class="foot">         
    </div>     
  </div>
</div>