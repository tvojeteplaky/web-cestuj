{* sablona pro editaci pobocek uzivatele *}
{assign var='editmode' value=$editmode|default:false}

{if !$included}
<div class="order-section">
<h2>{if $editmode}Změna údajů o pobočce{else}Vložení nové pobočky{/if}</h2>
<form action="" method="post">
{/if}
  {if $registration_error}<div class="caption captError">{$registration_error}</div>{/if}

  <table summary="registrační formulář">
    <tr class="sectiontableentry2">
      <td class="order-col-default1"><label for="nazev">Název pobočky *</label></td>
      <td><input type="text" class="inputbox-standard-text" name="branchedit[nazev]" id="branchedit.nazev" value="{$data.nazev}" />{if $registration_errors.nazev}<span class="error">{$registration_errors.nazev}</span>{/if}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td><label for="ulice">Ulice a číslo popisné *</label></td>
      <td><input type="text" class="inputbox-standard-text" name="branchedit[ulice]" id="branchedit-ulice" value="{$data.ulice}" />{if $registration_errors.ulice}<span class="error">{$registration_errors.ulice}</span>{/if}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td><label for="mesto">Město *</label></td>
      <td><input type="text" class="inputbox-standard-text" name="branchedit[mesto]" id="branchedit-mesto" value="{$data.mesto}" />{if $registration_errors.mesto}<span class="error">{$registration_errors.mesto}</span>{/if}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td><label for="psc">PSČ *</label></td>
      <td><input type="text" class="inputbox-short-text" name="branchedit[psc]" id="branchedit-psc" value="{$data.psc}" />{if $registration_errors.psc}<span class="error">{$registration_errors.psc}</span>{/if}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td><label for="email">E-mail *</label></td>
      <td><input type="text" class="inputbox-standard-text" name="branchedit[email]" id="branchedit-email" value="{$data.email}" />{if $registration_errors.email}<span class="error">{$registration_errors.email}</span>{/if}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td><label for="telefon">Telefon</label></td>
      <td><input type="text" class="inputbox-standard-text" name="branchedit[telefon]" id="branchedit-telefon" value="{$data.telefon}" />{if $registration_errors.telefon}<span class="error">{$registration_errors.telefon}</span>{/if}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td colspan="2">* povinné údaje</td>
    </tr> 
  </table>
  
  <input type="hidden" name="action" value="branchedit" />
  <input type="hidden" name="branchedit[branch_id]" value="{$data.id}" />
  
  <div class="order-bottom-buttons">
    <input type="submit" name="next_step" value="{if $editmode}Upravit pobočku{else}Vložit pobočku{/if}" class="right b140" />
    <a class="left b140" href="{$url_base}ucet-uzivatele">Zpět bez uložení</a>
  </div>
{if !$included}
</form>
</div>
{/if}