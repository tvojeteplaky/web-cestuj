{* obalujici formular nakupniho kosiku s navigacnimi tlacitky *}
<p>
	{if $place}
		Vaše odběrové místo: <a href="{$url_base}{$place.nazev_seo}" title="{$place.nazev}">{$place.nazev}</a>.<br/>
		{* Zboží si vyzvedněte následující týden po uzávěrce objednávek {foreach name=iter1 key=key from=$times item=time}{if $time.den=="čtvrtek" || $time.den=="středu"}ve{else}v{/if} {$time.den} od {$time.cas_dodavky_od} - {$time.cas_dodavky_do}{if !$smarty.foreach.iter1.last}, {/if}{/foreach} hod. *}
	{else}
		Pro nakupování se musíte nejprve <a href="{$url_base}prihlaseni-uzivatele" title="Přihlásit se">přihlásit</a>
		nebo <a href="{$url_base}registrace-uzivatele" title="Zaregistrovat">zaregistrovat</a>.
	{/if}
</p>

<div class="cart-list">
	<form action="{$current_url}" method="post" class="quantityForm">
		{* tabulka s obsahem kosiku *}
		{if $cart_products && count($cart_products)>0}
			{*Editace distribučního místa*}
			{*<br/>
			<h3>Při objednávce jste vybrali toto distribuční místo</h3>
			<select name="distributionplace_id">
				{foreach from=$product_places  key=key item=item name=places}
				<option value="{$item.id}" {if $item.id==$order.distributionplace_id}selected="selected"{/if}>{$item.mesto}, {$item.ulice}</option>
				{/foreach}
			</select>*}

			{*toto se pak smaže až se odkryje selectbox nahoře*}
			<input type="hidden" name="place_id" value="{$place.id}" />
		{/if}

		{$cart_detail_table}
		{* tabulka s cenovymi soucty *}
		{$order_price_summary_table}


{if $cart_products && count($cart_products)>0}
	{if !$merge_order}

			{if !$open_order_total}
				<div class="delivery-and-notes">
					<div class="left">
						<label for="order_shipping_id">Způsob dodání</label>
						<div class="line">
							<select name="order_shipping_id" id="order_shipping_id">
								{foreach item=item key=k from=$shippings}
									<option {if $shipping_id == $item.id}selected="selected"{/if} value="{$item.id}" {if $item.branch}data-branch="need"{/if}>{$item.nazev} - {$item.popis}{if (int)$item.cena} - {$item.cena} Kč{/if}</option>
								{/foreach}
							</select>
							<br /><br /><br />
							
					
						<label for="order_place_id">Změnit místo odběru?</label>
				
								<select name="place_id" id="myFirstOption">
			<option value="{$place.id}">Ponechat stávající</option>
            {foreach from=$product_places  key=key item=item name=places}
			<option value="{$item.id}" {if $item.id==$place_id or $data.place_id==$item.id}selected="selected"{/if}>{$item.mesto}</option>
            {/foreach}
        </select>
						</div>
						<div class="line delivery hidden">
							<p><strong>Dodací údaje</strong></p>

							<table>
								<tr>
									<th>
										<label for="order_branch_name">Jméno a příjmení * </label>
									</th>
									<td>
										<input type="text" value="{$name}" name="order_branch_name" id="order_branch_name" />
									</td>
								</tr>
								<tr>
									<th>
										<label for="order_branch_name">Ulice, č.p. * </label>
									</th>
									<td>
										<input type="text" value="{$street}" name="order_branch_street" id="order_branch_street" />
									</td>
								</tr>
								<tr>
									<th>
										<label for="order_branch_name">Město * </label>
									</th>
									<td>
										<input type="text" value="{$city}" name="order_branch_city" id="order_branch_city" />
									</td>
								</tr>
								<tr>
									<th>
										<label for="order_branch_name">PSČ * </label>
									</th>
									<td>
										<input type="text" value="{$zip}" name="order_branch_zip" id="order_branch_zip" />
									</td>
								</tr>
								<tr>
									<th>
										<label for="order_branch_phone">Telefon * </label>
									</th>
									<td>
										<input type="text" value="{$phone}" name="order_branch_phone" id="order_branch_phone" />
									</td>
								</tr>
								<tr>
									<th>
										<label for="order_branch_name">E-mail * </label>
									</th>
									<td>
										<input type="text" value="{$email}" name="order_branch_email" id="order_branch_email" />
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="right">
						<div class="warning">
							<strong>Upozornění!</strong>
							<div class="content">
								{static_content code="cart-info-box"}
							</div>
						</div>
						<label for="poznamka">Poznámka zákazníka</label>
						<div class="line">
							<textarea name="poznamka" id="poznamka" rows="12" cols="59">{$poznamka}</textarea>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			{/if}
		<div class="order-bottom-buttons">

			<a href="produkty" title="pokračovat v nákupu" class="button success f-left">Pokračovat v nákupu</a>

			{if $logged_in}
				<input type="submit" name="{if $open_order_total}merge_order{else}next_step{/if}" value="{if $open_order_total}Sloučit košík s objednávkou{else}Odeslat objednávku{/if}" class="button success f-right" />
			{else}
				<a href="prihlaseni-uzivatele" title="Přihlásit se" class="button cancel f-right">Pro nakupování se prosím přihlašte</a>
			{/if}

			<div class="clear"></div>
          </div>
      {else}
		<div class="order-bottom-buttons">
			 <div class="delivery-and-notes">
					<div class="left">
						<label for="order_shipping_id">Způsob dodání</label>
						<div class="line">
							<select name="order_shipping_id" id="order_shipping_id">
								{foreach item=item key=k from=$shippings}
									<option value="{$item.id}" {if $item.branch}data-branch="need"{/if}>{$item.nazev} - {$item.popis}{if (int)$item.cena} - {$item.cena} Kč{/if}</option>
								{/foreach}
							</select>
								<br /><br /><br />
							
					
						<label for="order_place_id">Změnit místo odběru?</label>
				
								<select name="place_id" id="myFirstOption">
			<option>Ponechat stávající</option>
            {foreach from=$product_places  key=key item=item name=places}
			<option value="{$item.id}" {if $item.id==$place_id or $data.place_id==$item.id}selected="selected"{/if}>{$item.mesto}</option>
            {/foreach}
        </select>
						</div>
						<div class="line delivery hidden">
							<p><strong>Dodací údaje</strong></p>

							<table>
								<tr>
									<th>
										<label for="order_branch_name">Jméno a příjmení * </label>
									</th>
									<td>
										<input type="text" value="{$name}" name="order_branch_name" id="order_branch_name" />
									</td>
								</tr>
								<tr>
									<th>
										<label for="order_branch_name">Ulice, č.p. * </label>
									</th>
									<td>
										<input type="text" value="{$street}" name="order_branch_street" id="order_branch_street" />
									</td>
								</tr>
								<tr>
									<th>
										<label for="order_branch_name">Město * </label>
									</th>
									<td>
										<input type="text" value="{$city}" name="order_branch_city" id="order_branch_city" />
									</td>
								</tr>
								<tr>
									<th>
										<label for="order_branch_name">PSČ * </label>
									</th>
									<td>
										<input type="text" value="{$zip}" name="order_branch_zip" id="order_branch_zip" />
									</td>
								</tr>
								<tr>
									<th>
										<label for="order_branch_phone">Telefon * </label>
									</th>
									<td>
										<input type="text" value="{$phone}" name="order_branch_phone" id="order_branch_phone" />
									</td>
								</tr>
								<tr>
									<th>
										<label for="order_branch_name">E-mail * </label>
									</th>
									<td>
										<input type="text" value="{$email}" name="order_branch_email" id="order_branch_email" />
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="right">
						<div class="warning">
							<strong>Upozornění!</strong>
							<p>Z hygienických důvodů neodesíláme maso poštou.</p>
						</div>
						<label for="poznamka">Poznámka zákazníka</label>
						<div class="line">
							<textarea name="poznamka" id="poznamka" rows="12" cols="59">{$poznamka}</textarea>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>

					<input type="submit" name="next_step" value="Potvrdit změnu objednávky" class="f-right button success" />
				</form>


				<form action="{$current_url}" method="post" class="backForm">
					<input type="hidden" name="open_order_id" value="{$old_open_order_id}" />
					<input type="submit" name="prev_step" value="Zrušit změny objednávky" class="f-left button cancel" />
			    </form>

      {/if}
  {/if}

 {if !$merge_order}
            {* detail otevřené objednávky*}
            {if $open_order_total}
                <h1>Otevřená objednávka</h1>
                <table>
                {foreach key=k item=product name=prod from=$open_order_cart_products}
                <tr>
                  <td class="open-order-col-nazev"><a href="{$product.nazev_seo}" title="Detailu produktu {$product.nazev}" class="basket-item">{$smarty.foreach.prod.index+1}. {$product.nazev}</a></td>
                  <td class="open-order-col-mnozstvi">
                    {if !$read_only}
                   {$product.units|units:'0'}
                    {else}
                      {$product.units}
                    {/if}
                      {$product.jednotka}
                  </td>
                  <td class="order-col-cena"><strong>{$product.total_price_with_tax|currency:'cz'} Kč</strong></td>
                </tr>
                {/foreach}
				</table>
              {* mezisoucty *}
			  <div class="order-col-celkem">
				<span class="total-price">
					Cena celkem: {$open_order_total|currency:'cz'},- Kč
				</span>
			  </div>
            {/if}
			</form>
   {/if}
  <div class="clear"></div>
  </div>