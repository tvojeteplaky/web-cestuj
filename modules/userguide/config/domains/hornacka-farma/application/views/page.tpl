{* sablona pro generovani statickych stranek (modul: page) *}
<div id="generic-page">
	<section>
		<h1>{$page.nadpis}</h1>
		{$page.popis}
	</section>

	{if $contactform}
		<h3>Kontaktujte nás</h3>
		{$contactform}
	{/if}
</div>