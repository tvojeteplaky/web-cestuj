{foreach from=$places  key=key item=item name=places}
<div class="place">
	<div class="image">
		{if $item.photo}
		<a href="{$item.nazev_seo}" title="{$item.nazev}">
			<img src="{$item.photo}" alt="{$item.nazev}" />
		</a>
		{/if}
	</div>
	<div class="info">
		<h3>
			<a href="{$item.nazev_seo}" title="{$item.nazev}">{$item.nazev}</a>
		</h3>
		<p>{$item.uvodni_popis}</p>
	</div>
	<div class="clear"></div>
</div>
{/foreach}