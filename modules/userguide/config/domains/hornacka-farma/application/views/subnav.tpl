{* sablona produktove subnavigace *}
{* implementovany pouze tri urovne *}
<div id="products-menu">
	<ul>
		{foreach name=iter1 from=$links key=key item=link}
		<li {if $link.sel}class="active"{/if}>
			<a href="{$url_base}{$link.nazev_seo}" title="{$link.nazev}">
				<span>{$link.nazev_short}</span>
			</a>

			{if $link.children}
			<ul class="children">
				{foreach name=iter2 from=$link.children key=key item=link2}
				<li {if $link2.sel}class="active"{/if}>
					<a href="{$url_base}{$link2.nazev_seo}" title="{$link2.nazev}">
						<span>{$link2.nazev_short}</span>
					</a>

					{if $link2.children}
					<ul class="children">
						{foreach name=iter3 from=$link2.children key=key item=link3}
						<li {if $link3.sel}class="active"{/if}>
							<a href="{$url_base}{$link3.nazev_seo}" title="{$link3.nazev}">- {$link3.nazev_short}</a>
						</li>
						{/foreach}
					</ul>
					{/if}
				</li>
				{/foreach}
			</ul>
			{/if}
		</li>
		{/foreach}
	</ul>

	<div class="product-menu-box">
		{if $logged_in}
			{static_content code="product-info-box"}
			{*
			<strong>
				Zboží si můžete převzít následující týden po uzávěrce objednávek<br>
				{foreach name=iter1 from=$times key=key item=time}
				{$time.den} {$time.cas_dodavky_od} - {$time.cas_dodavky_do} hod<br/>
				{/foreach}
			</strong>
			*}
		{else}
			Pro nakupování se musíte nejprve <a href="{$url_base}prihlaseni-uzivatele" title="Přihlásit se">přihlásit</a>
			nebo <a href="{$url_base}registrace-uzivatele" title="Zaregistrovat">zaregistrovat</a>.
		{/if}
	</div>
</div>