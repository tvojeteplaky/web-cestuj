{* sablona produktu detailu *}
<div id="product-detail">
	{* zpravy o pridani do kosiku *}
	{if $cart_message}
		<p class="message cart-message-2 cmtype-{$cart_message_type}">
			{$cart_message}
		</p>
	{/if}

	<h2>{$product.nazev}</h2>

	<div class="info">
		<div class="content">
			<div class="image">
				<span class="{if in_array('novinky',$product.specialni_kategorie)} news{/if}{if in_array('akce',$product.specialni_kategorie)} action{/if}{if in_array('vyprodej',$product.specialni_kategorie)} sale{/if}"></span>
				{if $product.photo}
					<a href="{$product.photo_detail}" title="{$product.nazev}" class="colorbox group">
						<img src="{$product.photo}" alt="{$product.nazev}" />
					</a>
				{else}
					<img src="{$media_path}img/no_photo_290x290.gif" width="290" height="290" alt="{$product.nazev}" />
				{/if}
			</div>
			{*
			<p><strong>Výrobce:</strong> {$product.vyrobce}</p>
			*}

			<p><strong>Skladem:</strong> {if $product.pocet_na_sklade>0}{$product.pocet_na_sklade}&nbsp;{$product.jednotka}{else}Ne{/if}</p>

			<p><strong>Balení:</strong> {$product.baleni}</p>

			<p><strong>Města, kde můžete tento produkt nakoupit:</strong></p>
			<ul class="cities">
				{foreach from=$cities  key=key item=item name=city}
				<li>{$item.nazev}</li>
				{/foreach}
			</ul>

			<p>{$product.uvodni_popis}</p>
		</div>
    </div>

	{if $product.cena_s_dph && $product.cena_s_dph>0}
		<div id="detail-price">
			<div class="product-detail-price">
				<p class="price-dph">Cena {$product.cena_s_dph|currency:'cz'},- Kč/{$product.jednotka}</p>
				{*{if $product.puvodni_cena && $product.puvodni_cena!="0.00"}<p>původní cena:<span class="strike">{$product.puvodni_cena},- Kč <span>/ ks</span></span></p>{/if}*}
				{*<p class="price-no-dph">bez DPH {$product.cena_bez_dph|currency:'cz'},- Kč / ks </p>*}
			</div>

			{if $product.pocet_na_sklade>0}
				{if !$stop_trading}
					<div class="product-detail-basket">
						<form action="{$url_self}" method="post">
							<span class="title">Koupit</span>
							<input type="text" name="product_quantity" id="ks1" size="2" value="1" />
							<span class="unit">{$product.jednotka}</span>
							<input type="submit" class="submit" value="koupit" />
						</form>
					</div>
				{/if}

				{if $stop_trading}
				<p>
					Obchodování je momentálně uzavřeno.
				</p>
				{/if}
			{else}
				<p class="price-dph">
					<span>Zboží není skladem</span>
				</p>
			{/if}
		</div>
	{else}
		<p class="price-dph">
			<span>Cena není uvedena</span>
		</p>
	{/if}

	<div class="box description">
		<h3>Informace</h3>
		<div class="inner">
			<p>{$product.popis}</p>
		</div>
	</div>

	{if count($product.fotogalerie)}
		<div class="box gallery">
			<h3>Fotogalerie</h3>
			<div class="inner">
				{$gallery}
			</div>
		</div>
	{/if}

	<div class="box contact-form">
		<h3>Dotaz na produkt</h3>
		<div class="inner">
			{$contactform}
		</div>
	</div>
</div>