{* sablona kontaktniho formulare *}{* soucast kontroleru contactform *}
<div id="FormSection"> 
<a name="form" id="form"></a>     
   
{if $contact_form_message}<div class="msg strong green">{$contact_form_message}</div>{/if}             
{if $contact_form_error}<div class="msg strong red">{$contact_form_error}</div>{/if}

  
  <div id="TabsF" class="box">         
                
      <h3>{translate str="Kontaktní formulář"}</h3>                      
           
    <div class="content">             
      <div id="TabsF-1">                 
        <form action="{$urlform}#form" method="post" class="presentationForm">                       
          <input type="hidden" name="action" value="chyba" />                       
          <input type="hidden" name="contact_location" value="{$current_url}" />                       
          <input type="hidden" name="nazev_produktu" value="{$subject_name}" />                     
                                  
         <table border="0" summary="kontaktní formulář">                           
            <tr>                               
              <td colspan="2">{translate str="Zaujaly vás informace na této stránce a chcete se"}                  
                <span class="bold">{translate str="dozvědět více"}?                 
                </span>. {translate str="Neváhejte a vyplňte následující kontaktní formulář. S odpovědí se Vám ozveme co nejdříve."}</td>                           
            </tr>
                        <tr>                               
              <td colspan="2">&nbsp;</td>                           
            </tr>                            
            <tr>                               
              <td class="col1">                
                <label for="dotaz_jmeno" {if isset($error.dotaz_jmeno)}class="red"{/if} >{translate str="Jméno a příjmení"}*                    
               
                </label>                
                <input type="text" id="dotaz_jmeno" name="dotaz_jmeno" value="{$form.dotaz_jmeno}" /></td>                <td>                
             <label for="dotaz_email" {if isset($error.dotaz_email)}class="red"{/if}>E-mail*        
                </label>                
                <input type="text" id="dotaz_email" name="dotaz_email" value="{$form.dotaz_email}" /></td>                           
            </tr>                           
                                      
            <tr>                               
              <td colspan="2">                
                <label for="text_dotazu" {if isset($error.text_dotazu)}class="red"{/if}>{translate str="Text zprávy"}*              
                </label>
<textarea id="text_dotazu" name="text_dotazu" >{$form.text_dotazu}</textarea></td>                                
            </tr>                           
            <tr>                               
              <td colspan="2">                
                <div class="botRow">{*{translate str="Pro odeslání dotazu vyplňte prosím následující kód"} (                   
                  <span class="red">*                   
                  </span>):   <br />                 
                  <span class="captcha">{$captcha}                   
                  </span>                     
                  <input type="text" id="dotaz_captcha" name="dotaz_captcha" class="kod" />{if isset($error.dotaz_captcha)}                    
                  <span class="red"> {$error.dotaz_captcha}                   
                  </span>{/if}*}                                        
                  <span class="info">                    
                    {*<img src="{$resources_path}img/ico_info.gif" alt="info" title="info" border="0" height="16" width="11" />*}{translate str="Položky označené"}                      
                    *                     
                                          
                    <span class="bold">{translate str="jsou povinné"}                    
                    </span>.                   
                  </span>                                          
                </div>                  
                <input type="hidden" value=""  name="subject"  />
                <input type="submit" value="{translate str="odeslat"}" title="odeslat" name="odeslat" class="submit" /></td>                           
            </tr>                       
          </table>                               
        </form>             
      </div>                  
    </div>         
    <div class="foot">         
    </div>     
  </div>
</div>