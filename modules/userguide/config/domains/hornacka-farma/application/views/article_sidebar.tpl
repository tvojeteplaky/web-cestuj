{* sablona sidebaru novinek *}
<div class="article-sidebar">
<div class="head"><a href="{$url_base}{language cz="clanky\">Novinky" en="articles\">News" lng=$language}</a></div>

  {foreach name=art from=$articles key=key item=item}
    <div class="item">
      <div class="title"><a href="{$url_base}{$item.nazev_seo}" title="{$item.nazev}">{$item.nazev}</a></div>
      <div class="content">       
        {$item.uvodni_popis}
        <a href="{$url_base}{$item.nazev_seo}" title="{$item.nazev}">více &raquo;</a>
      </div>
    </div>
    <div class="clear line"></div>
  {/foreach}

</div>