{* sablona tabulky pro vyber platby *}
{* tato sablona je soucasti objednavky - nadrazena sablona "order_payment_payment.tpl" *}

<table summary="formulář s výběrem způsobu platby" id="payment-table">
  <tr>
    <th>Způsob platby</th>
    <th>&nbsp;</th>
    <th class="order-col-cena txtRight">Doplatek</th>
    <th class="order-col-smazat">&nbsp;</th>
  </tr>
  {foreach from=$payment_types key=key item=item}
  <tr class="sectiontableentry2">
    <td><input type="radio" name="payment_id" class="chbradio ajaxradio" value="{$item.id}" id="payment-variant-{$item.id}" {if $item.checked}checked="checked" {/if}/> <label for="payment-variant-{$item.id}">{$item.nazev}</label></td>
    <td>&nbsp;</td>
    <td class="txtRight">
    {if $item.cena<>0}
      {if $item.typ==1}
        {$item.cena|currency:'cz'} Kč
      {elseif $item.typ==2}
        {$item.cena|currency:'cz'} %
      {/if}
    {else}
      zdarma
    {/if}
    </td>
    <td>&nbsp;</td>
  </tr>  
  {/foreach}
</table>