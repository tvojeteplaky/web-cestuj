{* sablona pro registraci uzivatele *}



<div class="registration">
<h2>Reklamační formulář</h2>
<form action="" method="post" id="registrationForm">

   
  {if $registration_error_change}<div class="caption captError">{$registration_error_change}</div>{/if}
     <h3>Vyberte objednávku, kterou chcete reklamovat</h3>
  <select name="user-complaint[order_id]">
        {foreach from=$orders  key=key item=item name=places}
              
             <option value="{$item.order_code}" >Objednávka číslo {$item.order_code}</option>
      
        {/foreach}
  </select>
  <br/><br/>
  <table summary="registrační formulář">
    
    <tr>
      <th colspan="2">Kontaktní informace</th>
    </tr>
    <tr class="sectiontableentry2">
      <td class="order-col-default1"><label for="nazev">Jméno a příjmení, firma *</label></td>
      <td><input type="text" class="inputbox-standard-text" name="user-complaint[nazev]" id="user-complaint.nazev" value="{$data.nazev}" />{if $registration_errors.nazev}<span class="error">{$registration_errors.nazev}</span>{/if}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td><label for="ulice">Ulice a číslo popisné *</label></td>
      <td><input type="text" class="inputbox-standard-text" name="user-complaint[ulice]" id="user-complaint-ulice" value="{$data.ulice}" />{if $registration_errors.ulice}<span class="error">{$registration_errors.ulice}</span>{/if}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td><label for="mesto">Město *</label></td>
      <td><input type="text" class="inputbox-standard-text" name="user-complaint[mesto]" id="user-complaint-mesto" value="{$data.mesto}" />{if $registration_errors.mesto}<span class="error">{$registration_errors.mesto}</span>{/if}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td><label for="psc">PSČ *</label></td>
      <td><input type="text" class="inputbox-short-text" name="user-complaint[psc]" id="user-complaint-psc" value="{$data.psc}" />{if $registration_errors.psc}<span class="error">{$registration_errors.psc}</span>{/if}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td><label for="email">E-mail *</label></td>
      <td><input type="text" class="inputbox-standard-text" name="user-complaint[email]" id="user-complaint-email" value="{$data.email}" />{if $registration_errors.email}<span class="error">{$registration_errors.email}</span>{/if}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td><label for="telefon">Telefon *</label></td>
      <td><input type="text" class="inputbox-standard-text" name="user-complaint[telefon]" id="user-complaint-telefon" value="{$data.telefon}" />{if $registration_errors.telefon}<span class="error">{$registration_errors.telefon}</span>{/if}</td>
    </tr>
    <tr>                             
              <td colspan="2">                
                <label for="text_dotazu">Text reklamace*              
                </label>
<textarea id="text_dotazu" name="user-complaint[text]"></textarea></td>                                
            </tr>
    
   
  </table>
  
  <input type="hidden" name="action" value="user-complaint" />

 
  <div class="order-bottom-buttons">
    <input type="submit" name="next_step" value="Reklamovat" class="right submit b140" />
    <a class="left b140" href="/ucet-uzivatele" >Zpět</a>
  </div>
{if !$included}
</form>
</div>
{/if}