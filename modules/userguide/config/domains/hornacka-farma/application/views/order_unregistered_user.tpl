{* sablona pro rychle zadani osobnich udaju v objednavce, pokud se uzivatel nechce registrovat *}
{assign var='included' value=$included|default:false}

{if !$included}
<div class="order-section">
<form action="" method="post">
{/if}
  {if isset($error) && $error}<div class="caption captError">{$error}</div>{/if}

  <table summary="registrační formulář">
    <tr>
      <th colspan="2">Kontaktní informace</th>
    </tr>
    <tr class="sectiontableentry2">
      <td class="order-col-default1"><label for="nazev">Jméno a příjmení, firma *</label></td>
      <td><input type="text" class="inputbox-standard-text" name="unregistered-user[nazev]" id="unregistered-user-nazev" value="{$data.nazev}" />{if $user_errors.nazev}<span class="error">{$user_errors.nazev}</span>{/if}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td><label for="ulice">Ulice a číslo popisné *</label></td>
      <td><input type="text" class="inputbox-standard-text" name="unregistered-user[ulice]" id="unregistered-user-ulice" value="{$data.ulice}" />{if $user_errors.ulice}<span class="error">{$user_errors.ulice}</span>{/if}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td><label for="mesto">Město *</label></td>
      <td><input type="text" class="inputbox-standard-text" name="unregistered-user[mesto]" id="unregistered-user-mesto" value="{$data.mesto}" />{if $user_errors.mesto}<span class="error">{$user_errors.mesto}</span>{/if}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td><label for="psc">PSČ *</label></td>
      <td><input type="text" class="inputbox-short-text" name="unregistered-user[psc]" id="unregistered-user-psc" value="{$data.psc}" />{if $user_errors.psc}<span class="error">{$user_errors.psc}</span>{/if}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td><label for="email">E-mail *</label></td>
      <td><input type="text" class="inputbox-standard-text" name="unregistered-user[email]" id="unregistered-user-email" value="{$data.email}" />{if $user_errors.email}<span class="error">{$user_errors.email}</span>{/if}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td><label for="telefon">Telefon *</label></td>
      <td><input type="text" class="inputbox-standard-text" name="unregistered-user[telefon]" id="unregistered-user-telefon" value="{$data.telefon}" />{if $user_errors.telefon}<span class="error">{$user_errors.telefon}</span>{/if}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td><label for="ic">IČ</label></td>
      <td><input type="text" class="inputbox-standard-text" name="unregistered-user[ic]" id="unregistered-user-ic" value="{$data.ic}" />{if $user_errors.ic}<span class="error">{$user_errors.ic}</span>{/if}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td><label for="dic">DIČ</label></td>
      <td><input type="text" class="inputbox-standard-text" name="unregistered-user[dic]" id="unregistered-user-dic" value="{$data.dic}" />{if $user_errors.dic}<span class="error">{$user_errors.dic}</span>{/if}</td>
    </tr>
    <tr class="sectiontableentry2">
      <td colspan="2">* povinné údaje</td>
    </tr>  
  </table>
  
  {if !$included}<input type="hidden" name="action" value="order-unregistered-user" />{/if}
  
  <div class="order-bottom-buttons">
    <input type="submit" name="next_step" value="Pokračovat" class="right b110" />
  </div>

{if !$included}
</form>
</div>
{/if}