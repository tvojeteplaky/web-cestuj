{* tabulka se souctem cen (doprava, platebni metoda, apod. + cena s dph celkem), mimo souctu kosiku - ten si zobrazi sam *}
<table summary="tabulka cenových součtů">
{foreach name=pv from=$price_values key=key item=item}
  <tr>
    <td>&nbsp;</td>
    <td class="txtRight">{if isset($item.class)}<span class="{$item.class}">{$item.name}</span>{else}{$item.name}{/if}</td>
    <td class="order-col-cena txtRight">
    {if isset($item.class)}<span class="{$item.class}">{/if}
      {if $item.value<>0}{$item.value|currency:'cz'} {if $item.type==1}Kč{elseif $item.type==2}%{else}Kč{/if}{else}zdarma{/if}
    {if isset($item.class)}</span>{/if} 
    </td>
    <td class="order-col-smazat">&nbsp;</td>
  </tr>
{/foreach}
</table>