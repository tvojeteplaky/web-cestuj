{* specialni sablona s layoutem pro uvodni stranku *}
<div id="homepage">
	{* category blocks - previews *}
	<div class="categories">
	{foreach from=$product_categories  key=key item=item name=products}
		<div class="item">
			<div class="heading">
				<a href="{$item.nazev_seo}" title="{$item.nazev}">
					{if !empty($item.photo)}
						<img src="{$item.photo}" alt="{$item.nazev}" />
					{/if}
					<h2>{$item.nazev}</h2>
				</a>
			</div>
			<div class="link">
				<a href="{$item.nazev_seo}" title="{$item.nazev}">celá nabídka</a>
			</div>
		</div>
	{/foreach}
	</div>

	<div class="clear"></div>

	{if $message }
		{if $message=="usrlogout"}
			<p class="message cmtype-added">
				<strong>Uživatel byl odhlášen</strong>
			</p>
			<div class="overall"></div>
		{elseif $message=="usrreg"}
			<p class="message cmtype-added">
				<strong>Uživatel byl úspěšně registrován a přihlášen</strong>
			</p>
		{/if}
	{/if}

	{* homepage content *}
   <div class="home-content">
	   <div class="heading">
		   <h2>{$page.nadpis}</h2>
	   </div>
	   <div class="content">
		   <div class="text">
			   {$page.popis}
		   </div>
		   {*
		   <div class="image">
			   <img src="{$page.photo}" alt="{$page.nadpis}" />
		   </div>
		   *}
		   <div class="clear"></div>
	   </div>
   </div>

	{* news - novinky na úvodu *}
	<div class="news-container">
		<section>
			<h2>
				<a href="{$url_base}novinky" title="Novinky">Aktuálně z naší farmy</a>
			</h2>
			<div class="news">
				{$article_homepage}
			</div>
		</section>
	</div>

	{* places - výdejní místa *}
	<div class="places-container">
		<h2>
			<a href="{$url_base}vydejni-mista" title="Výdejní místa">Výdejní místa</a>
		</h2>
		<div id="places" class="places">
			{$place_homepage}
		</div>
	</div>

   <div class="clear"></div>

	{* @todo: top products! *}
	<div class="clear"></div>
</div>
