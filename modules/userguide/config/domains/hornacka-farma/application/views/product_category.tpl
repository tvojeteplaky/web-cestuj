{* vypis kategorie produktu - vnejsi obalujici sablona *}
<section>
	<div id="products">
		{if $category.nazev}
			<h1>{$category.nazev}</h1>
		{/if}

		{if !empty($category.popis)}
			<p>{$category.popis}</p>
		{/if}

		{if !$empty_category}
			{* zpravy o pridani do kosiku *}
			{if $cart_message}
				<p class="cart-message-2 cmtype-{$cart_message_type}">
					{$cart_message}
				</p>
			{/if}

			{* listy s razenim a filtrovanim produktu *}
			{if !$empty_category_by_filter}
				<div class="products-box">
					{* vnitni sablona - vypis produktu*}
					{$product_category_items}
				</div>
				<div class="clear"></div>

				{if $pagination}
					{$pagination}
				{/if}
			{else}
				<p>zvolenému nastavení filtrů neodpovídají žádné položky</p>
			{/if}
		{else}
			{* prazdna kategorie *}
			<p>nebyly nalezeny žádné položky</p>
		{/if}
	</div>
</section>
<div class="clear"></div>