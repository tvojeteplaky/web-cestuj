<div class="kontakty">
<h2>Stránka nenalezena</h2>
<br />
{if $language=="cz"}
<p><strong>Požadovaná stránka nebyla na serveru nalezena / The page you requested was not found on this server</strong></p>
<p>Obsah který hledáte se na této adrese momentálně nenachází. Je možné, že během neustálého rozvoje těchto stránek došlo ke změně jeho umístění.</p>
<p>Použijte prosím následující mapu stránek, která Vám pomůže najít požadované informace nebo zdroje.</p>
{else}
<br />
<p>The content you are looking for at this moment not exists. It is possible that during the continuous development of these sites has changed its location.</p>
<p>Please use the following site map to help you find the information or resources.</p>
<br />
{/if}
{$sitemap}
<br />

{$contactform}

</div>