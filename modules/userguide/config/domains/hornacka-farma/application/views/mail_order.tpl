{* obalova sablona mailu s objednavkou *}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="cs" />

  </head>
  <body style="color: #343434; text-align: left; font-family: "Lucida Grande", "Lucida Sans Unicode", Arial, sans-serif; line-height: 1.5;">
       {*{if $changed}<h2 style="width:100%;text-align:center;">Změna objednávky</h2>{/if}*}

Vážený zákazníku,<br />
<br />
<br />

{if $close}
 <strong>Dobrý den, děkujeme za Vaši objednávku. Objednávka č. {$order.order_code} byla uzavřena a pro tento termín není již možno přiobjednávat. Využijte možnost objednání pro další termín.</strong><br />
 {/if}
{if $opened}
 {if $changed}
      Vaše objednávka byla změněna.
 {else}
  {/if}
{elseif $close}
 <strong>Dobrý den, děkujeme za Vaši objednávku. Objednávka č. {$order.order_code} byla uzavřena a pro tento termín není již možno přiobjednávat. Využijte možnost objednání pro další termín.</strong><br />
   
    {else}
      <strong>Vaše objednávka byla odeslána ke zpracování. </strong><br />
    <br/>
	{*
    Zboží si vyzvedněte následující týden po uzávěrce objednávek v {$day}.
	*}

{/if}<br />
<br />
<br />
Děkujeme za Vaši objednávku.<br />
<br />
Hornacka-farma.cz<br /><br />
{* zde se vlozi sablona s obsahem objednavky *}

<table class="orderTable" style="border: 1px solid #DDDDD7; table-layout: auto; width: 100%; border-collapse: collapse;">
   <tr>
     <td colspan="2"><h2>Objednávka číslo: &nbsp; <span class="bold">{$order.order_code}</span></h2></td>
   </tr>
   <tr>
     <!-- sloupec s dodavatelem -->
     <td class="dodavatel p20">
       <span class="date">{$order.order_date}</span>&nbsp;| Stav objednávky: <span class="bold">{if $opened}{$order.order_status}{else}Uzavřená{/if}</span><br /><br />
       <div style="width:390px;">
           <div style="float:left;"><strong>dodavatel:</strong></div> <div style="float:right;">{*<strong>provozovatel:</strong>*}</div>
           <div style="clear:both;"></div>
            {*  <table style="float:right;">
               <tr><td colspan="3" class="title"><strong>{$order.owner_pro_firma}</strong></td></tr>
               <tr><td class="col1">{$order.owner_pro_ulice},</td>     <td> IČO:</td><td class="bold">{$order.owner_pro_ic}</td></tr>
               <tr><td colspan="3">{$order.owner_pro_psc} {$order.owner_pro_mesto}</td>  </tr>
               <tr><td colspan="3">&nbsp;</td></tr>
             </table>   *}

             <table style="float:left;">
               <tr><td colspan="3" class="title"><strong>{$order.owner_firma}</strong></td></tr>
               <tr><td class="col1">{$order.owner_ulice},</td>     <td> IČO:</td><td class="bold">{$order.owner_ic}</td></tr>
               <tr><td colspan="3">{$order.owner_psc} {$order.owner_mesto}</td>  </tr>
               <tr><td colspan="3">&nbsp;</td></tr>
             </table>
       </div>
         {*<table>
           <tr><td>Tel:</td><td class="bold">{$order.owner_tel}</td></tr>
           <tr><td>E-mail:</td><td>{$order.owner_second_email}</td></tr>
         </table> *}
     </td>

     <!-- sloupec s odberatelem -->
     <td class="odberatel" width="350">
       <div class="box p20" style="border: 2px solid #DDDDD7; display: block; margin-top: 15px;padding: 5px;">
         <p><strong>odběratel:</strong></p>
         <table>
           <tr><td colspan="3" class="title"><strong>{$order.order_shopper_name}</strong></td></tr>
           <tr><td class="col1">{$order.order_shopper_street}</td>     {if $order.order_shopper_ic}<td>IČ:</td><td class="bold">{$order.order_shopper_ic}</td>{else}<td colspan="2"></td>{/if}</tr>
           <tr><td>{$order.order_shopper_zip}, {$order.order_shopper_city}</td>  {if $order.order_shopper_dic}<td>DIČ</td><td class="bold">{$order.order_shopper_dic}</td>{else}<td colspan="2"></td>{/if}</tr>
           <tr><td colspan="3">&nbsp;</td></tr>
         </table>
         <table>
           <tr><td>Tel:</td><td class="bold">{$order.order_shopper_phone}</td></tr>
           <tr><td>E-mail:</td><td>{$order.order_shopper_email}</td></tr>
         </table>
         <!-- alternativne dodaci adresa -->
         <br />
         <p class="bold" ><strong>Dodací adresa:</strong></p>
         {if $order.no_delivery_address}
         <p>stejná jako fakturační</p>
         {else}
         <table>
           {*<tr><td colspan="2">{$order.order_branch_name}</td></tr>
           <tr><td colspan="2">{$order.order_branch_street}</td></tr>
           <tr><td colspan="2">{$order.order_branch_zip}, {$order.order_branch_city}</td></tr>
           <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
              <td>Telefon: &nbsp;</td>
              <td>{$order.order_branch_phone}</td>
            </tr>
            <tr>
              <td>E-mail:</td>
              <td>{$order.order_branch_email}</td>
            </tr>
            <tr><td><strong>{$name},</strong></td><tr>
            <tr><td><strong>{$street},{$city}</strong></td><tr>
             <tr><td><strong>Zboží si vyzvedněte následující týden po uzávěrce objednávek - {$day} </strong></td><tr>
			 *}
         </table>
         {/if}

       </div>
       {*
       <!-- volitelny text -->
       <!--
       <p class="p20">
          Tellus dictumst molestie Lorem Sed Cras Pellentesque quis nec egestas et. Quis tempor wisi at facilisi velit sem Nullam Sed Vestibulum ante. Id In pretium eu convallis Integer et at ac augue metus.
       </p>
       -->
       *}
     </td>
     </tr>
     <tr><td colspan="2">&nbsp;</td></tr>

     <tr>
       <td colspan="2" class="orderItems">
         <!-- vypis objednaneho zbozi -->
         <table border="0" cellspacing="0" cellpadding="3" width="100%" style="width:100.0%;border-collapse:collapse">
            <tr class="grey"><th style="background-color: #DBDBDB;">Název produktu</th><th class="oiFixCol1" style="background-color: #DBDBDB;" width="80">Kód</th><th class="oiFixCol0" style="background-color: #DBDBDB;" width="100">Množství</th><th style="background-color: #DBDBDB;text-align:right;  " class="oiFixCol2" width="140" >Cena s dph za m.j.</th><th style="background-color: #DBDBDB;padding-right:10px;" class="priceCol" width="110">Cena s dph</th></tr>

            {foreach item=item key=k name=iter from=$order.products}
            <tr {if $smarty.foreach.iter.iteration mod 2 == 0}class="grey"{/if}>
              <td {if $smarty.foreach.iter.iteration mod 2 == 0}style="background-color: #F3F3F0;"{/if}>
              {if $item.item_zmeneno}* {assign var='zmeneno' value=true}{/if}{$item.nazev}
              </td>
              <td {if $smarty.foreach.iter.iteration mod 2 == 0}style="background-color: #F3F3F0;"{/if}>{$item.code}</td>
              <td class="txtRight"  {if $smarty.foreach.iter.iteration mod 2 == 0}style="background-color: #F3F3F0;"{/if}>{$item.units} {$item.jednotka}</td>
              <td class="txtRight"  style="{if $smarty.foreach.iter.iteration mod 2 == 0}background-color: #F3F3F0;{/if}text-align:right;padding-right:10px;">{$item.price_with_tax|currency:'cz'} {translate str="Kč"}</td>
              <td class="priceCol" style="{if $smarty.foreach.iter.iteration mod 2 == 0}background-color: #F3F3F0;{/if}text-align:right;padding-right:10px;">{$item.total_price_with_tax|currency:'cz'} {translate str="Kč"}</td>
            </tr>
            {/foreach}
            <tr><td colspan="5" {if $smarty.foreach.iter.iteration mod 2 == 0}style="background-color: #F3F3F0;"{/if}>&nbsp;</td></tr>
            {if isset($zmeneno)}
            <tr><td colspan="5" class="txtRight" {if $smarty.foreach.iter.iteration mod 2 == 0}style="background-color: #F3F3F0;"{/if}>* množství položky bylo upraveno</td></tr>
            {/if}

          </table>
        </td>
      </tr>
     {* {if !$opened}
      <tr>
        <td colspan="2" class="orderAddition">
           <!-- udaje pro dopravu -->
           <table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse">
             <tr class="grey"><th style="background-color: #DBDBDB;">Způsob dopravy</th><th style="background-color: #DBDBDB;">Upřesnění dopravy</th><th class="priceCol" style="background-color: #DBDBDB;">Cena</th></tr>
             <tr><td>{$order.order_shipping_nazev}</td><td>{$order.order_shipping_descr}</td><td class="oaFixCol priceCol" style="text-align:right;padding-right:10px;">{$order.order_shipping_price} Kč</td></tr>
           </table>
        </td>
      </tr>

      <tr>
        <td colspan="2"  class="orderAddition noborder">
           <!-- udaje pro platbu -->
           <table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse">
             <tr class="grey"><th style="background:#F3F3F0;padding:.75pt .75pt .75pt .75pt">Způsob platby</th><th style="background:#F3F3F0;padding:.75pt .75pt .75pt .75pt" class="priceCol">Cena</th></tr>
             <tr><td>{$order.order_payment_nazev}</td><td class="oaFixCol priceCol" style="text-align:right;">{$order.order_payment_price} Kč</td></tr>
           </table>
        </td>
      </tr>
      {/if} *}
      <tr>
       <td colspan="2" class="orderItems">
          <table border="0" cellspacing="0" cellpadding="3" width="100%" style="width:100.0%;border-collapse:collapse">
            <tr><td colspan="5">&nbsp;</td></tr>
            {*<tr><td colspan="4" class="priceLabelCol" style="text-align:right;font-weight:bold;"> Mezisoučet (bez DPH): </td><td class="priceCol" width="110" style="text-align:right;padding-right:10px;">{$order.order_total_without_vat} Kč</td></tr> *}
            {*<tr><td colspan="4" class="priceLabelCol" style="text-align:right;font-weight:bold;"> Mezisoučet: 	</td><td class="priceCol" width="110" style="text-align:right;padding-right:10px;">{$order.order_total_with_vat} Kč</td></tr> *}
            {*<tr><td colspan="4" class="priceLabelCol" style="text-align:right;font-weight:bold;"> DPH objednaného zboží celkem: </td><td class="priceCol" width="110" style="text-align:right;padding-right:10px;">{$order.order_higher_vat} Kč </td></tr> *}
           {* {if !$opened}
              <tr><td colspan="5">&nbsp;</td></tr>
              <tr><td colspan="4" class="priceLabelCol" style="text-align:right;font-weight:bold;"> Dopravné a balné: </td><td class="priceCol" width="110" style="text-align:right;padding-right:10px;">{$order.order_shipping_price} Kč</td></tr>
              <tr><td colspan="4" class="priceLabelCol" style="text-align:right;font-weight:bold;">Cena za platební metodu: </td><td class="priceCol" width="110" style="text-align:right;padding-right:10px;">{$order.order_payment_price} Kč</td></tr>
            {/if} *}
            <tr><td colspan="5">&nbsp;</td></tr>
            <tr class="grayRow"><td colspan="4" style="width:82.5pt;background:#DBDBDB;padding:3.0pt 11.25pt 3.0pt 1.5pt" class="priceLabelCol"><p style="text-align:right;font-weight:bold;"> Celkem:</p></td><td  style="width:82.5pt;background:#DBDBDB;padding:3.0pt 11.25pt 3.0pt 1.5pt; text-align:right;" class="priceCol bold">{$order.order_total_CZK} Kč</td></tr>
         </table>
       </td>
     </tr>

     <tr>
       <td colspan="2" class="orderAddition p20">
         <p>Poznámka k platbě: platba v hotovosti při převzetí zboží </p>
        {if $order.order_shopper_note} <p>Poznámka zákazníka:</p><br />
         <p>{$order.order_shopper_note}</p>{/if}
       </td>
     </tr>

</table>





<br />




    </body>
</html>


