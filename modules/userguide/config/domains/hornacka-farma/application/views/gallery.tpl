{* sablona galerie *}
<ul id="product-gallery" class="jcarousel-skin-hof">
	{foreach name=photo from=$photos key=key item=item}
	<li>
		<a href="{$item.photo_detail}" title="{$item.nazev}" class="colorbox group">
			<img src="{$item.photo}" alt="detail fotografie" title="{$item.nazev}" />
		</a>
	</li>
	{/foreach}
</ul>