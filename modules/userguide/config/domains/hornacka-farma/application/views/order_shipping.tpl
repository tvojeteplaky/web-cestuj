{* sablona tabulky pro vyber dopravy *}
{* tato sablona je soucasti objednavky - nadrazena sablona: order_shipping_payment.tpl *}

<table summary="formulář s výběrem způsobu dopravy">
  <tr>
    <th>Způsob dopravy</th>
    <th>&nbsp;</th>
    <th class="order-col-cena txtRight">Cena s DPH</th>
    <th class="order-col-smazat">&nbsp;</th>
  </tr>
  {foreach from=$shipping_types key=key item=item}
  <tr class="sectiontableentry2">
    <td><input type="radio" class="chbradio ajaxradio" name="shipping_id" value="{$item.id}" id="shipping-variant-{$item.id}" {if $item.checked}checked="checked" {/if}/> <label for="shipping-variant-{$item.id}">{$item.nazev}</label></td>
    <td>{$item.popis}</td>
    <td class="txtRight">{if $item.cena>0}{$item.cena|currency:'cz'} Kč{else}zdarma{/if}</td>
    <td>&nbsp;</td>
  </tr>  
  {/foreach}
  
</table>