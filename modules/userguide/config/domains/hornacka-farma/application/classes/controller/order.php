<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Generovani stranek a podstranek patricich k objednavkam.
 */
class Controller_Order extends Controller
{
    /**
     *
     * @var Service_ShoppingCart
     */
    private $cart_service;

    /**
     *
     * @var Service_Order
     */
    private $order_service;

    /**
     *
     * @var Service_User
     */
    private $user_service;

    private $shopper;

    private $session;

    public function before()
    {
        parent::before();
        // vsechny metody jsou platne pouze v rozsahu internich requestu, zadne externi volani
        if(!$this->internal) throw new Kohana_Exception("externí request není povolen");
        $this->session = Session::instance();

        $this->cart_service = Service_ShoppingCart::instance();
        $this->order_service = Service_Order::instance();
        $this->user_service = Service_User::instance();
        $this->shopper = Shauth::instance();

    }

    /** zakladni metody kontrolery modulu **/

    /**
     * Metoda pro 1. krok objednavky (0.krok = kosik)
     */
    public function action_shipping_payment()
    {
        // zpracovani dat a zaslani vysledku ze submetod
        $order_shipping=Request::factory("internal/order/shipping")->execute()->response;
        $order_payment=Request::factory("internal/order/payment")->execute()->response;
        Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Objednávka - výběr dopravy a platby","nazev_seo"=>"doprava-a-platba"));

        if(!$this->user_service->logged_in()) $this->user_service->need_insert_user_data(true);

        if(isset($_POST["next_step"]))
        { // TODO doimplementovat kontrolu i v servisni metode
            if($this->order_service->process_selected_shipping_payment_types(isset($_POST["shipping_id"])?$_POST["shipping_id"]:false, isset($_POST["payment_id"])?$_POST["payment_id"]:false))
            {
                Request::instance()->redirect(url::base()."dodaci-udaje");
            }
            else
            {

            }
        }

        $order_container_template=new View("order_container");
        $order_shipping_payment_template=new View("order_shipping_payment");

        $order_container_template->cart_detail_table=Request::factory("internal/cart/detail_table/read_only")->execute()->response;

        // doprava
        $order_shipping_payment_template->order_shipping=$order_shipping;

        // platba
        $order_shipping_payment_template->order_payment=$order_payment;

        // prehled cen
        $order_shipping_payment_template->order_price_summary_table=Request::factory("internal/order/order_price_summary_table/")->execute()->response;

        $order_container_template->content=$order_shipping_payment_template;
        $order_container_template->order_step=1;
        $this->request->response=$order_container_template->render();

    }

    /**
     * Mezikrok mezi 2-3 (prihlaseni/registrace+prihlaseni/pouze kontaktni informace)
     */
    public function action_order_user_data_source()
    {
        $next_step_seo="dodaci-udaje";

        if($this->shopper->logged_in())
        {
            Request::instance()->redirect(url::base().$next_step_seo);
        }

        $order_container_template=new View("order_container");
        $order_user_data_source_template=new View("order_user_data_source");
        Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Objednávka - zadání údajů o uživateli","nazev_seo"=>"udaje-o-uzivateli"));


        // rozdeleni prace do subkontroleru ;-) TODO - umoznit nastaveni techto trech moznosti v adminu
        // 1. zjistit prihlaseni uzivatele a pripadne ho presmerovat

        // 2.1 zavolat kontroler pro prihlaseni
        $user_login = Request::factory("internal/user/login/".$next_step_seo."/true/")->execute()->response;

        // 2.2 zavolat kontroler pro registraci s prihlasenim
        $user_registration = Request::factory("internal/user/registration/".$next_step_seo."/true/")->execute()->response;

        // 2.2 zavolat kontroler pro pokracovani bez prihlaseni
        $order_unregistered_user = Request::factory("internal/order/unregistered_user/".$next_step_seo."/true/")->execute()->response;

        if($this->user_service->is_temporarily_stored())$order_user_data_source_template->action="order-unregistered-user";

        if(isset($_POST["action"])) $order_user_data_source_template->action=$_POST["action"];
        $order_user_data_source_template->user_login              =$user_login;
        $order_user_data_source_template->user_registration       =$user_registration;
        $order_user_data_source_template->order_unregistered_user =$order_unregistered_user;

        $order_container_template->content=$order_user_data_source_template;
        $order_container_template->order_step=2;
        $this->request->response=$order_container_template->render();
    }

    /**
     * Metoda pro 2. krok objednavky (vyber dodaci adresy)
    */
    public function action_order_delivery_data($msg=false)
    {
        $next_step_seo="potvrzeni-objednavky";

        // vyber platby a dopravy probehl uspesne, pokracuji k dalsimu kroku
        // pokud neni uzivatel prihlasen, jdu na "mezikrok"
        if(!$this->user_service->logged_in() && $this->user_service->need_insert_user_data())
        {
            Request::instance()->redirect(url::base()."udaje-o-uzivateli");
        }

        $order_container_template=new View("order_container");
        $order_user_delivery_data_template=new View("order_user_delivery_data");

        // vypsani pripadne hlasky
        $message="";
        if(isset($_GET["registration"]) && $_GET["registration"]=="ok") $message="regok";
        if(isset($_GET["login"]) && $_GET["login"]=="ok") $message="loginok";

        $order_user_delivery_data_template->message=$message;

        // ziskani dat o zakaznikovi
        $shopper=$this->user_service->get_user($this->shopper->get_user());
        $order_user_delivery_data_template->data=$shopper;

        // ziskani dat o zakaznikovych adresach
        $branch_id=(isset($_POST["branch_id"]))?$_POST["branch_id"]:false;
        $order_user_delivery_data_template->branches=$this->order_service->get_order_shopper_branches($shopper, $branch_id);


        if(isset($_POST["next_step"]))
        { // TODO doimplementovat kontrolu

            if(true)
            {
                Request::instance()->redirect(url::base().$next_step_seo);
            }
            else
            {

            }
        }

        Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Objednávka - zadání dodacích údajů","nazev_seo"=>"dodaci-udaje"));

        $order_container_template->content=$order_user_delivery_data_template;
        $order_container_template->order_step=2;
        $this->request->response=$order_container_template->render();
    }

    /**
     * Metoda pro 3. krok objednavky (prehled objednavky a jeji potvrzeni.
     */
    public function action_order_confirmation()
    {
        $next_step_seo="objednavka-odeslana";

        // vyber dodaci adresy probehl uspesne - pokracuju k potvrzeni objednavky
        // pokud neni uzivatel prihlasen, jdu na "mezikrok"
        if(!$this->user_service->logged_in() && !$this->user_service->is_temporarily_stored())
        {
            Request::instance()->redirect(url::base()."udaje-o-uzivateli");
        }

        $shopper=$this->user_service->get_user();

        $order_container_template=new View("order_container");
        $order_confirmation_template=new View("order_confirmation");
        $order_summary_table_template=new View("order_summary_table");

        if(isset($_POST["next_step"]))
        {
            if($this->order_service->create_order_for_shopper($shopper,$this->session->get("branch_id"),isset($_POST["poznamka"])?$_POST["poznamka"]:""))
            {
                // objednavka byla uspesne ulozena

                // odstranim obsah kosiku
                $this->cart_service->flush_cart();

                // odstranim pripadne udaje zadane uzivatelem bez registrace
                $this->user_service->delete_temporarily_stored_user();

                $_SESSION["order_saved"]="345SDFG";

                Request::instance()->redirect(url::base().$next_step_seo);
            }
            else
            {

            }
        }

        Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Objednávka - potvrzení objednávky","nazev_seo"=>"potvrzeni-objednavky"));

        $order_confirmation_template->shopping_cart=Request::factory("internal/cart/detail_table/read_only")->execute()->response;
        $order_confirmation_template->order_price_summary_table=Request::factory("internal/order/order_price_summary_table/")->execute()->response;

        $order_summary_table_template->billing_data=$this->user_service->get_user($this->shopper->get_user());
        $order_summary_table_template->branch_data=$this->user_service->get_user_branch($_SESSION["branch_id"],$this->shopper->get_user());
        $order_summary_table_template->doprava=$this->order_service->get_shipping_name();
        $order_summary_table_template->platba=$this->order_service->get_payment_name();
        $order_confirmation_template->order_summary_table=$order_summary_table_template;

        $order_container_template->content=$order_confirmation_template;
        $order_container_template->order_step=3;
        $this->request->response=$order_container_template->render();
    }

    /**
     * Sem se skript presmeruje po uspesnem zpracovani objednavky.
     */
    public function action_order_complete()
    {
        if(!isset($_SESSION["order_saved"]) || $_SESSION["order_saved"]!="345SDFG") throw new Kohana_Exception("Požadovaná stránka nebyla nalezena");

        // obecna sablona textove stranky
        $page=new View("message");
		
        $page->page=array("popis"=>("<div id='generic-page'><h1>Objednávka odeslána</h1><p>Vaše objednávka bude uzavřena a zpracována. Do té doby můžete stále Vaši objednávku měnit nebo upravovat. Info o převzetí obdržíte mailem."));
        $this->request->response=$page->render();
    }


    /** submetody kontroleru modulu **/

    /**
     * Submetoda pro generovani a prezentaci cenovych souctu. (Mimo kosiku, ten si vlastni cenu pocita a zobrazi sam)
     */
    public function action_order_price_summary_table()
    {
        $price_values=array();
        $order_price_summary_table=new View("order_price_summary_table");

        // prirazeni ceny kosiku
        $price_values[]=array("name"=>"Příplatek (sleva) za platební metodu","value"=>$this->order_service->get_payment_price_with_tax(),"type"=>$this->order_service->get_payment_type());
        $price_values[]=array("name"=>"Dopravné a balné","value"=>$this->order_service->get_shipping_price_with_tax());

        $price_values[]=array("name"=>"Cena celkem","value"=>$this->order_service->get_total_order_price_with_tax(),"class"=>"strong");
        $order_price_summary_table->price_values=$price_values;

        $this->request->response=$order_price_summary_table->render();
    }

    /*
     * Submetoda kontroleru pro obsluhu a zpracovani formulare dopravy.
     */
    public function action_shipping()
    {
        $order_shipping_template=new View("order_shipping");
        $shipping_id=(isset($_POST["shipping_id"]))?$_POST["shipping_id"]:false;
        $order_shipping_template->shipping_types=$this->order_service->get_shipping_types($shipping_id);
        $this->request->response=$order_shipping_template->render();
    }

    /**
     * Submetoda kontroleru pro obsluhu a zpracovani formulare platby.
     */
    public function action_payment()
    {
        $order_shipping_template=new View("order_payment");
        $payment_id=(isset($_POST["payment_id"]))?$_POST["payment_id"]:false;
        $order_shipping_template->payment_types=$this->order_service->get_shipping_payment_types($payment_id);
        $this->request->response=$order_shipping_template->render();
    }

    /**
     * Submetoda pro zobrazeni a obsluho formulare rychleho zadani kontaktu pro uzivatele - pri nakupu bez registrace.
     * @param string $on_success_redirect_to volitelne url na kterou se ma beh programu presmerovat po uspesnem prihlaseni
     * @param boolean $included priznak zda jde o samostatny formular, nebo je vlozeny do sirsi struktury formularu
     */
    public function action_unregistered_user($on_success_redirect_to=false, $included=false)
    {
        $order_unregistered_user_template=new View("order_unregistered_user");
        if(isset($_POST["action"]) && $_POST["action"]=="order-unregistered-user")
        {
            if($this->user_service->temporarily_store_user($_POST["unregistered-user"]))
            {
                $this->user_service->need_insert_user_data(false);
                if($on_success_redirect_to) Request::instance()->redirect(url::base().$on_success_redirect_to);
                //else
                //Request::instance()->redirect(url::base()."prihlaseni-uzivatele");
            }
            else
            {
                $order_unregistered_user_template->error = "Chyba zpracování dat, zkontrolujte prosím zadané údaje";
                $order_unregistered_user_template->user_errors=$this->user_service->errors;
                $order_unregistered_user_template->data=$_POST["unregistered-user"];
            }
        }
        else
        {
            if($this->user_service->is_temporarily_stored())
                $order_unregistered_user_template->data=$this->user_service->get_user();
        }
        $order_unregistered_user_template->included=$included;
        $this->request->response=$order_unregistered_user_template->render();
    }


    /**
     * Submetoda pro generování výpisu objednávek
     */
    public function action_order_summary()
    {
       $shopper=$this->user_service->get_user($this->shopper->get_user());
        if(!isset($shopper->id))  Request::instance()->redirect(url::base()."index");

        $price_values=array();
        $order_summary=new View("user_orders_list");
        $shopper=$this->user_service->get_user();
        $lang = Service_Route::instance()->get_last_language_id();

        Service_Route::instance()->set_selected_language_id($lang);
        if($lang==1){         Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Účet uživatele","nazev_seo"=>"ucet-uzivatele"));
                              Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Přehled objednávek","nazev_seo"=>"prehled-objednavek"));
                              }
        elseif($lang==3){Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"User Profile","nazev_seo"=>"profile"));
                        Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Order Summary","nazev_seo"=>"order-summary"));
        }

        $orders=Service_Order::instance()->get_orders_list($shopper->id,1);
        $order_summary->nove_objednavky=$orders;

        $orders=Service_Order::instance()->get_orders_list($shopper->id,2);
        $order_summary->zpracovavane_objednavky=$orders;


        $orders=Service_Order::instance()->get_orders_list($shopper->id,3);
        $order_summary->dodane_objednavky=$orders;

        if(isset($_GET['order_delete']) && isset($_GET['id'])) $order_summary->user_message="Opravdu chcete smazat objednávku číslo ".$_GET['id']." ?<br/><br/><a href='/smazani-objednavky?id=".$_GET['id']."' class='user-choice'>ANO</a><a href='/prehled-objednavek' class='user-choice'>NE</a>";



        $this->request->response=$order_summary->render();
    }

     /**
     * Submetoda pro zobrazení detailu objednávky
     */
    public function action_order_detail()
    {
        $shopper=$this->user_service->get_user($this->shopper->get_user());
        if(!isset($shopper->id))  Request::instance()->redirect(url::base()."index");


        $price_values=array();
        $order_detail=new View("user_order_detail");


        Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Účet uživatele","nazev_seo"=>"ucet-uzivatele"));
        Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Přehled objednávek","nazev_seo"=>"prehled-objednavek"));
        Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Detail objednávky","nazev_seo"=>"detail-objednavky"));

        if(isset($_POST["product_quantity"]))
            {
                $total_price_with_tax=0;
                $total_price_without_tax=0;
                foreach($_POST["product_quantity"] as $product_id=>$quantity)
                {
                    $total_price=explode(",",$this->order_service->update_open_order($_POST["order_id"],$product_id,$quantity));
                    $total_price_with_tax=$total_price_with_tax+$total_price[0];
                    $total_price_without_tax=$total_price_without_tax+$total_price[1];
                }
                $this->order_service->update_open_order_price($_POST["order_id"],$total_price_without_tax,$total_price_with_tax,$_POST["distributionplace_id"]);
            }

        if(isset($_GET["remove_item"]) && isset($_GET["id"]) && isset($_GET["shopper_id"]))
            {
                   //nejprve musíme zkontrolovat, jestli je přihlášený uživatel, který žádá smazání položky - bezpečností opatření

               if(isset($shopper->id) && $_GET["shopper_id"]==$shopper->id)$this->order_service->remove_from_open_order($_GET["remove_item"],$_GET["id"]);
            }
       $order_detail->product_places=Service_Product::instance()->get_distribution_places();



       $order=$this->order_service->dump_open_order_frontend($_GET["id"]);
       $order_detail->distributionplace_id=$shopper->distributionplace_id;
       $order_detail->order_cart_products=$this->order_service->get_order_items($order->id);
       $order_detail->open_order_total= $order->order_total_CZK;
       $order_detail->order=$order;






        $this->request->response=$order_detail->render();
    }

    /**
     * Submetoda pro smazání objednávky
     */
    public function action_order_delete()
    {
        $shopper=$this->user_service->get_user($this->shopper->get_user());
        if(!isset($shopper->id))  Request::instance()->redirect(url::base()."index");


        if(isset($_GET['id'])){$order=orm::factory("order")->where("order_shopper_id","=",$shopper->id)->where("id","=",$_GET['id'])->find();}else{
           Request::instance()->redirect(url::base()."index");
        }
        $result=$order->delete();

        if($result) Request::instance()->redirect(url::base()."prehled-objednavek");





        $this->request->response=$order_detail->render();
    }

  /**
     * Metoda obsluhující externí cron scripty
     */
  public function action_update_system()
  {
    if(!isset($_GET['process']))return false;

    $process = $_GET['process'];
    $owner=orm::factory("owner",1);

    $order_service = new Service_Order();
   if($process=="close_orders")
   {
         try{
          $orders=orm::factory("order")->where("open","=",1)->find_all();
          $order_array=array();
          $x=0;
          foreach($orders as $order){

            $email_service = new Service_Email();
            $order_data=$order_service->dump_order_for_email($order->order_shopper_id,$order->id);
            $email_service->sendOrderMail($order_data,$order);

            $export_directory = DOCROOT."import/orders/";
            $export_directory2 = DOCROOT."import_runner/orders/";

            $order_products = orm::factory("order_item")->where("order_id","=",$order->id)->find_all();

            if(isset($data))unset($data);
            foreach($order_products as $product){
                //$product_orm=orm::factory("product")->where("products.id","=",$product->product_id)->find();
                $data[]=array(
                       "id"         =>$product->product_id,
                       "code"       =>$product->code,
                       "nazev"      =>$product->nazev,
                       "jednotka"   =>$product->jednotka,
                       "hmotnost"           =>$product->hmotnost,
                       "pocet_na_sklade"    =>$product->pocet_na_sklade,
                       "min_order_quantity" =>$product->min_order_quantity,
                       "tax_code"           =>$product->tax_code,
                       "mnozstvi"   =>round($product->units,0),
                       "cena_s_dph" =>$product->price_with_tax,
                       "cena_bez_dph"=>$product->price_without_tax,
                       "cena_celkem_s_dph" =>$product->price_with_tax * round($product->units,0),
                    );

            }



            $order_service->export_order_to_xml("order_".$order->id.".xml",$export_directory,$order,$order->id,"eshop",$data);
            $order_service->export_order_to_xml("order_".$order->id.".xml",$export_directory2,$order,$order->id,"eshop",$data);

           /* print_r($order);*/


            $order_array[$x]=$order->id;
            $order->open=0;
            $order->order_state_id=2;

            $result=$order->save();

            $x++;

          }
          $export=$order_service->export_order_to_xls($owner,$order_array);
          if($export)$result2=$email_service->sendOrderMailToAdmin($order_array);
          Kohana::$log->add(Kohana::INFO, "Zavření objednávek proběhlo v pořádku v ".date("Y-m-d H:i:s"));
          //zakáže se obchodování
          //$owner->stop_trading=1;
          //$owner->save();
          if($result) return true;
          }catch(Exception $e){
            $email_service->sendError(Kohana::exception_text($e));
             Kohana::$log->add(Kohana::ERROR, Kohana::exception_text($e));
          }
   }

   if($process=="check_xmls"){
       //projedu všechny objednávky a updatuju status na 1 tam, kde je status xml ok
       $orders = db::query(Database::SELECT,'SELECT `orders` . * FROM  `orders` where status = 0 and open = 0 and (UNIX_TIMESTAMP(CURRENT_TIMESTAMP) - UNIX_TIMESTAMP( order_date )) > 864000 ')->execute()->as_array(); //pokud pět dní nevyzvednou mail (432 000 s)
       foreach($orders as $order) {
         $filename = 'import/orders_response/order_'.$order["id"].'.xml';

      if(file_exists($filename)){

                if($xml = simplexml_load_file($filename, 'SimpleXMLElement', LIBXML_NOCDATA)) {

                   foreach($xml->attributes() as $attribute => $value) {

                  if($attribute=="state" && $value=="ok") {$order=orm::factory('order',$order["id"]); $order->status=1; $order->save();}
                   }

              }


      } else  {
         $email_service = new Service_Email();
         //$email_service->sendBadOrdersMail($order["id"],$filename);

              }



    }
    //opětovná kontrola statusu, tentokrát pouzenových objednávek
    $orders = orm::factory('order')->where('order_state_id','=',1)->where('open','=',0)->find_all();
       $orderID="";
       foreach($orders as $order) {
          if($order->status==0) $orderID .= $order->id.", ";

       }
       if(!empty($orderID)) {
         $email_service = new Service_Email();
         //$email_service->sendBadOrdersMail($orderID);
       }
   }

      if($process=="open_trading")
   {

    //povolí obchodování
    $owner->stop_trading=0;
    $result = $owner->save();

    if($result) return true;
   }
   if($process=="send_emails")
   {

    $email_service = new Service_Email();
    $result=$email_service->sendStoredEmails(20);
    if($result) $email_service->cleanEmailQueue(1);
    if($result) return true; else
    echo "Odeslání mailů se nezdařilo";
   }




  }

    /**
     * Metoda obsluhující zobrazení objednávky (jako v administraci - náhled objednávky)
     */
  public function action_show_order()
  {
    if(!isset($_GET['id'])) Request::instance()->redirect(url::base()."index");
    $order_id = $_GET['id'];

    Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Účet uživatele","nazev_seo"=>"ucet-uzivatele"));
    Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Přehled objednávek","nazev_seo"=>"prehled-objednavek"));
    Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Detail objednávky","nazev_seo"=>"detail-objednavky"));

    $template=new View("order_detail");


        $this->order_service = new Service_Hana_Order();

        $order=orm::factory("order")->find($order_id);
            $distribution_place = orm::factory("distributionplace")->where("distributionplaces.id","=",$order->distributionplace_id)->find();
            $place = orm::factory("place")->where("places.id","=",$distribution_place->place_id)->find();
            $times=DB
              ::select('distributionplace_times.*')
              ->from("distributionplaces")
              ->join("distributionplace_times_distributionplaces")->on("distributionplaces.id","=","distributionplace_times_distributionplaces.distributionplace_id")
              ->join("distributionplace_times")->on("distributionplace_times.id","=","distributionplace_times_distributionplaces.distributionplace_time_id")
              ->where("distributionplaces.id","=",$distribution_place->id)->limit("1");
              $times=$times->execute()->as_array();
            $template->day=$times[0]["den"].", od ".$times[0]["cas_dodavek_od"]." do ".$times[0]["cas_dodavek_do"]." hodin";
            $template->street=$distribution_place->ulice_cp;
            $template->city=$place->nazev;
            $template->name=$distribution_place->nazev;
        // cenova kategorie zakaznika, kteremu patri objednavka
        $shopper=orm::factory("shopper")->find($order->order_shopper_id);

        $price_code=$shopper->price_category->kod;

        $order_data=$this->order_service->dump_order($order);
        $order_data["order_total_vat"]=$order_data["order_total_with_vat"] - $order_data["order_total_without_vat"];
        //print_r($order_data);

        $template->order=$order_data;

        $this->request->response=$template->render();

  }
     /**
     * Submetoda pro externí export objednávky do xml
     */
    public function action_backup_export()
    {
      $order_id= $_GET["id"];
       //$lang = Service_Route::instance()->get_last_language_id();
        $order=orm::factory("order")->where("id","=",$order_id)->find();
        $owner=orm::factory("owner",1);

        $order_items=orm::factory("order_item")->where("order_id","=",$order_id)->find_all();

        //   $euro = orm::factory("currency")->where("nazev","=","eur")->find(); //zjistim kolik je aktuální kurz eura z CNB
        print_r($order->id);
        foreach($order_items as $item)
                {

                    $product_orm=orm::factory("product")->where("products.id","=",$item->id)->find();

                    $cena_bez_dph=$item->price_without_tax;
                    $cena_s_dph=$item->price_with_tax;



                    $data[]=array(
                       "id"         =>$item->id,
                       "code"       =>$item->code,
                       //"ean"       =>$item->ean,
                       "nazev"      =>$item->nazev,
                       "jednotka"   =>$item->jednotka,
                       "hmotnost"           =>$item->hmotnost,
                       "pocet_na_sklade"    =>$item->pocet_na_sklade,
                       "min_order_quantity" =>$item->min_order_quantity,
                       "tax_code"           =>$item->tax_code,

                       "mnozstvi"   =>(int)$item->units,
                       "cena_s_dph" =>$cena_s_dph,
                       "cena_bez_dph"=>$cena_bez_dph,
                      // "cena_bez_dph_euro"=>round($cena_bez_dph/$euro->kurz,2),
                       "cena_celkem_bez_dph" =>$cena_bez_dph * $item->units,
                       //"cena_celkem_bez_dph_euro" =>round(($cena_bez_dph * $item->units)/$euro->kurz,2),
                       "cena_celkem_s_dph" =>$cena_s_dph * $item->units,

                    );

                }



        $_SESSION["order_id"]= $order_id;

        $export_directory = DOCROOT."import/orders/";
        $export_directory2 = DOCROOT."import_runner/orders/";
            $result=Service_Order::instance()->export_order_to_xml("order_".$order_id.".xml",$export_directory,$order,$order_id,"eshop",$data);
        $result=Service_Order::instance()->export_order_to_xml("order_".$order_id.".xml",$export_directory2,$order,$order_id,"eshop",$data);

       if($result)die("EXPORTOVÁNO");


    }



}
?>
