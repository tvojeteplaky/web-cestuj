<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Univerzalni generovani kontaktniho formulare.
 * subkontroler
 *
 * pripojena sablona: contactform.tpl
 * pozadovane servisni tridy: Email_Service
 * pozadovane modelove tridy: Model_Email_Queue, Model_Email_Contact, Model_Email_Recommend
 * administrace: n/a
 * priklad volani: Request::factory("internal/contactform/index/".$product_category["id"]."/product/item")->execute()->response;
 */
class Controller_Contactform extends Controller
{
    /**
     *
     * @param string $subject_name predmet dotazu (nazev) - zakodovany pomoci urlencode(str_replace(".", " ", $data)), url se zjisti automaticky (podle umisteni formulare)
     */
    public function action_index($subject_name="", $mode="", $template="contact_form")
    {
        $contactform = new View($template);
        $contactform->mode=$mode; // moznost nastavit mod kontaktniho formulare (dle sablony)

        // obsluha odeslani formularu
        $captcha = Captcha::instance();
        $contactform->captcha = $captcha->render();
        $subject_name=urldecode(str_replace("___", ".", $subject_name));
        $contactform->subject_name=$subject_name;
        if($subject_name) $contactform->form=array("zprava"=>__("Doporučuji prohlédnout si tuto stránku").": ".$subject_name. ", ".__("na adrese").": http://".$_SERVER['SERVER_NAME']."/".Service_Route::instance()->get_actual_nazev_seo());
        $contactform->current_url="".url::base().Service_Route::instance()->get_actual_nazev_seo();

        if(isset($_POST["action"]) && $_POST["action"]=="dotaz"){
           $contactform->display_question_form=true;
           $email_service = new Service_Email();
           if($email_service->sendContactForm($_POST)){
                $this->request->redirect(url::base().Service_Route::instance()->get_actual_nazev_seo()."?data_send=contact");

           }else{
                $contactform->contact_form_error=__("Chyba při zpracování zprávy.");
                $contactform->form=$_POST;
                $contactform->error=$email_service->getMailError();
           }
        }elseif(isset($_POST["action"]) && $_POST["action"]=="doporucit"){
           $contactform->display_recommend_form=true;
           $email_service = new Service_Email();
           if($email_service->sendRecommendForm($_POST)){
                $this->request->redirect(url::base().Service_Route::instance()->get_actual_nazev_seo()."?data_send=recommend");

           }else{
                $contactform->recommend_form_error=__("Chyba při zpracování zprávy.");
                $contactform->form=$_POST;
                $contactform->error=$email_service->getMailError();
           }
        }elseif(isset($_POST["action"]) && $_POST["action"]=="chyba"){
           $contactform->display_question_form=true;
           $email_service = new Service_Email();
           if($email_service->sendErrorForm($_POST)){
                $this->request->redirect(url::base().Service_Route::instance()->get_actual_nazev_seo()."?data_send=contact");

           }else{
                $contactform->contact_form_error=__("Chyba při zpracování zprávy.");
                $contactform->form=$_POST;
                $contactform->error=$email_service->getMailError();
           }
        }
        elseif(isset($_GET["data_send"]) && $_GET["data_send"]=="recommend"){
            $contactform->recommend_form_message=__("Doporučení bylo odesláno.");
        }elseif(isset($_GET["data_send"])){
            $contactform->contact_form_message=__("Zpráva byla odeslána.");
        }

        $this->request->response = $contactform->render();
    }

}
?>
