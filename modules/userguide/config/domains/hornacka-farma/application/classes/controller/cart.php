<?php defined('SYSPATH') or die('No direct script access.');

/**
 *
 *
 *
 * @package    Hana
 * @author     Pavel Herink
 * @copyright  (c) 2010 Pavel Herink
 */
class Controller_Cart extends Controller {

    /**
     *
     * @var Service_ShoppingCart
     */
    private $cart_service;

	/**
	 *
	 * @var Service_Order
	 */
	private $order_service;

    public function before() {
        parent::before();
        // vsechny metody jsou platne pouze v rozsahu internich requestu, zadne externi volani
        if(!$this->internal) throw new Kohana_Exception("externí request není povolen");
        $this->cart_service = Service_ShoppingCart::instance();
        $this->order_service = Service_Order::instance();

    }

    /**
     * Metoda generujici maly nahledovy nakupni kosik.
     */
    public function action_widget()
    {
		$user_service = Service_User::instance();
		$logged_in = $user_service->logged_in();

        $mini_cart_template=new View("cart_widget");

		$mini_cart_template->parameters = $this->cart_service->get_cart_parameters();
		$mini_cart_template->cart_items = count($this->cart_service->get_cart_content());

		$shopper = $user_service->get_user();

        if(isset($shopper->id)){
            $open_order=$this->order_service->check_open_order_exists($shopper->id);
            $mini_cart_template->order_total=$open_order->order_total;
			$mini_cart_template->order_number=$open_order->order_code;
        }

        $this->request->response=$mini_cart_template->render();
    }

    /**
     * Metoda generujici detail kosiku i s obsluznyma tlacitkama a hornima navigacnima ikonama (kompletni 1. krok objednavky).
     * @param string $mode - priznak (editable/read_only) zda se ma zobrazit editovatelny/needitovatelny kosik
     */
    public function action_detail($mode=false)
    {
        $order_container_template=new View("order_container");
        $cart_detail_container_template=new View("cart_detail_container");
		$cart_detail_container_template->current_url = url::base().Service_Route::instance()->get_actual_nazev_seo();

                 $product_service=Service_Hana_Product::instance();
        $cart_detail_container_template->product_places=$product_service->get_index_product_places();
		$this->order_service = new Service_Order();

		$cart_detail_container_template->shippings = $this->order_service->get_all_shipping_metods();

        Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Nákupní košík","nazev_seo"=>"nakupni-kosik"));

		$shopper = Service_User::instance()->get_user();

		$cart_detail_container_template->logged_in = (bool)Service_User::instance()->logged_in();

		$order_container_template->place = $shopper ? $shopper->place : NULL;
		$cart_detail_container_template->place = $shopper ? $shopper->place : NULL;

		if($mode=="editable") {
			if(isset($_POST["prev_step"])) {
				$order_container_template->merge_order = false;
				$old_open_order_items = $this->order_service->get_order_items($_POST["open_order_id"]);

				if(isset($_SESSION['open_order'])) {
					$this->cart_service->unmerge_order($_SESSION['open_order']);
				}

				unset($_SESSION['open_order']);
				//if(isset($open_order_items))$this->cart_service->merge_order($open_order,$open_order_items,$shopper->id);
			}

			$message = array();
            if(isset($shopper->id)) {
				$open_order = $this->order_service->check_open_order_exists($shopper->id);
			}

            if(isset($open_order->id))  {
				if(!isset($_SESSION['open_order'])) {
					$_SESSION['open_order']=$open_order->as_array();
				}

				$open_order_items=$this->order_service->get_order_items($open_order->id);
				$order_container_template->open_order_total=$open_order->order_total_CZK;
				$order_container_template->open_order_cart_products=$open_order_items;
            }

			if(isset($_SESSION['open_order'])) {
				$parameters = array(
					'name' => 'order_shopper_name',
					'street' => 'order_shopper_street',
					'city' => 'order_shopper_city',
					'zip' => 'order_shopper_zip',
					'phone' => 'order_shopper_phone',
					'email' => 'order_shopper_email',
					'poznamka' => 'order_shopper_note',
					'shipping_id' => 'shipping_id',
				);

				foreach ($parameters as $key => $value) {
					if(isset($_SESSION['open_order'][$value])) {
						$order_container_template->$key = $_SESSION['open_order'][$value];
					}
				}
			} else {
				$parameters = array(
					'name' => 'nazev',
					'street' => 'ulice',
					'city' => 'mesto',
					'zip' => 'psc',
					'phone' => 'telefon',
					'email' => 'email',
				);

				foreach ($parameters as $key => $value) {
					if(isset($shopper->$value)) {
						$order_container_template->$key = $shopper->$value;
					}
				}
			}


            // provedeni operaci nad kosikem
            // zmena mnozstvi v kosiku
            if(isset($_POST["product_quantity"])) {
                foreach($_POST["product_quantity"] as $product_id => $quantity) {
					$product = orm::factory("product", $product_id);

					if($product->jednotka == 'kg') {
						$quantity = (float)str_replace(array(' ', ','), array('', '.'), $quantity);
					} elseif ($product->jednotka == 'ks') {
						$quantity = (int)$quantity;
					}

                    $this->cart_service->update_quantity($product_id, $quantity);
                }
            }

            // smazani polozky v kosiku
            if(isset($_GET["remove_item"])) {
                $this->cart_service->remove_from_cart($_GET["remove_item"]);
            }
        }

		if(isset($_POST["merge_order"])) {
			$order_container_template->merge_order = true;
			$order_container_template->old_open_order_id = $open_order->id;

			if(isset($open_order_items)) {
				$this->cart_service->merge_order($open_order, $open_order_items, $shopper->id);
			}

			$order_container_template->place_id = $_POST["place_id"];
        }else {
			if(!isset($_POST["prev_step"])) {
				$_SESSION['open_order_items']=$this->cart_service->get_cart_content();
			}
		}

        // pokud je pozadavek na dalsi krok, provedu presmerovani
        if(isset($_POST["next_step"])) {

			$new_order = Service_Order::instance()->create_order_for_shopper(
					$shopper,
					isset($_POST["branch_id"]) ? $_POST["branch_id"] : "",
					isset($_POST["poznamka"]) ? $_POST["poznamka"] : "",
					isset($_SESSION['open_order']['id']) ? $_SESSION['open_order']['id'] : "",
					isset($_POST["place_id"]) ? $_POST["place_id"] : "",
					array(
						'shipping_id' => $_POST['order_shipping_id'],
						'nazev' => $_POST['order_branch_name'],
						'ulice' => $_POST['order_branch_street'],
						'mesto' => $_POST['order_branch_city'],
						'psc' => $_POST['order_branch_zip'],
						'telefon' => $_POST['order_branch_phone'],
						'email' => $_POST['order_branch_email'],
					)
			);

            if($new_order) {
                $_SESSION["order_saved"] = "345SDFG";
                $this->cart_service->flush_cart();
                unset($_SESSION['open_order']);
                unset($_SESSION['open_order_items']);
                Request::instance()->redirect(url::base()."objednavka-odeslana");
            }
        }

		/*
        $times = Service_Product::instance()->get_product_times();
        $cart_detail_container_template->times = $times;
		*/

        $cart_detail_template = Request::factory("internal/cart/detail_table/".$mode)->execute()->response;
        $cart_detail_container_template->cart_detail_table = $cart_detail_template;
        // tabulka souctu cen
        //$cart_detail_container_template->order_price_summary_table=Request::factory("internal/order/order_price_summary_table/")->execute()->response;

        //pokud je cena menší než 300, tak nepůjde objednat
        //if($this->cart_service->get_cart_price_with_tax()<300)$order_container_template->stop=true;


        $order_container_template->content = $cart_detail_container_template;
        $this->request->response = $order_container_template->render();
    }

    /**
     * Tato metoda zobrazi pouze tabulku s kosikem - bez obsluznych tlacitek a ikon.
     * @param string $mode - priznak (editable/read_only) zda se ma zobrazit editovatelny/needitovatelny kosik
     */
    public function action_detail_table($mode=false)
    {
        $cart_detail_template=new View("cart_detail_table");

        $cart_detail_template->read_only=($mode=="editable")?false:true;
        $cart_detail_template->cart_products=$this->cart_service->get_cart_content();
        $cart_detail_template->price_total_products=$this->cart_service->get_cart_price_with_tax();

        //zmeneno $cart_detail_template->price_total=$this->cart_service->get_total_order_price_with_tax();

        $this->request->response=$cart_detail_template->render();

    }

}
?>
