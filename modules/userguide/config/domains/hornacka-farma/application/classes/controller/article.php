<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Generovani novinek.
 * pripojena sablony:
 * - hlavni clanky: article.tpl
 * - clanky na indexove strance: article_index.tpl
 * - clanky v postranim panelu: article_sidebar.tpl
 *
 * pozadovane servisni tridy: Article_Service
 * pozadovane modelove tridy: Model_Article, Model_Article_Data
 * administrace: ano
 */
class Controller_Article extends Controller
{
    // nastaveni modulu
    protected $module_key="article";

    protected $no_of_articles_indexpage=2;             // pocet clanku na indexpage
    protected $no_of_articles_sidebar=3;               // pocet clanku na sidebaru
    protected $no_of_articles_main=5;                  // pocet clanku v hlavnim seznamu

    protected $article_detail_photo_suffix="t1";
    protected $articles_main_photo_suffix="t2";        // suffix fotky na hlavnim seznamu clanku
    protected $articles_indexpage_photo_suffix="t2";   // suffix fotky na seznamu clanku na indexu
    protected $articles_sidebar_photo_suffix="t2";     // suffix fotky na seznamu clanku v sidebaru

    protected $unrelated_page=false;                    // novinky nejsou soucasti hlavni navigace (title, keywords, description a uvodni text se budou brat z nezarazenych stranek)

    protected $with_gallery=false;                       // priznak zda bude v detailu pripojena galerie
    protected $gallery_type="carousel";
    protected $gallery_template="gallery";
    protected $gallery_thumb_suffix="t2";
    protected $gallery_detail_suffix="t1";

    // zakladni promenne modulu - nezasahovat
    protected $template;
    protected $article_service;
    protected $module_nazev_seo="clanky";

	private $route;

	// definice jazykovych url (id jazyku dle DB)
    protected $url=array(
                    "novinky"=>array("nazev"=>"Novinky","lang_id"=>1),
                    "news"=>array("nazev"=>"News","lang_id"=>2)
                    );

    protected $languages=array(
                    1=>array("nazev"=>"Aktuality","nazev_seo"=>"novinky","back_link"=>"Zpět na seznam aktualit"),
                    2=>array("nazev"=>"Articles","nazev_seo"=>"articles","back_link"=>"Back to list")
                    );

    public function before()
    {
            parent::before();

			$this->article_service = Service_Article::instance();
			$this->route = Service_Route::instance();
    }

    /**
     * Hlavni seznam clanku.
     * @param string $article_category
     */
    public function action_index($nazev_seo, $page=1)
    {
        $template = $this->template= new View("article_list");

		$service_page = Service_Hana_Page::instance();
		$article_service = $this->article_service;

		$article_service->photo_thumb_suffix = 't3';

		$page_data = $this->unrelated_page
						? $service_page->get_unrelated_page_by_nazev_seo($nazev_seo, true)
						: $service_page->get_page_by_nazev_seo($nazev_seo, true);

        // TODO pripadne vyplneni nazvu a popisu, ktery se zobrazi nad vypisem clanku
        //$route=Service_Route::instance();
        // $route->add_breadcrumbs_to_the_end(array("nazev"=>$this->url[$nazev_seo]["nazev"],"nazev_seo"=>$nazev_seo));
		//$route->set_selected_language_id($this->url[$nazev_seo]["lang_id"]);
		$route = $this->route
				->set_selected_language_id($this->url[$nazev_seo]["lang_id"]);

        if(is_object($page_data)) {
			$route->set_title($page_data->title)
					->set_description($page_data->description)
					->set_keywords($page_data->keywords);
        } else {
			$route->set_title($page_data["title"])
					->set_description($page_data["description"])
					->set_keywords($page_data["keywords"]);
        }

        $pagination = Pagination::factory(array(
              'current_page'   => array('source' => $nazev_seo, 'value'=>$page),
              'total_items'    => $this->article_service->get_model()->get_all_allowed_items_count(),
              'items_per_page' => $this->no_of_articles_main,
              'view'              => 'pagination/basic',
              'auto_hide'      => TRUE
        ));

		$template->module_article_mode = "list";
        $template->title = $this->url[$nazev_seo]["nazev"];
        $template->articles = $article_service->get_article_list($this->no_of_articles_main, $pagination->offset);
		$template->pagination = $pagination->render();

		$this->request->response = $this->template->render();
    }



    /**
     * Hlavni detail clanku.
     * @param string $nazev_seo
     */
    public function action_detail($nazev_seo="")
    {
		$template = $this->template = new View("article_detail");
		$route = $this->route;
		$article_service = $this->article_service;
		$template->module_article_mode="detail";

		$article_service->photo_detail_suffix = $this->article_detail_photo_suffix;
		$article_data = $article_service->get_article_detail($nazev_seo);

        if(!$article_data["id"]) {
			Request::instance()->status = 404;
			$this->request->response = View::factory('errors/404');
			return;
        }

        //$route->add_breadcrumbs_to_the_end(array("nazev"=>$this->languages[$lang_id]["nazev"],"nazev_seo"=>$this->languages[$lang_id]["nazev_seo"]));
		$route->add_breadcrumbs_to_the_end(array(
			"nazev" => $article_data["nadpis"],
			"nazev_seo" => $article_data["nazev_seo"]
		));

		$lang_id = $route->get_selected_language_id();

        $this->template->back_link_url = $this->languages[$lang_id]["nazev_seo"];
        $this->template->back_link_text = $this->languages[$lang_id]["back_link"];
        $this->template->photo = $article_data["photo"];
        $this->template->title = $article_data["nadpis"];
        $this->template->description = $article_data["popis"];

		if($this->with_gallery) {
			$this->template->gallery = Request::factory("internal/gallery/index/".$article_data["id"]."/".$this->module_key."/item/".$this->gallery_type."/".$this->gallery_template."/".$this->gallery_thumb_suffix."/".$this->gallery_detail_suffix)
					->execute()
					->response;;
        }

		$route->set_title($article_data["title"])
				->set_description($article_data["description"])
				->set_keywords($article_data["keywords"]);

        $this->request->response = $this->template->render();
	}

    /**
     * Seznam clanku v postranim panelu.
     */
    public function action_sidebar()
    {
        $sidebar_template=new View("article_sidebar");

        $this->article_service->photo_thumb_suffix=$this->articles_sidebar_photo_suffix;
        $sidebar_template->articles = $this->article_service->get_article_list($this->no_of_articles_sidebar, 0);

        $this->request->response = $sidebar_template->render();
    }

    /**
     * Seznam clanku na indexove strance.
     */
    public function action_indexpage()
    {
        $indexpage_template=new View("article_indexpage");

        $this->article_service->photo_thumb_suffix = $this->articles_indexpage_photo_suffix;
        $indexpage_template->articles = $this->article_service->get_article_list($this->no_of_articles_indexpage, 0);

        $this->request->response = $indexpage_template->render();
    }

}
?>
