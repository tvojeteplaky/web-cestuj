<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Generovani stranek s vypisem a detailem produktu + stranky s nakupnim kosikem.
 */
class Controller_User extends Controller
{
    /**
     *
     * @var Service_User
     */
    private $user_service;

    public function before()
    {
        parent::before();
        // vsechny metody jsou platne pouze v rozsahu internich requestu, zadne externi volani
        if(!$this->internal) throw new Kohana_Exception("externí request není povolen");
        $this->user_service = Service_User::instance();
    }

    /**
     * Akce pro obsluhu uzivatelskeho boxiku (widgetu) vlozeneho do stranek.
     */
    public function action_widget()
    {

        $user_widget=new View("user_widget");
        //$this->action_login();
        //echo $_POST["action"];
        if($this->user_service->logged_in())
        {
            $user_widget->logged_in=true;
            $user=$this->user_service->get_user();
            $user_widget->user=$user;
        }
        else
        {
            $user_widget->logged_in=false;
        }
        $this->request->response=$user_widget->render();
    }

    /**
     * Akce pro obsluhu prihlaseni uzivatele.
     * @param string $on_success_redirect_to volitelne url na kterou se ma beh programu presmerovat po uspesnem prihlaseni
     * @param boolean $included priznak zda jde o samostatny formular, nebo je vlozeny do sirsi struktury formularu
     */
    public function action_login($on_success_redirect_to=false, $included=false)
    {
        if($this->user_service->logged_in())
        {
            // obecna sablona textove stranky
			/*
            $page=new View("page");
            $page->page=array("popis"=>"<br /><h1>Uživatel byl přihlášen</h1>");
            $this->request->response=$page->render();
			 *
			 */

			/**
			 * Just redirect already logged in user on homepage
			 */
			Request::instance()->redirect(url::base());
        }
        else
        {
            $user_login_template=new View("user_login");

            // validace a zpracovani dat
            if(isset($_POST["action"]) && $_POST["action"]=="user-login")
            {
                if($this->user_service->login(isset($_POST["username"])?$_POST["username"]:null, isset($_POST["password"])?$_POST["password"]:null))
                {
                    // pokud byl ulozen docasny uzivatel, smazu ho
                    $this->user_service->delete_temporarily_stored_user();
                    //header("Refresh: 0;");
					Request::instance()->redirect(url::base()."?login=ok");
                   /* if($on_success_redirect_to) Request::instance()->redirect(url::base().$on_success_redirect_to."?login=ok");
                    else
                    Request::instance()->redirect(url::base()."prihlaseni-uzivatele?login=ok"); */
                }
                else
                {
                    $user_login_template->registration_error = "Přihlášení uživatele se nezdařilo";
                    //$user_login_template->registration_errors=$this->user_service->errors;
                }
            }

            // pokud neni kontroler includovany, vygeneruje zbytek cesty
            //if(!$included) Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Přihlášení uživatele","nazev_seo"=>"prihlaseni-uzivatele"));

            $user_login_template->included=$included;
            $this->request->response=$user_login_template->render();
        }
    }

    public function action_logout()
    {
        // obecna sablona textove stranky
        $this->user_service->logout();

        Request::instance()->redirect(url::base()."?usrlogout=true");

//        $page=new View("user_logout");
//        $page->page=array("popis"=>"<p>Uživatel byl odhlášen</p>");
//        $this->request->response=$page->render();
    }

    /**
     * Akce pro obsluhu registrace a zaroven prihlaseni uzivatele
     * @param string $on_success_redirect_to volitelne url na kterou se ma beh programu presmerovat po uspesnem prihlaseni
     * @param boolean $included priznak zda jde o samostatny formular, nebo je vlozeny do sirsi struktury formularu

     */
    public function action_registration($on_success_redirect_to=false, $included=false)
    {
        // pokud probehla registrace defaultne presmeruju na uvodni stranku
        if(isset($_GET["registration"]) && $_GET["registration"]=="ok") Request::instance()->redirect(url::base()."?usrreg=true");

        $user_registration_template=new View("user_registration");

        $product_service=Service_Hana_Product::instance();
        $user_registration_template->product_places=$product_service->get_index_product_places();

        if(isset($_POST["place_id"])) $user_registration_template->place_id = $_POST["place_id"];

		/*
		if(isset($_POST["user-registration"]) && !isset($_POST["user-registration"]["place_id"])) {
               $user_registration_template->registration_error_change = "Registrace se nezdařila, zadejte distribuční místo";
                $user_registration_template->data=$_POST["user-registration"];
                $user_registration_template->included=$included;
                $this->request->response=$user_registration_template->render();
                return;

        }
		 *
		 */

        $editmode=(isset($_GET["mode"]) && $_GET["mode"]=="edit")?true:false;
        $user_registration_template->editmode=$editmode;
        if($editmode)
        {
            Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Účet uživatele","nazev_seo"=>"ucet-uzivatele"));
            Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Změna registračních údajů","nazev_seo"=>"nastaveni-uctu"));
        }
        else
        {
            Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Registrace nového uživatele","nazev_seo"=>"registrace-uzivatele"));
        }


        if($this->user_service->logged_in()) // pokud je uzivatel prihlasen, predvyplnime jeho udaje
        {
            $user_registration_template->data=$this->user_service->get_user();
        }

        // validace a zpracovani dat
        if(isset($_POST["action"]) && $_POST["action"]=="user-registration")
        {
            $old_shopper=$this->user_service->get_user();
           if(isset($old_shopper->id)) $old_shoppers_open_order=Service_Order::instance()->check_open_order_exists($old_shopper->id);
           // echo $old_shopper->distributionplace_id.$_POST["user-registration"]["distributionplace_id"].$old_shoppers_open_order->id;
            if($this->user_service->logged_in())
            {
                //níže zaremovaná část slouží pro obsluhu změny distribučního místa
               // if($old_shopper->place_id!=$_POST["user-registration"]["place_id"] && isset($old_shoppers_open_order->id)){

              //  }else{

                $result=$this->user_service->change_registration_data($_POST["user-registration"]);

               // }

            }
            else
            {
                $result = $this->user_service->register_user($_POST["user-registration"]);
            }
             if(isset($result)){
            if($result)
            {
                // registrace probehla uspesne, prihlasim uzivatele
                Shauth::instance()->force_login($_POST["user-registration"]["username"]);
                // pokud byl ulozen docasny uzivatel, smazu ho
                $this->user_service->delete_temporarily_stored_user();

                // presmeruju
                if(Shauth::instance()->logged_in())
                {

                  if($on_success_redirect_to) Request::instance()->redirect(url::base().$on_success_redirect_to."?registration=ok");
                }
                else
                {
                    // TODO chyba registrace probehla ale uzivatele nebylo mozne prihlasit
                }
            }
            else
            {
                $user_registration_template->registration_error_change = "Registrace se nezdařila, zkontrolujte prosím zadané údaje";
                $user_registration_template->registration_errors=$this->user_service->errors;
                $user_registration_template->data=$_POST["user-registration"];

            }
            }else{
              $user_registration_template->registration_error_change = "Změnou distribučního města smažete aktuální otevřenou objednávku, chcete pokračovat? <a href='#' id='change-dist-place' title='Uložit údaje' style='text-decoration:underline;margin-left:150px;'>ANO</a> <a href='/nastaveni-uctu?mode=edit' title='Storno' style='text-decoration:underline;margin-left:50px;'>NE</a>";
              $user_registration_template->registration_errors=$this->user_service->errors;
              $user_registration_template->data=$_POST["user-registration"];

            }
        }

        //if(isset($_POST["user-registration"])) $user_registration_template->data=$_POST["user-registration"];
        $user_registration_template->included=$included;

        $this->request->response=$user_registration_template->render();
    }


    /**
     * Akce pro obsluhu zmeny udaju o pobocce
     * @param string $on_success_redirect_to volitelne url na kterou se ma beh programu presmerovat po uspesnem prihlaseni

     */

    public function action_user_registration_dist_place()
    {
    $shopper=$this->user_service->get_user();
    if(Shauth::instance()->logged_in()){

          $result=$this->user_service->change_registration_data($_GET["user-registration"]);
          $order=orm::factory("order")->where("order_shopper_id","=",$shopper->id)->where("open","=",1)->find();
          $order_items=orm::factory("order_item")->where("order_id","=",$order->id)->find();
          $order->delete();
          $order_items->delete();

    }else{return false;}


    }

    public function action_branchedit()
    {
        $on_success_redirect_to="ucet-uzivatele";

        if(!$this->user_service->logged_in())
        {
            Request::instance()->redirect(url::base());
        }
        else
        {
            $user=$this->user_service->get_user();
        }

        $user_registration_template=new View("user_account_branch_edit");

        $editmode=(isset($_GET["brid"]) && $_GET["brid"])?true:((isset($_POST["branchedit"]["branch_id"]) && $_POST["branchedit"]["branch_id"])?true:false);

        //TODO kontrola zda pobocka, kterou uzivatel hodla editovat mu skutecne patri



        $user_registration_template->editmode=$editmode;
        if($editmode)
        {
            Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Účet uživatele","nazev_seo"=>"ucet-uzivatele"));
            Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Změna údajů o pobočce","nazev_seo"=>"pobocka-uzivatele"));
            $user_registration_template->data=$this->user_service->get_user_branch($_GET["brid"], $user);
        }
        else
        {
            Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Účet uživatele","nazev_seo"=>"ucet-uzivatele"));
            Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Vložení nové pobočky","nazev_seo"=>"pobocka-uzivatele"));
        }


        // validace a zpracovani dat
        if(isset($_POST["action"]) && $_POST["action"]=="branchedit")
        {

            $result=$this->user_service->change_branch_data($_POST["branchedit"], $user->id);

            if($result)
            {
              if($on_success_redirect_to) Request::instance()->redirect(url::base().$on_success_redirect_to."?branchedit=ok");
            }
            else
            {
                $user_registration_template->registration_error = "Vložení/editace pobočky se nezdařílo, zkontrolujte prosím zadané údaje";
                $user_registration_template->registration_errors=$this->user_service->errors;
                $user_registration_template->data=$_POST["branchedit"];
            }
        }

        //if(isset($_POST["user-registration"])) $user_registration_template->data=$_POST["user-registration"];

        $this->request->response=$user_registration_template->render();
    }


    /**
     * Akce pro obsluhu hlavni stranky (rozcestniku), ktera se zobrazi po prihlaseni.
     */
    public function action_user_main_page()
    {
        $on_success_redirect_to="ucet-uzivatele";
        Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Účet uživatele","nazev_seo"=>"ucet-uzivatele"));

        $message="";
        if(isset($_GET["registration"]) && $_GET["registration"]=="ok") $message="regok";
        if(isset($_GET["complaint"]) && $_GET["complaint"]=="ok") $message="compok";
        if(isset($_GET["branchedit"]) && $_GET["branchedit"]=="ok") $message="branchok";
        if(isset($_GET["branchdeleted"]) && $_GET["branchdeleted"]=="ok") $message="branchdeleted";

        $shopper=$this->user_service->get_user();
        if(!isset($shopper->id))  Request::instance()->redirect(url::base()."index");
        // byl dan pozadavek na smazani pobocky
        if(isset($_GET["delbranch"]) && $_GET["delbranch"])
        {
            if($this->user_service->delete_user_branch($_GET["delbranch"], $shopper))
            {
                Request::instance()->redirect(url::base().$on_success_redirect_to."?branchdeleted=ok");
            }
        }

        $user_account_template=new View("user_account_index");
        $user_account_template->message=$message;

        $orders=orm::factory("order")->where("order_shopper_id","=",$shopper->id)->find();
        if(isset($orders->id))$user_account_template->orders=true;

        $user_account_template->shopper = $shopper;
        $user_account_template->place = $shopper->place;

        $user_account_template->branches=$this->user_service->get_all_user_branches($shopper);

        $this->request->response=$user_account_template->render();

    }


    public function action_user_account_setup()
    {
        $user_account_setup_template=new View("user_account_setup");

        $user_account_setup_template->user_registration = Request::factory("internal/user/registration/ucet-uzivatele")->execute()->response;

        $this->request->response=$user_account_setup_template->render();
    }

     public function action_forgotten_password()
    {
        $user_forgotten_password_template=new View("user_forgotten_password");
        Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Zapomenuté heslo","nazev_seo"=>"zapomenute-heslo"));
        //$user_forgotten_password_template->user_registration = Request::factory("internal/user/registration/ucet-uzivatele")->execute()->response;
        if(isset($_POST["action"]) && $_POST["action"]="forgotten-password"){
        $user_forgotten_password_template->message="Pokud jste zadali správný registrační mail, příjde Vám nové heslo";
       if(isset($_POST["registration_mail"])) $this->user_service->send_forgotten_mail($_POST["registration_mail"]);

        }


        $this->request->response=$user_forgotten_password_template->render();
    }

    public function  action_complaint()
    {
        $shopper=$this->user_service->get_user();
        if(!isset($shopper->id))  Request::instance()->redirect(url::base()."index");

        $user_complaint_template=new View("user_complaint_form");

        Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Účet uživatele","nazev_seo"=>"ucet-uzivatele"));
        Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>"Reklamační formulář","nazev_seo"=>"reklamace"));

        $user_complaint_template->data=$this->user_service->get_user();
        $user_complaint_template->orders=orm::factory("order")->where("order_shopper_id","=",$shopper->id)->find_all();

        // validace a zpracovani dat
        if(isset($_POST["action"]) && $_POST["action"]=="user-complaint")
        {
                $email_service = new Service_Email();


                $result=$email_service->sendComplaintMail($_POST["user-complaint"]);


            if($result)
            {

                if(Shauth::instance()->logged_in())
                {

                 Request::instance()->redirect(url::base()."ucet-uzivatele/?complaint=ok");
                }

            }
            else
            {
                $user_complaint_template->registration_error_change = "Odeslání reklamačního formuláře se nezdařilo - vyplňte prosím všechny povinné položky";
                $user_complaint_template->registration_errors=$this->user_service->errors;
                $user_complaint_template->data=$_POST["user-complaint"];

            }

         }
        $this->request->response=$user_complaint_template->render();
    }

    public function action_get_distribution_places(){
       $registration_selectboxes=new View("registration_selectboxes");
      $places=orm::factory("distributionplace")->where("place_id","=",$_GET['place_id'])->find_all();
      $x=0;
    echo '[{"optionValue":"", "optionDisplay":"Vyberte distribuční místo"}';
      foreach($places as $place){

        echo ',{"optionValue":"'.$place->id.'", "optionDisplay":"'.$place->ulice_cp.'"}';
      }
     echo  ']';

       $this->request->response=$registration_selectboxes->render();

       //echo '[{"optionValue":"", "optionDisplay":"Select Model"},{"optionValue":"TBD", "optionDisplay":"TBD"},{"optionValue":"SCCM", "optionDisplay":"SCCM-standalone"},{"optionValue":"Manual", "optionDisplay":"Manual-standalone"},{"optionValue":"SCCM-VMWare", "optionDisplay":"SCCM-VMWare"},{"optionValue":"Manual-VMWare", "optionDisplay":"Manual-VMWare"},{"optionValue":"SCCM Hybrid", "optionDisplay":"SCCM Hybrid"},{"optionValue":"SCCM Hybrid VMWare", "optionDisplay":"SCCM Hybrid VMWare"},{"optionValue":"SCCM Offline", "optionDisplay":"SCCM Offline"},{"optionValue":"SCCM Offline - VMWare", "optionDisplay":"SCCM Offline - VMWare"}]';
    }

}
?>
