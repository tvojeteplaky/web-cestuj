<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Zakladni kontroler na generovani hlavni navigace.
 */
class Controller_Navigation extends Controller
{
    public function before() {
        parent::before();
        // vsechny metody jsou platne pouze v rozsahu internich requestu, zadne externi volani
        if(!$this->internal) throw new Kohana_Exception("externí request není povolen");
    }

    public function action_index()
    {
        $nav=new View("nav");
        $selected_controller=$this->request->param("id1");
        $selected_page=$this->request->param("id2");

        $links=Service_Page::instance()->get_navigation_structure();
        $sel_links=Service_Page::instance()->get_selected_navigation_links($selected_controller, $selected_page);

        $nav->links=$links;
        $nav->sel_links=$sel_links;

        //print_r($links);
        $this->request->response=$nav->render();

    }

    public function action_breadcrumbs(){
        $breadcrumbs=new View("breadcrumbs");
        $data=Service_Route::instance()->get_breadcrumbs();
        $breadcrumbs->data = $data;

        $this->request->response=$breadcrumbs->render();
    }

}
