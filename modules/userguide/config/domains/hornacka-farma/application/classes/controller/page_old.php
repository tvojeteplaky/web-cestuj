<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Generovani statickych stranek.
 */
class Controller_Page extends Controller
{
    /**
     * Metoda generujici obsah uvodni stranky a vsech statickych stranek vkladanych do hlavniho obsahu.
     * @param <type> $nazev_seo
     */
    public function action_index($nazev_seo="index")
    {
        if($nazev_seo=="index")
        {
            $product_service=Service_Hana_Product::instance();

            // sestavim specificky layout uvodky
            $homepage_template = new View("homepage");

            // hlavni akcni produkt (posledni pridane akcni zbozi)
            $action_product=$product_service->get_special_products("akcni-zbozi", "id", "desc", 1, "t2");
            //die(print_r(current($action_product["products"])));
            if(!empty($action_product["products"]))$homepage_template->action_product=current($action_product["products"]);

            // vylistovane kategorie
            $homepage_template->product_categories=$product_service->get_index_product_categories();

            // akcni produkty v prave sekci
            $right_action_products=$product_service->get_special_products("akcni-zbozi", "poradi", "desc", 3, "t3");
            if(!empty($right_action_products) && isset($right_action_products["products"]))$homepage_template->right_action_producs=$right_action_products["products"];

            // staticky text

            // novinky na uvodce
            $homepage_template->article_homepage=Request::factory("internal/article/indexpage/")->execute()->response;

            $this->request->response = $homepage_template;
        }
        else
        {
            $template=new View("page");
            // sestavim layout staticke stranky
            $page_data = Service_Hana_Page::instance()->get_page_by_nazev_seo($nazev_seo);

            if($page_data===false)
            {
                Request::instance()->status = 404;
                $this->request->response = View::factory('errors/404');
                return;
            }

            $template->page=$page_data;

            $this->request->response = $template;
        }
    }

    /**
     * Metoda generujici obecny staticky obsah.
     * @param string $kod
     */
    public function action_static($kod)
    {
        if(!$this->internal) throw new Kohana_Exception("externí request není povolen");
        $this->request->response = Service_Hana_Page::instance()->get_static_content_by_code($kod);
    }


    public function action_unrelated($nazev_seo)
    {
        $template=new View("page");
        // sestavim layout staticke stranky
        $page_data = Service_Hana_Page::instance()->get_unrelated_page_by_nazev_seo($nazev_seo);
        Service_Route::instance()->set_selected_language_id($page_data->language_id);

        if($page_data===false)
        {
            Request::instance()->status = 404;
            $this->request->response = View::factory('errors/404');
            return;
        }
        $template->page=$page_data;
        Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>$page_data->nazev,"nazev_seo"=>$nazev_seo));
        $this->request->response = $template->render();
    }

    public function action_sitemap()
    {
        $template=new View("page");
        $sitemap=new View("sitemap");
        Service_Route::instance()->set_selected_language_id(Service_Route::instance()->get_last_language_id());
        $links=Service_Page::instance()->get_navigation_structure();
        Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>__("Mapa stránek"),"nazev_seo"=>"mapa-stranek"));
        $sitemap->links=$links;
        $page=array("nadpis"=>__("Mapa stránek"),"popis"=>$sitemap->render());
        $template->page=$page;
        $this->request->response = $template->render();
    }

}
