<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Zakladni kontroler na frontendu - sestavi stranku na zaklade obdrzeneho pozadavku. Provede volani na sub-kontrolery.
 */
class Controller_Site extends Controller_Template
{
    public $template = 'base_template';
    private $default_main_content_controller = "page"; //pokud se nepodari zjistit prislusny kontroller k pozadavku
    private $default_main_content_action = "index";

    public function action_index()
    {
//    $route = Route::get('admin')->uri(array('controller' => "admin"));
//	die($route);

    $this->template->url_base=url::base();
    $this->template->media_path=url::base()."media/";

    $this->template->search_value=isset($_GET["search_string"])?$_GET["search_string"]:"vyhledej";

    $route = Service_Route::instance()->get_page_route_by_nazev_seo();

    // vyvolam prislusny kontroler a jeho akci dle typu stranek
    $main_controller = ($route->page_type->kod)?$route->page_type->kod:$this->default_main_content_controller;
    $main_controller_action = ($route->action)?$route->action:$this->default_main_content_action;
    // prvni parametr akce kontroleru beru primarne z DB, jinak je to defaultne prvni segment url adresy (typicky nazev_seo)
    $main_controller_param_id1 = ($route->param_id1)?$route->param_id1:Service_Route::instance()->get_actual_nazev_seo();

    // volitelne druhy parametr akce kontroleru = 2. segment url adresy

    $main_controller_param_id2 = (Service_Route::instance()->get_parameter()?"/".Service_Route::instance()->get_parameter():"");


    // spusteni hlavniho kontroleru - melo by byt jako prvni, protoze akce zde mohou ovlivnit zobrazeni ostatnich prvku na strance
    $main_content=Request::factory("internal/".$main_controller."/".$main_controller_action."/".$main_controller_param_id1.$main_controller_param_id2)->execute()->response;
       //speci�ln� url pro generov�n� obsahu druh�ho selectboxu p�i registraci - poidle m�sta to do druh�ho selectboxu p�id� ulice
    if($main_controller_param_id1=="get-distrubition-places") {$this->auto_render=false;return false;}
    // widget kosiku
    $cart_widget = Request::factory("internal/cart/widget")->execute()->response;

    // widget uzivatele
    $user_widget = Request::factory("internal/user/widget")->execute()->response;

    // ziskani obecnych seo prvku: title, description a keywords
    $page_title = Service_Route::instance()->get_title();
    $page_description = Service_Route::instance()->get_description();
    $page_keywords = Service_Route::instance()->get_keywords();

	if(request::$is_ajax) {
        // ajaxovy pozadavek vygeneruju vysledny JSON objekt, ktery pote zpracuje obsluzny klientsky JavaScript
        $this->auto_render=false;

		echo(json_encode(
				array(
					"main_content"=>$main_content,
					"cart_widget"=>$cart_widget,
					"user_widget"=>$user_widget,
					"page_title"=>$page_title,
					"page_description"=>$page_description,
					"page_keywords"=>$page_keywords,
					)
		));

		return;
    } else {
        // neajaxovy pozadavek - standardni generovani do sablony (base_template)

        // vyplneni title, description a keywords
        $this->template->title = $page_title;
        $this->template->description = $page_description;
        $this->template->keywords = $page_keywords;
        $this->template->main_content = $main_content;
        $this->template->cart_widget = $cart_widget;
        $this->template->user_widget = $user_widget;

        // Vygenerovani ostatnich casti stranek, ktere neni nutne generovat pri ajaxovem pozadavku:

        // navigacni kontrolery by mely byt volany na poslednim miste, protoze stranka se behem pozadavku muze interne presmerovat
        // hlavni navigace
        $navigation = Request::factory("internal/navigation/index/".$main_controller."/".Service_Route::instance()->get_actual_nazev_seo())->execute()->response;
        $this->template->nav = $navigation;

        // google analytics script
        $this->template->ga_script = orm::factory("owner")->get_google_analytics_script();

        // vyvolam kontroler produktove subnavigace a drobitkoveho menu, vzdy v pripade ze nejde o index
        if(Service_Route::instance()->get_actual_nazev_seo()!="index")
        {
            if($main_controller=="product"){
                $subnavigation = Request::factory("internal/product/category_navigation/".Service_Route::instance()->get_actual_nazev_seo())->execute()->response;
                $this->template->subnav = $subnavigation;
            }
            $breadcrumb = Request::factory("internal/navigation/breadcrumbs")->execute()->response;
            $this->template->breadcrumb=$breadcrumb;
            $this->template->is_indexpage=false;
        }


        // vyplnim pripadny dalsi zakladni (staticky) spolecny obsah pro vsechny stranky v base_template
		}

    }

    public function after()
    {
        parent::after();
        // Showing the profiler if using debug mode
        // $this->request->response .= View::factory('profiler/stats');
    }

}
?>
