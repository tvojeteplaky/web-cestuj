<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Kontroler pro obsluhu externich pozadavku - importu, exportu
 * pripojena sablony: -
 *
 * pozadovane servisni tridy: ...
 * pozadovane modelove tridy: ...
 * administrace: ne
 */
class Controller_External extends Controller
{
    /**
     * /external/updateProductQuantity?product_code=123&quantity=10
     * @return boolean 
     */
    public function action_updateProductQuantity()
    {
        // zjednodusena implementace
        $product_code = isset($_GET["product_code"])?$_GET["product_code"]:null; 
        $quantity = isset($_GET["quantity"])?$_GET["quantity"]:null;
        
        if($product_code && $quantity)
        {
            $product = orm::factory("product")->where("code","=",$product_code)->find();
            if(!$product->id) return false;
            $product->pocet_na_sklade=$quantity;
            if($product->save())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
    /**
     * /external/updateProductPrice?product_code=123&price=1400
     * @return boolean 
     */
    public function action_updateProductPrice()
    {
        $product_code = isset($_GET["product_code"])?$_GET["product_code"]:null; 
        $price = isset($_GET["price"])?$_GET["price"]:null; 
        $price_code = isset($_GET["price_code"])?$_GET["price_code"]:"D0"; 
        
        if($product_code && $price)
        {
            $product_price = orm::factory("price_categories_product")
                    ->join("price_categories")->on("price_categories_products.price_category_id","=","price_categories.id")
                    ->join("products")->on("price_categories_products.product_id","=","products.id")
                    ->where("products.code","=",$product_code)->where("kod","=",$price_code)->find();
            if(!$product_price->id) return false;
            $product_price->hodnota=$price;

            if($product_price->save())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
                    
    }
    
    public function insertProduct($product_code, $uvodni_popis, $popis, $rozmer, $profil, $sirka, $cena, $price_code="D0")
    {
        
    }
    
}
?>
