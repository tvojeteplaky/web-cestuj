<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Generovani stranek s vypisem a detailem produktu + stranky s nakupnim kosikem.
 */
class Controller_Product extends Controller
{
    private $items_per_page=3;

    protected $module_name="product";

    protected $with_gallery=true;                       // priznak zda bude v detailu pripojena galerie
    protected $gallery_type="carousel";
    protected $gallery_template="gallery";
    protected $gallery_thumb_suffix="t2";
    protected $gallery_detail_suffix="t1";

    /**
     *
     * @var Service_Product
     */
    private $product_service;
    /**
     *
     * @var Service_ShoppingCart
     */
    private $cart_service;

    public function before() {
        parent::before();
        // vsechny metody jsou platne pouze v rozsahu internich requestu, zadne externi volani
        if(!$this->internal) throw new Kohana_Exception("externí request není povolen");

        $this->product_service = Service_Product::instance();
        $this->cart_service = Service_ShoppingCart::instance();
        $this->user_service = Service_User::instance();
    }

    /**
     * Uvodka produktu.
     */
    public function action_index($nazev_seo="",$page=1)
    {
        if(!isset($_POST["search_string"]) && !isset($_GET["search_string"]))
        {
            //$nazev_seo=$this->product_service->get_first_product_category_seo();
			$page = Service_Page::instance()->get_page_by_nazev_seo("produkty"); // ? v názvu seo je seo podkategorie, nevím odkud
			
			$pc_template_outer=new View("product_categories");
			$pc_template_inner=new View("product_categories_items");

			$pc_template_inner->categories = $this->product_service->get_product_categories(false, false);
			$pc_template_outer->page = $page;
			$pc_template_outer->product_categories_items = $pc_template_inner->render();

            //Service_Route::instance()->set_nazev_seo($nazev_seo);
            //$this->action_category($nazev_seo);
            //$this->request->response="index";
			$this->request->response = $pc_template_outer->render();
        }
        else
        {
            Service_Route::instance()->set_nazev_seo("vysledky-vyhledavani");
            $this->action_category("vysledky-vyhledavani",$page);
        }
    }


    /**
     * Metoda generujici produktovou navigaci.
     * @param string $selected_category_seo
     */
    public function action_category_navigation($selected_category_seo=false)
    {
        $subnav=new View("subnav");
//        $selected_controller=$this->request->param("id1");
//        $selected_page=$this->request->param("id2");

        $links=$this->product_service->get_product_categories($selected_category_seo);
//        $sel_links=Service_Page::instance()->get_selected_navigation_links($selected_controller, $selected_page);
        //print_r($links);
        $subnav->links=$links;
        //$times=$this->product_service->get_product_times();
        //$subnav->times=$times;
        //$subnav->sel_links=$this->product_service->get_selected_categories_by_nazev_seo($selected_category_seo);

        $this->request->response = $subnav->render();
    }

    /**
     * Metoda generujici prehled produktu v kategorii.
     * @param <type> $nazev_seo
     */
    public function action_category($nazev_seo, $page=1)
    {



        if($nazev_seo=="vysledky-vyhledavani")
        {
            $search_mode=true;
            $nazev_seo=false;
        }
        else
        {
            $search_mode=false;
        }
        $pc_template_outer=new View("product_category");
        $pc_template_inner=new View("product_category_items");

            if($this->user_service->logged_in())
            {
                $pc_template_inner->logged_in=true;
            }else
            {
                $pc_template_inner->logged_in=false;
            }
        // provedeni vsech pripadnych akci po zaslani dat
        // pridani zbozi do kosiku
        if(isset($_POST["product_id"]) && isset($_POST["product_quantity"]) && $_POST["product_quantity"])
        {
            $add_to_cart_result=$this->cart_service->add_to_cart($_POST["product_id"], $_POST["product_quantity"]);


            $pc_template_outer->cart_message=$this->cart_service->messages;

            if($add_to_cart_result)
            {
                $pc_template_outer->cart_message_type="added";
            }
            else
            {
                $pc_template_outer->cart_message_type="error";
            }
        }


        // ziskani vsech parametru z filtru a z razeni
        $parameters=array();
        if(isset($_GET["products_order"])) $parameters["order_by"]=$_GET["products_order"];
        if(isset($_GET["sort"])) $parameters["order_direction"]=$_GET["sort"];
        if(isset($_GET["products_filter_action"])) $parameters["products_filter_action"]=$_GET["products_filter_action"];
        if(isset($_GET["price_selected_max"])) $parameters["price_selected_max"]=$_GET["price_selected_max"];
        if(isset($_GET["price_selected_min"])) $parameters["price_selected_min"]=$_GET["price_selected_min"];
        if(isset($_GET["products_filter_manufacturer"]) && $_GET["products_filter_manufacturer"] && !isset($_GET["products_filter_manufacturers"])) $parameters["products_filter_manufacturer"]=$_GET["products_filter_manufacturer"];
        if(isset($_GET["products_filter_profile"]) && $_GET["products_filter_profile"]) $parameters["products_filter_profile"]=$_GET["products_filter_profile"];
        if(isset($_GET["products_filter_dimension"]) && $_GET["products_filter_dimension"]) $parameters["products_filter_dimension"]=$_GET["products_filter_dimension"];
        if(isset($_GET["products_filter_radius"]) && $_GET["products_filter_radius"]) $parameters["products_filter_radius"]=$_GET["products_filter_radius"];

        if(isset($_GET["products_filter_manufacturers"]) && ($_GET["products_filter_manufacturers"])) $parameters["products_filter_manufacturers"]=$_GET["products_filter_manufacturers"];
        if(isset($_GET["search_string"]) && ($_GET["search_string"])) $parameters["search_string"]=$_GET["search_string"];
        if(isset($_POST["search_string"]) && ($_POST["search_string"])) $parameters["search_string"]=$_POST["search_string"];

        // prepocteni cen z price slideru podle toho zda je dana sleva procentem
        $price_category_value=$this->product_service->get_price_category_percentual_value();
        if($price_category_value)
        {
            if(isset($parameters["price_selected_max"]))$parameters["price_selected_max"]=$parameters["price_selected_max"] * (100/(100-$price_category_value));
            if(isset($parameters["price_selected_min"]))$parameters["price_selected_min"]=$parameters["price_selected_min"] * (100/(100-$price_category_value));
        }

        // sestrojeni pole podminek a parametru pro vybrani orm objektu

        $category_parameters=$this->product_service->get_category_parameters_by_nazev_seo($nazev_seo, $parameters);
        // inicializace strankovani
        $pagination = Pagination::factory(array(
              'current_page'   => array('source' => $search_mode?"produkty":$nazev_seo, 'value'=>$page),
              'total_items'    => $category_parameters["total"],
              'items_per_page' => isset($_GET["products_show_pages"])?$_GET["products_show_pages"]:15,
              'view'              => 'pagination/basic',
              'auto_hide'      => TRUE
        ));

        $parameters["limit"]=$pagination->items_per_page;
        $parameters["offset"]=$pagination->offset;


        // vygenerovani vypisu dane kategorie
        $category=$this->product_service->get_category_detail_by_nazev_seo($nazev_seo,$parameters);

        //Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>isset($category["category_data"]["nazev"])?$category["category_data"]["nazev"]:"-","nazev_seo"=>$nazev_seo));


        $route=Service_Route::instance();
        if($search_mode)
        {   if(!isset($parameters["search_string"])) $parameters["search_string"]="";
            $route->set_title("výsledky vyhledávání");
            $category["category_data"]["nazev_full"]="Výsledky vyhledávání";
            $category["category_data"]["popis"]="Výsledky vyhledávání pro zadaný řetězec: ".$parameters["search_string"];
        }
        else
        {
            if(!empty($category["category_data"]))
            {
                $route->set_title($category["category_data"]["title"]);
                $route->set_description($category["category_data"]["description"]);
                $route->set_keywords($category["category_data"]["keywords"]);
            }
        }

        if(!empty($category["category_data"])) $pc_template_outer->category=$category["category_data"];

        // vygenerovani vypisu obsazenych produktu
        if(!$this->product_service->is_empty_category($nazev_seo))
        { // kategorie bez aplikace filtrovani obsahuje produkty

                // naplneni sablony produky
                if(isset($category["products"]))
                {
                    $pc_template_inner->products=$category["products"];
                }
                else
                {
                    // zadne produkty pro dane filtrovani
                    $pc_template_outer->empty_category_by_filter=true;
                }

                $pc_template_outer->current_query=htmlentities(url::query());

                // zjisteni hodnot a naplneni filtrovacich a tridicich prvku
                $pc_template_outer->manufacturers=$this->product_service->get_category_manufacturers($nazev_seo);
                $pc_template_outer->profiles=$this->product_service->get_category_profiles($nazev_seo);
                $pc_template_outer->dimensions=$this->product_service->get_category_dimensions($nazev_seo);
                $pc_template_outer->radiuses=$this->product_service->get_category_radiuses($nazev_seo);

                $prices=$this->product_service->get_category_price_range($nazev_seo);
                $pc_template_outer->prices=$prices;

                // zachovani kontextu - vsechny zvolene promenne z predchoziho pozadavku musim opet vyplnit
                //// vyhledavani
                if(isset($parameters["search_string"]))
                $pc_template_outer->search_string=$parameters["search_string"];

                //// polozka razeni
                if(isset($_GET["products_order"])) $pc_template_outer->products_order=$_GET["products_order"];

                //// smer razeni
                if(isset($_GET["sort"])) $pc_template_outer->sort=$_GET["sort"];
                $pc_template_outer->current_query_sort=(url::query()?htmlentities(url::query())."&amp;":"?");

                //// pocet na stranku
                if(isset($_GET["products_show_pages"])) $pc_template_outer->products_show_pages=$_GET["products_show_pages"];

                //// vyrobci, profily, rozmery, prumery - selectbox
                if(isset($_GET["products_filter_manufacturer"])) $pc_template_outer->products_filter_manufacturer=$_GET["products_filter_manufacturer"];
                if(isset($_GET["products_filter_profile"])) $pc_template_outer->products_filter_profile=$_GET["products_filter_profile"];
                if(isset($_GET["products_filter_dimension"])) $pc_template_outer->products_filter_dimension=$_GET["products_filter_dimension"];
                if(isset($_GET["products_filter_radius"])) $pc_template_outer->products_filter_radius=$_GET["products_filter_radius"];


                //// akce checkbox
                if(isset($_GET["products_filter_action"])) $pc_template_outer->products_filter_action=1;

                //// checkboxy vyrobcu
                if(isset($_GET["products_filter_manufacturers"]) && $_GET["products_filter_manufacturers"])
                {
                    $manchbids=array_combine($_GET["products_filter_manufacturers"], $_GET["products_filter_manufacturers"]);
                    if(count($manchbids))
                    {
                        $pc_template_outer->manchbids=$manchbids;
                        $pc_template_outer->show_manufacturer_list=1;
                    }
                }


                $pc_template_outer->price_selected_min=(isset($_GET["price_selected_min"])?$_GET["price_selected_min"]:$prices["price_min"]);
                $pc_template_outer->price_selected_max=(isset($_GET["price_selected_max"])?$_GET["price_selected_max"]:$prices["price_max"]);

                // zaslani dodatecnych dat a vlozeni produktu do vnejsi sablony
                $pc_template_outer->pagination = $pagination->render();
                if($search_mode)$nazev_seo="produkty";
                $pc_template_outer->current_page=url::base().$nazev_seo.(($page>1)?"/".$page:"");
                $pc_template_outer->product_category_items=$pc_template_inner;


        }
        else
        {
            // prazdna kategorie
            $pc_template_outer->empty_category=true;
        }
        //příkaz pro zakázaní obchodování
        $pc_template_outer->stop_trading = orm::factory("owner",1)->stop_trading;

        $this->request->response = $pc_template_outer->render();
    }

    /**
     * Metoda generujici deteil produktu.
     * @param <type> $nazev_seo
     */
    public function action_product($nazev_seo)
    {
        $pd_template = new View("product_detail");

		$pd_template->logged_in = $this->user_service->logged_in() ? TRUE : FALSE;
		$pd_template->product = $product = $this->product_service->get_product_detail_by_nazev_seo($nazev_seo);

        // pridani zbozi do kosiku
		if(isset($_POST["product_quantity"])) {

			if($product['jednotka'] == 'kg') {
				$product_quantity = (float)str_replace(array(' ', ','), array('', '.'), $_POST['product_quantity']);
			} elseif ($product['jednotka'] == 'ks') {
				$product_quantity = (int)$_POST['product_quantity'];
			}

            $add_to_cart_result = $this->cart_service->add_to_cart($product['id'], $product_quantity);

            $pd_template->cart_message = $this->cart_service->messages;
			$pd_template->cart_message_type = $add_to_cart_result ? 'added' : 'error';
        }



		//zobrazení měst, kde lze produkt nakoupit
        $pd_template->cities = DB::select('place_data.nazev')
				->from("place_data")
				->join("place_products")->on("place_data.place_id","=","place_products.place_id")
				->where("place_products.product_id","=",$product['id'])
				->execute()
				->as_array();

        $route = Service_Route::instance()
				->set_title($product["title"])
				->set_description($product["description"])
				->set_keywords($product["keywords"]);

        // kontaktni formular
        $popis=$_SERVER['SERVER_NAME']."/".$nazev_seo;

        $pd_template->contactform=Request::factory("internal/contactform/index/".urlencode(str_replace(".", "___", $popis))."/normal/standalone_contact_form")->execute()->response;


        if($this->with_gallery) {
            $pd_template->gallery=Request::factory("internal/gallery/index/".$product["id"]."/".$this->module_name."/item/".$this->gallery_type."/".$this->gallery_template."/".$this->gallery_thumb_suffix."/".$this->gallery_detail_suffix)->execute()->response;
        }

        // formular s doporucenim
        //$pd_template->recform=Request::factory("internal/contactform/index/".urlencode(str_replace(".", "___", $popis))."/normal/standalone_recommend_form")->execute()->response;

		$pd_template->tab=isset($_GET["tab"])?$_GET["tab"]:false;
        //příkaz pro zakázaní obchodování
        $pd_template->stop_trading = orm::factory("owner",1)->stop_trading;

        $this->request->response = $pd_template->render();
    }
}