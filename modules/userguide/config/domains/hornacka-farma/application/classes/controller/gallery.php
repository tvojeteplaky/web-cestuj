<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Univerzalni generovani fotogalerii
 * subkontroler
 * pripojena sablona: gallery.tpl
 * pozadovane servisni tridy: bez servisni tridy
 * pozadovane modelove tridy: pripojene modelove tridy _photos, _photo_data
 * administrace: ano
 * priklad volani: Request::factory("internal/gallery/index/".$product_category["id"]."/product/category")->execute()->response;
 */
class Controller_Gallery extends Controller
{
    private $type="normal"; // typ galerie: implementovano: normal, carousel
    private $template="gallery";
    private $thumb_suffix="t2";
    private $detail_suffix="t1";
    private $photos_resources_dir="media/photos/";

    /**
     * Detail galerie (univerzalne pouzitelny)
     * @param id $id id polozky modulu
     * @param string $module kod modulu s pripojenou fotogalerii
     */
    public function action_index($id, $module="page", $submodule="item", $type="normal", $template="gallery", $thumb_suffix="t2", $detail_suffix="t1")
    {   
        $this->type=$type;
        $this->template=$template;
        $this->thumb_suffix=$thumb_suffix;
        $this->detail_suffix=$detail_suffix;

        $gallery = new View($this->template);
        $gallery->type=$this->type;

        $submodule_suffix=($submodule!="item")?("_".$submodule):"";

        // vykonny kod je pro svoji jednoduchost vlozen primo do kontroleru
        $module_orm_photos_name=$module.$submodule_suffix."_photos";
        $module_orm=orm::factory($module.$submodule_suffix, $id);
        $photos=$module_orm->$module_orm_photos_name->where("zobrazit","=",1)->order_by("poradi","asc")->find_all();
        //vzhledem ke �patn� navr�en�m tabulk�m, zde mus� b�t pro manufacturers vyj�mka
        if($module=="manufacturer"){$module="product";$submodule="manufacturer";}
        $dirname=$this->photos_resources_dir.$module."/".$submodule."/gallery/images-".$module_orm->id."/";
        $photos_array=array();
        $x=1;
        foreach($photos as $photo)
        {
            if($photo->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$dirname.$photo->photo_src."-t2.jpg"))
            {
                $photos_array[$x]["photo"]=url::base().$dirname.$photo->photo_src."-".$this->thumb_suffix.".jpg";
                $photos_array[$x]["photo_detail"]=url::base().$dirname.$photo->photo_src."-".$this->detail_suffix.".jpg";
                $photos_array[$x]["nazev"]=$photo->nazev;
                $x++;
            }
        }

        $gallery->photos=$photos_array;

        $this->request->response = trim($gallery->render());
    }
    public function action_index2($id, $module="page", $submodule="item", $type="normal", $template="gallery2", $thumb_suffix="t2", $detail_suffix="t1")
    {
        $this->type=$type;
        $this->template="gallery2";
        $this->thumb_suffix=$thumb_suffix;
        $this->detail_suffix=$detail_suffix;

        $gallery = new View($this->template);
        $gallery->type=$this->type;

        $submodule_suffix=($submodule!="item")?("_".$submodule):"";

        // vykonny kod je pro svoji jednoduchost vlozen primo do kontroleru
        $module_orm_photos_name=$module.$submodule_suffix."_photos";
        $module_orm=orm::factory($module.$submodule_suffix, $id);
        $photos=$module_orm->$module_orm_photos_name->where("zobrazit","=",1)->order_by("poradi","asc")->find_all();

        $dirname=$this->photos_resources_dir.$module."/".$submodule."/gallery/images-".$module_orm->id."/";
        $photos_array=array();
        $x=1;
        foreach($photos as $photo)
        {
            if($photo->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$dirname.$photo->photo_src."-t2.jpg"))
            {
                $photos_array[$x]["photo"]=url::base().$dirname.$photo->photo_src."-".$this->thumb_suffix.".jpg";
                $photos_array[$x]["photo_detail"]=url::base().$dirname.$photo->photo_src."-".$this->detail_suffix.".jpg";
                $photos_array[$x]["nazev"]=$photo->nazev;
                $x++;
            }
        }

        $gallery->photos=$photos_array;

        $this->request->response = trim($gallery->render());
    }

}
?>
