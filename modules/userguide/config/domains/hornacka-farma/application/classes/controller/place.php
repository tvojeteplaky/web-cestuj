<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Zakladni kontroler na pro distribuční místa
 */
class Controller_Place extends Controller
{
       protected $module_key="place";


    protected $no_of_places_indexpage=3;             // pocet clanku na indexpage
    protected $no_of_places_sidebar=3;               // pocet clanku na sidebaru
    protected $no_of_places_main=12;                  // pocet clanku v hlavnim seznamu

    protected $place_detail_photo_suffix="t1";
    protected $places_main_photo_suffix="t1";        // suffix fotky na hlavnim seznamu clanku
    protected $places_indexpage_photo_suffix="t1";   // suffix fotky na seznamu clanku na indexu
    protected $places_sidebar_photo_suffix="t1";     // suffix fotky na seznamu clanku v sidebaru

    protected $unrelated_page=false;                    // novinky nejsou soucasti hlavni navigace (title, keywords, description a uvodni text se budou brat z nezarazenych stranek)

    protected $with_gallery=false;                       // priznak zda bude v detailu pripojena galerie
    protected $gallery_type="carousel";
    protected $gallery_template="gallery";
    protected $gallery_thumb_suffix="t2";
    protected $gallery_detail_suffix="t1";

    // zakladni promenne modulu - nezasahovat
    protected $template;
    protected $place_service;
    protected $module_nazev_seo="vydejni-mista";

    // definice jazykovych url (id jazyku dle DB)
    protected $url=array(
                    "vydejni-mista"=>array("nazev"=>"Výdejní místa","lang_id"=>1),
                    "manufacters"=>array("nazev"=>"Manufacters","lang_id"=>2)
                    );

    protected $languages=array(
                    1=>array("nazev"=>"Výdejní místa","nazev_seo"=>"vydejni-mista","back_link"=>"Zpět na seznam výdejních míst"),
                    2=>array("nazev"=>"Manufactures","nazev_seo"=>"manufacters","back_link"=>"Back to list")
                    );

    public function before()
    {
            parent::before();
            $this->place_service = Service_Place::instance();

    }


    public function action_index($nazev_seo, $page=1)
    {
       $this->template=new View("place_list");
        $this->template->module_place_mode="list";


        if($this->unrelated_page)
        {
            $page_data = Service_Hana_Page::instance()->get_unrelated_page_by_nazev_seo($nazev_seo,true);
        }
        else
        {
            $page_data = Service_Hana_Page::instance()->get_page_by_nazev_seo($nazev_seo,true);
        }

        // TODO pripadne vyplneni nazvu a popisu, ktery se zobrazi nad vypisem clanku
        $route=Service_Route::instance();
       // $route->add_breadcrumbs_to_the_end(array("nazev"=>$this->url[$nazev_seo]["nazev"],"nazev_seo"=>$nazev_seo));
        $route->set_selected_language_id($this->url[$nazev_seo]["lang_id"]);

        if(is_object($page_data))
        {
          $route->set_title($page_data->title);
          $route->set_description($page_data->description);
          $route->set_keywords($page_data->keywords);
        }else{
          $route->set_title($page_data["title"]);
          $route->set_description($page_data["description"]);
          $route->set_keywords($page_data["keywords"]);
        }

        $pagination = Pagination::factory(array(
              'current_page'   => array('source' => $nazev_seo, 'value'=>$page),
              'total_items'    => $this->place_service->get_model()->get_all_allowed_items_count(),
              'items_per_page' => $this->no_of_places_main,
              'view'              => 'pagination/basic',
              'auto_hide'      => TRUE
        ));

       $this->template->title=$this->url[$nazev_seo]["nazev"];

        $this->place_service->photo_thumb_suffix=$this->places_main_photo_suffix;
        //print_r($pagination->offset);
		$this->place_service->photo_thumb_suffix = 't2';
		$this->place_service->order_by = 'places.poradi';
		$this->place_service->order_direction = 'asc';
		
        $this->template->articles = $this->place_service->get_place_list($this->no_of_places_main, $pagination->offset);
        $this->template->pagination = $pagination->render();
        $this->request->response = $this->template->render();



    }


     //hlavní zobrazovací action pro města
     public function action_detail($nazev_seo="")
    {
        $this->template=new View("place_detail");
        $this->template->module_article_mode="detail";

        $this->place_service->photo_detail_suffix=$this->place_detail_photo_suffix;
        $article_data = $this->place_service->get_place_detail($nazev_seo);

        if(!$article_data["id"])
        {
            Request::instance()->status = 404;
            $this->request->response = View::factory('errors/404');
            return;
        }

        $route=Service_Route::instance();

        $lang_id=$route->get_selected_language_id();

        //$route->add_breadcrumbs_to_the_end(array("nazev"=>$this->languages[$lang_id]["nazev"],"nazev_seo"=>$this->languages[$lang_id]["nazev_seo"]));
       $route->add_breadcrumbs_to_the_end(array("nazev"=>$article_data["nazev"],"nazev_seo"=>$article_data["nazev_seo"]));

        $this->template->back_link_url=$this->languages[$lang_id]["nazev_seo"];
        $this->template->back_link_text=$this->languages[$lang_id]["back_link"];
        $this->template->photo=$article_data["photo"];
        $this->template->title=$article_data["nazev"];
        $this->template->description=$article_data["popis"];

        if($this->with_gallery)
        {
            $this->template->gallery=Request::factory("internal/gallery/index/".$article_data["id"]."/".$this->module_key."/item/".$this->gallery_type."/".$this->gallery_template."/".$this->gallery_thumb_suffix."/".$this->gallery_detail_suffix)->execute()->response;;
        }

        $route->set_title($article_data["nazev"]);
        $route->set_description(strip_tags($article_data["popis"]));
        $route->set_keywords($article_data["nadpis"]);

        $this->request->response = $this->template->render();
    }




    public function action_homepageplaces()
    {
     $template=new View("place");

      $template->places=Service_Product::instance()->get_index_distribution_places();

     $this->request->response=$template->render();

    }
        //smazani casu z administrace
        public function action_delete_time()
    {
      if(!isset($_GET['time_row_id']))return false;


      $id_row = $_GET['time_row_id'];


     $result_db=DB::delete('distributionplace_times_distributionplaces')
                    ->where("distributionplace_times_distributionplaces.id","=",$id_row);
                     $result_db= $result_db->execute();






    }

}
?>
