<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Obsluha vyhledavani.
 * pripojena sablona: search_results.tpl
 * pozadovane servisni tridy:
 * pozadovane modelove tridy:
 * administrace: n/a
 */
class Controller_Search extends Controller
{

    public function action_index()
    {
        //$language=1;
        if(isset($_POST["search_text"]) && $_POST["search_text"]){
            // nastaveni jazyku
            Service_Route::instance()->set_selected_language_id($_POST["context_language_id"]);
            $language=$_POST["context_language_id"];

            $search_service=new Service_Search();

            $search_text=strip_tags($_POST["search_text"]);

            // nastaveni parametru vyhledavani
            $search_config=array(
              "page"=>array(
                  "title"=>"V�sledky ve str�nk�ch",
                  "language"=>true,
                  "display_title"=>"page_data.nazev",
                  "display_text"=>"page_data.popis",
                  "search_columns"=>array("page_data.nazev", "page_data.popis", "page_data.uvodni_popis"),
//                  "display_category_title"=>"product_category_data.nazev",
//                  "display_category_text"=>"product_category_data.uvodni_popis",
//                  "search_category_columns"=>array("product_category_data.nazev", "product_category_data.uvodni_popis", "product_category_data.popis")
              ),
              "unrelated_page"=>array(
                  "title"=>"V�sledky ve str�nk�ch",
                  "language"=>true,
                  "display_title"=>"unrelated_page_data.nazev",
                  "display_text"=>"unrelated_page_data.popis",
                  "search_columns"=>array("unrelated_page_data.nazev", "unrelated_page_data.popis"),
//                  "display_category_title"=>"product_category_data.nazev",
//                  "display_category_text"=>"product_category_data.uvodni_popis",
//                  "search_category_columns"=>array("product_category_data.nazev", "product_category_data.uvodni_popis", "product_category_data.popis")
              ),
              "product"=>array(
                  "title"=>"V�sledky v sekci \"Produkty\"",
                  "language"=>true,
                  "display_title"=>"product_data.nazev",
                  "display_text"=>"product_data.uvodni_popis",
                  "search_columns"=>array("product_data.nazev", "product_data.uvodni_popis", "product_data.popis", "product_data.baleni" , "product_data.nazev_doplnek"),
                  "display_category_title"=>"product_category_data.nazev",
                  "display_category_text"=>"product_category_data.uvodni_popis",
                  "search_category_columns"=>array("product_category_data.nazev", "product_category_data.uvodni_popis", "product_category_data.popis")
              )

            );

            $search_message="";
            $search_results=array();

            if(strlen($search_text)>1)
            {
                $search_results = $search_service->search($search_text, $search_config, $language);
                if(empty($search_results)) $search_message=__("Pro zvolen� kl��ov� slovo nebylo nic nalezeno");
            }
            else
            {
                $search_message=__("Hledan� �et�zec mus� m�t alespo� 2 znaky");
            }
            //print_r($search_results);
            $search_results_template=new View("search_results");
            $search_results_template->keyword=$search_text;
            $search_results_template->search_results=$search_results;
            $search_results_template->search_message=$search_message; 
            $search_results_template->is_indexpage=false;
            $this->request->response=$search_results_template->render();
            

        }
    }


    public function action_form()
    {
       // TODO - formular s pokrocilym vyhledavanim

    }

    

    
}
?>
