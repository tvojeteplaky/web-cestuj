<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Generovani statickych stranek.
 */
class Controller_Page extends Controller
{
    public $with_photo = true; // soucasti stranek bude i fotka
    public $photo_suffix="t1"; // suffix fotky

    protected $service_page;

    public function before() {
        parent::before();
        $this->service_page=Service_Page::instance();
        $this->service_page->with_photo=$this->with_photo;
        $this->service_page->photo_suffix=$this->photo_suffix;
    }

    /**
     * Metoda generujici obsah uvodni stranky a vsech statickych stranek vkladanych do hlavniho obsahu.
     * @param <type> $nazev_seo
     */
    public function action_index($nazev_seo="index")
    {
        $route=Service_Route::instance();
        if($lang=$this->service_page->is_index_page($nazev_seo))
        {
            // sekce homepage
            $route->set_selected_language_id($lang);

            // ziskani obsahu stranky
			$this->service_page->photo_suffix = 't3';
            $page_data = $this->service_page->get_page_by_nazev_seo($nazev_seo);

            $product_service=Service_Hana_Product::instance();

            // sestavim specificky layout uvodky
            $template = new View("homepage");
            $template->is_indexpage=true;
            if(Service_User::instance()->logged_in())
            {
               $template->logged_in=true;
            }else{
              $template->logged_in=false;
            }


            // hlavni akcni produkt (posledni pridane akcni zbozi)
            //$action_product=$product_service->get_special_products("akcni-zbozi", DB::expr("RAND()"), "desc", 1, "t2");
            //die(print_r(current($action_product["products"])));
            if(!empty($action_product["products"]))$template->action_product=current($action_product["products"]);

            // vylistovane kategorie
            $template->product_categories=$product_service->get_index_product_categories();

            // distribuřní místa pro selectbox na uvodce
            $template->product_places=$product_service->get_index_product_places();

            // akcni produkty v prave sekci
            $right_action_products=$product_service->get_special_products("akcni-zbozi", "poradi", "desc", 3, "t3");
            if(!empty($right_action_products) && isset($right_action_products["products"]))$template->right_action_producs=$right_action_products["products"];

            $template->page=$page_data;

            // novinky na uvodce
            $template->article_homepage=Request::factory("internal/article/indexpage/")->execute()->response;

            //místa na uvodce
            $template->place_homepage=Request::factory("internal/place/homepageplaces/")->execute()->response;

            // pripadne hlasky
              // odhlaseni uzivatele
              if(isset($_GET["usrlogout"]) && $_GET["usrlogout"] && (!isset($_POST["username"]) && !Service_User::instance()->logged_in())) $template->message="usrlogout";

              // registrace uzivatele
              if(isset($_GET["usrreg"]) && $_GET["usrreg"]) $template->message="usrreg";

        }
        else
        {
            $template=new View("page");
            // sestavim layout staticke stranky
            $page_data = $this->service_page->get_page_by_nazev_seo($nazev_seo);

            if($nazev_seo=="kontakt"){
                      // kontaktni formular
                  $popis=$_SERVER['SERVER_NAME']."/".$nazev_seo;

                  $template->contactform=Request::factory("internal/contactform/index/".urlencode(str_replace(".", "___", $popis))."/full-page/standalone_contact_form")->execute()->response;


            }
            if($page_data===false)
            {
                return $this->action_404($nazev_seo);
            }
        }



        $route->set_title($page_data["title"]);
        $route->set_description($page_data["description"]);
        $route->set_keywords($page_data["keywords"]);
        $template->page=$page_data;

        // echo $page_data["title"];
        $this->request->response=$template->render();
    }

    /**
     * Metoda generujici obecny staticky obsah.
     * @param string $kod
     */
    public function action_static($kod)
    {
        if(!$this->internal) throw new Kohana_Exception("externí request není povolen");
        $this->request->response = $this->service_page->get_static_content_by_code($kod);
    }


    public function action_unrelated($nazev_seo)
    {
        $template=new View("page");
        // sestavim layout staticke stranky
        $page_data = $this->service_page->get_unrelated_page_by_nazev_seo($nazev_seo);
        Service_Route::instance()->set_selected_language_id($page_data->language_id);

        if($page_data===false)
        {
            return $this->action_404($nazev_seo);
        }
        $template->page=$page_data;
        Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>$page_data->nazev,"nazev_seo"=>$nazev_seo));
        $this->request->response = $template->render();
    }

    public function action_sitemap()
    {
        $template=new View("page");
        $sitemap=new View("sitemap");
        Service_Route::instance()->set_selected_language_id(Service_Route::instance()->get_last_language_id());
        $links=Service_Page::instance()->get_navigation_structure();
        Service_Route::instance()->add_breadcrumbs_to_the_end(array("nazev"=>__("Mapa stránek"),"nazev_seo"=>"mapa-stranek"));
        //print_r($links);
        $sitemap->links=$links;
        $page=array("nadpis"=>__("Mapa stránek"),"popis"=>$sitemap->render());
        $template->page=$page;
        $this->request->response = $template->render();
    }

    public function action_404($nazev_seo="")
    {
        $route=Service_Route::instance();
        $route->set_selected_language_id(Service_Route::instance()->get_last_language_id());
        Request::instance()->status = 404;
        $sitemap=new View("sitemap");
        $links=Service_Page::instance()->get_navigation_structure();
        $route->add_breadcrumbs_to_the_end(array("nazev"=>__("Požadovaná stránka nebyla nalezena")." (404) ","nazev_seo"=>"404"));
        $route->set_title(__("Požadovaná stránka nebyla nalezena"));
        $route->set_description("");
        $route->set_keywords("");
        $sitemap->links=$links;

        $template = View::factory('errors/404');
        $template->sitemap=$sitemap;
        $popis=$_SERVER['SERVER_NAME']."/".$nazev_seo;
        $template->contactform=Request::factory("internal/contactform/index/".urlencode(str_replace(".", "___", $popis))."/error/error_form")->execute()->response;
        $this->request->response=$template->render();
    }

}
