<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Generovani výrobců.
 * pripojena sablony:
 * - hlavni clanky: manufacter.tpl

 * administrace: ano
 */
class Controller_Manufacturer extends Controller
{
    // nastaveni modulu
    protected $module_key="manufacturer";

    protected $no_of_manufacturers_indexpage=3;             // pocet clanku na indexpage
    protected $no_of_manufacturers_sidebar=3;               // pocet clanku na sidebaru
    protected $no_of_manufacturers_main=5;                  // pocet clanku v hlavnim seznamu

    protected $manufacturer_detail_photo_suffix="t3";
    protected $manufacturers_main_photo_suffix="t3";        // suffix fotky na hlavnim seznamu clanku
    protected $manufacturers_indexpage_photo_suffix="t3";   // suffix fotky na seznamu clanku na indexu
    protected $manufacturers_sidebar_photo_suffix="t3";     // suffix fotky na seznamu clanku v sidebaru

    protected $unrelated_page=false;                    // novinky nejsou soucasti hlavni navigace (title, keywords, description a uvodni text se budou brat z nezarazenych stranek)

    protected $with_gallery=true;                       // priznak zda bude v detailu pripojena galerie
    protected $gallery_type="carousel";
    protected $gallery_template="gallery";
    protected $gallery_thumb_suffix="t2";
    protected $gallery_detail_suffix="t1";

    // zakladni promenne modulu - nezasahovat
    protected $template;
    protected $manufacturer_service;
    protected $module_nazev_seo="vyrobci";

    // definice jazykovych url (id jazyku dle DB)
    protected $url=array(
                    "vyrobci"=>array("nazev"=>"Výrobci","lang_id"=>1),
                    "manufacters"=>array("nazev"=>"Manufacters","lang_id"=>2)
                    );

    protected $languages=array(
                    1=>array("nazev"=>"Výrobci","nazev_seo"=>"vyrobci","back_link"=>"Zpět na seznam výrobců"),
                    2=>array("nazev"=>"Manufactures","nazev_seo"=>"manufacters","back_link"=>"Back to list")
                    );

    public function before()
    {
            parent::before();
            $this->manufacturer_service = Service_Manufacturer::instance();

    }

    /**
     * Hlavni seznam clanku.
     * @param string $article_category
     */
    public function action_index($nazev_seo, $page=1)
    {
        $this->template=new View("manufacturer");
        $this->template->module_manufacturer_mode="list";

        if($this->unrelated_page)
        {
            $page_data = Service_Hana_Page::instance()->get_unrelated_page_by_nazev_seo($nazev_seo,true);
        }
        else
        {
            $page_data = Service_Hana_Page::instance()->get_page_by_nazev_seo($nazev_seo,true);
        }

        // TODO pripadne vyplneni nazvu a popisu, ktery se zobrazi nad vypisem clanku
        $route=Service_Route::instance();
       // $route->add_breadcrumbs_to_the_end(array("nazev"=>$this->url[$nazev_seo]["nazev"],"nazev_seo"=>$nazev_seo));
        $route->set_selected_language_id($this->url[$nazev_seo]["lang_id"]);
        
        if(is_object($page_data))
        {
          $route->set_title($page_data->title);
          $route->set_description($page_data->description);
          $route->set_keywords($page_data->keywords);
        }else{
          $route->set_title($page_data["title"]);
          $route->set_description($page_data["description"]);
          $route->set_keywords($page_data["keywords"]);
        }

        $pagination = Pagination::factory(array(
              'current_page'   => array('source' => $nazev_seo, 'value'=>$page),
              'total_items'    => $this->manufacturer_service->get_model()->get_all_allowed_items_count(),
              'items_per_page' => $this->no_of_manufacturers_main,
              'view'              => 'pagination/basic',
              'auto_hide'      => TRUE
        ));

       $this->template->title=$this->url[$nazev_seo]["nazev"];

        $this->manufacturer_service->photo_thumb_suffix=$this->manufacturers_main_photo_suffix;
        //print_r($pagination->offset);
        $this->template->articles = $this->manufacturer_service->get_manufacturer_list($this->no_of_manufacturers_main, $pagination->offset);
        $this->template->pagination = $pagination->render();
        $this->request->response = $this->template->render();
    }



    /**
     * Hlavni detail clanku.
     * @param string $nazev_seo
     */
    public function action_detail($nazev_seo="")
    {
        $this->template=new View("manufacturer");
        $this->template->module_article_mode="detail";

        $this->manufacturer_service->photo_detail_suffix=$this->manufacturer_detail_photo_suffix;
        $article_data = $this->manufacturer_service->get_manufacturer_detail($nazev_seo);
        //print_r($article_data);
        if(!$article_data["id"])
        {
            Request::instance()->status = 404;
            $this->request->response = View::factory('errors/404');
            return;
        }

        $route=Service_Route::instance();

        $lang_id=$route->get_selected_language_id();

        //$route->add_breadcrumbs_to_the_end(array("nazev"=>$this->languages[$lang_id]["nazev"],"nazev_seo"=>$this->languages[$lang_id]["nazev_seo"]));
        $route->add_breadcrumbs_to_the_end(array("nazev"=>$article_data["title"],"nazev_seo"=>$article_data["nazev_seo"]));

        $this->template->back_link_url=$this->languages[$lang_id]["nazev_seo"];
        $this->template->back_link_text=$this->languages[$lang_id]["back_link"];
        $this->template->photo=$article_data["photo"];
        $this->template->title=$article_data["title"];
        $this->template->description=$article_data["popis"];

        if($this->with_gallery)
        {
            $this->template->gallery=Request::factory("internal/gallery/index/".$article_data["id"]."/".$this->module_key."/item/".$this->gallery_type."/".$this->gallery_template."/".$this->gallery_thumb_suffix."/".$this->gallery_detail_suffix)->execute()->response;;
        }

        $route->set_title($article_data["title"]);
        $route->set_description($article_data["description"]);
        $route->set_keywords($article_data["keywords"]);

        $this->request->response = $this->template->render();
    }

    /**
     * Seznam clanku v postranim panelu.
     */
    public function action_sidebar()
    {
        $sidebar_template=new View("article_sidebar");

        $this->manufacturer_service->photo_thumb_suffix=$this->manufacturers_sidebar_photo_suffix;
        $sidebar_template->articles = $this->manufacturer_service->get_manufacturer_list($this->no_of_articles_sidebar, 0);

        $this->request->response = $sidebar_template->render();
    }

    /**
     * Seznam clanku na indexove strance.
     */
    public function action_indexpage()
    {
        $indexpage_template=new View("article_indexpage");

        $this->manufacturer_service->photo_thumb_suffix=$this->manufacturers_indexpage_photo_suffix;
        $indexpage_template->articles = $this->manufacturer_service->get_manufacturer_list($this->no_of_manufacturers_indexpage, 0);

        $this->request->response = $indexpage_template->render();
    }

}
?>
