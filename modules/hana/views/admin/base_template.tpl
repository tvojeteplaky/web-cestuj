{* zakladni sablona systemu Hana 2 - copyright 2011 Pavel Herink *}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">	
  <head>		
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />		
    <meta http-equiv="content-language" content="cs" />		
    <meta name="author" content="Pavel Herink" />		
    <meta name="copyright" content="2011" />		
    <meta name="description" content="Systém pro správu obsahu Hana verze 2" />		
    <meta name="keywords" content="" />	
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
    	
    <link rel="stylesheet" href="{$media_path}admin/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="{$media_path}admin/css/style-print.css" type="text/css" media="print" />
    <link rel="stylesheet" href="{$media_path}admin/css/jquery-ui-1.8.6.custom.css" type="text/css" />
    <link rel="stylesheet" href="{$media_path}admin/css/showLoading.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="{$media_path}admin/css/agile-uploader.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="{$media_path}admin/css/foundation-icons/foundation-icons.css" type="text/css" media="screen" />
    
    <link rel="stylesheet" href="{$media_path}admin/js/ui.daterangepicker.css" type="text/css" />
    <link rel="stylesheet" href="{$media_path}admin/js/jquery-lightbox/css/jquery.lightbox.css" type="text/css" />
    
    <script type="text/javascript" src="{$media_path}admin/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/jquery-ui-1.8.18.custom.min.js"></script>  
    {*<script type="text/javascript" src="{$media_path}admin/js/fckeditor/fckeditor.js"></script>*}
    <script type="text/javascript" src="{$media_path}admin/js/ckeditor/ckeditor.js"></script>

    <script type="text/javascript" src="{$media_path}admin/js/ui.datepicker-cs.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/daterangepicker.jQuery.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/jquery.showLoading.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/jquery.tristateCheckbox.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/jquery.tablednd.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/jquery-lightbox/js/jquery.lightbox.js"></script>
    
    <script type="text/javascript" src="{$media_path}admin/js/admin.js"></script>
    
    <title>{$title} - administrační rozhranní</title>	
    
    <script type="text/javascript">
    /* <![CDATA[ */ 
    $(function(){
     $.Lightbox.construct({
        "base_url":    "{$url_base}"  
    });
    /* ]]> */ 
    });
    </script>
    
  </head>
  <body>
  {if $main_logo}
  <div id="MainLogo"><a href="{$main_logo.url}" target="_blank"><img src="{$media_path}{$main_logo.src}" alt="{$main_logo.title}" title="{$main_logo.title}" /></a></div>
  {/if}
  
  
  <div id="Page">
		<!-- hlavicka -->
		<div id="Header">
			
      <div class="left">    
      <!--  hlavni menu L1 -->
      {if !empty($admin_menu_L1)}				
				<ul id="Nav">
          {foreach key=k item=link name=nav from=$admin_menu_L1}
            {if $link.global_access_level<=$role_level}
            <li{if isset($link.sel)} class="sel"{/if}>
              <a href="{$link.href}">{$link.nazev}</a>
            </li>
            {/if}
          {/foreach}
            {* odkaz na odhlaseni - staticky *}
            
            <li>
              <a href="{$url_base}admin/logout">odhlásit</a>
            </li>
            
				</ul>
			{/if}	
			</div>
		</div>
		
		<!-- submenu L2 -->
		{if !empty($admin_menu_L2)}
		<div id="SubnavSection">			
  		<ul>
        {foreach key=k item=link name=nav from=$admin_menu_L2}
          {if $link.global_access_level<=$role_level}
          <li>
            <a href="{$link.href}" {if isset($link.sel)}class="sel"{/if}>{$link.nazev}</a>
          </li>
          {/if}
        {/foreach}
  		</ul>
		</div>
		{else}
		<div id="SubnavSectionSpacing"></div>
		{/if}
		
		<!-- stredni cast -->	
		<div id="CenterSection">	
		  {if !empty($center_section)}{$center_section}{/if}
		  
		  <!-- leve menu -->
		  <div id="LeftSection">
  		  {if !empty($admin_menu_L3)}
  		  <div class="box" id="ModuleLinks">
  			  <div class="top"></div>
  			  <div class="content">
            <ul>
    					{foreach key=k item=link from=$admin_menu_L3 name=admLeft}
    					    {if $link.global_access_level<=$role_level}
                  <li {if $smarty.foreach.admLeft.last}class="last"{/if}>
                    <a class="{if isset($link.sel)}sel{/if} {if $link.pristup==3}italic bold{/if}" href="{$link.href}">{$link.nazev}</a>
                  </li>
                  {/if}
                {/foreach}
    				</ul>
  				</div>
  				<div class="bottom"></div>
  			</div>
  			{/if}
			</div>
			
			<!-- hlavni obsah -->
      <div id="ContentSection">
		     {if !empty($admin_content)}{$admin_content}{/if}
		     
		    
			</div>
		 <div class="correct"></div>
     <!--  patička -->
  	 <div id="Footer">
  	    <hr />
  			<div class="right">Powered by <a href="#" title="">HANA 2</a> CMS (v2.5)</div>
  			{*<div class="right"><a href="#" title="Realizace">Realizace</a></div>*}
  			<div class="correct"></div>
  	 </div>
		</div>
	</div><!-- MainBox -->
</body>
</html>