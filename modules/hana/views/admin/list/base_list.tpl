{* sablona standardniho administracniho "listu" *}

<div id="ListTableSection">
  <form action="{$form_url}" method="post">
  <!--  1. sekce hornich ovladacich tlacitek -->
  
  {if $filter_button}
    {if !$list_table.filtering_row.do_filter}
    <div class="filtering filteringSwitch">
      <a class="filt" href="#"><img src="{$media_path}admin/img/filter.png" alt="filtrovat seznam" title="filtrovat seznam" /></a> <a class="filt filteringA" href="#">filtrovat seznam</a> <a href="#" class="filt filteringB"><img src="{$media_path}admin/img/rozbalit.gif" alt="filtrovat seznam" title="filtrovat seznam" /></a>
    </div>
    {else}
    <div class="filtering">
      <a class="filt" href="{$form_url}?destroy-filter=true"><img src="{$media_path}admin/img/filter.png" alt="filtrovat seznam" title="filtrovat seznam" /></a> <a class="filt filteringA" href="{$form_url}?destroy-filter=true">zrušit filtrování</a> <a href="{$form_url}?destroy-filter=true" class="filt filteringB"><img src="{$media_path}admin/img/sbalit.gif" alt="zrušit filtrování" title="zrušit filtrování" /></a>
    </div>
    {/if}
  {/if}
  
  <div id="ToolButts"{if !$filter_button} class="tb_full"{/if}>
  {if $new_link}
    <a class="item tb_newLink" href="{$new_link}" title="Nová položka"><img src="{$media_path}admin/img/add.png" alt="přidat novou položku" title="přidat novou položku" /> <span>Nová položka</span></a>
  {/if}
    <a class="item right" href="?render=csv" title="Exportovat aktuální zobrazení seznamu do formátu CSV (UTF-8)"><img src="{$media_path}admin/img/application_view_columns.png" alt="Exportovat aktuální zobrazení seznamu do formátu CSV (UTF-8)" title="Exportovat aktuální zobrazení seznamu do formátu CSV (UTF-8)" /></a> 
    <a class="item right printButton" href="?render=print" target="_blank" title="Zformátovat zvolený seznam pro tisk"><img src="{$media_path}admin/img/printer.png" alt="Zformátovat zvolený seznam pro tisk" title="Zformátovat zvolený seznam pro tisk" /></a>
  </div>
  
  {* oblast, kde se zobrazi zprava o ulozeni/neulozeni dat - typy: error, default, ok *}
    {if !empty($message)}
    <div class="ui-widget correct">
			<div class="ui-state-{if $message=="deleted"}highlight{else}{$message}{/if} ui-corner-all"> 
				<p>
        {if $message=="error"} 
        <span class="ui-icon ui-icon-alert"></span>
				<strong>Chyba:</strong> Při zpracování dat došlo k chybě, zkontrolujte prosím zadané údaje.
        {elseif $message=="ok"}
        <span class="ui-icon ui-icon-check"></span>
        Změny {if $affected_rowid}na položce ID: {$affected_rowid} {/if}byly uloženy.{if $affected_rowid} (<a href="#to_rowid_{$affected_rowid}">přejít na řádek</a>){/if}
        {elseif $message=="deleted"}
        <span class="ui-icon ui-icon-info"></span>
        Zvolené položky byly smazány.
        {elseif $message=="highlight"}
        <span class="ui-icon ui-icon-info"></span>
        Na položce (položkách) s ID: <span class="bold">{$error_rows}</span> byl zadán špatný formát dat, změny zde proto nemohly být uloženy. 
        {/if}
        </p>
			</div>
		</div>
		{/if}
  
  <div class="tableTop"><div class="in"></div></div>
    <table summary="seznam položek" id="ItemsList">
    <thead>
    <!--  2. sekce vypisove tabulky -->
     
    <!--  nadpisy sloupcu -->
    <tr class="nodrop nodrag">
    {foreach name=iter from=$list_table.head_row key=key item=item}
      <th{if $item.html} {$item.html}{/if}>{$item.content}</th>
    {/foreach}
    
    {* vypocteni obecnych promennych *}
    {assign var='total_cols' value=$smarty.foreach.iter.total}
    {assign var='total_rows' value=$list_table.data_section|@count}

    </tr>
    </thead>
    <tbody>
    <!--  filtrovaci radek -->
    {if $filter_button}

      <tr class="filteringRow filteringRowHide fr1 nodrop nodrag">
        {foreach name=iter from=$list_table.filtering_row key=key item=item}
          {if !$smarty.foreach.iter.last}<td>{$item}</td>{/if}{* posledni polozka v poli je priznak zda bylo filtrovani aktivovano *}
        {/foreach}
      </tr>
      <tr class="filteringRow filteringRowHide fr2 nodrop nodrag">
        <td colspan="{$total_cols}">
          <div class="right">
            <input type="submit" class="subm button" name="do-filter" value="filtrovat" />
            <input type="submit" class="subm button" name="destroy-filter" value="zrušit filtrování" />
          </div>
        </td>
      </tr>
    {/if} 
    
    <!--  telo tabulky -->
      
      {if $total_rows>0}
        {foreach name=iter1 from=$list_table.data_section key=key1 item=row}
        <tr id="rowid_{$key1}"{if $affected_rowid==$key1} class="affected"{/if}>
          {foreach name=iter2 from=$row key=key item=item}
          {* vylistovani dat v jednotlivych radcich - html bunek je stejne se zahlavim *}
          <td{if $list_table.head_row.$key.html} {$list_table.head_row.$key.html}{/if}>{if $smarty.foreach.iter2.first}<a name="to_rowid_{$key1}" id="to_rowid_{$key1}"></a>{/if}{$item}</td>
          {/foreach}
        </tr>
        {/foreach}
       {else}
          <tr><td colspan="{$total_cols}"><div class="txtCenter">
          {if !$list_table.filtering_row.do_filter}
          -- bez záznamu --
          {else}
          -- pro zvolené nastavení filtru nebyl nalezen žádný záznam --
          {/if}
          </div></td></tr>
       {/if}
    <!--  3. sekce - sumarizacni radek - nepovinne --> 
      {if !empty($list_table.summary_row)}
      <tr class="summaryRow">
      {foreach name=iter from=$list_table.head_row key=key item=item}
        <td {if $list_table.head_row.$key.html} {$list_table.head_row.$key.html}{/if}>{if isset($list_table.summary_row.$key)}{$list_table.summary_row.$key}{/if}</td>
      {/foreach}
      </tr>  
      {/if}
    </tbody>
    </table>
    
   
    
  <!--  4. sekce listovani -->
  
  <div id="ListTablePagination">
    <div class="left">{$count_records}&nbsp;&nbsp;|&nbsp;&nbsp;{$pagination}
    </div>
    <div class="right">Stránkování po: 
      {foreach from=$items_per_pg key=key item=item}
         {if $key==$items_per_pg_sel}
         <span class="sel">{$item}</span>
         {else}
         <a href="{$form_url}?it_per_pg={$key}" title="Stránkování po {$item} řádcíh" class="ajaxelement">{$item}</a>
         {/if}  
      {/foreach}
      <div class="correct">
      </div>
    </div>
    <div class="correct">
    </div>
  </div>
  
  <!--  5. sekce spodnich ovladacich tlacitek -->
  
  <div id="ListTableFormControls">
    <div class="info">
      {if $drag_reorder}<img src="{$media_path}admin/img/arrow_up_down.png" alt="Řazení tažením myší je aktivní" width="16" height="16" /> Řazení položek tažením myší je <span class="bold">aktivováno</span>.{/if}
    </div>
    
    <input type="checkbox" name="sellAll" class="sellAll" /> vybrat vše &nbsp;&nbsp;
    {if $delete_button}<input type="submit" class="subm button" name="hana_form_action[delete]" value="smazat označené"  onclick="javascript: return confirm('Opravdu smazat vybrané položky?'); " />{/if}
    {if $save_button}<input type="submit" class="subm button" name="hana_form_action[save]" value="{$save_button}"  onclick="javascript: return confirm('Opravdu uložit provedené změny?'); " />{/if}
    {if !empty($default_buttons)}
       {foreach name=db from=$default_buttons item=item}
          <input type="{$item.type}" class="subm button" id="button_{$item.action}" name="hana_form_action[{$item.action}]" value="{$item.nazev}" {if $item.onclick}onclick="javascript: return confirm('{$item.onclick}');" {/if}/>
       {/foreach}
    {/if}
  </div>

  </form>
  <div class="tableBottom"><div class="in"></div></div>
</div>


{literal}
<script type="text/javascript">
$(document).ready(function(){
  {/literal}{if $list_table.filtering_row.do_filter}{literal}
  $(".filteringRow").removeClass("filteringRowHide");
  {/literal}{/if}{literal}
 
  {/literal}{if $drag_reorder}{literal}
  $("#ListTableSection table").tableDnD({
    onDrop: function(table, row) {
      
                   xurl = $(this).attr("href"); 
                   $('#ContentSection tbody').showLoading(); 
                   $(this).attr("href","javascript: //ajax_request_process;"); 
                   $.ajax({
                     type: "GET",
                     url: "?hana_form_action=drag_change_order&reorder_item="+row.id+"&" + $.tableDnD.serialize(),
                     success: function(msg){
                     //alert( "Obdržená data: " + msg );
                     $('#ContentSection tbody').hideLoading(); 
                     $('#ContentSection').html(msg);
                     //$(this).attr("href",xurl);
                     },
                     error: function(XMLHttpRequest, textStatus, errorThrown){
                        $('#ContentSection tbody').hideLoading(); 
                        alert("Při zpracování dat došlo k chybě");
                        //alert("Při zpracování dat došlo k chybě: " + XMLHttpRequest + ", textStatus: " + textStatus+ ", errorThrown: " + errorThrown);
                     }
                 
      });
    }
   
  });
  {/literal}{/if}{literal}
  
});     
</script>
{/literal}
