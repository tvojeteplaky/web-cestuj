
<div id="ModalForm2Container">
  <div id="ModalForm2" class="photoeditForm">
    <!--  chybova zprava -->
    {if !empty($form_errors)}
    <div class="ui-widget">
			<div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"> 
				<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
				<strong>Chyba při zpracování zip archivu.</strong></p>
			</div>
		</div>
		{/if}

    <div class="left">
      <table>
        <td><label for="nazev">Společný název</label></td><td><input class="w235" type="text" name="nazev" id="nazev" value="{if !empty($nazev)}{$nazev}{/if}" />{if !empty($form_errors.nazev)}<br /><span class="error">{$form_errors.nazev}</span>{/if}</td></tr>
        <td><label for="popis">Společný popisek</label></td><td><textarea class="w235" name="popis" id="popis">{if !empty($popis)}{$popis}{/if}</textarea></td></tr>
        <td><label for="zobrazit">Zobrazit vše</label></td><td><input type="checkbox" name="zobrazit" id="zobrazit"{if !empty($zobrazit) && $zobrazit} checked="checked"{/if} /></td></tr>
        {if $language_id==1}
          <td><label for="nazev">Zdroj</label></td><td><input type="file" name="gallery_image_src" id="gallery_image_src" />{if !empty($form_errors.src)}<br /><span class="error">{$form_errors.src}</span>{/if}</td>
        {else}
          <td colspan="2">Zpracování zip archivu lze provést po přepnutí<br /> na základní jazykovou verzi.</td>
        {/if}
        </tr>
        
        <tr>
          <td><label for="zobrazit">Seřadit soubory</label></td>
          <td>
          <select name="extract_type">
            <option value="name">podle názvu</option>
            <option value="time">podle času vytvoření</option>
          </select>
          
          </td>
        </tr>
        <tr>
          <td colspan="2">
          <span class="littleInfo">Doporučená velikost archivu do 30 fotek nebo 20MB, přesné hodnoty závisí na nastavení serveru. Velké zip archivy nemusí být řádně zpracovány.</span>
          </td>
        </tr>  
        
        <input type="hidden" name="language_id" value="{$language_id}" />
        <input type="hidden" name="photoedit_action_add_zip" value="{$entity_name}" />
      </table>
    </div>
  </div>
</div>

{literal}
<script type="text/javascript">
     $(function() {

      // premisteni modalniho formulare mimo hlavni formular - jinak se neodeslou data
      var data=$("#ModalForm2").html();
      $('#JqueryForm form').append(data);
      $('#JqueryForm').attr("title","Přidání obrázků ze zip archivu");

      $("#ModalForm2Container").remove();
      // nasetovani modalniho formulare pridani zavady
      $("#JqueryForm").dialog({
              autoOpen: true,
              bgiframe: true,
              height: {/literal}{if !empty($form_errors)}430{else}380{/if}{literal},
              width: 480,
              modal: true,
              resizable: false,
              open: function(event, ui) {},
              //close: function(event, ui) {\$("#ModalForm2 form .error").html(""); \$("#ModalForm2 .date").datepicker("hide"); \$("#ModalForm2 .date").datepicker("destroy");},
              buttons: {
                      'Extrahovat archiv': function() {
                               // odeslani formulare
                                 //$(this).dialog('close');
                                  
                                 // odeslani dialogu
                                  $('#JqueryForm form').submit();
                                 
                                 // smazani dat z dialogu
                                 // $("#ModalForm2 form .delAfterSend").val("");
                      },
                      Zrušit: function() {
                              $(this).dialog('close');
                              $('a.lightbox-enabled').lightbox();
                      }
              }
      });

      });
</script>
{/literal}
   