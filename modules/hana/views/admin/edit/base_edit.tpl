{* sablona standardniho administracniho "editu" *}

<div id="EditTableSection">
  <form action="{$form_url}" method="post" enctype="multipart/form-data" name="EditForm" id="EditForm">
    <div id="ToolButts" class="left">
      {if isset($back_to_item) && $back_to_item}<a class="item" href="{$back_to_item}"><img src="{$media_path}admin/img/page_go.png" alt="Zpět" title="Zpět" /><span>{$back_to_item_text}</span></a>{/if}
      
      <a class="item" href="{$back_link}"><img src="{$media_path}admin/img/page_go.png" alt="Zpět" title="Zpět" /><span>{$back_link_text}</span></a>
      {if $clone_link}<a class="item" href="{$clone_link}"><img src="{$media_path}admin/img/page_copy.png" alt="Otevře formulář pro vložení nové položky s předvyplněnými základními daty" /><span>{$clone_link_text}</span></a>{/if}
      
    </div>   

    {if !empty($edtiable_languages)}
    <div class="languageButts">
      <span class="title">Jazyková verze:</span>
      <ul>
        {foreach from=$edtiable_languages key=k item=item}
           <li class="{if $k==$sel_language_id}sel{elseif $disable_other_languages}disabled{/if}"><a {if $disable_other_languages && $k!=$sel_language_id}href="#" title="Uložte formulář nejprve v základní jazykové verzi"{else}href="?admlang={$k}" title="jazyková verze: {$item}"{/if}>{$item}</a></li>
        {/foreach}
        {if $sel_language_id!=1 && $copy_lang_link}<li><a href="?copylang=true">kopírovat z {$main_language}</a></li>{/if}
        
      </ul>
      
    </div>
    {/if}
    
    {* oblast, kde se zobrazi zprava o ulozeni/neulozeni dat - typy: error, default, ok *}
    {if !empty($message)}  
    <div class="ui-widget correct">
			<div class="ui-state-{$message} ui-corner-all"> 
				<p>
        {if $message=="error"} 
        <span class="ui-icon ui-icon-alert"></span>
				<strong>Chyba:</strong> Při zpracování dat došlo k chybě, zkontrolujte prosím zadané údaje.
        {elseif $message=="ok"}
        <span class="ui-icon ui-icon-check"></span>
        Položka byla úspěšně uložena.
        {elseif $message=="highlight"}
        <span class="ui-icon ui-icon-info"></span>
        Změna byla provedena.
        {/if}
        </p>
			</div>
		</div>
		{/if}

    
    <div class="tableTop"><div class="in"></div></div>
    {if $script}
    <script type="text/javascript">
    {$script}
    </script>
    {/if}
    <table summary="editační formulář">
      {*<tr><th colspan="2" class="center">editační formulář</th></tr>*}
      {if !empty($action_buttons)}
      <tr>
          <td class="buttonsBar" colspan="2">
              {foreach name=a_butts from=$action_buttons key=key item=item}
              <input type="submit" id="{$item.name}" name="{$item.name}" value="{$item.value}" class="button" />
              {/foreach}
          </td>
      </tr>
      {/if}
      <tr><td colspan="2">&nbsp;</td></tr>
      
      {foreach name=data_section from=$edit_table.data_section key=key item=item} 
        <tr{if !empty($row_parameters.$key.class)} class="{$row_parameters.$key.class}"{/if}>
          {if !empty($row_parameters.$key.variant) && $row_parameters.$key.variant == "one_col"} {else}<td class="col1">{if $row_parameters.$key.label}<label for="item_{$key}">{$row_parameters.$key.label}</label>{/if}</td> {/if}
          <td class="col2" id="item_{$key}" {if !empty($row_parameters.$key.variant) && $row_parameters.$key.variant == "one_col"}colspan="2"{/if}>{$item} {if empty($row_errors.$key)}{if !empty($row_parameters.$key.condition)}{$row_parameters.$key.condition}{/if}{else}<span class="text_error">{$row_errors.$key}</span>{/if}</td>
        </tr>
      {/foreach}

      <tr><td colspan="2">{if !empty($edit_form_subinfo)}{$edit_form_subinfo}&nbsp;{/if}</td></tr>
      {if !empty($action_buttons)}
      <tr>
          <td class="buttonsBar" colspan="2">
              {foreach name=a_butts from=$action_buttons key=key item=item}
              <input type="submit" id="{$item.name}" name="{$item.name}" value="{$item.value}" class="button" />
              {/foreach}
          </td>
      </tr>
      {/if}
    </table>
    <div id="AdminTableBottom" style="width: 801px"><div class="in"></div></div>
    <input type="hidden" id="hana_edit_action" name="hana_form_action" value="main" /> {* ststicky definovana akce "main" na odeslani formulare *}
              
  </form>
  <div id="JqueryForm" title="editace položky"><form id="JqueryFormIN" action="{$form_url}" method="post" enctype="multipart/form-data">{* sem vklada jquery dynamicky vytvorene formulate - vnorene v hlavnim formulari nefunguji*}</form></div>
  <div class="tableBottom"><div class="in"></div></div>
</div>


