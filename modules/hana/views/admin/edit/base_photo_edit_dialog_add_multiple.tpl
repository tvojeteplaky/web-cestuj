
<script src="{$media_path}admin/js/jquery.flash.min.js" type="text/javascript"></script>
<script src="{$media_path}admin/js/agile-uploader-3.0.js" type="text/javascript"></script>

<div id="ModalForm1Container">
  <div id="ModalForm1" class="photoeditForm" title="Editace parametru">
    <!--  chybova zprava -->
    {if !empty($form_errors)}
    <div class="ui-widget">
			<div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"> 
				<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
				<strong>Chyba:</strong> obrázek nemohl být uložen</p>
			</div>
		</div>
		{/if}

    
      {*
      <label for="title">Title</label><br />
      <input id="title" type="input" name="title" />
      *}
      <input type="hidden" name="photoedit_action_add_multiple" value="{$entity_name}" />      
      <div id="Multiple"></div>   
      <div class="correct mediumMT">Maximální počet současně vkládaných obrázků: {$max_files}.</div>
      
    
      
    {*<a href="#" onClick="document.getElementById('agileUploaderSWF').submit();" id="SubmAgiler">Submit</a>*}
    </div>
    

    
  </div>
</div>

{literal}
<script type="text/javascript">
     $(function() {

      // premisteni modalniho formulare mimo hlavni formular - jinak se neodeslou data
      var data=$("#ModalForm1").html();
      $('#JqueryForm form').append(data);

      $("#ModalForm1Container").remove();
   
      
      
      	
    	
      // nasetovani modalniho formulare
      $("#JqueryForm").dialog({
              autoOpen: true,
              bgiframe: true,
              height: 520,
              width: 570,
              modal: true,
              resizable: false,
              open: function(event, ui) {},
              close: function(event, ui) {location.reload();},
              buttons: {
                      'Uložit': function(event) {
                               // odeslani formulare
                                 //$(this).dialog('close');   
                                 $(event.target).hide();
                                 document.getElementById('agileUploaderSWF').submit();
                                 // smazani dat z dialogu
                                 // $("#ModalForm1 form .delAfterSend").val("");
                      },
                      Zrušit: function() {
                              //$(this).dialog('close');
                              //$('a.lightbox-enabled').lightbox();
                              location.reload();
                      }
              }
      });
      
      $('#Multiple').agileUploader({
      		submitRedirect: '{/literal}{$base_admin_path}?message=highlight{literal}',
      		formId: 'JqueryFormIN',
      		flashVars: {
      			firebug: false,
          	form_action: '{/literal}{$base_admin_path}{literal}',
          	max_width: 1024,
          	max_height: 768,
          	jpg_quality: 90,
      			file_limit: {/literal}{$max_files}{literal},
      			max_post_size: (10000 * 1024),
          	}
      	});
      	

      });
</script>
{/literal}
   