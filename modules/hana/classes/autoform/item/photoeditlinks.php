<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Trida reprezentujici odkazy na pridani fotky a fotky ze zipu v galerii (Photoedit)
 * 
 * @package    Hana/AutoForm
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class AutoForm_Item_Photoeditlinks extends AutoForm_Item
{
    
    public function generate($data_orm, $template=false)
    {
        
        $href=$this->settings["href"];

        $result="<a href=\"".$href."_editphoto=0\" class=\"button ajaxelement\">přidat/editovat fotografii</a>";
        $result.="<a href=\"".$href."_addmultiple=true\" class=\"button ajaxelement\">hromadné přidání fotografií</a>";
        $result.="<a href=\"".$href."_addzip=true\" class=\"button ajaxelement\">přidání fotografií ze zipu</a>";


        return $result;
    }




}

?>
