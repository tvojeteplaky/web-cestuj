 <?php defined('SYSPATH') or die('No direct script access.');


class AutoForm_Item_Pricebox extends AutoForm_Item
{
	public function generate($data_orm, $template=false)
 	{
 		$setting = $this->settings;

 		$return ="<table>";

 		foreach ($setting['rows'] as $row) {
 			$return .= "<tr>";
 			for ($i=0; $i <= $setting['cols'] ; $i++) { 
 				if(!empty($row[$i]['value'])){
	 				if(!empty($row[$i]['colspan'])) {
	 					$colspan = "colspan='".$row[$i]['colspan']."'";
	 				} else {
	 					$colspan = "";
	 				}
	 				if(!empty($row[$i]['class'])) {
	 					$class = "class='".$row[$i]['class']."'";
	 				} else {
	 					$class = "";
	 				}
	 				$return .= "<td ".$colspan." ".$class.">".$row[$i]['value']."</td>";
	 			}
 			}
 			$return .= "</tr>";
 		}
 		$return .= '</table>';
 		return $return;;
 	}
}