<?php defined('SYSPATH') or die('No direct script access.');

/**
 *
 * Vygeneruje list customerů s možností editovat
 *
 * @package    Hana
 * @author     Ondřej Brabec
 * @copyright  (c) 2015 Ondřej Brabec
 */
class AutoForm_Item_Customers extends AutoForm_Item {
    private $up_orm;

    private $customers_orm;

    private $lang;

    public function pregenerate($data_orm) {
        $this->subject_id=$data_orm->id;

        $this->lang=Session::instance()->get("admlang");
        if(!$this->lang) $this->lang=1;
        $this->up_orm =orm::factory('reservation',$data_orm->id);
        $this->customers_orm=$this->up_orm->customers;

        $result_data=array();
        // if(isset($_POST["customer_action_add"]))
        // {
        //     $result_data["hana_form_action"]="customer_add";
        //     $result_data['customer_id'] = isset($_POST['customer_id'])?$_POST['customer_id']:0;
        //     $result_data["count"] = count($_POST['firstname']);
        //     for($i = 0; $i< $result_data['count']; $i++){
        //         $result_data['customers'][$i]['firstname'] = $_POST['firstname'][$i];
        //         $result_data['customers'][$i]['lastname'] = $_POST['lastname'][$i];
        //         $result_data['customers'][$i]['born'] = $_POST['born'][$i];
        //     }
        // }
        // pozadavek na smazani
        if(isset($_GET["customer_action_delete"]) && $_GET["customer_action_delete"])
        {
            $result_data["hana_form_action"]="customer_delete";
            $result_data["customer_id"]=$_GET["customer_id"];
        }
        return $result_data;
    }

    public function generate($data, $template=false) {

    	$customers_template = new View("admin/edit/customers");
    	$customers_template->customers = $this->customers_orm->find_all();
    	$customers_template->entity_name = $this->entity_name;
    	$customers_template->admin_path=$this->data_src["href"];
        $customers_template->count_customers = count($customers_template->customers);
    	$result_data=$customers_template->render();
    	// if (isset($_GET[$this->entity_name."_edit"])) {
    	// 	$customer_form=new View("admin/edit/customers_modal");
     //        $customer_id = isset($_GET['customer_id'])?$_GET['customer_id']:0;
     //        $data = $this->customers_orm->where('id','=',$customer_id)->find();
     //        $customer_form->entity_name = $this->entity_name;
     //        if($data->id){
     //            $customer_form->data = $data;
     //        } else {
     //            $customer_form->data = false;
     //            $customer_form->count_customers = count($customers_template->customers);
     //        }

    	// 	$result_data.=$customer_form->render();
    	// }

    	return $result_data;
    }
}
