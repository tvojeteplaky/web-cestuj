<?php defined('SYSPATH') or die('No direct script access.');

/**
 * 
 * Servisa pro obsluhu statickych stranek.
 *
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class Service_Hana_Page extends Service_Hana_Module_Base
{
    public static $photos_resources_dir="media/photos/";
    public static $navigation_module="page";
    protected static $chainable=true;
    
    /**
     * Nacte stranku dle route_id
     * @param int $id
     * @return orm 
     */
    public static function get_page_by_route_id($id)
    {
        $page_orm= orm::factory("page")->where("route_id","=",$id)->find();
        if($page_orm->direct_to_sublink)
        {
            $language_id=  Hana_Application::instance()->get_actual_language_id();
            //throw new Kohana_Exception("Požadovaná stránka nebyla nalezena", array(), "404");
            $first_subpage_seo=DB::select("routes.nazev_seo")
                ->from("pages")
                ->join("page_data")->on("pages.id","=","page_data.page_id")
                ->join("routes")->on("page_data.route_id","=","routes.id")
                ->where("pages.parent_id","=",db::expr($page_orm->id))
                ->where("routes.language_id","=",DB::expr($language_id))
                ->where("routes.zobrazit","=",DB::expr(1))
                ->order_by("poradi")
                ->limit(1)
                ->execute()->get("nazev_seo");
            Request::instance()->redirect(url::base().$first_subpage_seo);
        }
        
        $result_data=$page_orm->as_array();
        
        // cesta k obrazku
        $dirname=self::$photos_resources_dir."page/item/images-".$page_orm->id."/";
        

        if($page_orm->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$dirname.$page_orm->photo_src."-t1.jpg"))
        {
            $result_data["photo"]=url::base().$dirname.$page_orm->photo_src."-t1.jpg";
            $result_data["photo_detail"]=url::base().$dirname.$page_orm->photo_src."-ad.jpg";
        }
        else
        {
        $dirname=self::$photos_resources_dir."page/unrelated/images-".$page_orm->id."/";
        

        if($page_orm->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$dirname.$page_orm->photo_src."-t1.jpg"))
        {
            $result_data["photo"]=url::base().$dirname.$page_orm->photo_src."-t1.jpg";
            $result_data["photo_detail"]=url::base().$dirname.$page_orm->photo_src."-ad.jpg";
        }
        }
        
        return $result_data;
    }
    
    /**
     * Najde podřízené stránky. 
     * @param type $page_id
     * @return orm  
     */
    public static function get_page_index($page_id)
    {
        $language_id=  Hana_Application::instance()->get_actual_language_id();
        $index_pages=DB::select("pages.photo_src")->select("pages.id")->select("page_data.nazev")->select("page_data.uvodni_popis")->select("page_data.akce_text")->select("routes.nazev_seo")
                ->from("pages")
                ->join("page_data")->on("pages.id","=","page_data.page_id")
                ->join("routes")->on("page_data.route_id","=","routes.id")
                ->where("pages.parent_id","=",db::expr($page_id))
                ->where("routes.language_id","=",DB::expr($language_id))
                ->where("routes.zobrazit","=",DB::expr(1))     
                ->order_by("poradi")
                ->execute();
        
        $index_pages=$index_pages->as_array();
        $result_data=array();
        
        foreach ($index_pages as $page)
        {
            $result_data[$page["nazev"]]["nazev"]=$page["nazev"];
            $result_data[$page["nazev"]]["nazev_seo"]=$page["nazev_seo"];
            $result_data[$page["nazev"]]["uvodni_popis"]=$page["uvodni_popis"];
            $result_data[$page["nazev"]]["akce_text"]=$page["akce_text"];
            
            $dirname=self::$photos_resources_dir."page/item/images-".$page["id"]."/";
            if($page["photo_src"] && file_exists(str_replace('\\', '/',DOCROOT).$dirname.$page["photo_src"]."-t2.jpg"))
            {
                $result_data[$page["nazev"]]["photo_detail"]=url::base().$dirname.$page["photo_src"]."-t2.jpg";
            }
        }
        
        
        return $result_data;
    }
    
    
    /**
     * 
     * @param type $code
     * @param type $language_id
     * @return type 
     */
    public static function get_static_content_by_code($code, $language_id=1)
    {
        return orm::factory("static_content")->where("kod","=",$code)->where("language_id","=",$language_id)->find();
    }
    
    public static function search_config()
    {
        return array(

                  "title"=>"Výsledky ve stránkách",
                  "display_title"=>"page_data.nazev",
                  "display_text"=>"page_data.popis",
                  "search_columns"=>array("page_data.nazev", "page_data.popis", "page_data.uvodni_popis"),
//                  "display_category_title"=>"product_category_data.nazev",
//                  "display_category_text"=>"product_category_data.uvodni_popis",
//                  "search_category_columns"=>array("product_category_data.nazev", "product_category_data.uvodni_popis", "product_category_data.popis")
              
        );
    }
    
   public static function get_reservations($page, $lang_id = 1)
    { 
          $datum=date('Y-m-d');
          $datum=date('Y-m-d',mktime(0,0,0,date("m", strtotime($datum)),1,date("Y", strtotime($datum))));

          $datum=self::add_months($datum, ($page-1)*12);
          $reservations=array();
          $cluny=DB::select("shop_id","nazev")->from("shop_data")->order_by("shop_id")->execute()->as_array();
          
          for ($index = 0; $index < 12; $index++)
          {
              $actual_month=date("m", strtotime($datum));
              $reservations[$index]["mesic"]=self::get_mesic($actual_month, $lang_id);
              $month=date("m", strtotime($datum));
              $day=date('Y-m-d',mktime(0,0,0,$month,1,date("Y", strtotime($datum))));
             while ($month==$actual_month)
              {
                 $i=date("d", strtotime($day));
               $reservations[$index][$i]["datum"]=$day;
              
               foreach ($cluny as $clun) {
              $rezervace=db::select()->from("reservations")->where("datum", "=", $day)
                      ->where("clun_id","=",$clun["shop_id"])->execute()->as_array();
              $reservations[$index][$i]["den"]=self::get_den(date("N", strtotime($day)), $lang_id);
              if(isset($rezervace[0]))
              {
                 
                  $reservations[$index][$i]["clun"][$clun["shop_id"]]["dopo"]=$rezervace[0]["dopo"];
                  $reservations[$index][$i]["clun"][$clun["shop_id"]]["odpo"]=$rezervace[0]["odpo"];  
                                    
              }
              else
              {
                 
                  $reservations[$index][$i]["clun"][$clun["shop_id"]]["dopo"]=0;
                  $reservations[$index][$i]["clun"][$clun["shop_id"]]["odpo"]=0; 
                   
              }
              }
             
                 
                 
                  $day=self::add_days($day,1);
                  $month=date("m", strtotime($day));
              } 
              $datum=self::add_months($datum, 1);
          }
         
          return $reservations;
    }
    
    public static function get_mesic($mesic, $language_id = 1)
    {
        $mesic=(int)$mesic;
        
        if($language_id == 1){
            $nazvy = array(1 => 'leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec');
        } else{
            $nazvy = array(1 => 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december');   
        }
        
        return $nazvy[$mesic];
    }
public static function get_den($den, $lang_id = 1) {
    if ($lang_id == 1)
        $nazvy = array(1 => 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota','neděle');
    else
        $nazvy = array(1 => 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday','sunday');
    return $nazvy[$den];
}
    
     public static function add_months($givendate, $mth) 
     {
         if($mth<0)
             $znamenko="-";
         else
             $znamenko="+";
        $givendate = strtotime($mth." months", strtotime($givendate));
        $givendate = strftime ( '%Y-%m-%d' , $givendate );
        return $givendate;
     }
    
      public static function add_days($givendate,$day=0,$mth=0,$yr=0) {
      $cd = strtotime($givendate);
      $newdate = date('Y-m-d', mktime(date('h',$cd),
    date('i',$cd), date('s',$cd), date('m',$cd)+$mth,
    date('d',$cd)+$day, date('Y',$cd)+$yr));
      return date('Y-m-d',strtotime($newdate));
              }
    
}
?>
