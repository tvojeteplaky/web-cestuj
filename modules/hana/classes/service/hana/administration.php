<?php defined('SYSPATH') or die('No direct script access.');

/**
 * 
 * Servisni trida pro obsluhu zakladnich funkci administracni casti.
 *
 * @package    Hana
 * @author     Pavel Herink
 * @copyright  (c) 2010 Pavel Herink
 */
class Service_Hana_Administration
{

    private static $cache_enabled=true;

    private static $menu_L1_sel;
    private static $menu_L2_enabled=false;
    private static $menu_L2_sel;
    private static $menu_L3_enabled=false;
    private static $menu_L3_sel;

   
    public static function get_main_navigation_links($module, $submodule, $controller)
    {
        // otestuju L3, pripadne urcim vsecky rodice
        self::$menu_L3_sel = DB::select("id","parent_id")->from("admin_structure")->where("admin_menu_section_id","=",3)->where("module_code","=",$module)->where("submodule_code","=",$submodule)->where("module_controller","=",$controller)->execute()->as_array();
        if(empty(self::$menu_L3_sel))
        {
            self::$menu_L3_sel = DB::select("id","parent_id")->from("admin_structure")->where("admin_menu_section_id","=",3)->where("module_code","=",$module)->where("submodule_code","=",$submodule)->execute()->as_array();
        }

        // otestuju L2 a urcim rodice
        if(empty(self::$menu_L3_sel))
        {
            self::$menu_L2_sel = DB::select("id","parent_id")->from("admin_structure")->where("admin_menu_section_id","=",2)->where("module_code","=",$module)->where("submodule_code","=",$submodule)->where("module_controller","=",$controller)->execute()->as_array();
            if(empty(self::$menu_L2_sel))
            {
                self::$menu_L2_sel = DB::select("id","parent_id")->from("admin_structure")->where("admin_menu_section_id","=",2)->where("module_code","=",$module)->where("submodule_code","=",$submodule)->execute()->as_array();
            }
        }
        else
        {
            self::$menu_L2_sel = DB::select("id","parent_id")->from("admin_structure")->where("admin_menu_section_id","=",2)->where("id","=",self::$menu_L3_sel[0]["parent_id"])->execute()->as_array();
        }

        // zvolim polozku v L1
        if(empty(self::$menu_L3_sel) && empty(self::$menu_L2_sel))
        {
            self::$menu_L1_sel = DB::select("id")->from("admin_structure")->where("parent_id","=",0)->where("module_code","=",$module)->where("submodule_code","=",$submodule)->where("module_controller","=",$controller)->execute()->as_array();
            if(empty(self::$menu_L1_sel)) // zkusim zvolit polozku ze stejneho submodulu
            {
                self::$menu_L1_sel = DB::select("id")->from("admin_structure")->where("parent_id","=",0)->where("module_code","=",$module)->where("submodule_code","=",$submodule)->execute()->as_array();
            }
        }
        elseif(!empty(self::$menu_L2_sel))
        {
            self::$menu_L1_sel = DB::select("id")->from("admin_structure")->where("parent_id","=",0)->where("id","=",self::$menu_L2_sel[0]["parent_id"])->execute()->as_array();
        }
        else
        {
             self::$menu_L1_sel = DB::select("id")->from("admin_structure")->where("parent_id","=",0)->where("id","=",self::$menu_L3_sel[0]["parent_id"])->execute()->as_array();
        }


        // test
        //echo("L3:".print_r(self::$menu_L3_sel)." L2:".print_r(self::$menu_L2_sel)." L1:".print_r(self::$menu_L1_sel));

        $links=orm::factory("admin_structure")->where("zobrazit","=",1)->order_by("poradi","asc")->where("parent_id","=",0)->find_all();
        $result_array=array();
        foreach($links as $link)
        {
           $result_array[$link->id]["nazev"]=$link->nazev;
           $result_array[$link->id]["href"]=url::base().Kohana::$index_file."admin/".$link->module_code."/".$link->submodule_code."/".$link->module_controller;
           $result_array[$link->id]["global_access_level"]=$link->global_access_level;
           if($link->id==self::$menu_L1_sel[0]["id"]) $result_array[$link->id]["sel"]="sel";

        }
        
        return $result_array;
    }

    public static function get_subnavigation_links()
    {
        $parent_id=self::$menu_L1_sel[0]["id"];
        $links=orm::factory("admin_structure")->where("zobrazit","=",1)->order_by("poradi","asc")->where("parent_id","=",$parent_id)->where("admin_menu_section_id","=",2)->find_all();
        $result_array=array();
        foreach($links as $link)
        {
           $result_array[$link->id]["nazev"]=$link->nazev;
           $result_array[$link->id]["href"]=url::base().Kohana::$index_file."admin/".$link->module_code."/".$link->submodule_code."/".$link->module_controller;
           $result_array[$link->id]["global_access_level"]=$link->global_access_level;
           if($link->id==self::$menu_L2_sel[0]["id"]) $result_array[$link->id]["sel"]="sel";

        }

        return $result_array;
    }

    public static function get_module_links()
    {
        $parent_id=(isset(self::$menu_L2_sel[0]["id"]))?self::$menu_L2_sel[0]["id"]:self::$menu_L1_sel[0]["id"];
        $links=orm::factory("admin_structure")->where("zobrazit","=",1)->order_by("poradi","asc")->where("parent_id","=",$parent_id)->where("admin_menu_section_id","=",3)->find_all();
        $result_array=array();
        foreach($links as $link)
        {
           if($link->id)
           {
               $result_array[$link->id]["nazev"]=$link->nazev;
               $result_array[$link->id]["pristup"]=$link->global_access_level;
               $result_array[$link->id]["href"]=url::base().Kohana::$index_file."admin/".$link->module_code."/".$link->submodule_code."/".$link->module_controller;
               $result_array[$link->id]["global_access_level"]=$link->global_access_level;
               if(isset(self::$menu_L3_sel[0]) && isset(self::$menu_L3_sel[0]["id"]) && $link->id==self::$menu_L3_sel[0]["id"]) $result_array[$link->id]["sel"]="sel";
           }
        }
        return $result_array;
    }
}
?>
