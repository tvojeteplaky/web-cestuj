<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'Mapa stránek'    => 'Sitemap',
  'Ochrana osobních údajů' => 'Privacy policy',
'Prohlášení o přístupnosti' =>  'Accessibility statement',
  'Více informací' => 'More info',
  'Novinky' => 'Articles',
    'locale'    =>  'en_GB',
    'lang'  =>  'en',
  
  'Home'  => 'Home',
  'index'    => 'index-en',
  'mapa-stranek'    => 'sitemap',
   
  'přihlásit'    => 'Login',
  'prihlasit'    => 'login',
  'ucet-uzivatele'    => 'sitemap',
    
    'více'  =>  'more',
    'Fotogalerie'   =>  'Photo gallery',
    'Najdete nás na Facebooku'  =>  'You can find us on Facebook',
    
    'Místo nenalezeno'  =>  'Not found',
    
    'Klávesové zkratky - rozšířené' =>  'Shortcuts - extended',
    
    
    'Místo ze kterého vyjíždíte'    => 'Your location',
    'Hledat'    =>  'Search',
    
    'obsazeno'  =>  'occupied',
    'volno' =>  'available',
    'Objednat apartmán' =>  'Apartment order',
    'Jméno' =>  'Name',
    'Telefon'   =>  'Phone number',
    'Apartmán'  =>  'Apartment',
    'Termín rezervace'  =>  'Reservation date',
    'Od'    =>  'From',
    'Do'    =>  'To',
    'ZÁVAZNĚ REZERVOVAT'    =>  'RESERVE',
    'Objednávka apartmánu'  =>  'Apartment order',
    'Příjezd'   =>  'Arrival',
    'Odjezd'   =>  'Departure',
    'odpoledne' =>  'afternoon',
    'dopoledne' =>  'morning',
    'Vaše rezervace byla uložena'   =>  'Your reservation was saved.',
    "Počet osob" => "Number of persons",
    "Osoby" => "Persons",
    "Příjmení" => "Surname",
    "Datum narození" => "Birthdate",
    "Platba převodem v" => "Payment transfer in",
    "Rezervovat" => "Reserve",
    "Rezervovat apartmán" => "Reserve apartment",
    "Storno" => "Cancle",
    "Narozen" => "Born",
    
    
  );
