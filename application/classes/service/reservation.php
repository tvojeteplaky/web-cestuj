<?php

/**
 * Class Service_Reservation
 */

class Service_Reservation extends Service_Hana_Module_Base
{
	// Měsíce v češtině
	private static $czMesice = array('leden','únor','březen','duben','květen','červen','červenec','srpen','září','říjen','listopad','prosinec');
	private static $mimo_sezonu =array("month"=>11,"day" => 1);
	private static $sezona = array("month" =>5, "day"=>1);



	public static function get_actual_price($apartman_id = null,$currency=1) {
		$date = new DateTime();

		$mysql_format = $date->format("Y-m-d");

		$tarif = orm::factory("reservation_price")
				->where('termin_od','<=',$mysql_format)
				->where('termin_do','>=',$mysql_format)
				->where('shop_id','=',$apartman_id)
			   ->find();
		if(!$tarif->id) {
			$tarif = orm::factory("reservation_price")
				->where('termin_od','<=',$mysql_format)
				->where('termin_do','>=',$mysql_format)
				->where("shop_id","=", 0)
			   ->find();
			 return $tarif->price;

		} else {
			return $tarif->price;
		}
	}

	/**
	* Funkce vracející ceny za rezervované období
	*
	* Nejdříve se zjistí cena za ubytování v daném apartmánu za daný počet dní.
	* Zjistí se počet lidí starších 15 let. Připočte se sleva.
	* Připočítají a spočítají se povinné poplatky a příplatky.
	* Pokud je jiná měna tak se cena přepočítá na novou měnu.
	*
	*
	* @param int $reservation_id
	* @param int $apartman_id
	* @param array $dates
	* @param int $discount
	* @param int $currency
	*
	* @return array
	*/
	public static function recount_total_price($reservation_id,$apartman_id,$dates=array(),$discount=0,$currency=1){
		// definování proměných
		$begin = new DateTime($dates[0]);
		$end = new DateTime($dates[1]);
		$interval = new DateInterval('P1D');
		$daterange = new DatePeriod($begin, $interval ,$end);
		$more_then_fifteen = 0;
		$result_data = array();
		$price_basic = 0;
		$price_total = 0;
		$change = 1;

		// Definování modelů a jiných potřebných věcí
		$reservation = orm::factory('reservation',$reservation_id);
		$apartman = $reservation->apartman->find();
		$people = $reservation->customers->find_all();
		$currency_old = $reservation->currency->find();
		$dDiff = $begin->diff($end);
		$currency_new = orm::factory('reservation_currency',$currency);

		// Je-li měna rozdílná spočítá se hodnota v nové měně
		if($currency_old->id != $currency_new->id) {
			$change = $currency_new->value /$currency_old->value;
		}

		// Vypočítání základní ceny pouze za ubytování
		$result_data['tarifs'] = array();
		
		foreach ($daterange as $date) {
			$mysql_format = $date->format('Y-m-d');
			$month = (int)$date->format("m");
			$day = (int)$date->format("d");
			if((self::$sezona["month"]<$month && self::$sezona["day"]<$day) && (self::$mimo_sezonu["month"]>$month && self::$mimo_sezonu["day"]>$day)) {
				$tarif["nazev"] = "Sezona" . $apartman->nazev ;
				$tarif["price"] = $apartman->cena_sezona;
				$i=0;
			} else {
				$tarif["nazev"] = "Sezona" . $apartman->nazev ;
				$tarif["price"] = $apartman->cena_mimo_sezona;
				$i=1;
			}
			// $tarif = orm::factory("reservation_price")
			// 	->where('termin_od','<=',$mysql_format)
			// 	->where('termin_do','>=',$mysql_format)
			//    ->find();

			// Data o cenách potřebná do tabulek
			if(empty($result_data['tarifs'][$i])) {
				$result_data['tarifs'][$i]['nights'] = 1;
				$result_data['tarifs'][$i]['name'] = $tarif["nazev"] . ' '. $tarif["price"] . "KČ/DEN";
				$result_data['tarifs'][$i]['only_name'] = $tarif["nazev"];
				$price =($reservation->free || $reservation->tax_free)?0:$tarif["price"] ;
				$result_data['tarifs'][$i]['price_night'] = $price.' EUR/NOC';
				$result_data['tarifs'][$i]['price'] = $price*$change;
			} else {
				$price =($reservation->free || $reservation->tax_free)?0:$tarif["price"];
				$result_data['tarifs'][$i]['nights']++;
				$result_data['tarifs'][$i]['price']+=$price*$change;
			}
			$price =($reservation->free || $reservation->tax_free)?0:$tarif["price"];
			$price_basic += $price;
		}

		// Kolika lidem je více jak 15 let?
		// foreach ($people as $human) {
		// 	$born = new DateTime($human->born);
		// 	$yearDiff = $born->diff($begin);
		// 	if ($yearDiff->y >= 15){
		// 		$more_then_fifteen++;
		// 	}
		// }

		$price_total = $price_basic;
		// Sleva
		if($discount>0) {
			$discount = $discount/100;
			$price_total -= $price_basic*$discount;

			foreach ($result_data['tarifs'] as $key=>$tarif) {
				$result_data['tarifs'][$key]['price'] -= $tarif['price']*$discount;
			}
		}

		$result_data['extras'] = array();
		if(!$reservation->free){
		// Připočítání poplatků a příplatků

		$price_total += $apartman->poplatek_energie;
		//$price_total += $apartman->poplatek_energie * $dDiff->days;
		//$price_total += $apartman->priplatek_taxa * $more_then_fifteen * $dDiff->days;
	 	}

		$result_data['extras']['days'] = $dDiff->days;
		//$result_data['extras']['tax']['people'] = $more_then_fifteen;
		if(!$reservation->free){
			$result_data['extras']['services'] = $apartman->poplatek_energie*$change;
		} else {
			$result_data['extras']['services'] = 0;
		}
		
		$result_data['total'] = ceil($price_total*$change);
		$result_data['advance'] = ceil(($price_total*$change)/2);
		$result_data['additional'] = $result_data['total']-$result_data['advance'];
		$result_data['corection'] = abs($result_data["total"] - ($price_total*$change));


		//vrácení všech cen za ubytování
		return $result_data;
	}

	/**
	* Funkce kontrolující dostupnost apartmanu v datumovém rozpětí
	*
	*/
	public static function is_available($reservation_id,$apartman_id,$dates =array())
	{
		$beginD = new DateTime($dates[0]);
		$endD = new DateTime($dates[1]);

		$interval = new DateInterval('P1D');
		$daterange = new DatePeriod($beginD, $interval ,$endD);
		$count = 0;
			foreach ($daterange as $date) {
				$mysql_format = $date->format("Y-m-d");
				$reservation = orm::factory('reservation')
					->where('apartman_id','=',$apartman_id)
					->where('termin_od','<',$mysql_format)
					->where('termin_do','>',$mysql_format)
					->where('id','!=',$reservation_id)
					->find();
				if($reservation->id && $reservation->id != $reservation_id){
					return false;
				}
			}
		return true;
	}

	public static function get_reservations_ordered_by_date($apartmans,$begin_month,$months = 12)
	{
		//definice proměných
		$beginD 		= new DateTime();
		$endD 			= new DateTime();
		$result_data	= array();
		$interval 		= new DateInterval('P1D');

		//nastavení proměných
		$beginD->setDate($beginD->format('Y'),$begin_month,1);
		$endD->setDate($beginD->format('Y'),$begin_month+$months,1);



		// hledání po dnech
		foreach ($apartmans as $apartman) {

			//definování časové periody
			$daterange = new DatePeriod($beginD, $interval ,$endD);

			foreach($daterange as $date) {
				// Základní definice do returnu
				$mysql_format 				= $date->format('Y-m-d');

				if(empty($result_data[$apartman->id])) {
					$result_data[$apartman->id]	= $apartman->as_array();
				}

					$result_data[$apartman->id]['months'][$date->format('n')]['month'] 	= self::$czMesice[$date->format('n')-1];


				$result_data[$apartman->id]['months'][$date->format('n')]['days'][$date->format('j')]['date'] 	= $date;


				// Načtení rezervací z DB
				$reservation = orm::factory('reservation')
					->where('termin_od','<=',$mysql_format)
					->where('termin_do','>',$mysql_format)
					->where('apartman_id','=',$apartman->id)
					->find();

				//reservace do result data
				if(is_string($reservation->id)){
					$result_data[$apartman->id]['months'][$date->format('n')]['days'][$date->format('j')]["rezervace"] = true;
				} else if(is_null($reservation->id)) {
					$result_data[$apartman->id]['months'][$date->format('n')]['days'][$date->format('j')]["rezervace"] = false;
				}

			}
		}

		return $result_data;
	}
	/**
	 * Pridání nové rezervace z frondendu
	 *
	 * Nejdříve zjistíme jestli není apartmán obsazený v daný termín.
	 * Vložíme do db základní data. Vyřešíme relace.
	 *
	 * @param array $data
	 * @return boolean
	 */
	public static function add_new_reservation($data)
	{
		if(!self::is_available(0,$data['apartman'],array($data['termin_od'],$data['termin_do']))){
			//die(var_dump($data['apartman']));
			$apartman = orm::factory('shop')->where("shops.id","=",$data['apartman'])->where('language_id','=',1)->find();

			throw new Exception("REZERVOVANÝ KARAVAN ".mb_strtoupper($apartman->nadpis)." JE JIŽ V POŽADOVANÉM TERMÍNU OBSAZEN, ZADEJTE JINÝ KARAVAN, DĚKUJEME");
		}

		$reservation = orm::factory('reservation');

		// poslední kod(variabilní symbol)
		$last_code = orm::factory('reservation')->order_by('code','DESC')->find();
		if($last_code->code >999){
			$reservation->code = $last_code->code+1;
		} else {
			$reservation->code = 1000;
		}

		// převedení na mysql format (pro jistotu)
		$termin_od = new DateTime($data['termin_od']);
		$termin_do = new DateTime($data['termin_do']);
		$reservation->termin_od = $termin_od->format('Y-m-d');
		$reservation->termin_do = $termin_do->format('Y-m-d');

		$reservation->email = $data['email'];
		$reservation->phone = $data['phone'];
		$reservation->prevodem = $data['prevodem'];
		$reservation->discount = 0;
		$reservation->splatnost = 7;
		$reservation->note = $data['note'];
		$splatnost_zaloha = new DateTime();
		$splatnost_zaloha->modify("+3days");
		$reservation->splatnost_zaloha_at = $splatnost_zaloha->format('Y-m-d');
		
		$splatnost = new DateTime();
		$splatnost->modify("+7days");
		$reservation->splatnost_at = $splatnost->format('Y-m-d');


		//určení měny
		$currency = orm::factory('reservation_currency',$data['mena']);
		if($currency->id){
			$reservation->currency_id = $currency->id;
		} else {
			$reservation->currency_id = 7;
		}

		// určení aparmanu
		$apartman = orm::factory('shop',$data['apartman']);
		if($apartman->id){
			$reservation->apartman_id = $apartman->id;
		} else {
			throw new Exception("Neexistující apartmán");
		}
		// uložení a vytvoření lidí
		if($reservation->save()){
			//1-4 zákazníci zařízeno na frontendu
			for ($i=0; $i < count($data['firstname']); $i++) {
				$customer = orm::factory('reservation_customer');
				$customer->firstname = $data['firstname'][$i];
				$customer->lastname = $data['lastname'][$i];

				// narozen mysql format (jistota)
				// $born = new DateTime($data['born'][$i]);
				// $customer->born = $born->format('Y-m-d');
				$customer->reservation_id = $reservation->id;
				if(!$customer->save()){
					throw new Exception("Ani očko nenasadíš");
				}
			}
			return $reservation->id;
		} else {
			throw new Exception("Nejde uložit");
		}
	}

	/**
	 * Informace o jedné konkrétní rezervaci i s cenami
	 *
	 */
	public static function get_reservation($id, $currency_def = null)
	{
		$result = array();
		$reservation = orm::factory('reservation',$id);
		$begin = new DateTime($reservation->termin_od);
		$end = new DateTime($reservation->termin_do);
		$interval = new DateInterval('P1D');
		$daterange = new DatePeriod($begin, $interval ,$end);

		$apartman = orm::factory("shop")->where("shops.id","=",$reservation->apartman_id)->find();
		$currency = orm::factory('reservation_currency',$reservation->currency_id);
		$person = $reservation->customers->find();
		$people = $reservation->customers->find_all();

		$result = $reservation->as_array();
		$result['ucet'] = $currency->ucet;
		if($apartman->currency->id > 0){
			$result['ucet'] = $apartman->currency->ucet;
		}
		$result['reservation_code'] = $reservation->code;
		$result['apartman'] = $apartman->as_array();
		$result['currency'] = $currency->as_array();
		$result['jmeno'] = $person->firstname . ' ' . $person->lastname;
		$result['people'] = $people->as_array();


		//Splatnost zálohy
		$zaplat_zalohu_do = new DateTime();
		if(!empty($reservation->splatnost_zaloha_at)) {
			$zaplat_zalohu_do = new DateTime($reservation->splatnost_zaloha_at);
		} else {
			$zaplat_zalohu_do->add(new DateInterval('P3D'));
		}
		$result['splat_zalohu_do'] = $zaplat_zalohu_do->format('j.n.Y');


		//Splatnost doplatku
		$zaplat_do = new DateTime();
		if(!empty($reservation->splatnost_at)) {
			$zaplat_do = new DateTime($reservation->splatnost_at);
		} else {
			$zaplat_do->add(new DateInterval('P7D'));
		}
		$result['splat_do'] = $zaplat_do->format('j.n.Y');


		$currency_prices = is_null($currency_def)?$reservation->currency_id:$currency_def;

		$result['prices'] = self::recount_total_price($reservation->id,$reservation->apartman_id,array($reservation->termin_od,$reservation->termin_do), $reservation->discount, $currency_prices);
		return $result;
	}
}
