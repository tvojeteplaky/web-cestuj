<?php defined('SYSPATH') or die('No direct script access.');

/**
 * 
 * Servisa pro obsluhu clanku.
 *
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class Service_Reference extends Service_Hana_Module_Base
{
    public static $navigation_module="reference";
    public static $order_by="poradi";
    public static $order_direction="desc";


    public static $photos_resources_dir="media/photos/";
    public static $photos_resources_subdir="";
    
    /**
     * Nacte clanek dle route_id
     * @param int $id
     * @return array 
     */
    public static function get_reference_by_route_id($id)
    {
        $reference= orm::factory(self::$navigation_module)->where("route_id","=",$id)->find();
        
        $result_data=array();
        $result_data=$reference->as_array();
        $result_data["nazev_seo"]=$reference->route->nazev_seo;

        $filename=self::$photos_resources_dir.self::$navigation_module."/item/".self::$photos_resources_subdir."images-".$reference->id."/".$reference->photo_src."-t2.jpg";
        if(file_exists(str_replace('\\', '/',DOCROOT).$filename))
        {
            $result_data["photo"]=url::base().$filename;
        }
        else
        {
            $result_data["photo"]=false;
        }
        
        $photos=$reference->reference_photos->where("reference_photos.zobrazit","=",1)->order_by("poradi")->order_by("poradi")->find_all();
        $dirname=self::$photos_resources_dir."reference/item/gallery/images-".$reference->reference_id."/";
        $photos_array=array();
        $y=0;
        foreach($photos as $photo)
        {
            if($photo->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$dirname.$photo->photo_src."-t2.jpg"))
            {
                $photos_array[$y]["photo"]=url::base().$dirname.$photo->photo_src."-t2.jpg";
                $photos_array[$y]["photo_big"]=url::base().$dirname.$photo->photo_src."-t3.jpg";
                $photos_array[$y]["photo_seo"]=$photo->photo_src;
                $photos_array[$y]["photo_detail"]=url::base().$dirname.$photo->photo_src."-ad.jpg";
                $photos_array[$y]["nazev"]=$photo->nazev;
                $y++;
            }
        }
          $result_data["fotogalerie"]=$photos_array;   
        
        
        return $photos_array;
    }
    
      public static function get_reference_on_homepage()
    {
        $reference= orm::factory(self::$navigation_module)->where("on_homepage","=",1)->find();
        
        $result_data=array();
        $result_data=$reference->as_array();
        $result_data["nazev_seo"]=$reference->route->nazev_seo;

        $filename=self::$photos_resources_dir.self::$navigation_module."/item/".self::$photos_resources_subdir."images-".$reference->id."/".$reference->photo_src."-t2.jpg";
        if(file_exists(str_replace('\\', '/',DOCROOT).$filename))
        {
            $result_data["photo"]=url::base().$filename;
        }
        else
        {
            $result_data["photo"]=false;
        }
        
        $photos=$reference->reference_photos->where("reference_photos.zobrazit","=",1)->order_by("poradi")->order_by("poradi")->find_all();
        $dirname=self::$photos_resources_dir."reference/item/gallery/images-".$reference->reference_id."/";
        $photos_array=array();
        $y=1;
        foreach($photos as $photo)
        {
            if($photo->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$dirname.$photo->photo_src."-t1.jpg"))
            {
                $photos_array[$y]["photo"]=url::base().$dirname.$photo->photo_src."-t1.jpg";
                $photos_array[$y]["photo_seo"]=$photo->photo_src;
                $photos_array[$y]["photo_detail"]=url::base().$dirname.$photo->photo_src."-ad.jpg";
                $photos_array[$y]["nazev"]=$photo->nazev;
                $y++;
            }
        }
          $result_data["fotogalerie"]=$photos_array;   
        
        
        return $photos_array;
    }
    
    public static function get_reference_total_items_list($language_id,$category=0)
    {
        return DB::select(db::expr("COUNT(references.id) as pocet"))->from("references")->join("reference_data")->on("references.id","=","reference_data.reference_id")->join("routes")->on("reference_data.route_id","=","routes.id")->where("routes.zobrazit","=",1)->where("routes.language_id","=",$language_id)->execute()->get("pocet");   
    }
    
    /**
     * Nacte sadu clanku podle kategorie a jazykove verze
     * @param type $language_id
     * @return boolean 
     */
    public static function get_reference_list($language_id,$category=0,$limit=100,$offset=0,$homepage=false)
    {
        $references=orm::factory("reference")
                ->join("routes")->on("reference_data.route_id","=","routes.id")
                ->where("language_id","=",$language_id)
                //->where("reference_category_id","=",db::expr($category))
                ->where("zobrazit","=",1)
                ->where("on_homepage","=",0)
                ->order_by(self::$order_by,self::$order_direction)
                ->limit($limit)
                ->offset($offset)
                ->find_all();
        
        $result_data=array();
        foreach ($references as $reference)
        {
            $result_data[$reference->id]=$reference->as_array();
            $result_data[$reference->id]["nazev_seo"]=$reference->route->nazev_seo;
            
            
            // hlavni fotka
            $filename=self::$photos_resources_dir.self::$navigation_module."/item/".self::$photos_resources_subdir."images-".$reference->id."/".$reference->photo_src."-t3.jpg";
            if(file_exists(str_replace('\\', '/',DOCROOT).$filename))
            {
                $result_data[$reference->id]["photo"]=url::base().$filename;
            }
            else
            {
                $result_data[$reference->id]["photo"]=false;
            }
            
            //fotky do galerie
            
             $ref=orm::factory("reference")->where("reference_id","=",$reference->id)->find();
         
            $photos=$ref->reference_photos->where("reference_photos.zobrazit","=",1)->order_by("poradi")->order_by("poradi")->find_all();
        $dirname=self::$photos_resources_dir."reference/item/gallery/images-".$ref->reference_id."/";
        $photos_array=array();
        $y=1;
        foreach($photos as $photo)
        {
            if($photo->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$dirname.$photo->photo_src."-t2.jpg"))
            {
                $photos_array[$y]["photo"]=url::base().$dirname.$photo->photo_src."-t2.jpg";
                $photos_array[$y]["photo_seo"]=$photo->photo_src;
                $photos_array[$y]["photo_detail"]=url::base().$dirname.$photo->photo_src."-ad.jpg";
                $photos_array[$y]["nazev"]=$photo->nazev;
                $y++;
            }
        }
          $result_data[$reference->id]["fotogalerie"]=$photos_array;   
        }

        return $result_data;
    }
}
?>

