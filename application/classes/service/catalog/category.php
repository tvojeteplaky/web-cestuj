<?php defined('SYSPATH') or die('No direct script access.');

/**
 *
 * Servisa pro obsluhu katalogu kategorii produktu.
 *
 * @author     Pavel Herink
 * @copyright  (c) 2010 Pavel Herink
 */

class Service_Catalog_Category implements Interface_Searchable 
{
    public static $photos_resources_dir="media/photos/";
    public static $files_resources_dir="media/files/";
    
    public static $default_order_by="price";
    public static $default_order_direction="asc";
    
    
    public static function get_product_category_by_route_id($route_id=false, $parameters=array())
    {
        // ziskani kategorie
        $category=orm::factory("product_category")->where("product_category_data.route_id","=",$route_id);
        $category->where("zobrazit","=",1);
        $category=$category->find();
        $nazev_seo=$category->route->nazev_seo;

        // Generovani vypisu produktu
        $category_data=array();
        $category_data=$category->as_array();
        
        
//        $dirname=self::$photos_resources_dir."product/category/images-".$category->id."/";
//
//
//        if($category->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$dirname.$category->photo_src."-t1.jpg"))
//        {
//            $product_data["photo_detail"]=url::base().$dirname.$category->photo_src."-t1.jpg";
//        }

        return $category_data;
    }
    
    
    public static function get_product_categories_by_parent_category_id($parent_id)
    {
        // ziskani kategorie
        $categories=orm::factory("product_category")->where("product_categories.parent_id","=",$parent_id);
        $categories->where("zobrazit","=",1);
        $categories=$categories->find_all();
        
        $category_data=array();
        
        foreach ($categories as $category)
        {
            $category_data[$category->id]=$category->as_array();
            $category_data[$category->id]["nazev_seo"]=$category->route->nazev_seo;
            
            
            $dirname=self::$photos_resources_dir."catalog/category/images-".$category->id."/";

            if($category->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$dirname.$category->photo_src."-t3.jpg"))
            {
                $category_data[$category->id]["photo_detail"]=url::base().$dirname.$category->photo_src."-t3.jpg";
            } 
        }
        // Generovani vypisu produktu      
        
        return $category_data;
    }
    
    
    
    public static function get_navigation_category_breadcrumb($nazev_seo)
    {
        $result_data = DB::select("product_category_data.nazev","routes.nazev_seo","routes.language_id","product_categories.parent_id")
                ->from("product_categories")
                ->join("product_category_data")->on("product_categories.id","=","product_category_data.product_category_id")
                ->join("routes")->on("product_category_data.route_id","=","routes.id")
                ->where("routes.nazev_seo","=",$nazev_seo)
                //->where("routes.zobrazit","=",DB::expr(1))
                ->execute()->as_array();
        
        if(isset($result_data[0]))
        {
            $result_data=$result_data[0];
            $result_data["parent_nazev_seo"]="";
            if($result_data["parent_id"]>0)
            {
                    $parent=DB::select("nazev_seo")
                    ->from("product_category_data")
                    ->join("routes")->on("product_category_data.route_id","=","routes.id")->on("routes.language_id","=",DB::expr($result_data["language_id"]))
                    ->where("product_category_data.product_category_id","=",$result_data["parent_id"])
                    ->execute()->as_array();
            $result_data["parent_nazev_seo"]=$parent[0]["nazev_seo"];
            }
        
            return($result_data);
        }
        else
        {
            return array();
        }
  
    }
    
    // konfigurace vyhledavani
    public static function search_config()
    {
        return array(
                  "title"=>"Kontinenty",
                  "display_title"=>"product_category_data.nazev",
                  "display_text"=>"product_category_data.uvodni_popis",
                  "search_columns"=>array("product_category_data.nazev", "product_category_data.uvodni_popis", "product_category_data.popis")
        );
    }
}

?>
