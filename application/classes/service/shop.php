<?php defined('SYSPATH') or die('No direct script access.');

/**
 * 
 * Servisa pro obsluhu clanku.
 *
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class Service_Shop extends Service_Hana_Module_Base
{
    public static $navigation_module="shop";
    public static $order_by="date";
    public static $order_direction="desc";


    public static $photos_resources_dir="media/photos/";
    public static $photos_resources_subdir="";
    
    /**
     * Nacte clanek dle route_id
     * @param int $id
     * @return array 
     */
    public static function get_shop_by_route_id($id)
    {
        $shop= orm::factory(self::$navigation_module)->where("route_id","=",$id)->find();
        
        $result_data=array();
        $result_data=$shop->as_array();
        $result_data["nazev_seo"]=$shop->route->nazev_seo;

        $filename=self::$photos_resources_dir.self::$navigation_module."/item/".self::$photos_resources_subdir."images-".$shop->id."/".$shop->photo_src."-t1.jpg";
        $filename2=self::$photos_resources_dir.self::$navigation_module."/item/".self::$photos_resources_subdir."images-".$shop->id."/".$shop->photo_src."-ad.jpg";

        if(file_exists(str_replace('\\', '/',DOCROOT).$filename))
        {
            $result_data["photo"]=url::base().$filename;
            $result_data["photo_detail"]=url::base().$filename2;
        }
        else
        {
            $result_data["photo"]=false;
        }
        
         $art=orm::factory("shop")->where("shop_id","=",$shop->id)->find();
         
            $photos=$art->shop_photos->where("shop_photos.zobrazit","=",1)->order_by("poradi")->order_by("poradi")->find_all();
        $dirname=self::$photos_resources_dir."shop/item/gallery/images-".$art->shop_id."/";
        $photos_array=array();
        $y=1;
        foreach($photos as $photo)
        {
            if($photo->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$dirname.$photo->photo_src."-t1.jpg"))
            {
                $photos_array[$y]["photo"]=url::base().$dirname.$photo->photo_src."-t1.jpg";
                $photos_array[$y]["photo_seo"]=$photo->photo_src;
                $photos_array[$y]["photo_detail"]=url::base().$dirname.$photo->photo_src."-ad.jpg";
                $photos_array[$y]["nazev"]=$photo->nazev;
                $y++;
            }
        }
          $result_data["fotogalerie"]=$photos_array;   
       
        return $result_data;
    }
    
    public static function get_shop_total_items_list($language_id,$category=0)
    {
        return DB::select(db::expr("COUNT(shops.id) as pocet"))->from("shops")->join("shop_data")->on("shops.id","=","shop_data.shop_id")->join("routes")->on("shop_data.route_id","=","routes.id")->where("routes.zobrazit","=",1)->where("routes.language_id","=",$language_id)->execute()->get("pocet");   
    }
    
    /**
     * Nacte sadu clanku podle kategorie a jazykove verze
     * @param type $language_id
     * @return boolean 
     */
    public static function get_shop_list($language_id,$category=0,$limit=100,$offset=0,$homepage=false)
    {
        $shops=orm::factory("shop")
                ->join("routes")->on("shop_data.route_id","=","routes.id")
                ->where("language_id","=",$language_id)
                //->where("shop_category_id","=",db::expr($category))
                ->where("zobrazit","=",1)
                ->order_by(self::$order_by,self::$order_direction)
                ->limit($limit)
                ->offset($offset)
                ->find_all();
        
        $result_data=array();
        $application_context=Hana_Application::instance();
        $route_id=$application_context->get_actual_route_id();
        foreach ($shops as $shop)
        {
            $result_data[$shop->id]=$shop->as_array();
            $result_data[$shop->id]["nazev_seo"]=$shop->route->nazev_seo;
            $result_data[$shop->id]["price"] = Service_Reservation::get_actual_price($shop->id);
            if($result_data[$shop->id]["route_id"]==$route_id)
                $result_data[$shop->id]["active"]=true;
            else
                $result_data[$shop->id]["active"]=false;
            
            $filename=self::$photos_resources_dir.self::$navigation_module."/item/".self::$photos_resources_subdir."images-".$shop->id."/".$shop->photo_src."-t3.jpg";
            if(file_exists(str_replace('\\', '/',DOCROOT).$filename))
            {
                $result_data[$shop->id]["photo"]=url::base().$filename;
            }
            else
            {
                $result_data[$shop->id]["photo"]=false;
            }
            
        }

        return $result_data;
    }
    
    /**
     * Nacte sadu clanku podle kategorie a jazykove verze
     * @param type $language_id
     * @param type $category
     * @return boolean 
     */
    public static function get_shop_banner_list($language_id,$category=0)
    {
        $shops=orm::factory("shop")
                ->where("language_id","=",$language_id)
                ->where("shop_category_id","=",db::expr($category))
                ->where("zobrazit","=",1)
                ->where("zobrazit_rotator","=",1)
                ->order_by(self::$order_by,self::$order_direction)
                ->find_all();
        
        $result_data=array();
        foreach ($shops as $shop)
        {
            $result_data[$shop->id]=$shop->as_array();
            $result_data[$shop->id]["nazev_seo"]=$shop->route->nazev_seo;
            
            $filename=self::$photos_resources_dir.self::$navigation_module."/wide_item/".self::$photos_resources_subdir."images-".$shop->id."/".$shop->photo_src."-t3.jpg";
            if(file_exists(str_replace('\\', '/',DOCROOT).$filename))
            {
                $result_data[$shop->id]["photo"]=url::base().$filename;
            }
            else
            {
                $result_data[$shop->id]["photo"]=false;
            }
            
        }
        //die(print_r($result_data));
        return $result_data;
    }  
  
}
?>

