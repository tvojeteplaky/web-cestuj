<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Generovani statickych obsahovych stranek a textu.
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class Controller_Page extends Controller
{
    /**
     * Staticka promenna kvuli urceni typu subnavigace v navaznosti na hlavni stranku. 
     * @var type 
     */
    public static $current_page_category_id=3;
    
    /**
     * Metoda generujici obsah uvodni stranku.
     */
    public function action_index()
    {
        //Hana_Widgets::instance()->assign("MujWidget", "common/widget2/index");
        //Hana_Widgets::instance()->remove("MujWidget");
        
        $route_id=$this->application_context->get_actual_route_id();
        $template=new View("homepage");
        $template->item=Service_Page::get_page_by_route_id($route_id);
        $template->gallery=Service_Reference::get_reference_on_homepage();

		$query = DB::query(Database::SELECT, 'SELECT * FROM page_photos INNER JOIN page_photo_data ON page_photos.id = page_photo_data.page_photo_id WHERE page_id = :page_id AND zobrazit = 1 ORDER BY poradi ASC');
		$query->param(':page_id', $template->item['id']);
		$photos = $query->execute()->as_array();
		$template->photos = $photos;

        $this->request->response=$template->render();
    }
    
    public function action_reservation()
    {

        $route_id=$this->application_context->get_actual_route_id();
        $lang_id = $this->application_context->get_actual_language_id();
        
        $template=new View("page_reservations");
        

        $template->item=Service_Page::get_page_by_route_id($route_id);
        $table=new View("reservation_front");
        $apartmans=orm::factory('shop')->where('language_id','=',$lang_id)->find_all();
        $table->reservations=Service_Reservation::get_reservations_ordered_by_date($apartmans,date('m'),12);
        $template->data=$this->response_object->get_data();
        $errors=$this->response_object->get_errors();
        $template->errors=!empty($errors["reservation"])?$errors["reservation"]:array();
        $template->send=$this->response_object->get_status();

        $template->table= $table->render();
        $template->apartmans = $apartmans;
        $this->request->response=$template->render();
    }

    public function action_apartman()
    {
        //Hana_Widgets::instance()->assign("MujWidget", "common/widget2/index");
        //Hana_Widgets::instance()->remove("MujWidget");
        
        $route_id=$this->application_context->get_actual_route_id();
        $language_id = $this->application_context->get_actual_language_id();
        $template=new View("page_apartman");
        $template->item=Service_Page::get_page_by_route_id($route_id);
        $template->gallery=Service_Reference::get_reference_by_route_id(80);
        $template->apartmans=Service_Shop::get_shop_list($language_id);
        $this->request->response=$template->render();
    }
    
       public function action_contact()
    {
        //Hana_Widgets::instance()->assign("MujWidget", "common/widget2/index");
        //Hana_Widgets::instance()->remove("MujWidget");
        
         $route_id=$this->application_context->get_actual_route_id();
        $page=Service_Page::get_page_by_route_id($route_id);
        $nazev_seo=$this->application_context->get_actual_seo();
        
      
            $template=new View("page_contact");
      
        
        $template->nazev_seo=$nazev_seo;
        
        $template->item=$page;

        $sublinks=((Hana_Navigation::instance()->get_navigation($this->application_context->get_actual_language_id(),self::$current_page_category_id)));
        $template->sublinks=isset($sublinks[$this->application_context->get_actual_seo()]["children"])?$sublinks[$this->application_context->get_actual_seo()]["children"]:array();
        
        self::$current_page_category_id=$page["page_category_id"];
        
        if(isset($_POST["unlock"])){Session::instance()->set("page_unlock", true);}
        
        /* chranene stranky */
        if($page["protected"])
        {
            if(Session::instance()->get("page_unlock", false))
            {
                $template->protected=false; 
            }
            else
            {
                $template->protected=true;
            }
        }
        else
        {
            $template->protected=false;
        }
        
        
        $this->request->response=$template->render();
    }
    
    /**
     * Metoda generujici vsechny stranky vkladane do hlavniho obsahu.
     */
    public function action_detail()
    {
        $route_id=$this->application_context->get_actual_route_id();
        $page=Service_Page::get_page_by_route_id($route_id);
        $nazev_seo=$this->application_context->get_actual_seo();
        
        if($page["show_child_pages_index"])
        {
            $this->action_index();
            return;
        }
        else
        {
            $template=new View("page");
        }
        
        $template->nazev_seo=$nazev_seo;
        
        $template->item=$page;

        $sublinks=((Hana_Navigation::instance()->get_navigation($this->application_context->get_actual_language_id(),self::$current_page_category_id)));
        $template->sublinks=isset($sublinks[$this->application_context->get_actual_seo()]["children"])?$sublinks[$this->application_context->get_actual_seo()]["children"]:array();
        
        self::$current_page_category_id=$page["page_category_id"];
        
        if(isset($_POST["unlock"])){Session::instance()->set("page_unlock", true);}
        
        /* chranene stranky */
        if($page["protected"])
        {
            if(Session::instance()->get("page_unlock", false))
            {
                $template->protected=false; 
            }
            else
            {
                $template->protected=true;
            }
        }
        else
        {
            $template->protected=false;
        }
        
        
        $this->request->response=$template->render();
    }
    
        public function action_camera()
    {
        $route_id=$this->application_context->get_actual_route_id();
        $page=Service_Page::get_page_by_route_id($route_id);
        $nazev_seo=$this->application_context->get_actual_seo();
        $template=new View("page_camera");
         
        $template->nazev_seo=$nazev_seo;
        
        $template->item=$page;

        $sublinks=((Hana_Navigation::instance()->get_navigation($this->application_context->get_actual_language_id(),self::$current_page_category_id)));
        $template->sublinks=isset($sublinks[$this->application_context->get_actual_seo()]["children"])?$sublinks[$this->application_context->get_actual_seo()]["children"]:array();
        
        self::$current_page_category_id=$page["page_category_id"];
        
        if(isset($_POST["unlock"])){Session::instance()->set("page_unlock", true);}
        
        /* chranene stranky */
        if($page["protected"])
        {
            if(Session::instance()->get("page_unlock", false))
            {
                $template->protected=false; 
            }
            else
            {
                $template->protected=true;
            }
        }
        else
        {
            $template->protected=false;
        }
        
        
        $this->request->response=$template->render();
    }

    public function action_static($kod)
    {
        
        $language_id=$this->application_context->get_actual_language_id();
        $this->request->response=Service_Page::get_static_content_by_code($kod, $language_id)->popis;
    }

    /* 
     * @deprecated -> action_detail
     */
    public function action_unrelated($nazev_seo)
    {
        
    }
    
    public function action_page_subnav($nazev_seo)
    {
        $subnav=new View("subnav");
        $links     = Hana_Navigation::instance()->get_navigation($this->application_context->get_actual_language_id(),self::$current_page_category_id);
        $sel_links = Hana_Navigation::instance()->get_navigation_breadcrumbs();
        //die(print_r($links));
        //die(print_r($sel_links));
        $sel_link=array_pop($sel_links);
        $subnav->title=$sel_link["nazev"];
        
        $subnav->links = !empty($links[$sel_link["nazev_seo"]]["children"])?$links[$sel_link["nazev_seo"]]["children"]:array();
        $subnav->sel_links = $sel_links; 
        $this->request->response=$subnav->render();
    }

    public function action_sitemap()
    {
        $template=new View("page");
        $route_id=$this->application_context->get_actual_route_id();
        $page=Service_Page::get_page_by_route_id($route_id);
        
        // vylistovani vsech viditelnych "struktur"
        $sitemap=new View("sitemap");
        $sitemap->zarazene = Hana_Navigation::instance()->get_navigation($this->application_context->get_actual_language_id(),3);
        $sitemap->nezarazene = Hana_Navigation::instance()->get_navigation($this->application_context->get_actual_language_id(),2);
        
        $page["popis"]=$sitemap->render();
        
        $template->item=$page;
        $this->request->response=$template->render();
    }

    
}

?>
