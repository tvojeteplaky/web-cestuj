<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Report_Item_List extends Controller_Hana_Default
{
	private $czMesice = array('leden','únor','březen','duben','květen','červen','červenec','srpen','září','říjen','listopad','prosinec');

	public function before() {
        parent::before();
    }

    public function action_index()
    {

    	if(isset($_GET["page"]))
			$page = $_GET["page"];
		else
			$page = 1;
		$result_data = array();

		$apartmans = orm::factory('shop')->where('language_id',"=",1)->find_all();
		$currency = orm::factory('reservation_currency')->where('shop_id','=',0)->find_all();
    	$template = new View('admin_report_table');
    	$headers = array("0"=>"Rok","1"=>"Měsíc");
    	$startDate = new DateTime();
    	$endDate = new DateTime();

    	$startDate->setDate($startDate->format('Y')+($page-1),'1','1');
    	$endDate->setDate($startDate->format('Y'),'12','31');
    	$endDate->modify('+1day');
    	$interval = new DateInterval('P1M');
		$daterange = new DatePeriod($startDate, $interval ,$endDate);
		$apartmans_id = array();
		$result_data['year'] = $startDate->format('Y');
		$result_data['money'] = 0;
		foreach ($apartmans as $apartman) {
			$apartmans_id[$apartman->id]['id'] = $apartman->id;
    			$apartmans_id[$apartman->id]['total'] = 0;
		}

			foreach ($daterange as $month) {
				$result_data['months'][$month->format('m')]['nazev'] = $this->czMesice[$month->format('m')-1];
				$reservations  = orm::factory('reservation')
					->where('MONTH("termin_od")','=',$month->format('m'))
					->where('YEAR("termin_od")','=',$month->format('Y'))
					->find_all();

				$result_data['months'][$month->format('m')]["money"] = 0;
				foreach ($reservations as $reservation) {
					if(!isset($result_data['months'][$month->format('m')]["reservations"][$reservation->apartman_id])){
						$result_data['months'][$month->format('m')]["reservations"][$reservation->apartman_id] = 0;
					}

	    				$price_def = Service_Reservation::get_reservation($reservation->id,7);
	    				$price = $price_def['prices'];
	    				$apartmans_id[$reservation->apartman_id]['total'] += $price['total'];

					$result_data['months'][$month->format('m')]["reservations"][$reservation->apartman_id] += $price['total'];
					$result_data['months'][$month->format('m')]["money"] += $price['total'];
				}
				$result_data["money"] += $result_data['months'][$month->format('m')]["money"];
			}
			foreach ($apartmans as $apartman) {
			$headers[] = $apartman->nadpis;
			}

    	$headers[] = "Celkem";

    	$template->headers = $headers;
    	$template->apartmans = $apartmans_id;
    	$template->page = $page;
    	$template->currency = $currency->as_array();
    	$template->result_data = $result_data;
    	$this->template->admin_content= $template->render();
	$this->request->response=$this->template->render();
    }
}