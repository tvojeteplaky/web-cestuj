<?php defined('SYSPATH') or die('No direct script access.');
 /**
 * Administrace clanku - seznam.
 *
 * @package    Hana/AutoForm
 * @author     Pavel Herink
 * @copyright  (c) 2010 Pavel Herink
 */

class Controller_Admin_Shop_Item_List extends Controller_Hana_List
{
    protected $with_route=true;

    protected $default_order_by = "poradi";
    protected $order_by = "poradi";
    protected $default_order_direction= "desc";
    protected $shop_category_id=0;

    public function before() {
        $this->orm=new Model_Shop();

        parent::before();
    }

    protected function _column_definitions()
    {
        $this->auto_list_table->column("id")->label("# ID")->width(30)->set();
        $this->auto_list_table->column("nazev")->type("link")->label("Název")->item_settings(array("hrefid"=>$this->base_path_to_edit))->css_class("txtLeft")->filterable()->sequenceable()->width(300)->set();
  //      $this->auto_list_table->column("date")->type("text")->label("Datum")->item_settings(array("special_format"=>"cz_date"))->css_class("txtLeft")->sequenceable()->filterable(array("type"=>"daterangepicker"))->width(150)->set();
        $this->auto_list_table->column("technicky_popis")->label("Text")->css_class("txtLeft")->item_settings(array("maxlenght"=>100))->filterable()->set();
        $this->auto_list_table->column("poradi")->type("changeOrderShifts")->label("")->item_settings(array("restriction"=>array("name"=>"shop_category_id","value"=>$this->shop_category_id)))->sequenceable()->width(32)->exportable(false)->printable(false)->set();
  $this->auto_list_table->column("fotogalerie")->type("relatedDetail")->label("Galerie")->data_src(array("db_query"=>db::select(array(db::expr("count(id)"),"count"))->from(strtolower($this->orm->class_name)."_photos"),"db_query_where_colid"=>"shop_id"))->item_settings(array("hrefid"=>$this->base_path_to_gallery,"alt"=>"editovat fotogalerii","alt_empty"=>"vložit nové fotky do fotogalerie","image"=>"images.png","image_empty"=>"images_empty.png"))->width(40)->set();
        if(Kohana::config("languages")->get("enabled"))
        $this->auto_list_table->column("available_languages")->type("languages")->item_settings(array("hrefid"=>$this->base_path_to_edit))->width(58)->set();
        $this->auto_list_table->column("zobrazit")->type("switch")->item_settings(array("action"=>"change_visibility","states"=>array(0=>array("image"=>"lightbulb_off.png","label"=>"neaktivní"),1=>array("image"=>"lightbulb.png","label"=>"aktivní"))))->sequenceable()->filterable(array("col_name"=>"routes.zobrazit"))->label("")->width(32)->set();
        
        $this->auto_list_table->column("delete")->type("checkbox")->value(0)->label("")->width(30)->exportable(false)->printable(false)->set();
    }
    
    protected function _orm_setup()
    {
        $this->orm->join("routes")->on("shop_data.route_id","=","routes.id");
        
//            

    }
    
      protected function _form_action_change_order($data)
    {
        $this->module_service->reorder_two_items($data["item_id"], $data["direction"],false, false, "shop_category_id");
        $this->data_saved=true;
    }

}
?>
