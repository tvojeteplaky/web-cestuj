<?php defined('SYSPATH') or die('No direct script access.');
 /**
 * Administrace nastaveni.
 *
 * @package    Hana/AutoForm
 * @author     Pavel Herink
 * @copyright  (c) 2010 Pavel Herink
 */

class Controller_Admin_Product_Eshopsettings_Edit extends Controller_Hana_Edit
{
    protected $with_route=false;
    protected $back_link_url;
    protected $back_link_text="Zpět";
    protected $cloneable=false;
    protected $item_name_property="editace nastavení e-shopu";

    public function before() {
        $this->orm=new Model_Product_Setting();

        $this->back_link_url = url::base()."admin/product/item/list";

        parent::before();
    }

    protected function _column_definitions()
    {
        $this->auto_edit_table->row("L1")->variant("one_col")->value("Fakturační údaje e-shopu")->type("label")->set();
        $this->auto_edit_table->row("billing_data_nazev")->type("edit")->label("Název")->set();
        $this->auto_edit_table->row("billing_data_email")->type("edit")->label("E-mail")->set();
        $this->auto_edit_table->row("billing_data_telefon")->type("edit")->label("Telefon")->set();
        $this->auto_edit_table->row("billing_data_ulice")->type("edit")->label("Ulice")->set();
        $this->auto_edit_table->row("billing_data_mesto")->type("edit")->label("Město")->set();
        $this->auto_edit_table->row("billing_data_psc")->type("edit")->label("PSČ")->set();
        $this->auto_edit_table->row("billing_data_ic")->type("edit")->label("IČ")->set();
        $this->auto_edit_table->row("billing_data_dic")->type("edit")->label("DIČ")->set();
        
        $this->auto_edit_table->row("L2")->variant("one_col")->value("Dárkový program")->type("label")->set();
        $this->auto_edit_table->row("present_enabled")->type("checkbox")->label("Zapnout funkci dárku")->set();
        $this->auto_edit_table->row("present_price_threshold")->type("edit")->label("Cena nákupu v Kč s DPH pro aktivaci dárku")->set();
        

    }

}