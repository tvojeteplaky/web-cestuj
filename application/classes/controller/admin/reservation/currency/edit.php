<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Admin_Reservation_Currency_Edit extends Controller_Hana_Edit
{
	protected $with_route=false;

	public function before() {
       		$this->orm=new Model_Reservation_Currency();
       		parent::before();
          print_r($this->orm->shop_id);
       	}
       	protected function _column_definitions(){
        		$this->auto_edit_table->row("id")->item_settings(array("with_hidden"=>true))->label("# ID")->set();
        		$this->auto_edit_table->row("name")->type("edit")->label("Název")->set();
        		$this->auto_edit_table->row("value")->type("edit")->label("Vztah k euru")->set();
            $this->auto_edit_table->row("ucet")->type("edit")->label("Učet pro platbu s touto měnou")->set();
            $this->auto_edit_table->row("shop_id")->type("selectbox")->label("Speciálně pro aparmán")->item_settings(array("HTML"=>array("class"=>"special")))->data_src(array("related_table_1"=>"shop","column_name"=>"nazev","orm_tree"=>false,"null_row"=>"---"))->set();
        	        // obsluzny jquery
        $jquery='
            $(document).ready(function(){
                $(\'select.special\').val('.$this->orm->shop_id.');
            });
        
        ';
        $this->auto_edit_table->add_script($jquery);
        
          }
}