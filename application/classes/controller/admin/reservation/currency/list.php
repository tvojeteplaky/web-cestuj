<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Admin_Reservation_Currency_List extends Controller_Hana_List
{
	protected $with_route=false;

	protected $default_order_by = "id";
    	protected $default_order_direction= "desc";

    	public function before() {
        		$this->orm=new Model_Reservation_Currency();

       		parent::before();
    	}

    	protected function _column_definitions()
    	{
    		$this->auto_list_table->column("id")->label("# ID")->width(30)->set();
    		$this->auto_list_table->column("name")->type("link")->label("Název")->item_settings(array("hrefid"=>$this->base_path_to_edit))->css_class("txtLeft")->filterable()->sequenceable()->width(300)->set();
    		$this->auto_list_table->column("value")->label("Vztah k euru")->css_class("txtLeft")->item_settings(array("maxlenght"=>20))->filterable()->set();
            $this->auto_list_table->column("ucet")->label("Účet pro platby")->css_class("txtLeft")->filterable()->set();
        	$this->auto_list_table->column("delete")->type("checkbox")->value(0)->label("")->width(30)->exportable(false)->printable(false)->set();
    	}

}