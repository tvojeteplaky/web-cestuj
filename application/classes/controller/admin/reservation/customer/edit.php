<?php 


class Controller_Admin_Reservation_Customer_Edit extends Controller_Hana_Edit 
{
	protected $with_route=false;
    protected $reservation_id;


	public function before() {
        $this->orm=new Model_Reservation_Customer();
        
        parent::before();
        

    }

    protected function _column_definitions()
    {
    	$this->auto_edit_table->row("id")->item_settings(array("with_hidden"=>true))->label("# ID")->set();

        $this->reservation_id = isset($_GET['reservation_id'])?$_GET['reservation_id']:$this->orm->reservation_id;
        $this->auto_edit_table->row("reservation_id")->item_settings(array("with_hidden"=>true))->label("ID rezervace")->value($this->reservation_id)->set();

    	$this->auto_edit_table->row("firstname")->type('edit')->label("Jméno")->set();
    	$this->auto_edit_table->row("lastname")->type('edit')->label("Příjmení")->set();
    	$this->auto_edit_table->row("born")->type('datepicker')->label("Datum Narození")->set();
        if($this->orm->id)
            $this->auto_edit_table->row("L10")->type('link')->value('Smaž')->item_settings(array('href'=>'/admin/reservation/item/edit/1/'. $this->reservation_id. "/?smaz_cloveka=".$this->orm->id,'HTML'=>array('class'=>'smaz',"style"=>"color:red;")))->set();
    
        $jquery = "
        $(function(){
          $('a.smaz,#smaz').click(function(e){
            return confirm('Opravdu chcete smazat tohoto člověka?');
          });
            $('input.datepicker').datepicker('option','changeMonth',true);
            $('input.datepicker').datepicker('option','changeYear',true);
        });";
        $this->auto_edit_table->add_script($jquery);
    }

    protected function _form_action_main_prevalidate($data) {
        parent::_form_action_main_prevalidate($data);
        $this->send_link_url ="/admin/reservation/item/edit/1/" .$data['reservation_id']; 
        $this->back_to_item = $this->send_link_url;
        return $data;
    }
}