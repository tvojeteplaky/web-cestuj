<?php defined('SYSPATH') or die('No direct script access.');
 /**
 * Administrace clanku - seznam.
 *
 * @package    Hana/AutoForm
 * @author     Pavel Herink
 * @copyright  (c) 2010 Pavel Herink
 */

class Controller_Admin_Reservation_Item_List extends Controller_Hana_Default
{
	protected $with_route=false;

	public $template="admin/admin_content";

	private $czDays = array('Neděle','Pondělí','Úterý','Středa','Čtvrtek','Pátek','Sobota');

	public function before() {
	   parent::before();
	}
	
	public function action_index(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(isset($_GET["change_state"]) && isset($_GET["type"]) && $_GET["change_state"] ) {
				$this->_change_reservation_state($_GET["change_state"],$_GET["type"]);
			}
			die();
		} else {
		$template=new View("admin_reservation_table");

		if(!empty($_GET['smaz'])&&$_GET['smaz']){
			$this->_delete_reservation($_GET['smaz']);
		}

		if(isset($_GET["change_state"]) && isset($_GET["type"]) && $_GET["change_state"] ) {
			$this->_change_reservation_state($_GET["change_state"],$_GET["type"]);
		}
		if(isset($_GET["page"]))
			$page = $_GET["page"];
		else
			$page = 1;
		$result_data=array();

		$begin = new DateTime();
		if($page>1){
			$begin->setDate($begin->format('Y')+($page-1),1,1);
		} elseif($page<1) {
			$begin->setDate($begin->format('Y')+($page),1,1);
		}

		$end = new DateTime();
		$end->setDate($begin->format('Y'),'12','31');
		$end->modify('+1day');

		$interval = new DateInterval('P1D');
		$daterange = new DatePeriod($begin, $interval ,$end);

		foreach($daterange as $date){
			$mysql_format = $date->format('Y-m-d');
			$result_data[$mysql_format]['mysql_format'] = $mysql_format;
			$result_data[$mysql_format]['date'] = $date->format('d.m.Y');
			$result_data[$mysql_format]['day_of_week'] = $date->format('N');
			$result_data[$mysql_format]['day'] = $date->format('j');
			$result_data[$mysql_format]['date_name'] = $this->czDays[$date->format('w')];
			$result_data[$mysql_format]['reservations']  = array();
			$reservations = orm::factory('reservation')
				->where('termin_od','<=',$mysql_format)
				->where('termin_do','>',$mysql_format)
				->find_all();
			foreach ($reservations as $reservation) {
				$apartman =$reservation->apartman->find();
				$result_data[$mysql_format]['reservations'][$reservation->apartman_id] = $reservation->as_array();
				$result_data[$mysql_format]['reservations'][$reservation->apartman_id]['currency'] = $reservation->currency->find()->as_array(); 
				$customers = $reservation->customers->find_all()->as_array();
				$i = 0;
				$result_data[$mysql_format]['reservations'][$reservation->apartman_id]['customers'] = array();
				foreach ($customers as $customer) {
					$result_data[$mysql_format]['reservations'][$reservation->apartman_id]['customers'][$i] = $customer->as_array();
					$i++;
				}
				

			}
			$tarif = orm::factory("reservation_price")
				->where('termin_od','<=',$mysql_format)
				->where('termin_do','>=',$mysql_format)
				->find();
			$result_data[$mysql_format]['tarif'] = array();
			$result_data[$mysql_format]['tarif']['nazev'] = $tarif->nazev;
			$result_data[$mysql_format]['tarif']['price'] = $tarif->price;

		}

		$apartmans = orm::factory('shop')
			->where('language_id','=','1')
			->order_by('shops.id')
			->find_all();

		foreach ($apartmans as $apartman) {
			$res_apartmans[$apartman->id] = $apartman->as_array();
		}


		$template->result_data = $result_data;
		$template->apartmans = $apartmans;
		$template->page=$page;
		$this->template->admin_content= $template->render();
		$this->request->response=$this->template->render();
		}
	 }
	private function _delete_reservation($reservation_id)
	{
		$reservation 	= new Model_Reservation($reservation_id);
		

		if($reservation->customers->delete_all() && $reservation->delete()){
			return true;
		}
	}

	private function _change_reservation_state($reservation_id, $what = "paid") {
		$reservation_orm = orm::factory("reservation")->where("id","=",$reservation_id)->find();
		switch ($what) {
			case 'keys':
				$reservation_orm->keys = ($reservation_orm->keys)?0:1;
				break;
			
			default:
				$reservation_orm->paid = ($reservation_orm->paid)?0:1;
				break;
		}
		$reservation_orm->save();

	}
}
?>
