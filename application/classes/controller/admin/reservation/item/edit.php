<?php defined('SYSPATH') or die('No direct script access.');
 /**
 * Administrace clanku - edit.
 *
 * @package    Hana/AutoForm
 * @author     Pavel Herink
 * @copyright  (c) 2010 Pavel Herink
 */

class Controller_Admin_Reservation_Item_Edit extends Controller_Hana_Edit
{
    protected $with_route=false;
    private $email = "rezervace@alpineliving.cz";
   // private $email = "ondrej.brabec@dgstudio.cz";
    private $jmeno = "Alpine Living";
    //protected $item_name_property=array("nazev"=>"s názvem");
    

    public function before() {
        $this->orm=new Model_Reservation();
        $this->action_buttons = array_merge($this->action_buttons,array(
          "odeslat_3"=>array("name"=>"odeslat_3","value"=>"uložit a odeslat mail klientovi","hrefid"=>"reservation/item/list/" ),
            "odeslat_4"=>array("name"=>"odeslat_4","value"=>"odeslat mail nám","hrefid"=>"reservation/item/list/")
            ));
        parent::before();
        
    }

    protected function _column_definitions()
    {
        if(!empty($_GET['smaz_cloveka'])){
          $this->_delete_customer($_GET['smaz_cloveka']);
        }
        $this->auto_edit_table->row("id")->item_settings(array("with_hidden"=>true))->label("# ID")->set();
        $this->auto_edit_table->row("code")->item_settings(array("with_hidden"=>true))->label("Číslo rezervace (Variabilní symbol)")->set();
       
        $this->auto_edit_table->row("apartman_id")->type("selectbox")->label("Vozidlo")->data_src(array("related_table_1"=>"apartman","column_name"=>"nazev","orm_tree"=>false, 'language'=> 1))->set();
        /*if(isset($this->orm->id)){
          $customers = $this->orm->customers->find_all();
          $customers = $customers->as_array();
          $first = array_shift($customers);
          $id = !is_object($first)?$first['id']:$first->id;
          $first = orm::factory('reservation_customer',$id);
          $narozen = new DateTime($first->born);
          $this->auto_edit_table->row("L10")->type("link")->item_settings(array('href'=>'/admin/reservation/customer/edit/1/'. $first->id))->value(mb_strtoupper($first->firstname. ' ' . $first->lastname) . '  narozen: ' .$narozen->format('j.n.Y'))->set();

        }*/
        

        $this->auto_edit_table->row("termin_od")->type("datepicker")->label("Od")->set();
        $this->auto_edit_table->row("termin_do")->type("datepicker")->label("Do")->condition("Minimální délka pobytu jsou 3 dny.")->set();
        
        $this->auto_edit_table->row("L2")->variant("one_col")->value("KLIENTI")->type("label")->set();
        if($this->orm->id)
        {
            $this->auto_edit_table->row("customers")->type('customers')->data_src(array("href"=>$this->base_admin_path))->set();
        } else {
            $this->auto_edit_table->row("L12")->value("Před přidáním lidí musí být rezervace uložena")->type("text")->set();
        }
        $this->auto_edit_table->row("email")->type("edit")->label("E-mail")->set();
        $this->auto_edit_table->row("phone")->type("edit")->label("Telefon")->set();

       $this->auto_edit_table->row("L4")->variant("one_col")->value("Vyúčtování")->type("label")->set();

       $this->auto_edit_table->row("discount")->type("selectbox")->label("Sleva")->data_src(array('data_array' => array(0=>"0%",3=>"3%",5=>"5%",7=>"7%",10=>"10%",13=>"13%",15=>"15%",20=>"20%",25=>"25%")))->set();
       $this->auto_edit_table->row("currency_id")->type("selectbox")->label("Měna")->data_src(array("related_table_1"=>"currency","column_name"=>"name","orm_tree"=>false,"condition"=>array('shop_id','=',0)))->set();
       $this->auto_edit_table->row("prevodem")->type("selectbox")->label("Platba")->data_src(array('data_array' => array(0=>"Hotově",1=>"Převodem")))->set();
       if ($this->orm->id) {
           $this->auto_edit_table->row("L41")->type('pricebox')->item_settings($this->_prepare_price_table())->set();
       }
       $this->auto_edit_table->row("free")->type("checkbox")->default_value(0)->label("Zdarma")->set();
       $this->auto_edit_table->row("tax_free")->type("checkbox")->default_value(0)->label("Zdarma - pouze taxa a úklid")->set();
       // $range = array();
       // for ($i=1; $i <=30 ; $i++) { 
       //   $range[$i] = $i; 
       // } 

        $ucet = $this->orm->currency->ucet;
       if ($this->orm->apartman->currency->id>0)
        $ucet = $this->orm->apartman->currency->ucet;

       $this->auto_edit_table->row("L6")->value($ucet)->type("text")->label('Účet')->set();
        if($this->orm->id) {
          $date = new DateTime($this->orm->created_at);
          $this->auto_edit_table->row("L5")->type("text")->label("Rezervace vytvořena")->value($date->format('j.n.Y'))->set();
        }
       $this->auto_edit_table->row("splatnost_at")->type("datepicker")->label("Splatnost do")->set();
       $this->auto_edit_table->row("splatnost_zaloha_at")->type("datepicker")->label("Splatnost zálohy do")->set();
       // $this->auto_edit_table->row("splatnost")->type("selectbox")->label("Splatnost do")->default_value(15)->data_src(array('data_array' => $range))->set();
       
       // if($this->orm->id) {
       //  $date = new DateTime($this->orm->created_at);
       //  if($this->orm->splatnost>1){
       //    $date->modify('+'.$this->orm->splatnost.'days');
       //  } else {
       //    $date->modify('+'.$this->orm->splatnost.'day');
       //  }
       //  $this->auto_edit_table->row("L5")->type("text")->label("Splatnost do datumu")->value($date->format('j.n.Y'))->set();
       // }
       // $this->auto_edit_table->row("splatnost_zaloha")->type("selectbox")->label("Splatnost zálohy do")->default_value(7)->data_src(array('data_array' => $range))->set();
       // if($this->orm->id) {
       //  $date = new DateTime($this->orm->created_at);
       //  if($this->orm->splatnost_zaloha>1){
       //    $date->modify('+'.$this->orm->splatnost_zaloha.'days');
       //  } else {
       //    $date->modify('+'.$this->orm->splatnost_zaloha.'day');
       //  }
       //  $this->auto_edit_table->row("L51")->type("text")->label("Splatnost zálohy do datumu")->value($date->format('j.n.Y'))->set();
       // }
       //$this->auto_edit_table->row("price")->item_settings(array("with_hidden"=>true))->label("Cena(ve zvolené měně) se všemi poplatky")->set();

        
        $this->auto_edit_table->row("note")->type("textarea")->label("Poznámka")->set();
        $this->auto_edit_table->row("status")->type("selectbox")->label("Status")->data_src(array('data_array' => array(0=>"Karta nebyla zatím odeslána",1=>" Karta od apartmánu zaslána PPL (1x karta)",2=>"Karta od apartmánu zaslána PPL (2x karta)",3=>"Karta od apartmánu předána osobně (1x karta)",4=>"Karta od apartmánu předána osobně (2x karta)",10=>"Karta (karty) od apartmánu vrácena")))->set();

        if($this->orm->id)
           $this->auto_edit_table->row("L3")->type("link")->item_settings(array('href'=>'/admin/reservation/item/list/?smaz='. $this->orm->id,'HTML'=>array('class'=>'smaz',"style"=>"color:red;")))->value('Smaž položku')->set();

        $jquery = "
        $(function(){
          $('a.button').button();
          $('a.smaz,#smaz').click(function(e){
            return confirm('Opravdu chcete smazat tuto rezervaci?');
          });";
          if(!$this->orm->id){
          $jquery .= "$( 'input[name=termin_od]' ).datepicker( 'option', 'minDate', new Date());
            var min = $( 'input[name=termin_od]' ).datepicker( 'getDate' );
              min.setDate(min.getDate()+3);
              console.log(min);
              
                $( 'input[name=termin_do]' ).datepicker( 'option', 'minDate', min);
                $('input[name=termin_od]').change(function(){
                  var min = $( 'input[name=termin_od]' ).datepicker( 'getDate' );
                  min.setDate(min.getDate()+3);
                  $( 'input[name=termin_do]' ).datepicker( 'option', 'minDate', min );
                  var old = $( 'input[name=termin_do]' ).datepicker( 'getDate' );
                  if(min> old) {
                    $( 'input[name=termin_do]' ).datepicker( 'option', 'setDate', min );
                  }
                });";
              }
        $jquery .= "});";
        $this->auto_edit_table->add_script($jquery);
    }

    protected function _form_action_main_prevalidate($data) {
          if(isset($_POST["customers_raw"])) {

            $this->_form_action_customer_add(array("customers" => $_POST["customers_raw"]));
           }
        parent::_form_action_main_prevalidate($data);
        // specificka priprava dat, validace nedatabazovych zdroju (pripony obrazku apod.)
        $dStart = new DateTime($data['termin_od']);
        $dEnd  = new DateTime($data['termin_do']);

        $dDiff = $dStart->diff($dEnd);
        if($dDiff->days<3) {
            $this->data_processing_errors = array_merge($this->data_processing_errors,array('termin_do'=>'Minimální délka pobytu jsou 3 dny'));
        }
        // je v apartmánu volno
            
            $data['termin_od'] = $dStart->format("Y-m-d");
            $data['termin_do'] = $dEnd->format("Y-m-d");
            if($this->orm->created_at<=0) {
              $now = new DateTime;
              $data['created_at'] = $now->format('Y-m-d');
            }
            
            if(!Service_Reservation::is_available($data['id'],$data['apartman_id'],array($data['termin_od'],$data['termin_do']))){
                $this->data_processing_errors = array_merge($this->data_processing_errors,array('termin_do'=>'Apartmán je v tyto termíny obsazený'));
            }
            if($data['free']){
              $data['price'] = 0;
            } else {
              $discount = $data['tax_free']?100:$data['discount'];
              $prices = Service_Reservation::recount_total_price($data['id'],$data["apartman_id"],array($data['termin_od'],$data['termin_do']),$discount,$data['currency_id']);
              $data['price'] = $prices['total'];
            }
          if(!$this->orm->code){
            $last_code = orm::factory('reservation')->order_by('code','DESC')->find();
            if($last_code->code >999){
              $data['code'] = $last_code->code+1;
            } else {
              $data['code'] = 1000;
            }
            
          }  

        return $data;
    }

    
    protected function _form_action_main_postvalidate($data) {
       parent::_form_action_main_postvalidate($data);

       

       if(isset($_POST["odeslat_3"])) {
          Service_Email::send_reservation_mail(array("email"=>$this->email, "jmeno"=>$this->jmeno), $this->orm->id);
       }

       if(isset($_POST["odeslat_4"])) {
          Service_Email::send_reservation_mail_to_us(array("email"=>$this->email, "jmeno"=>$this->jmeno), $this->orm->id);
       }
       // vlozim o obrazek
       if(isset($_FILES["main_image_src"]) && $_FILES["main_image_src"]["name"])
       {
           // nahraju si z tabulky settings konfiguracni nastaveni pro obrazky - tzn. prefixy obrazku a jejich nastaveni
           $image_settings = Service_Hana_Setting::instance()->get_sequence_array($this->module_key, $this->submodule_key, "photo");
           $this->module_service->insert_image("main_image_src", $this->subject_dir, $image_settings, $this->orm->route->nazev_seo);
       }
       
       if(isset($_FILES["wide_image_src"]) && $_FILES["wide_image_src"]["name"])
       {
           // nahraju si z tabulky settings konfiguracni nastaveni pro obrazky - tzn. prefixy obrazku a jejich nastaveni
           $image_settings = Service_Hana_Setting::instance()->get_sequence_array($this->module_key, $this->submodule_key, "photo_wide");
           $this->module_service->insert_image("wide_image_src", "shop/wide_item/", $image_settings, $this->orm->route->nazev_seo, true, "jpg", "photo_wide_src");
       }

    }

    /**
     * Akce na smazani obrazku !
     * @param <type> $data
     */
    protected function _form_action_main_image_delete($data)
    {
        $this->module_service->delete_image($data["delete_image_id"], $this->subject_dir);
    }
    
    protected function _form_action_wide_image_delete($data)
    {
        $this->module_service->delete_image($data["delete_image_id"], "shop/wide_item/", false, false, false,"photo_wide_src");
    }

    protected function _form_action_customer_delete($data){
      $customer = orm::factory('reservation_customer',$data['customer_id']);
      if($customer->reservation_id == $this->orm->id){
        if($customer->delete()){
          $this->request->redirect(URL::site(Request::detect_uri()));
        } else {
          return false;
        }
      }
    }

    protected function _form_action_customer_add($data){

      foreach ($data['customers'] as $customer) {
        $customer_orm = orm::factory('reservation_customer');
        if(isset($customer['customer_id']) && $customer['customer_id']>0){
          $customer_orm->where('id','=',$customer['customer_id'])->find();
        } else {
          $customer_orm->reservation_id = $this->orm->id;
        }

        $customer_orm->firstname = $customer['firstname'];
        $customer_orm->lastname = $customer['lastname'];
        $born = new DateTime($customer['born']);
        $customer_orm->born = $born->format("Y-m-d");
        $customer_orm->save();
      }
    }

    protected function _prepare_price_table() {
      $prepared_table = array();
      $prepared_table['cols'] = 3;
      $prepared_table['rows'] = array();
      $prepared_table['rows'][0] = array(array('value'=>'Počet nocí'),array('value'=>'tarif'),array('value'=>'cena('.$this->orm->currency->name.')'));
              $discount = $this->orm->tax_free?100:$this->orm->discount;
      $data = Service_Reservation::recount_total_price($this->orm->id,$this->orm->apartman_id,array($this->orm->termin_od,$this->orm->termin_do),$discount,$this->orm->currency_id);

      foreach ($data['tarifs'] as $tarif) {
        $prepared_table['rows'][] = array(array('value'=>$tarif['nights']),array('value'=>$tarif['name']),array('value'=>$tarif['price']));
      }
      $prepared_table['rows'][] = array(array('value'=>'<strong>Příplatky(povinné)</strong>','colspan'=>'3'));
      //$tax = $data['extras']['tax'];
      //$prepared_table['rows'][] = array(array('value'=>'počet osob ' . $tax['people'].'x'.$data['extras']['days'].' dní'),array('value'=>'Pobytová taxa'),array('value'=>$tax['price']));
      $prepared_table['rows'][] = array(array('value'=>' '),array('value'=>'Jednorázový poplatek'),array('value'=>$data['extras']['services']));
      $prepared_table['rows'][] = array(array('value'=>' '));
      $prepared_table['rows'][] = array(array('value'=>' '),array('value' => 'Korekce'),array("value" => $data['corection']));
      $prepared_table['rows'][] = array(array('value'=>' '),array('value'=>'<strong>Záloha</strong>'),array('value'=>$data['advance']));
      $prepared_table['rows'][] = array(array('value'=>' '),array('value'=>'<strong>Celkem</strong>'),array('value'=>'<strong>'.round($data['total']).'</strong>'));
      return $prepared_table;
    }
}