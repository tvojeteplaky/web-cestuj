<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Admin_Reservation_Price_Edit extends Controller_Hana_Edit
{
	protected $with_route=false;

	public function before() {
       		$this->orm=new Model_Reservation_Price();

       		parent::before();
       	}
       	protected function _column_definitions(){
        		$this->auto_edit_table->row("id")->item_settings(array("with_hidden"=>true))->label("# ID")->set();
        		$this->auto_edit_table->row("nazev")->type("edit")->label("Název")->condition("Položka musí mít minimálně 3 znaky.")->set();
        		$this->auto_edit_table->row("termin_od")->type("datepicker")->label("Od")->set();
        		$this->auto_edit_table->row("termin_do")->type("datepicker")->label("Od")->set();
            $this->auto_edit_table->row("shop_id")->type("selectbox")->label("Pro karavan")->item_settings(array("max_tree_level"=>1))->data_src(array("related_table_1"=>"shop","column_name"=>"nazev","orm_tree"=>false,"null_row"=>"---","language"=>false))->set();
        		$this->auto_edit_table->row("price")->type("edit")->label("Cena (CZK)")->set();
        		$this->auto_edit_table->row("zobrazit")->type("checkbox")->label("Zobrazit")->default_value(1)->set();

        	}
}

