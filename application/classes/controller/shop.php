<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Generovani statickych obsahovych stranek a textu.
 */
class Controller_Shop extends Controller
{
    /**
     * Metoda generujici seznam clanku.
     */
    public function action_index($page=1)
    {
        $template=new View("shop_list");
        $language_id=$this->application_context->get_actual_language_id();
        $route_id=$this->application_context->get_actual_route_id();
        $page_orm=Service_Page::get_page_by_route_id($route_id);
        $template->page=Service_Page::get_page_by_route_id($route_id);
        
        //die(print_r($page));
        $template->page=$page_orm;
        $items_per_page=50;
        $pagination = Pagination::factory(array(
          'current_page'   => array('source' => $this->application_context->get_actual_seo(), 'value'=>$page),
          'total_items'    => Service_Shop::get_shop_total_items_list($language_id, 0),
          'items_per_page' => $items_per_page,
          'view'              => 'pagination/basic',
          'auto_hide'      => TRUE
        ));

		$start_date = date("Y").'-05-01';
		$end_date = date("Y").'-10-30';
		$date_from_user = date('Y-m-d');

		$in_season = $this->check_in_range($start_date, $end_date, $date_from_user);
		if($in_season)
			$template->season = 1;
		else
			$template->season = 0;
            
        $template->items=Service_Shop::get_shop_list($language_id,0,$items_per_page,$pagination->offset);
        $template->pagination=$pagination->render();
        $this->request->response=$template->render();
    }
    
    /**
     * Metoda generujici vsechny stranky vkladane do hlavniho obsahu.
     */
    public function action_detail()
    {
        $route_id=$this->application_context->get_actual_route_id();
        $template=new View("shop_detail");
        $language_id=$this->application_context->get_actual_language_id();
        $template->items=Service_Shop::get_shop_list($language_id,0,10,0);
        $template->item=Service_Shop::get_shop_by_route_id($route_id);
		$shop_id = $template->item["id"];

		$table=new View("reservation_front");
        $apartmans=orm::factory('shop')->where('language_id','=',$language_id)->where('shop_id','=',$shop_id)->find_all();
        $table->reservations=Service_Reservation::get_reservations_ordered_by_date($apartmans,date('m'),12);
//        $template->data=$this->response_object->get_data();
//        $errors=$this->response_object->get_errors();
//        $template->errors=!empty($errors["reservation"])?$errors["reservation"]:array();
//		  $template->send=$this->response_object->get_status();

		$query = DB::query(Database::SELECT, 'SELECT * FROM shop_photos INNER JOIN shop_photo_data ON shop_photos.id = shop_photo_data.shop_photo_id WHERE shop_id = :shop_id AND zobrazit = 1 ORDER BY poradi ASC');
		$query->param(':shop_id', $template->item['shop_id']);
		$photos = $query->execute()->as_array();
		$template->photos = $photos;

		$template->table= $table->render();

        $this->request->response=$template->render();
    }

	public function check_in_range($start_date, $end_date, $date_from_user)
	{
  	// Convert to timestamp
  		$start_ts = strtotime($start_date);
  		$end_ts = strtotime($end_date);
  		$user_ts = strtotime($date_from_user);

  		// Check that user date is between start & end
  		return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
	}
    
    public function action_shop_subnav($nazev_seo)
    {
        $subnav=new View("subnav");
        $links     = Service_Shop::get_navigation($this->application_context->get_actual_language_id());
        $sel_links = Hana_Navigation::instance()->get_navigation_breadcrumbs();

        $sel_link=array_pop($sel_links);
        $subnav->links = $links;
        $subnav->sel_links = $sel_links; 
        $this->request->response=$subnav->render();
    }

    
}

?>
