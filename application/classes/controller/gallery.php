<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Obecna pripojitelna galerie - widget.
 * 
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class Controller_Gallery extends Controller
{
    
    public function action_default_widget()
    {
        
    }
    
    public function action_slider_widget($seo, $settings="t1-t2")
    {
       $gallery=new View("gallery_slider");
       $gallery->photos=$this->_core_functionality($settings);
       $gallery->module=$this->application_context->get_main_controller();
       $this->request->response=$gallery->render();
    }
    
    public function action_custom_widget($seo, $settings="t1-t2")
    {
       $gallery=new View("gallery_with_thumbnails");
       $gallery->photos=$this->_core_functionality($settings);
       $gallery->module=$this->application_context->get_main_controller();
       $this->request->response=$gallery->render();
    }
    
    public function action_carousel_widget()
    {
       $gallery=new View("gallery_simple_carousel");
       $gallery->photos=$this->_core_functionality("t1-t1");
       $gallery->module=$this->application_context->get_main_controller();
       $this->request->response=$gallery->render();
    }
    
    
    
    private function _core_functionality($settings="t1-t2", $special_no="", $module="")
    {
        
        if(!$module) $module=$this->application_context->get_main_controller();
 
        $route_id=$this->application_context->get_actual_route_id();

        $suffixes=explode("-", $settings);
        $detail_suffix=$suffixes[0];
        $thumbnail_suffix=$suffixes[1];
        
        // specialni id
        if(isset($suffixes[2]))
        {
            $id=$suffixes[2];
        }
        else
        {
            // automaticky z routy
            $id=DB::select(array($module."_data.".$module."_id","id"))->from($module."_data")->where("route_id","=",$route_id)->execute()->get('id');
        }
        
        $photos=orm::factory($module."_p".$special_no."hoto")->where($module."_id","=",$id)->where("zobrazit","=",1)->order_by("poradi","asc")->find_all();;
        
        $photodir="media/photos/".$module."/item/gallery".$special_no."/images-".$id."/";
        
        
        $photos_array=array();

        foreach($photos as $photo)
        {

            if($photo->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$photodir.$photo->photo_src."-".$detail_suffix.".jpg"))
            {
                $photos_array[$photo->poradi]=$photo->as_array();
                $photos_array[$photo->poradi]["photo"]=url::base().$photodir.$photo->photo_src."-".$thumbnail_suffix.".jpg";
                $photos_array[$photo->poradi]["photo_detail"]=url::base().$photodir.$photo->photo_src."-".$detail_suffix.".jpg";
                $photos_array[$photo->poradi]["nazev"]=$photo->nazev;
            }
        }
        //die(print_r($photos_array));
        return($photos_array);

    }
    
  
}

?>
