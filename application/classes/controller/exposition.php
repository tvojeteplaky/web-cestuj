<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Sobrazeni vypisu a detailu produktu
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class Controller_Exposition extends Controller
{ 
    public function action_detail()
    {
        
        $template=new View("exposition_detail");
        
        $route_id=$this->application_context->get_actual_route_id();
        
        $template->item=  Service_Exposition::get_exposition_detail_by_route_id($route_id);
        
        $this->request->response=$template->render();
    }
    
    
}

?>
