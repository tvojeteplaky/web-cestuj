<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Generovani statickych obsahovych stranek a textu.
 */
class Controller_Catalog extends Controller
{
    /**
     * Metoda generujici seznam clanku.
     */
    public function action_index($page=1)
    {
        $template=new View("catalog_category_homepage");
        $language_id=$this->application_context->get_actual_language_id();
        $route_id=$this->application_context->get_actual_route_id();
        $page_orm=Service_Page::get_page_by_route_id($route_id);
        
        //die(print_r($page));
        $template->item=$page_orm;
        $items_per_page=100;
       /* $pagination = Pagination::factory(array(
          'current_page'   => array('source' => $this->application_context->get_actual_seo(), 'value'=>$page),
          'total_items'    => Service_catalog::get_catalog_total_items_list($language_id, 0),
          'items_per_page' => $items_per_page,
          'view'              => 'pagination/basic',
          'auto_hide'      => TRUE
        ));
         */   
        $template->items=Service_catalog::get_navigation($language_id);
       // $template->pagination=$pagination->render();
        $this->request->response=$template->render();
    }
    
    public function action_category()
    {
        $route_id=$this->application_context->get_actual_route_id();
        $category = Service_Catalog_Category::get_product_category_by_route_id($route_id);
        

            $template   = new View("catalog_category");
            $categories = Service_Catalog_Category::get_product_categories_by_parent_category_id($category["id"]);
            $template->categories=$categories;
            $template->products = Service_Catalog::get_catalog_items_list($category["id"], $this->application_context->get_actual_language_id());

        $template->item=$category;
        $this->request->response=$template->render();
    }
    
    /**
     * Metoda generujici seznam clanku - uvodka.
     */
    public function action_homepage_list()
    {
        $template=new View("catalog_homepage_list");
        $categories = Service_Catalog_Category::get_product_categories_by_parent_category_id(0);
        $template->categories=$categories;
        $this->request->response=$template->render();
    }
    
    
    /**
     * Metoda generujici vsechny stranky vkladane do hlavniho obsahu.
     */
    public function action_detail()
    {
        $route_id=$this->application_context->get_actual_route_id();
        $template=new View("catalog_detail");
        
        $template->item=Service_Catalog::get_catalog_item_by_route_id($route_id);
        $this->request->response=$template->render();
    }
    
    public function action_main_subnav($nazev_seo)
    {
        $subnav=new View("catalog_top_subnav");
        $links     = Service_catalog::get_navigation($this->application_context->get_actual_language_id(),1,1);
        $sel_links = Hana_Navigation::instance()->get_navigation_breadcrumbs();

        $subnav->links = $links;
        $subnav->sel_links = $sel_links; 
        $this->request->response=$subnav->render();
    }
    
     /**
     * Specificka navigace 
     * @param type $nazev_seo 
     */
    public function action_second_subnav($nazev_seo)
    {
        $subnav=new View("catalog_second_subnav");
        $route_id  = $this->application_context->get_actual_route_id();

        $links     = Service_catalog::get_navigation($this->application_context->get_actual_language_id(),1,3,array());
        
        //die(print_r($links));
        
//        if(isset($links[$nazev_seo]["children"]))
//        {
//            $links=$links[$nazev_seo]["children"];
//        }
//        else
//        {
//            $links=array();
//        }
        
        $sel_links = Hana_Navigation::instance()->get_navigation_breadcrumbs();
        //print_r($sel_links);
        //$sel_link=array_pop($sel_links);
        $first=current($links);
        $subnav->class = isset($first["class"])?$first["class"]:"saacke";
        //print_r($sel_links);
        $subnav->links = $links;
        $subnav->sel_links = $sel_links; 
        $this->request->response=$subnav->render();
    }
    
    /**
     * Specificka navigace 
     * @param type $nazev_seo 
     */
    public function action_third_subnav($nazev_seo)
    {
        $subnav=new View("catalog_second_subnav");
        $route_id  = $this->application_context->get_actual_route_id();

        $links1     = Service_catalog::get_navigation($this->application_context->get_actual_language_id(),1,2,array());
        
        $sel_links = Hana_Navigation::instance()->get_navigation_breadcrumbs();
        $sel_link=array_pop($sel_links);
        

        if(!empty($sel_link["nazev_seo"]) && !empty($links1[$sel_link["nazev_seo"]]["children"]))
        {
            $links=$links1[$sel_link["nazev_seo"]]["children"];
        }
        else
        {
           $links=array(); 
        }
        
        $subnav->class = "saacke";
        $subnav->links = $links;
        $subnav->sel_links = $sel_links; 
        $this->request->response=$subnav->render();
    }
    
    
    
}

?>
