<?php

class Controller_Reservation extends Controller {

    private $email = "rbunka@centrum.cz";
    private $jmeno = "Cestuj karavanem";


    public function action_save()
    {
        // Získá data z formuláře
        $data = array();
        $error = false;
        foreach ($_POST['reservationform'] as $key => $item)
        {
                $data[$key] = $item;
        }
        //die(var_dump($_POST['reservationform']));
            try {
                $id = Service_Reservation::add_new_reservation($data);
                Service_Email::send_reservation_mail(array("email"=>$this->email, "jmeno"=>$this->jmeno), $id);
                $this->response_object->set_redirect(true);
                $this->response_object->set_message(__('Rezervace proběhla úšpěšně'), Hana_Response::MSG_PROCESSED);
            } catch (Exception $e) {
                $this->response_object->set_redirect=false;
                $this->response_object->set_message(__($e->getMessage()), Hana_Response::MSG_ERROR);
            }
    }

    /*private function send_mails($id)
    {
        $email_service = new Service_Email();
        if ($email_service->send_reservation_mail(array("email"=>$this->email, "jmeno" => $this->jmeno), $id))
            return true;
        else
            throw new Exception("Nepodařilo se odeslat email s vyúčtováním.", 1);
    }*/

}

