<?php defined('SYSPATH') or die('No direct script access.');

class Model_Shop extends ORM_Language
{
    protected $_join_on_routes=true;
    
    protected $_has_many = array(
      'shop_photos' => array()
    );
    
    // Validation rules
    protected $_rules = array(
            'nazev' => array(
            'not_empty'  => NULL,
            ),
    );

    protected $_has_one = array(
        'currency' => array('model' => 'reservation_currency', 'foreign_key' => 'shop_id'),
    );
}
?>
