<?php defined('SYSPATH') or die('No direct script access.');
/*
 * Zjednoduseny model pro produkty katalogu. E-shop pouziva Model_Product. 
 */
class Model_Reservation_Currency extends ORM
{
	
	protected $_table_name = "currencies";
	
	protected $_join_on_routes=false;

	protected $_rules = array(
		'name' => array(
			'not_empty'  => NULL,
		),
	);
	protected $_has_one = array(
		'shop' => array('model' => 'shop', 'foreign_key' => 'id'),
		);
}