<?php
class Model_Reservation extends ORM {

	protected $_belongs_to = array(
		'currency' => array('model' => 'reservation_currency', 'foreign_key' => 'currency_id'),
		'apartman' => array('model' => 'shop', 'foreign_key' => 'apartman_id')
	);

	protected $_has_many = array(
		'customers' => array('model' => 'reservation_customer', 'foreign_key' => 'reservation_id')
	);
}
?>