<div id="ListTableSection">
	<div id="ToolButts">
        <a class="item"  href="?page={$page - 1}"> &lt; Předchozí</a> 
        <a class="item" href="?page={$page + 1}">Další &gt;</a>
	</div>
<div class="tableTop"><div class="in"></div></div>
    <table summary="seznam položek">
    	<thead>
    		<tr>
    			{foreach from=$headers item=header key=key name=name}
    				<th>{$header}</th>
    			{/foreach}
    			
    		</tr>
    	</thead>
    	<tbody>

    	{foreach from=$result_data.months item=month key=key name=months}
    		<tr>
    			{if $smarty.foreach.months.first}
    				<td><div class="txtCenter"><strong>{$result_data.year}</strong></div></td>
    			{else}
    				<td></td>
    			{/if}
    			<td><div class="txtCenter">{$month.nazev}</div></td>
    			{foreach from=$apartmans item=apartman key=key name=name}
					<td>
	    				{if not empty($month.reservations[{$apartman.id}])}
	    					<div class="txtCenter">
                            {foreach from=$currency item=single key=key name=currency}
                                {if !$smarty.foreach.currency.first}
                                    /
                                {/if}
                                {math equation="ceil(x*y)" x=$month.reservations[{$apartman.id}] y=$single->value} {$single->name}
                            {/foreach}
                            </div>
	    				{/if}
    				</td>
    			{/foreach}
    			<td>
	    			{if $month.money>0}
                        {foreach from=$currency item=single key=key name=currency}
                            {if !$smarty.foreach.currency.first}
                                /
                            {/if}
                            {math equation="ceil(x*y)" x=$month.money y=$single->value} {$single->name}
                        {/foreach}
	    			{/if}
    			</td>
    		</tr>
    	{/foreach}
    		
    	</tbody>
    	<tfoot>
    		<tr>
    		<td colspan="2">
                <strong>Za celý rok:</strong>
    		</td>
            {foreach from=$apartmans item=apartman key=key name=name}
                <td>
                        {foreach from=$currency item=single key=key name=currency}
                            {if !$smarty.foreach.currency.first}
                                /
                            {/if}
                            {math equation="ceil(x*y)" x=$apartman.total y=$single->value} {$single->name}
                        {/foreach}
                </td>
            {/foreach}
    		<td>
    			<strong>
                    {foreach from=$currency item=single key=key name=currency}
                            {if !$smarty.foreach.currency.first}
                                /
                            {/if}
                            {math equation="ceil(x*y)" x=$result_data.money y=$single->value} {$single->name}
                        {/foreach}
                </strong>
    		</td>
    		</tr>
    	</tfoot>
    </table>

  <div class="tableBottom"><div class="in"></div></div>
</div>