{*
   CMS system Hana ver. 2.6 (C) Pavel Herink 2012
   zakladni spolecna sablona layoutu stranek

   automaticky generovane promenne spolecne pro vsechny sablony:
   -------------------------------------------------------------
   $url_base      - zakladni url
   $tpl_dir       - cesta k adresari se sablonama - pro prikaz {include}
   $url_actual    - autualni url
   $url_homepage  - cesta k homepage
   $media_path    - zaklad cesty do adresare "media/" se styly, obrazky, skripty a jinymi zdroji
   $controller
   $controller_action
   $language_code - kod aktualniho jazyku
   $is_indexpage  - priznak, zda jsme na indexove strance

   $web_setup     - pole se zakladnim nastavenim webu a udaji o tvurci (DB - admin_setup)
   $web_owner     - pole se zakladnimi informacemi o majiteli webu - uzivatelske informace (DB owner_data)
   -------------------------------------------------------------

   doplnujici custom Smarty funkce
   -------------------------------------------------------------
   {translate str="nazev"}                                      - prelozeni retezce
   {static_content code="index-kontakty"}                       - vlozeni statickeho obsahu
   {widget name="nav" controller="navigation" action="main"}    - vlozeni widgetu - parametr "name" je nepovinny, parametr "action" je defaultne (pri neuvedeni) na hodnote "widget"
   {hana_secured_post action="add_item" [module="shoppingcart"]}        nastaveni akce pro zpracovani formulare (interni overeni parametru))
   {hana_secured_multi_post action="obsluzna_akce_kontroleru" [submit_name = ""] [module="nazev_kontoleru"]}
   {$product.cena|currency:$language_code}

   Promenne do base_template:
   -------------------------------------------------------------
   {$page_description}
   {$page_keywords}
   {$page_name} - {$page_title}
   {$main_content}



*}

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="cs" />
    <meta name="author" content="{$web_setup.nazev_dodavatele }" />
    <meta name="copyright" content="{$web_owner.copyright}" />
    <meta name="keywords" content="{$page_keywords}" />
    <meta name="description" content="{$page_description} " />
    <meta name="robots" content="all,follow" />
    <meta name="googlebot" content="snippet,archive" />
    <meta name="viewport" content="width=1000, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />

    <link rel="stylesheet" href="{$media_path}css/normalize.css" type="text/css" media="screen" charset="utf-8">
	<link rel="stylesheet" href="{$media_path}css/fitgrid.css" type="text/css" media="screen" charset="utf-8">
	<link rel="stylesheet" href="{$media_path}css/lightbox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="{$media_path}css/jquery.modal.css" type="text/css" media="screen" />
	<link type="text/css" href="{$media_path}css/style.css" rel="stylesheet" media="all" />
    <link type="text/css" href="{$media_path}css/jquery_ui.css" rel="stylesheet" media="all" />
    <link type="text/css" href="{$media_path}css/jquery.fancybox.css" rel="stylesheet" media="all" />
    <link type="text/css" href="{$media_path}css/jquery.bxslider.css" rel="stylesheet" media="all" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700&subset=latin-ext' rel='stylesheet' type='text/css'>
 {*  <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>

    {literal}



<style type="text/css">
h1, h2, h3, h4, h5, h6, p, label, input, textarea, ol, ul, li {
	font-family: 'PT Sans' !important;
  font-style:  normal;
}

</style>

{/literal}
*}

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="{$media_path}js/jquery-ui.js"></script>
    <script type="text/javascript" src="{$media_path}js/jquery.basicSlider.js"></script>
    <script type="text/javascript" src="{$media_path}js/slimbox-2.05/js/slimbox2.js"></script>
    <script type="text/javascript" src="{$media_path}js/jquery.message.js"></script>
    <script type="text/javascript" src="{$media_path}js/hana_custom.js"></script>
    <script type="text/javascript" src="{$media_path}js/jquery.modal.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="{$media_path}js/jquery.message.js" charset="utf-8"></script>
    <script type="text/javascript" src="{$media_path}js/jquery.bxslider.min.js" charset="utf-8"></script>

    <script type="text/javascript" src="{$media_path}js/jquery.default-values.js"></script>

    <!-- Lightbox -->
    <script type="text/javascript" src="{$media_path}js/lightbox-2.6.min.js"></script>
    <link href="{$media_path}css/lightbox.css" rel="stylesheet" />

    <!-- slider -->
    <script src="{$media_path}js/bjqs-1.3.js"></script>
    <link rel="stylesheet" href="{$media_path}css/bjqs.css">

    <!-- jCarousel -->
    <script type="text/javascript" src="{$media_path}js/jquery.jcarousel.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{$media_path}css/skin.css" />

     <!-- superfish menu -->
     <link type="text/css" href="{$media_path}css/superfish.css" rel="stylesheet" media="all" />
     <script type="text/javascript" src="{$media_path}js/superfish.js"></script>
     <script type="text/javascript" src="{$media_path}js/jquery.hoverIntent.js"></script>
     <script type="text/javascript" src="{$media_path}js/jquery.fancybox.pack.js"></script>
<script type="text/javascript">
      // hide URL field on the iPhone/iPod touch
      function hideUrlBar() {

        document.getElementsByTagName("body")[0].style.marginTop="1px";
        window.scrollTo(0, 1);
      }
      window.addEventListener("load", hideUrlBar);
      window.addEventListener("resize", hideUrlBar);
      window.addEventListener("orientationchange", hideUrlBar);
    </script>
    <script type="text/javascript">
      $(document).ready(function(){
        $('.bxslider').bxSlider();
      });
    </script>
    <style type="text/css" title="Default">
      .warning {
        font-weight: bold;
      }
      /* fix for scroll bars on webkit & Mac OS X Lion */
      ::-webkit-scrollbar {
        background-color: rgba(0,0,0,0.5);
        width: 0.75em;
      }
      ::-webkit-scrollbar-thumb {
          background-color:  rgba(255,255,255,0.5);
      }
    </style>


    <title>{$page_name} - {$page_title}</title>

                 {literal}
    <script type="text/javascript">
    $(function() {
        $(".default-value").defaultValues();

       // $('#modal-form').modal();

    });
    </script>

     {/literal}

  </head>



     	<!-- body -->
	<body class="bg-site">

		<!-- site header -->
		<div class="site-header">
			<div class="center">
				<div class="row fg-no-gutter">
					<div class="fg5">
						<a class="logo" href="{$url_homepage}">Alpine living</a>
					</div>
                    
					<div class="partners fg2 push3">
						
					</div>
					<div class="partners fg2">
						
					</div>
				</div>

				<div class="row fg-no-gutter">
					{widget name="message" controller="site" action="message"}

        			{widget name="nav" controller="navigation" action="main"}
        		</div>
			</div>
		</div>
		<!-- end site header -->
		<div class="clear">

  </div>

		<!-- site content -->
		<div class="site-content center">

        {$main_content}
    </div>
    	<div class="shadow-corner-both"> </div>
		<!-- end site content -->

		<!-- site footer -->
		<div class="site-footer">
			<div class="center">
				<div class="row fg-no-gutter">
					<a class="logo fg2" href="./">Alpine living</a>
					<div class="contacts fg10">
						<span>Tel.: +420 604 271 883</span> /
						<span>www.cestujkaravanem.cz</span> /
						<span>E-mail: pujcovna@cestujkaravanem.cz</span>
					</div>
				</div>
			</div>
		</div>
		<!-- end site footer -->

        {*include file="`$tpl_dir`dg_footer.tpl"*}

		<!-- dg footer -->
		<div class="dg-footer">
			<div class="center">
				<div class="footer-left">
					<span>Copyright &copy; 2016 Cestuj karavanem | www.cestujkaravanem.cz</span>
				</div>
				<div class="footer-right">
					<span>
						Realizace:
						<a href="http://www.dgstudio.cz" title="DG studio - Tvorba webových stránek, on-line marketing, desing a grafika" target="_blank">
							<img src="{$media_path}img/dgstudio_logo.png" alt="dgstudio">
						</a>
					</span>
				</div>
			</div>
		</div>
		<!-- end dg footer -->

</body>



</html>

