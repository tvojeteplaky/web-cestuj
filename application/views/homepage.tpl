			<!-- slider -->
			<div class="slider">
				<div class="wrapper">
					<div class="bxslider">
  					{$counter = 1}					
					{foreach from=$photos item=photo key=key name=name}
						<div class="slide">
						{if $photo.zobrazit == 1}
							<img src="{$media_path}photos/page/item/gallery/images-{$photo.page_id}/{$photo.photo_src}-ad.jpg" alt="" />
						    {static_content code="slide-{$counter}"}
						{/if}
						{$counter = $counter + 1}
						</div>		
					{/foreach}
					</div>
				</div>
				<div class="controls"> </div>
			</div>
			<div class="shadow-corner-both"> </div>
			<!-- end slider -->
			
			<!-- page content -->
			<div class="shadow-article-top"> </div>
			<div class="page-content 2columns row fg-no-gutter">
				
				<div class="left fg8">
					<h1>{$item.nadpis}</h1>    
          {static_content code="homepage-info-1"}
          <br /><br />
          	<div class="more">
						<a class="button-pujcovna" href="/pujcovna">{translate str="PŘEJÍT DO PŮJČOVNY"}</a>
					</div>
				</div>
				
				<div class="right fg4">
					<img src="{$media_path}img/fb-widget-{translate str="lang"}.jpg" alt="fb widget">  
			
				<div id="fb-root"></div>
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FCestuj-karavanem-580211062146613%2F&tabs=timeline&width=264&height=258&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="264" height="258" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
				
				</div>			
		
			</div>
			<!-- end page content -->
			
			<!-- hp gallery -->
			<div class="gallery-widget">
			<div class="clear">   
   			</div>
   			<div class="shadow-corner-right"> </div>
				<h2 class="hp">{translate str="Fotogalerie"}</h2>
				<div class="wrapper row fg-no-gutter">
				{foreach name=art from=$gallery key=key item=item2}
        
       	<div class="item">  
         <a href="{$item2.photo_detail}" title="{$item2.nazev}" data-lightbox="image" >
     <img src="{$item2.photo}" alt="fotogalerie" />
      </a> 
      </div>
      {/foreach}
				
				
				
				</div>
				<div class="more">
					<a class="button" href="{$url_base}{translate str=fotogalerie}">{translate str="více"}</a>
				</div>
			</div>
			<!-- end hp gallery -->
	

 {*       
    
       {widget action="homepage_list" controller="article"} 
 {widget action="homepage_list" controller="catalog"}
{translate str="Všechny novinky"}
          
 *}
          
