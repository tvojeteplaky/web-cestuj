<table>
	<thead>
		<tr>
			<th>Jméno</th>
			<th>Příjmení</th>
			<th>Narozen</th>
			{* <th>Editovat</th> *}
			<th>Smazat</th>
		</tr>
	</thead>
	<tbody id="target">
	{foreach from=$customers item=customer key=key name=customer}
		<tr class="copy">
			<input type="hidden" name="customers_raw[{$customer.id}][customer_id]" value="{$customer.id}">
			<td><input type="text" name="customers_raw[{$customer.id}][firstname]" value="{$customer.firstname}" data-type="firstname" placeholder="Křestní jméno" required=""></td>
			<td><input type="text" name="customers_raw[{$customer.id}][lastname]" value="{$customer.lastname}" data-type="lastname" placeholder="Příjmení" required=""></td>
			<td><input type="text" name="customers_raw[{$customer.id}][born]" class="born" data-type="born" value="{$customer.born|date_format:'%d.%m.%Y'}"  required=""></td>
			{*<td><a class="" href="{$admin_path}?customers_edit={$entity_name}&amp;customer_id={$customer.id}">Editovat</a></td>*}
			<td><a class="confirmDelete" href="{$admin_path}?customer_action_delete={$entity_name}&amp;customer_id={$customer.id}">Smazat</a></td>
		</tr>
	{foreachelse}
		<tr class='copy'>
            <td class="odrazeno">
                <input type="text" class='firstname' name="customers_raw[0][firstname]" placeholder="Jméno" data-id="0" data-type="firstname" required="">
            </td>
            <td >
                <input type="text" name="customers_raw[0][lastname]" placeholder="Příjmení" data-type="lastname" data-id="0" required="">
            </td>
            <td class="odrazeno">
                <input type="text" class="born" name="customers_raw[0][born]" data-type="born" placeholder="Narozen" data-id="0" required="">
            </td>
            <td>
            	
            </td>
        </tr>
	{/foreach}
	</tbody>
	{if count($customers)<4}
		<tfoot>
			<tr>
				<td colspan="4">
					{* <a class="button" href="{$admin_path}?customers_edit={$entity_name}">Nový klienti</a>  *}
					Počet klientů:
					<select name="count" id="count">
                        {for $num=1 to 4}
                            <option value="{$num}" {if $count_customers == $num}selected="selected"{/if}>{$num}</option>
                        {/for}
                        </select>
				</td>
			</tr>
		</tfoot>
	{/if}
</table>
<script type="text/javascript">
	function insert (row,num) {
        var myrow = row.eq(-1);
        myrow.find('input.born').datepicker();
        var newDiv = myrow.clone(false).attr("id", "someRow"+num).insertBefore(myrow);
        newDiv.find('input.born')
          .attr("id", "")
          .removeClass('hasDatepicker')
          .removeData('datepicker')
          .unbind()
          .datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "1950:2015",
                dateFormat: "dd.mm.yy"
          });
          myrow.find("input").each(function(k,v) {
          	$(v).val("");
          	$(v).data("id",(num+1));
          	$(v).attr("name","customers_raw["+(num+1)+"]["+$(v).data("type")+"]")
          });
          myrow.find(".confirmDelete").remove();
    }
    function multiplier(me) {
        var row = $('tr.copy');
            var need = $(me).val()*1;
            var have = row.length;
            var target = '#target';
            if(need>have) {
                var diff = need-have;
                for (var i = 0; i < diff; i++) {
                    insert(row,i)
                };
            }
            else if(need<have){
                var diff = have-need;
                for (var i = 1; i <= diff; i++) {
                    row.eq(-i).remove();
                };
            }
    }
	$(function(){
		$('.button').button();
		$('input.born').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "1950:2015",
                dateFormat: "dd.mm.yy"
          });
		$('#count').change(function() {
            multiplier(this);
        });

	});

</script>