{* 
<style>
    th.hed{
        min-width: 70px;
        text-align: center;
    }
    td.odrazeno{
        padding: 5px;
    }

</style>
<div id="ModalForm1Container">
    <div id="ModalForm1">
        <div id="dialog-form" title="Zákazníci">
            <table>
            <input type="hidden" name="customer_action_add" value="{$entity_name}" />
            {if empty($data)}
                <tr>
                    <th class='hed'>
                        <label for='count'>Počet osob:</label>
                    </th>
                    <td class="odrazeno">
                        <select name="count" id="count">
                        {for $num=1 to 4-$count_customers}
                            <option value="{$num}">{$num}</option>
                        {/for}
                        </select>
                    </td>
                </tr>
                {else}
                    <input type="hidden" name="customer_id" value="{$data.id}">
                {/if}
                <tr >
                    <th class='hed'>Klienti</th>
                    <td>
                        <table>
                            <thead>
                                <tr>
                                    <th>Jméno</th>
                                    <th>Příjmení</th>
                                    <th>Datum narození</th>
                                </tr>
                            </thead>
                            <tbody id="target">
                                <tr class='copy'>
                                    <td class="odrazeno">
                                        <input type="text" class='firstname' name="firstname[]" placeholder="Jméno" {if $data}value="{$data.firstname}" {/if}required="">
                                    </td>
                                    <td class="odrazeno">
                                        <input type="text" name="lastname[]" placeholder="Příjmení" {if $data}value="{$data.lastname}" {/if}required="">
                                    </td>
                                    <td class="odrazeno">
                                        <input type="text" class="born" name="born[]" placeholder="Narozen" {if $data}value="{$data.born|date_format:'%d.%m.%Y'}" {/if}required="">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button type="submit" id="submit">Přidat</button>
                    </td>
                </tr>
   
            </table>

        </div>
    </div>
</div>

                                 {literal}
<script type="text/javascript">
    function insert (row,num) {
        var myrow = row.eq(-1);
        myrow.find('input.born').datepicker();
        var newDiv = myrow.clone(false).attr("id", "someRow"+num).insertAfter(myrow);
        newDiv.find('input.born')
          .attr("id", "")
          .removeClass('hasDatepicker')
          .removeData('datepicker')
          .unbind()
          .datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "1950:2015",
                dateFormat: "dd.mm.yy"
          });
    }
    function multiplier(me) {
        var row = $('tr.copy');
            var need = $(me).val()*1;
            var have = row.length;
            var target = '#target';
            if(need>have) {
                var diff = need-have;
                for (var i = 0; i < diff; i++) {
                    insert(row,i)
                };
            }
            else if(need<have){
                var diff = have-need;
                for (var i = 1; i <= diff; i++) {
                    row.eq(-i).remove();
                };
            }
    }
    $(function(){
       var data=$("#ModalForm1").html();
      $('#JqueryForm form').append(data);


      $("#ModalForm1Container").remove();

        $("#submit, #reset").css("display", "none");
        var dialog = $( "#JqueryForm" ).dialog({
            autoOpen: true,
            bgiframe: true,
            height: 370,
            width: 700,
            modal: true,
            resizable: false,
            open: function(event, ui) {},
            buttons: {
                "Uložit": function() {
                    $('#submit').click();
                },
                "Zrušit": function() {
                    dialog.dialog( "close" );
                }
            }
        });
        $('input.born').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "1950:2015",
                dateFormat: "dd.mm.yy"
          });
        $('#count').change(function() {
            multiplier(this);
        });

    });

</script>
{/literal} *}