<div id="TopCategories">
  <div class="left">
     {foreach from=$categories key=key item=item} 
     <a href="{$item.nazev_seo}" class="{$item.class}">{$item.nazev_full}</a>
     {/foreach} 
  </div>
  <div class="right">
    {foreach from=$categories key=key item=item name=cat}
    <div id="TCImg_{$item.class}" {if !$smarty.foreach.cat.first}class="hidden"{/if}>
      <h2>{$item.nazev_full2}</h2>
      {$item.uvodni_popis}
    </div>
    {/foreach}  
  </div>
</div>

{literal}
<script type="text/javascript">
  $("#TopCategories .left a").mouseover(function(){
     var xclass=$(this).attr('class');
     $("#TopCategories .right div").fadeOut();
     $("#TopCategories .right div#TCImg_"+xclass).fadeIn();
  }); 
</script>
{/literal}