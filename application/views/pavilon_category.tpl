{* sablona pro generovani statickych stranek (modul: page) *}

{widget action="breadcrumbs" controller="navigation" }

<div id="CenterSection">
  <div id="CenterSectionLeft">
    {widget name="subnav" controller="pavilon" action="category_subnav"}
  </div>
  
  <div id="ContentSection">
    <div id="TextTopSection"></div>      
    <div id="TextSection">
      {*
      {if !empty($item.photo_detail)}
      <div class="mainPhoto">
      <!--  741 x 215 -->
         <img src="{$item.photo_detail}" alt="{$item.nazev}" title="{$item.nazev}" />
      </div>
      {/if}
      *}
      
      <div class="mainPhoto">
      {widget action="pavilon_carousel_widget" controller="gallery" name="gallery" param="t1-t2"}
      </div>

      <h1>{$item.nazev}</h1>
      <div class="textSectionIN">
      {$item.popis}
      </div>
    </div>
  </div> 
</div>