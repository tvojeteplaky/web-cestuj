<!-- Inliner Build Version 4380b7741bb759d6cb997545f3add21ad48f010b -->
<!DOCTYPE html>
<html>
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="UTF-8">
<title></title>
</head>
<body style="font-family: Calibri, sans-serif; color: #0070c0; font-size: 11pt;">
<style type="text/css">
a:visited {
color: #0070c0;
}
a:active {
color: #0070c0;
}
a:hover {
color: #500050;
}
</style>
<p>
    Dobrý den,
    <br>
    děkujeme Vám za rezervaci automobilu na <a href="http://www.cestujkaravanem.cz">www.cestujkaravanem.cz</a>.
{*    <br>
    Prosíme o kontrolu Vámi zadaných údajů a o provedení úhrady pobytu.*}
</p>
<br/><p>Detaily rezervace:</p><br>
<table style="font-size: 11pt; font-family: Calibri, sans-serif;">
<tr>
<td style="padding-right: 15px;"><b >Rezervace číslo.:</b></td>
<td style="padding-right: 15px;">{$data.code}</td>
    </tr>
<tr>
<td style="padding-right: 15px;"><b >Automobil:</b></td>
<td style="padding-right: 15px;">{$data.apartman.nazev}</td>
    </tr>
<tr>
<td style="padding-right: 15px;"><b >Od:</b></td>
<td style="padding-right: 15px;">{$data.termin_od|date_format:"%d.%m.%Y"} (po 17:00 hod)</td>
    </tr>
<tr>
<td style="padding-right: 15px;"><b >Do:</b></td>
<td style="padding-right: 15px;">{$data.termin_do|date_format:"%d.%m.%Y"} (do 10:00 hod)</td>
    </tr>
<tr>
<td rowspan="4" class="top" style="vertical-align: top; padding-right: 15px;" valign="top"><b >Klient:</b></td>
{foreach from=$data.people item=person key=key name=people}
    <td style="padding-right: 15px;">{$person->firstname} {$person->lastname} <span style="float: right;">{$person->born|date_format:"%d.%m.%Y"}</span>
    </td>
    </tr>
    {if not $smarty.foreach.people.last}
        <tr>
    {/if}

{/foreach}
</table>
<br/><p>Pro další upřesnění se Vám ozveme na uvedené kontakty hned jakmile to bude možné.</p><br/>

<p><b>Vyúčtování:</b></p>

<table class="border" style="border-collapse: collapse; margin-left: 100px; margin-bottom: 15px; font-size: 11pt; font-family: Calibri, sans-serif;">
{foreach from=$data.prices.tarifs item=tarif key=key name=tarifs}
<tr>
<td style="border-collapse: collapse; padding: 5px 10px; border: 1px solid #000000;">{$tarif.nights} {if $tarif.nights<5}noci{else}nocí{/if}    {$tarif.name}</td>
<td class="right" style="border-collapse: collapse; text-align: right; padding: 5px 10px 5px 30px; border: 1px solid #000000;" align="right">{$tarif.price} {$data.currency.name}</td>
    </tr>
    {/foreach}
{* <tr>
<td style="border-collapse: collapse; padding: 5px 10px; border: 1px solid #000000;">Pobytová taxa {$data.apartman.priplatek_taxa} EUR / osobu /noc</td>
<td class="right" style="border-collapse: collapse; text-align: right; padding: 5px 10px 5px 30px; border: 1px solid #000000;" align="right">{$data.prices.extras.tax.price} {$data.currency.name}</td>
    </tr> *}
<tr>
<td style="border-collapse: collapse; padding: 5px 10px; border: 1px solid #000000;">Jednorázový poplatek</td>
<td class="right" style="border-collapse: collapse; text-align: right; padding: 5px 10px 5px 30px; border: 1px solid #000000;" align="right">{$data.prices.extras.services} {$data.currency.name}</td>
    </tr>
<tr>
<td style="border-collapse: collapse; padding: 5px 10px; border: 1px solid #000000;"><b >CELKEM K ÚHRADĚ</b></td>
<td class="right" style="border-collapse: collapse; text-align: right; padding: 5px 10px 5px 30px; border: 1px solid #000000;" align="right"><b >{$data.prices.total} {$data.currency.name}</b></td>
    </tr>
</table>
<table style="font-size: 11pt; font-family: Calibri, sans-serif;">
<tr>
<td style="padding-right: 15px;"><b >Číslo účtu:</b></td>
<td style="padding-right: 15px;">{$data.ucet}</td>
    </tr>
<tr>
<td style="padding-right: 15px;"><b >Variabilní symbol:</b></td>
<td style="padding-right: 15px;">{$data.code}</td>
    </tr>
<tr>
<td style="padding-right: 15px;"><b >Záloha:</b></td>
<td style="padding-right: 15px;">{$data.prices.advance} {$data.currency.name} splatná do {$data.splat_zalohu_do}</td>
    </tr>
<tr>
<td style="padding-right: 15px;"><b >Doplatek:</b></td>
<td style="padding-right: 15px;">{$data.prices.additional} {$data.currency.name} splatný do {$data.splat_do}</td>
    </tr>
</table>
<p>
    Prosíme o provedení platby v uvedených termínech, platba zálohy je zároveň potvrzením rezervace ze strany klienta. <br> (V případě že nebude záloha uhrazena v požadovaném termínu, bude rezervace zrušena). V případě jakýchkoliv dotazů nás neváhejte kontaktovat. 
<p></p>
    <b>Team cestujkaravanem.cz</b>
</p>
<p>
    <br>
Tel: + 420 604 271 883<span style="margin-left: 20px;">E-mail: </span><a href="mailto:pujcovna@cestujkaravanem.cz" class="margin" style="margin-right: 20px;">pujcovna@cestujkaravanem.cz</a> 
</p>
</body>
</html>
