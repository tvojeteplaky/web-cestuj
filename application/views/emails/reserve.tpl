{* sablona emailu s informacemi z odeslaneho kontaktniho formulare *}

<br /><br />
<table cellpadding="5" style="border: none; border-collapse: collapse;">
  <tr>
    <td><strong>{translate str="Objednávka apartmánu"}</strong></td>
    <td>{$data.nazev_projektu}</td>
  </tr>
  
  <tr>
    <td><strong>{translate str="Jméno"}</strong></td>
    <td>{$data.jmeno}</td>
  </tr>
   <tr>
    <td><strong>Email</strong></td>
    <td>{$data.email}</td>
  </tr>
  
  {if !empty($data.telefon)}
  <tr>
    <td><strong>{translate str="Telefon"}</strong></td>
    <td>{$data.telefon}</td>
  </tr>
  {/if}
  
   {if !empty($data.apartman)}
  <tr>
    <td><strong>{translate str="Apartmán"}</strong></td>
      <td>{$data.nazev_projektu}</td>
  </tr>
  {/if}


    <tr>
        <td><strong>{translate str="Termín rezervace"}</strong></td>
        <td>{$data.termin_od} - {$data.termin_do}</td>
    </tr>

    <tr>
        <td><strong>{translate str="Příjezd"}</strong></td>
        {capture name="prijezd" assign="prijezd"}{$data.prijezd}{/capture}
        <td>{translate str=$prijezd}</td>
    </tr>

    <tr>
        <td><strong>{translate str="Odjezd"}</strong></td>
        {capture name="odjezd" assign="odjezd"}{$data.odjezd}{/capture}
        <td>{translate str=$odjezd}</td>
    </tr>

 

  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>

</table>