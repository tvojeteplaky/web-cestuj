{* sablona pro generovani statickych stranek (modul: page) *}

{widget action="breadcrumbs" controller="navigation" }

<div id="CenterSection">
  <div id="CenterSectionLeft">
    {widget name="subnav" controller="pavilon" action="category_subnav"}
  </div>
  
  <div id="ContentSection">
    <div id="TextTopSection">
    {widget action="custom_widget" controller="gallery" name="gallery" param="t1-t2"}
    </div>
    <div id="TextSection" class="expositions">
  
      <h1>{$item.nazev}{*$item.nadpis*}</h1>
        {*{$item.popis}*}
      
        <div id="AnimalsList">
          {foreach name=anl from=$animals key=key item=item}
          <div class="item">
            
            <div class="photo">
               {if !empty($item.photo)}
               <!--  199 x 129 -->
               <img src="{$item.photo}" alt="{$item.nazev}" title="{$item.nazev}" />
               <div class="over"></div>
               {/if}
            </div>
            <div class="head">
              <h2>{$item.nazev}</h2>
              {$item.popis|strip_tags|mb_substr:0:300}
              <div class="correct defaultMT"></div>
              <a class="moreInfo" href="{$url_base}{$item.nazev_seo}">{translate str="Čtěte více"}</a>
            </div>
            
          </div>
          <div class="correct bigMT"></div>
          {/foreach}
        </div>
        
      
      
      
    </div>
  </div> 
</div>