		<!-- dgs footer -->
		<div id="" class="dgs-footer">
  			<div class="center logo">
          		<p>
          			<a href="http://www.ippi.cz/klavesove-zkratky/">{translate str="Klávesové zkratky - rozšířené"}</a> | 
          			<a href="{$url_base}prohlaseni-o-pristupnosti" title="Prohlášení o přístupnosti">{translate str="Prohlášení o přístupnosti"}</a> | 
          			<a href="{$url_base}ochrana-osobnich-udaju" title="Ochrana osobních údajů">{translate str="Ochrana osobních údajů"}</a> | 
          			<a href="{$url_base}mapa-stranek" title="Mapa stránek" accesskey="3">{translate str="Mapa stránek"}</a><br />
          			realizace: 
          			<a href="http://www.dgstudio.cz/" title="tvorba www stránek – dg studio">tvorba www stránek</a> | 
          			<a href="http://www.virtualni-prohlidky.eu/" title="virtuální prohlídky">virtuální prohlídky</a> | 
          			<a href="http://www.web-na-splatky.cz/" title="web na splátky">web na splátky</a> / dg studio | 
          			<a href="http://validator.w3.org/check/referer" title="XHTML validátor">XHTML 1.0</a> | 
          			<a href="http://jigsaw.w3.org/css-validator/" title="CSS validator">CSS 2</a>
          		</p>
      		</div>
		</div>
		<!-- end dgs footer -->