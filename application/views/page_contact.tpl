{* sablona pro generovani statickych stranek (modul: page) *}
			
<!-- page content -->

<div class="page-content 2columns row fg-no-gutter">
	
	<div class="left fg6">
		<h1>{$item.nadpis}</h1>					
		{$item.popis}
		<br />
	</div>

    <div class="right fg6">
    	<img alt="vizitka" src="/media/userfiles/vizitka.png">
    </div>
    </div>
		







{*<div class="google-map">
<form class="map_search"  id="search_route" action="#">
    <p class="display-none">Do:&#160;	<input type="text" size="25" id="toAddress1" name="to" value="47.408348,13.767209" /></p>
    <p class="display-none"><select class="display-none" id="locale1" name="locale">
    <option value="cs">Česky</option>
    </select></p>
    <div class="misto">
        <label for="fromAddress1">{translate str="Místo ze kterého vyjíždíte"}:</label><input type="text" class="input-text" size="25" id="fromAddress1" name="from" value="" />
        <input type="submit" id="submit_button_map" name="submit" title="{translate str="Hledat"}" class="input-button" value="{translate str="Hledat"}" />
    </div>
</form>
<div class="bot">
<div id="map_kontakty-1" class="map_kontakty"></div>
<div id="directions-1" class="directions">&#160;</div>
<div class="clear">&#160;</div>
</div>
</div>*}
<div class="clear">&#160;</div>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&language={translate str=lang}"></script>
    {literal}
<script type="text/javascript">
    var Latlng = new google.maps.LatLng(47.408348, 13.767209);
    var rendererOptions = {
        draggable: true
    };
    
    var directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);;
    var directionsService = new google.maps.DirectionsService();
    var map;
    
    function initialize() {
        var mapOptions = {
            zoom: 14,
            center: Latlng
        }
        
        map = new google.maps.Map(document.getElementById('map_kontakty-1'), mapOptions);

        var marker = new google.maps.Marker({
            position: Latlng,
            map: map,
            title: 'Alpineliving',
        });
        
        directionsDisplay.setMap(map);
        directionsDisplay.setPanel(document.getElementById('directions-1'));
        
        $("#search_route").submit(function(e){
            var request = {
                origin: $("#fromAddress1").val(),
                destination: Latlng,
                travelMode: google.maps.TravelMode.DRIVING
            };  
            
            directionsService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                } else {
                    {/literal}
                    alert("{translate str="Místo nenalezeno"}.");
                     {literal}
                }
            });
            
            e.preventDefault();
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
              </script>
    {/literal}
<div class="clear">&#160;</div>
