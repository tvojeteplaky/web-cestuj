<link type="text/css" href="{$media_path}css/admin-table.css" rel="stylesheet" media="all" />
<div id="ToolButts">
        		<a class="item"  href="?page={$page - 1}"> &lt; Předchozí</a> 
              	<a class="item" href="?page={$page + 1}">Další &gt;</a>
     	 
	<a class="item right tb_newLink" href="/admin/reservation/item/edit" title="Nová položka"><img src="/media/admin/img/add.png" alt="přidat novou položku" title="přidat novou položku"> <span>Nová položka</span></a>
</div>
<div class="clear"></div>
<div class="reservation ">                  
<table>
	<thead>
		<tr>
			<th class="small">den</th>
			<th class="small">datum</th>
			<th class="small">příjezd/odjezd</th>
			{foreach from=$apartmans item=apartman key=key name=name}
				<th class="big">{$apartman.nadpis}</th>
			{/foreach}
			{* <th class="small">tarif</th> *}
		</tr>
	</thead>
	<tbody>
	{foreach from=$result_data item=date key=key name=dates}
		<tr>
			<td rowspan="2"  style='{if $date.day_of_week == 7}border-bottom: 2px solid black;{/if}{if $date.day == 1}border-top: 5px solid red;{/if}'>{$date.date_name}</td>
			<td rowspan="2"  style='{if $date.day_of_week == 7}border-bottom: 2px solid black;{/if}{if $date.day == 1}border-top: 5px solid red;{/if}'>{$date.date}</td>
			<td style='{if $date.day == 1}border-top: 5px solid red;{/if}'>do 10:00 hod</td>
			{if $smarty.foreach.dates.first}
			{foreach from=$apartmans item=apartman key=key name=name}
				<td></td>
			{/foreach}
			{/if}
			
			
		</tr>
		<tr>
			<td {if $date.day_of_week == 7}style='border-bottom: 2px solid black;'{/if}>po 17:00 hod</td>
			{foreach from=$apartmans item=apartman key=key name=name}
				{if not empty($date.reservations[$apartman.id].id)}
					<td rowspan="2" style="padding-left: 4em; position:relative">

						<a href="/admin/reservation/item/list?change_state={$date.reservations[$apartman.id].id}&amp;type=keys" title="Změnit status klíčů" class="changeState" data-state="{$date.reservations[$apartman.id].keys}">
							<i class="fi-key" style="font-size: 1.5em; left:5px;position:absolute; color:{if $date.reservations[$apartman.id].keys}green{else}red{/if};"></i>
						</a>
						<a href="/admin/reservation/item/list?change_state={$date.reservations[$apartman.id].id}&amp;type=paid" title="Změnit status zaplacení" class="changeState" data-state="{$date.reservations[$apartman.id].paid}">
							<i class="fi-dollar-bill" style="font-size: 1.5em; left:1.5em;position:absolute; color:{if $date.reservations[$apartman.id].paid}green{else}red{/if};"></i>
						</a>
				{* 	{if $date.reservations[$apartman.id].status == 0}
						<i class="fi-x-circle" style="font-size: 1.5em;left:5px;position:absolute; margin-right: 10px"></i>
					{elseif $date.reservations[$apartman.id].status < 10}
						<i class="fi-key" style="font-size: 1.5em; left:5px;position:absolute; color:red;"></i>
					{else}
						<span class="ui-icon ui-icon-check"></span>
					{/if} *}
					
					
					<a href="/admin/reservation/item/edit/1/{$date.reservations[$apartman.id].id}">
						č {$date.reservations[$apartman.id].code}, 
						{if not empty($date.reservations[$apartman.id].customers)}
							{$date.reservations[$apartman.id].customers[0].firstname} 
							{$date.reservations[$apartman.id].customers[0].lastname}, 
						{/if}
						
						platba 
						{if $date.reservations[$apartman.id].prevodem}
						převodem
						{else}
						hotově
						{/if}
					</a>, 
					<a href="?smaz={$date.reservations[$apartman.id].id}" id="" class="smaz red">Smazat rezervaci</a>
					</td>
				{else}
					<td rowspan="2"> </td>
				{/if}

			{/foreach}
			{* <td rowspan="2">{if !empty($date.tarif.nazev)}{$date.tarif.nazev} <br> {$date.tarif.price} €/noc{/if}</td> *}
		</tr>
	{/foreach}
	</tbody>
</table>
</div>
{literal}
<script type="text/javascript">
$(function(){
	$('a.smaz,#smaz').click(function(e){
		return confirm('Opravdu chcete smazat tuto rezervaci?');
	});
	$(".changeState").click(function(e) {
		e.preventDefault();
		var parentThis = this;
		$.get($(this).attr("href"), function() {
			$("tbody").find("a[href='"+$(parentThis).attr("href")+"']").each(function(k,v) {
				$(v).data("state", !($(v).data("state")*1));
				if($(v).data("state")) {
					$(v).find("i").css("color","green");
				} else {
					$(v).find("i").css("color","red");
				}
			});	
		});
	});
});
	
</script>
{/literal}