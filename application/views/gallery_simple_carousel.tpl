{* jednoduchy carousel *}

<div class="basicCarousel">
   {foreach name=pht2 from=$photos key=key item=item}
      <img src="{$item.photo}" alt="{$item.nazev}" title="{$item.nazev}" />
   {/foreach}
</div>


{literal}  
<script type="text/javascript"> 
  $(document).ready(function () {
      // Set first div to show
      $('.basicCarousel img:first').show();

{/literal}{if $smarty.foreach.pht2.total>1}{literal}
       
      setInterval(function() {
      $('.basicCarousel img:first-child').fadeOut().next('img').fadeIn().end().appendTo('.basicCarousel');
      }, 4000);
{/literal}{/if}{literal}      
      
  });
</script>
{/literal}