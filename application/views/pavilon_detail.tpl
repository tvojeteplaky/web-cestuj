{* sablona pro generovani statickych stranek (modul: page) *}

{widget action="breadcrumbs" controller="navigation" }

<div id="CenterSection">
  <div id="CenterSectionLeft">
    {widget name="subnav" controller="pavilon" action="category_subnav"}
  </div>
  
  <div id="ContentSection">
    <div id="TextTopSection">
    {widget action="custom_widget" controller="gallery" name="gallery" param="t1-t2"}
    </div> 
          
    <div id="TextSection"{*{if !empty($item.photo_detail) || !empty($item.youtube_code)}class="textHalf"{/if}*} class="textHalf">
      
      <h1>{$item.nazev}</h1>
      <div class="textSectionIN">
      {$item.popis}
      </div>
    
    </div>
     
    {*{if !empty($item.photo_detail) || !empty($item.youtube_code) }  *}
    <div id="TextSectionRight">
        {if !empty($item.photo_detail)}
        <div id="PavilonMap">
           <div class="title">{translate str="Umístění pavilonu"}</div>
           <a href="{$item.photo_detail}" title="{$item.nazev}" class="lightbox-enabled" rel="lightbox-photo"><img src="{$media_path}img/img_pavilon_map.jpg" alt="Mapa pavilonu" title="Mapa pavilonu" width="317" height="184" /></a>
        </div>
        {/if}
        
        {if !empty($item.youtube_code)}
         <div class="title tVideo">{translate str="Video"}</div>     
          {*<iframe width="560" height="315" src="http://www.youtube.com/embed/l_cb6WtaY0k" frameborder="0" allowfullscreen></iframe>*}
          <iframe width="317" height="194" src="{$item.youtube_code}" frameborder="0" allowfullscreen></iframe>
        {/if}
        
        {widget action="pavilon_widget" controller="gallery" name="gallery" param="t2-t1"}
    </div>
    {*{/if}  *}
    
  </div> 
</div>