<div id="SearchSection">
  <form action="{$url_base}{translate str="vysledky-vyhledavani"}" method="post">
     <fieldset>
       <input type="text" value="" name="search_text" class="searchTxt" id="SearchInput" />
       <input type="submit" value="odeslat" class="searchSubm" />
       {hana_secured_post action="index" module="search"}
     </fieldset>
  </form>
</div>

{* implementace našeptávače *}
{literal}
<script type="text/javascript">
 /* <![CDATA[ */

$(function() {
  $("#SearchInput").autocomplete({
    source: function(request, response) {
      jQuery.ajax({
        url: "{/literal}?{hana_secured_get action="show_suggestions" module="search"}{literal}",
        type: "get",
        dataType: "json",
        data: {
          term: request.term
        },
        success: function(data) {
          response(jQuery.map(data.main_content, function(item) {
            return {
                url: item.url,
                value: item.name
            }
          }))
        }
      })
    },
    select: function( event, ui ) {
      window.location.href = ui.item.url;
    },
    minLength: 2
  });

});

 /* ]]> */
</script>
{/literal}