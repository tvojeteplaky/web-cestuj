{if $admin}  <link type="text/css" href="{$media_path}css/admin-table.css" rel="stylesheet" media="all" />
           {/if}

      	<div class="reservation left">
      <div class="controls">
        	<a class="prev rounded-left" href="?page={$page - 1}">Předchozí</a> 
                <a class="next rounded-right" href="?page={$page + 1}">Další</a>
      </div>
      <br />
                <div class="clear">
                  
                </div>
	{foreach name=months from=$reservations key=key item=mesic}				
					<table class="table_reservations">
						<thead>
 <tr>
								<td class="controls">
								<strong>{$mesic.mesic}</strong>
                </td>
                {foreach name=cluny from=$cluny key=key item=clun}
								<td class="apartment" colspan="2">{$clun.nazev}</td>
								<td class="odsadit"></td>
								{/foreach}
						
							</tr>
						</thead>
						<tbody>
	   {foreach name=days from=$mesic key=key item=item}
	   {if !$smarty.foreach.days.first}
  <tr>
  <td>{$item.den} {$item.datum|date}</td>
  {foreach name=cluny from=$item.clun key=key item=clun}

  <td {if $clun.dopo == 1}class="reserved"{else}class="free"{/if} id="{$item.datum}">
  {if $admin}{if $clun.dopo == 1}
  <a href="?page={$page}&date={$item.datum}&clun={$key}&doba=dopo&storno=true#{$item.datum}">storno</a>
{else}
<a href="?page={$page}&date={$item.datum}&clun={$key}&doba=dopo&reservation=true#{$item.datum}">rezervovat</a>{/if}
{else}do 10.00{/if}
</td>
   
<td class="{if $clun.odpo == 1}reserved{else}free{/if}">
{if $admin}{if $clun.odpo == 1}
<a href="?page={$page}&date={$item.datum}&clun={$key}&doba=odpo&storno=true#{$item.datum}">storno</a>

{else}<a href="?page={$page}&date={$item.datum}&clun={$key}&doba=odpo&reservation=true#{$item.datum}">
rezervovat</a>{/if}{else}od 15.00{/if}</td>
<td class="odsadit"></td>
  {/foreach}
  </tr>
  {/if}
   {/foreach}
  
	</tbody>
					</table>
				 {/foreach}	
				</div>
      
      

				