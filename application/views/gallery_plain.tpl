{* sablona galerie - slideru *}
<div class="photoGallery">
{if !empty($photos)} 
  {foreach name=pht2 from=$photos key=key item=item}
  <div class="item">
    <a href="{$item.photo_detail}" alt="{$item.nazev}" title="{$item.nazev}" class="lightbox-enabled" rel="lightbox-pavilon"><img src="{$item.photo}" alt="{$item.nazev}" title="{$item.nazev}" /></a>
  </div>
  {/foreach}
{/if}
</div>