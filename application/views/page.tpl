{* sablona pro generovani statickych stranek (modul: page) *}



	
<!-- page content -->

<div class="page-content 2columns row fg-no-gutter">
	
	<div class="left fg12">
		<h1>{$item.nadpis}</h1>					
		{$item.popis}

		{if $nazev_seo=='pujcovna' }
		<div class="more">
			<a class="button" href="#openModal">{translate str="Rezervovat tento vůz"}</a>
		</div>
		<div id="openModal" class="modal-reservation">
			<div>
				<a href="#close" title="Zavřít" class="modal-reservation__close">X</a>
				<h2>Rezervace vozidla</h2>
				<form action="" method="post">
					<label>Vozidlo:</label>
					<select name="vozidlo">
						<option>Speciální automobil SRE85</option>
					</select><br>
					<label>Vypůjčení:</label>
					<input type="text" name="vypujceni" value="" required><br>
					<label>Vrácení:</label>
					<input type="text" name="vraceni" value="" required><br>
					<label>Jméno:</label>
					<input type="text" name="jmeno" value="" required><br>
					<label>Příjmení:</label>
					<input type="text" name="prijmeni" value="" required><br>
					<label>Datum narození:</label>
					<input type="text" name="narozeni" value="" required><br>
					<label>E-mailová adresa:</label>
					<input type="text" name="email" value="" required><br>
					<label>Telefon:</label>
					<input type="text" name="telefon" value=""><br>
					<label>Platba v:</label>
					<select name="platba">
						<option>EUR</option>
						<option>CZK</option>
					</select><br>

					<input type="submit" name="" value="Rezervovat">
				</form>
			</div>
		</div>
		{/if}
	</div>
	
</div> 

<div class="clear">
</div>
<!-- <div class="shadow-corner-right"> </div> -->
<!-- end page content -->
