<!-- page content -->
<div class="page-content 2columns row fg-no-gutter">
	
	<div class="left fg8">
		<h1>{$item.nazev}</h1>					
		{$item.detail_popis}
    <div class="more modal-window">
      <a class="button" href="#openModal">{translate str="Rezervovat tento vůz"}</a>
    </div>
  </div>
  <div class="left fg4">
    <ul class="detail-submenu">
{foreach from=$items key=key item=item2}
<li><a href="/{$item2.nazev_seo}" class="{if $item2.active == 1}active{/if}">{$item2.nazev}</a></li>
{/foreach}
    </ul>
{*    <p>
      <img alt="" src="/media/userfiles/detail-img.jpg" style="width: 300px; height: 200px;">
    </p>*}
  </div>
</div>
<div class="clear"></div>
<div class="page-content 2columns row fg-no-gutter">

		<div class="gallery-widget">
		<div class="clear"></div>
		<div class="shadow-corner-right"> </div>
		<h2 class="hp">Fotogalerie</h2>
		<div class="wrapper row fg-no-gutter">
			{if isset($photos)}
			{foreach from=$photos item=photo key=key name=name}
				<div class="item">
					<a href="{$media_path}photos/shop/item/gallery/images-{$photo.shop_id}/{$photo.photo_src}-ad.jpg" data-lightbox="gallery">						
						<img src="{$media_path}photos/shop/item/gallery/images-{$photo.shop_id}/{$photo.photo_src}-ad.jpg" alt="">	
					</a>
				</div>
			{/foreach}
			{/if}
		</div>
	</div>
  <div class="clear"></div>

{$table}

		<div class="more modal-window" style="margin-left: 10px">
			<a class="button" href="#openModal">{translate str="Rezervovat tento vůz"}</a>
		</div>
		<div id="openModal" class="modal-reservation">
			<div>
				<a href="#close" title="Zavřít" class="modal-reservation__close">X</a>
				<h2>Rezervace vozidla {$item.nazev}</h2>
				<form action="#form" method="post">
					{*<label>Vozidlo:</label>
					<select name="vozidlo">
						<option>Speciální automobil SRE85</option>
					</select><br>*}
					<label>Vypůjčení:</label>
					<input type="text" id="termin_od" name="reservationform[termin_od]" value="" required><br>
					<label>Vrácení:</label>
					<input type="text" id="termin_do" name="reservationform[termin_do]" value="" required><br>
					<label>Jméno:</label>
					<input type="text" name="reservationform[firstname][]" value="" required><br>
					<label>Příjmení:</label>
					<input type="text" name="reservationform[lastname][]" value="" required><br>
{* 					<label>Datum narození:</label>
					<input type="text" id="born" name="reservationform[born][]" value="" required><br> *}
					<label>E-mailová adresa:</label>
					<input type="email" name="reservationform[email]" value="" required><br>
					<label>Telefon:</label>
					<input type="phone" name="reservationform[phone]" value=""><br>
                              <label>Poznámka k rezervaci:</label>
                              <textarea name="reservationform[note]" style="width:40%;height: 80px;border: 1px solid #e9e9e9;background: #ffffff;"></textarea><br>
		                  <input type="hidden" name="reservationform[apartman]" value="{$item.id}">
 					<input type="hidden" name="reservationform[prevodem]" value="1">
					<input type="hidden" name="reservationform[mena]" value="7">
{*					<label>Platba v:</label>
					<select name="reservationform[mena]">
						<option>EUR</option>
						<option>CZK</option>
					</select><br>*}
                    {hana_secured_post action="save" module="reservation"}
					<input type="submit" id="submit" name="" value="Rezervovat">
                    <button type="reset" id="reset">{translate str="Reset"}</button>
				</form>
			</div>
		</div>
</div>

<div class="clear">
</div>
<!-- <div class="shadow-corner-right"> </div> -->
<!-- end page content -->

    {literal}
    <script type="text/javascript">
    function parseDate(string){
        var tmp = string.split('.');
        var date = new Date(tmp[2],tmp[1],tmp[0]);
        return date;
    }
  $(function(){
    $.datepicker.regional['cs'] = { 
                closeText: 'Cerrar', 
                prevText: 'Předchozí', 
                nextText: 'Další', 
                currentText: 'Hoy', 
                monthNames: ['Leden','Únor','Březen','Duben','Květen','Červen', 'Červenec','Srpen','Září','Říjen','Listopad','Prosinec'],
                monthNamesShort: ['Le','Ún','Bř','Du','Kv','Čn', 'Čc','Sr','Zá','Ří','Li','Pr'], 
                dayNames: ['Neděle','Pondělí','Úterý','Středa','Čtvrtek','Pátek','Sobota'], 
                dayNamesShort: ['Ne','Po','Út','St','Čt','Pá','So',], 
                dayNamesMin: ['Ne','Po','Út','St','Čt','Pá','So'], 
                weekHeader: 'Sm', 
                dateFormat: 'dd.mm.yy', 
                firstDay: 1, 
                isRTL: false, 
                showMonthAfterYear: false, 
                yearSuffix: ''}; 
    {/literal}{if $language_code == "cz"}{literal}
    $.datepicker.setDefaults($.datepicker.regional['cs']);
    {/literal}{else}{literal}
    $.datepicker.setDefaults($.datepicker.regional['en']);
    {/literal}{/if}{literal}
    var hash = window.location.hash.split('#');
 //       $("#submit, #reset").css("display", "none");
        var dialog = $( "#dialog-form" ).dialog({
            autoOpen: false,
            height: 500,
            width: 800,
            modal: true,
            buttons: {
              /*  "Reset": function() {
                    $("#reset").click();
                },*/
                "{/literal}{translate str="Storno"}{literal}": function() {
                    dialog.dialog( "close" );
                },
                "{/literal}{translate str="Rezervovat"}{literal}": function() {
                    $("#submit").click();
                }
            },
            close: function() {
                $("#reset").click();
            }
        });
        $( "#openForm" ).click(function(e) {
            e.preventDefault();
            dialog.dialog( "open" );
        });

        if(hash[1]==='form'){
            dialog.dialog( "open" );
        }

    $('#termin_od').datepicker({
      minDate: new Date(),
      dateFormat: "dd.mm.yy",
    });
    $('#termin_do').datepicker({
      dateFormat: "dd.mm.yy",
    });
    $('#born').datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "1900:2016",
      dateFormat: "dd.mm.yy",
    });
    $('#termin_od').change(function(){
      var min = $( "#termin_od" ).datepicker( "getDate" );
      min.setDate(min.getDate()+3);
      $( "#termin_do" ).datepicker( "option", "minDate", min );
      var old = $( "#termin_do" ).datepicker( "getDate" );
      if(min> old) {
        $( "#termin_do" ).datepicker( "option", "setDate", min );
      }
    });
    $('#count').change(function() {

      var row = $('tr.copy');
      var need = $(this).val()*1;
      var have = row.length;
      var target = '#target';

      if(need>have) {
        var diff = need-have;

        for (var i = 0; i < diff; i++) {
          row.eq(0).clone().appendTo(target);
        };
      }
      else if(need<have){
        var diff = have-need;

        for (var i = 1; i <= diff; i++) {
          row.eq(-i).remove();
        };
      }
    });
  });
  </script>
  {/literal}
