{* sablona seznamu clanku*}

 {foreach name=art from=$items key=key item=item}
  <div class="content-text">
   
   {if ($smarty.foreach.art.index == 0)}
    <div id="novinky-left">
      
      <h1>Novinky</h1>        

    </div> 
    {/if}
        
    <div id="novinky-right">
  
     <div class="novinky-obr">        
      {if isset($item.photo) && $item.photo}
      <a href="{$url_base}{$item.nazev_seo}" title="{$item.nazev}"><img src="{$item.photo}" width="213" height="156" alt="{$item.nazev}" /></a>
      {/if}        
     </div>       
        
     <div class="novinky-text"> 
      <h2>{$item.nazev}</h2>       
      <p><span>{$item.date|date:cz}</span></p>
       {$item.uvodni_popis}
      <br />
 
      <div class="novinky-vse"> 
      <a href="{$url_base}{$item.nazev_seo}" title="novinky"><span>Více informací</span><img src="{$media_path}img/sipka.png" width="17" height="17" alt="{$item.nazev}" /></a>   
      </div>  
      
     </div>     
 
    </div>

   </div>      
     
   {/foreach}  
 


{*


 
<div id="ContentSection">
  <div id="ArticleList" class="content">
      {foreach name=art from=$items key=key item=item}
      <div class="item">
         <div class="photo">{if isset($item.photo) && $item.photo}<img src="{$item.photo}" alt="{$item.nazev}" />{/if}</div>
         <div class="content">
           <h2><a href="{$url_base}{$item.nazev_seo}">{$item.nazev}</a></h2>
           <div class="date">{$item.date|date:cz}</div>
           {$item.uvodni_popis}
           <a class="moreinfo" href="{$url_base}{$item.nazev_seo}">více informací<span> &raquo;</span></a>
         </div>
         <div class="correct"></div>
      </div>
      {/foreach}
      
      {$pagination}
    </div>
</div>
      
    
  *}