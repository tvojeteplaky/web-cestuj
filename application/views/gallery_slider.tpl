{* sablona galerie - slideru *}

{if !empty($photos)}
  <div class="basicSlider">
    <a href="#" class="previous">předchozí</a> 
    <div class="restriction">
      <div class="itemBelt">
         {foreach name=pht from=$photos key=key item=item}
         <div class="item">
           <a class="lightbox-enabled" title="" rel="lightbox-{$module}" href="{$item.photo_detail}">
             <img src="{$item.photo}" alt="{$item.nazev}" title="{$item.nazev}" width="160" height="107" /> 
           </a>
         </div>
         {/foreach} 
      </div>
    </div>
    <a href="#" class="next">další</a> 
  </div>
  
  {*
  {literal}
  <script type="text/javascript">
  $(document).ready(function(){
     $(".basicSlider").basicSlider({'showItems': 4});
  });
  </script> 
  {/literal}
  *}
{/if}
