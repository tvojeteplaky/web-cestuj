{*sablona seznamu clanku*}
{* sablona pro generovani statickych stranek (modul: page) *}


<!-- slider -->
{if $page.photo_src}
<div class="slider">
</div>
<!-- end slider -->
{/if}
  
<!-- page content -->
<div class="page-content 2columns row fg-no-gutter">
        
<!-- hp gallery -->
<div class="gallery">
  <h1>Půjčovna vozidel</h1>
  <div class="wrapper row fg-no-gutter">
{foreach from=$items key=key item=item2}
  <div class="item">
    <a href="{$url_base}{$item2.nazev_seo}" title="{$item2.nazev}">
        {if !empty($item2.photo_src)}     
        	<img src="{$media_path}photos/shop/item/images-{$item2.id}/{$item2.photo_src}-ad.jpg" alt="{$item2.nazev}" height="155" width="442">
        {/if}
     <h2>{$item2.nazev}</h2>
      <p>{$item2.technicky_popis|strip_tags:false|truncate:150:"":false}</p>
    </a>
      <div class="page-content 2columns row fg-no-gutter">
        <div class="left fg6">
          <span class="car-price">{if $season == 1}{$item2.cena_sezona}{else}{$item2.cena_mimo_sezona}{/if},- Kč/den
          </span>
        </div>
        <div class="left fg6">
          <div class="more">
            <a class="button" href="{$url_base}{$item2.nazev_seo}">{translate str="Rezervovat tento vůz"}</a>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    
      <div class="shadow-corner-right-small"> </div>
  </div>
  {/foreach}
             
             
<div class="clear">
        
</div>
</div>
</div>
        <!-- end hp gallery -->     
    
</div>

<div class="clear">
</div>
<!-- <div class="shadow-corner-right"> </div> -->
<!-- end page content -->
<!--
  <div class="content-text">
   
    <div id="submenu-left">
      
     <div id="submenu">
        <ul>
      {foreach name=art from=$items key=key item=item2}
      
      
      <li {if $item2.active}class="active" {/if}>  <a href="{$url_base}{$item2.nazev_seo}" title="novinky">{$item2.nazev}</a>   </li>
      
      
      {/foreach} 
      </ul>  
     </div>

    </div> 
        
    <div class="prodejny-right">
    <h1>{$page.nadpis}</h1>
  
     <div class="text-content">
      {$page.popis}
    </div>
   
    <img src="{$media_path}img/prodejny-obecne.jpg" />
       
 
    </div>

   </div>      
     
-->


{*


 
<div id="ContentSection">
  <div id="ArticleList" class="content">
      {foreach name=art from=$items key=key item=item}
      <div class="item">
         <div class="photo">{if isset($item.photo) && $item.photo}<img src="{$item.photo}" alt="{$item.nazev}" />{/if}</div>
         <div class="content">
           <h2><a href="{$url_base}{$item.nazev_seo}">{$item.nazev}</a></h2>
           <div class="date">{$item.date|date:cz}</div>
           {$item.uvodni_popis}
           <a class="moreinfo" href="{$url_base}{$item.nazev_seo}">více informací<span> &raquo;</span></a>
         </div>
         <div class="correct"></div>
      </div>
      {/foreach}
      
      {$pagination}
    </div>
</div>
      
    
 *}
