{* sablona pro generovani statickych stranek (modul: page) *}


			<!-- slider -->
			<div class="slider">
				<div class="wrapper">
					<ul>
						<li class="slide"><img src="{$media_path}img/slider/apartment/apartment-01.jpg" alt="slide 01" /></li>
					</ul>
				</div>
				<div class="controls"> </div>
			</div>
			<div class="shadow-corner-both"> </div>
			<!-- end slider -->
			
			<!-- page content -->
			<div class="shadow-article-top"> </div>
			<div class="page-content 2columns row fg-no-gutter">
				
				<div class="left fg8">
					<h1>{$item.nadpis}</h1>
					
				{$item.popis}
				</div>
				
				<div class="right fg4">
				 <a href="{$item.photo_detail}" title="{$item.nazev}" data-lightbox="image1" >
					<img src="{$item.photo}" alt="apartment">
					</a>
				</div>			
		
			</div>
			<div class="shadow-corner-right"> </div>
			<!-- end page content -->
			
			<!-- hp gallery -->
			<div class="gallery-widget">
				<h2>{translate str="Fotogalerie"}</h2>
				<div class="wrapper row fg-no-gutter">
					{foreach name=gall from=$gallery key=key item=item2}
          {if $smarty.foreach.gall.index < 4}
       	<div class="item">  
         <a href="{$item2.photo_detail}" title="{$item2.nazev}" data-lightbox="image" >
     <img src="{$item2.photo}" alt="fotogalerie" />
      </a> 
      </div>
      {/if}
      {/foreach}
				</div>
				<div class="more">
					<a class="button" href="#">více</a>
				</div>
			</div>
			<div class="shadow-corner-right"> </div>
			<!-- end hp gallery -->



      {static_content code="cenik"}


  