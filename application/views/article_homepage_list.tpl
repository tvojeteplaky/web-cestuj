{* sablona seznamu clanku ktere jsou soucasti uvodni stranky *}

<div class="content-right">
  <h2>Novinky</h2> <br />
  {foreach name=art from=$items key=key item=item}  
  <h3>{$item.nazev}</h3> <br /> 
  <div class="content-novinky-left">    
    {if isset($item.photo) && $item.photo}   
    <a href="{$url_base}{$item.nazev_seo}" title="{$item.nazev}">
      <img src="{$item.photo}" width="100" height="73" alt="{$item.nazev}" />
    </a>
    {/if}        
  </div>       
  <div class="content-novinky-right">        
    <p><span>{$item.date|date:cz}</span></p>
    {$item.uvodni_popis}
      <span> <a class="moreinfo" href="{$url_base}{$item.nazev_seo}">více >></a></span>
    
    <br />
  </div>   
  <br /><br /><br />
  {/foreach}
  <div class="content-novinky-vse"> 
    <a href="{$url_base}novinky" title="novinky"><span>Všechny novinky </span><img src="{$media_path}img/sipka.png" width="17" height="17" alt="novinky1" /></a>   
  </div>   
</div> 
 
