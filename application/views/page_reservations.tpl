{* sablona pro generovani statickych stranek (modul: page) *}
<div class="page-content">
    <div class="content">
        <h1>{$item.nadpis}</h1>
        {$item.popis}
        <div class="legend">
            <table>
                <tr>
                    <td class="reserved"></td>
                    <td>{translate str="obsazeno"}</td>
                </tr>
                <tr>
                    <td class="free"></td>
                    <td>{translate str="volno"}</td>
                </tr>
            </table>
        </div>

    </div>
    <div class="shadow-strip"></div>

    <div class="content">
        {$table}

        <div id="dialog-form" title="{translate str="Rezervovat apartmán"}">
            <form method="post" action="#form">
                <table>
                    <tr>
                        <td>
                            <label for='apartman'>{translate str="Apartmán"}:</label>
                        </td>
                        <td>
                            <select id="apartman" name="reservationform[apartman]">
                              {foreach from=$apartmans item=apartman key=key name=apartman_list}
                                <option value="{$apartman.id}">{$apartman.nazev}</option>
                              {/foreach}
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="termin_od">{translate str="Příjezd"}:</label>
                        </td>
                        <td>
                            <input type="text" id="termin_od" name="reservationform[termin_od]" required="">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="termin_od">{translate str="Odjezd"}:</label>
                        </td>
                        <td>
                            <input type="text" id="termin_do" name="reservationform[termin_do]" required="">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for='apartman'>{translate str="Počet osob"}:</label>
                        </td>
                        <td>
                            <select name="reservationform[count]" id="count">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>{translate str="Osoby"}</td>
                        <td>
                            <table>
                                <thead>
                                    <tr>
                                        <th>{translate str="Jméno"}</th>
                                        <th>{translate str="Příjmení"}</th>
                                        <th>{translate str="Datum narození"}</th>
                                    </tr>
                                </thead>
                                <tbody id="target">
                                    <tr class='copy'>
                                        <td>
                                            <input type="text" name="reservationform[firstname][]" placeholder="{translate str="Jméno"}" required="">
                                        </td>
                                        <td>
                                            <input type="text" name="reservationform[lastname][]" placeholder="{translate str="Příjmení"}" required="">
                                        </td>
                                        <td>
                                            <input type="text" id="born" name="reservationform[born][]" placeholder="{translate str="Narozen"}" required="">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="email">{translate str="Email"}</label>
                        </td>
                        <td>
                            <input type="email" name="reservationform[email]" required="">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="phone">{translate str="Telefon"}</label>
                        </td>
                        <td>
                            <input type="phone" name="reservationform[phone]" required="">
                        </td>
                    </tr>
                    <input type="hidden" name="reservationform[prevodem]" value="1">
                    {*<tr>
                        <td>
                            <label for="prevodem">Platba:</label>
                        </td>
                        <td>
                            <select name="reservationform[prevodem]">
                                <option value="0">Hotově</option>
                                <option value="1">Převodem</option>
                            </select>
                        </td>
                    </tr>*}
                    <tr>
                        <td>
                            <label for="mena">{translate str="Platba převodem v"}:</label>
                        </td>
                        <td>
                            <select name="reservationform[mena]">
                                <option value="1">EUR</option>
                                <option value="2">CZK</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {hana_secured_post action="save" module="reservation"}
                            <button type="submit" id="submit">{translate str="Odeslat"}</button>
                            <button type="reset" id="reset">{translate str="Reset"}</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
    {literal}
    <script type="text/javascript">
    function parseDate(string){
        var tmp = string.split('.');
        var date = new Date(tmp[2],tmp[1],tmp[0]);
        return date;
    }
  $(function(){
    $.datepicker.regional['cs'] = { 
                closeText: 'Cerrar', 
                prevText: 'Předchozí', 
                nextText: 'Další', 
                currentText: 'Hoy', 
                monthNames: ['Leden','Únor','Březen','Duben','Květen','Červen', 'Červenec','Srpen','Září','Říjen','Listopad','Prosinec'],
                monthNamesShort: ['Le','Ún','Bř','Du','Kv','Čn', 'Čc','Sr','Zá','Ří','Li','Pr'], 
                dayNames: ['Neděle','Pondělí','Úterý','Středa','Čtvrtek','Pátek','Sobota'], 
                dayNamesShort: ['Ne','Po','Út','St','Čt','Pá','So',], 
                dayNamesMin: ['Ne','Po','Út','St','Čt','Pá','So'], 
                weekHeader: 'Sm', 
                dateFormat: 'dd.mm.yy', 
                firstDay: 1, 
                isRTL: false, 
                showMonthAfterYear: false, 
                yearSuffix: ''}; 
    {/literal}{if $language_code == "cz"}{literal}
    $.datepicker.setDefaults($.datepicker.regional['cs']);
    {/literal}{else}{literal}
    $.datepicker.setDefaults($.datepicker.regional['en']);
    {/literal}{/if}{literal}
    var hash = window.location.hash.split('#');
        $("#submit, #reset").css("display", "none");
        var dialog = $( "#dialog-form" ).dialog({
            autoOpen: false,
            height: 500,
            width: 800,
            modal: true,
            buttons: {
              /*  "Reset": function() {
                    $("#reset").click();
                },*/
                "{/literal}{translate str="Storno"}{literal}": function() {
                    dialog.dialog( "close" );
                },
                "{/literal}{translate str="Rezervovat"}{literal}": function() {
                    $("#submit").click();
                }
            },
            close: function() {
                $("#reset").click();
            }
        });
        $( "#openForm" ).click(function(e) {
            e.preventDefault();
            dialog.dialog( "open" );
        });

        if(hash[1]==='form'){
            dialog.dialog( "open" );
        }

    $('#termin_od').datepicker({
      minDate: new Date(),
      dateFormat: "dd.mm.yy",
    });
    $('#termin_do').datepicker({
      dateFormat: "dd.mm.yy",
    });
    $('#born').datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "1950:2015",
      dateFormat: "dd.mm.yy",
    });
    $('#termin_od').change(function(){
      var min = $( "#termin_od" ).datepicker( "getDate" );
      min.setDate(min.getDate()+3);
      $( "#termin_do" ).datepicker( "option", "minDate", min );
      var old = $( "#termin_do" ).datepicker( "getDate" );
      if(min> old) {
        $( "#termin_do" ).datepicker( "option", "setDate", min );
      }
    });
    $('#count').change(function() {

      var row = $('tr.copy');
      var need = $(this).val()*1;
      var have = row.length;
      var target = '#target';

      if(need>have) {
        var diff = need-have;

        for (var i = 0; i < diff; i++) {
          row.eq(0).clone().appendTo(target);
        };
      }
      else if(need<have){
        var diff = have-need;

        for (var i = 1; i <= diff; i++) {
          row.eq(-i).remove();
        };
      }
    });
  });
  </script>
  {/literal}
</div>