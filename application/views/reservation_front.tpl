{foreach name=apartmany from=$reservations key=key item=apartman}
<h2>{$apartman.nazev}</h2>
<div class="rezervace-responsive-box">
<table class="rezervace" border="0">
    <tr>
        <td colspan="3"></td>
        {for $foo=1 to 31}
        <td>{$foo}</td>
        {/for}
    </tr>
    
    {foreach from=$apartman.months item=month key=key name=months}
    
        <tr>
            <td>{$month.month}</td>
            <td>

                {foreach from=$month.days item=day key=key name=days}
                {if $smarty.foreach.days.first}
                    <td></td><td>
                {/if}

                {if not $smarty.foreach.days.first  or not $smarty.foreach.days.index eq 1}
                    <div class="odpo{if $day.rezervace} reserved{/if} {if $smarty.foreach.days.index eq 0}alone{/if}"></div>
                </td>
                {/if}

                <td>
                    <div class="dopo{if $day.rezervace} reserved{/if}"></div>
                {/foreach}
            </td>

        
        </tr>
    {/foreach}
</table>
</div>
 {/foreach}
<br>
</div>
<div class="legend">
            <table>
                <tr>
                    <td class="reserved"></td>
                    <td>{translate str="obsazeno"}</td>
                </tr>
                <tr>
                    <td class="free"></td>
                    <td>{translate str="volno"}</td>
                </tr>
            </table>
        </div>
{*
<div class="shadow-strip">

</div>

<div class="content">
*}
