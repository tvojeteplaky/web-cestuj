<div id="submenu">
{if $links|count}
 <ul{if !empty($class)} class="{$class}"{/if}>
   {foreach name=nav from=$links key=key item=item}
      <li{if array_key_exists($key,$sel_links)} class="active"{/if}{if $smarty.foreach.nav.last} class="last"{/if}{if !empty($item.nav_class)} {$item.nav_class}{/if}>
        <a href="{$url_base}{if empty($item.indexpage)}{$item.nazev_seo}{/if}">{$item.nazev}</a></li>
        {if !empty($item.children) && array_key_exists($key,$sel_links)}
          
          <ul class="second-submenu">
          {foreach name=navL2 from=$item.children key=key2 item=item2}
              <li{if array_key_exists($key2,$sel_links)} class="active"{/if}{if $smarty.foreach.navL2.last} class="last"{/if}><a href="{$url_base}{$item2.nazev_seo}">{$item2.nazev}</a></li>
          {/foreach}
          </ul>
          
          <br />
        {/if}
        
      
   {/foreach}
 </ul>
{/if} 
</div>