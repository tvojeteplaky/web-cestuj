{* sablona galerie - slideru *}
{if !empty($photos)}

   <div class="mainPhoto">
     {foreach name=pht1 from=$photos key=key item=item}
        <img class="photogal{$item.id}" src="{$item.photo_detail}" alt="{$item.nazev}" title="{$item.nazev}" {if !$smarty.foreach.pht1.first}class="noDisplay"{/if}/>
     {/foreach}
     <div class="over"></div>
   </div>
   <div class="thumbnails">
      {foreach name=pht2 from=$photos key=key item=item}
      <div class="item">
        <a href="#" class="photogal{$item.id}"><img src="{$item.photo}" alt="{$item.nazev}" title="{$item.nazev}" /></a>
      </div>
      {/foreach}
   </div>
   <div class="correct"></div>
 
  {literal}  
     <script type="text/javascript">
     $(document).ready(function(){
       $(".thumbnails a").hover(function(){
         $(".mainPhoto img").hide();
         $(".mainPhoto ."+$(this).attr("class")).show();
       
       });
     
     });
     </script>
  {/literal}
{/if}