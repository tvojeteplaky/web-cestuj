{* sablona drobitkove navigace *}
<div id="PathSection">

  <a href="{$url_base}" class="item">{translate str="Úvodní stránka"}</a>
  {foreach from=$items key=key item=item}
    {if isset($item.nazev_seo)}
      <span class="item">&raquo;</span> <a href="{$url_base}{$item.nazev_seo}" class="item">{$item.nazev}</a>
    {else}
      
    {/if}
  {/foreach}
</div>