{* sablona navigace *}

       {literal}
<script>

	$(document).ready(function(){
		$("ul.sf-menu").superfish();
	});

</script>
         {/literal}

      <!-- site navigation -->
					
				

  	<nav class="site-nav rounded-top">
						<ul class="inline sf-menu">

   {foreach name=nav from=$links key=key item=item}
      <li> 
        <a {if $item.nav_class}{$item.nav_class}{/if} class="{if array_key_exists($key,$sel_links) && $item.nav_class!="home" || $uvodka && $item.nazev=="Úvodní stránka"}active {/if}{if $item.anchor eq '0'}no-anchor{/if}" href="{if !$item.indexpage}{$url_base}{$item.nazev_seo}{else}{$url_homepage}{/if}">{$item.nazev}</a>
        {if !empty($item.children)}
          <ul class="submenu">
          {foreach name=navL2 from=$item.children key=key item=item2}
              <li class="{if $item.nav_class=="exposition"}nav_item_{$item2.nazev_seo}{/if}{if $smarty.foreach.navL2.last} last{else if $smarty.foreach.navL2.first} first {else} normal{/if}"><a href="{$url_base}{$item2.nazev_seo}">{$item2.nazev}</a>
                {if !empty($item2.children) && $item.nav_class=="exposition"}
                {* vyjimka pro expozici *}
                   <ul>
                     {foreach name=l3 from=$item2.children key=key3 item=item3}
                        {if !empty($item3.nazev)}<li{if $smarty.foreach.l3.last} class="last"{/if}><a href="{$url_base}{$item3.nazev_seo}">{$item3.nazev}</a></li>{/if}
                     {/foreach}
                   </ul>
                {/if}
              </li>
          {/foreach}
          </ul>
        {/if}
      </li>
   {/foreach}
      		</ul>
					</nav>
					<!-- end site navigation -->	