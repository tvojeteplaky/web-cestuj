{* sablona s jednoduchou zpravou *}
{if !empty($messages)}
<div id="OverlayMessage">
  <div class="content-msg" id="MessageWidget">
 <div class="content_IN" >
   <div {if $status===true}class="green"{else if $status===false}class="red"{/if}>
      {foreach name=msg from=$messages key=key item=item}
        {$item}<br />
      {/foreach}
   </div>
  <a href="#" class="button clickChange">Zavřít</a>
 </div>
</div>
</div>



{literal}
<script>
$(document).ready(function(){
show_message();
$('#OverlayMessage').delay(200000).fadeOut(1000);

$('#OverlayMessage .clickChange').click(function(){$('#OverlayMessage').hide(); return(false);}); 
});
</script>
{/literal}
 
{/if}
