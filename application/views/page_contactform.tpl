{* sablona pro detail kontaktniho f) *}

{widget action="breadcrumbs" controller="navigation" }

<div id="CenterSection">       
    
<div id="ContentSectionLeft">

  <h1>{$item.nadpis}</h1>
    <div id="TextSection">
    {$item.popis}
    
    
    {widget action="show" controller="contact" name="contactform"}
                  
    </div>
  </div>

  <div id="CenterSectionRight">
    <div class="title tBlue"><a href="{$url_base}{translate str="novinky"}">Poslední novinky</a></div>
      {widget action="sidebar_list" controller="article"}
      <div class="correct defaultMT">
     
    <div id="RegistrationForm">
      <div class="title tEmail">Chci dostávat novinky e-mailem</div>
      <form action="" method="post">
         <fieldset>
           <input type="text" value="" name="registration" class="text" />
           <input type="submit" value="odeslat" class="submit" />
         </fieldset>
      </form>
    </div>
    
    <div class="title tVideo">Video</div>
    <img src="{$media_path}img/img_video.jpg" alt="img_video.jpg, 17kB" title="img_video" width="316" height="194" />
  </div>
</div>
</div>
