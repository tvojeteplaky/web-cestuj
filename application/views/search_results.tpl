{* sablona pro zobrazeni vysledku vyhledavani *}

{widget action="breadcrumbs" controller="navigation" }

<div id="CenterSection">
  <div id="CenterSectionLeft">
    {widget name="subnav" controller="page" action="page_subnav"}
  </div>
  
  <div id="ContentSection">
    <div id="TextSection">
      
      <h1>{translate str="Vyhledávání pro výraz"}: "{$keyword}"</h1>
   
      {* iterace pres vysledky vyhledavani *}
      {if !empty($search_results)}
      <ul>
      {foreach name=search_res from=$search_results key=key item=module_item}
        <h2>{$key}:</h2>
        {foreach name=it from=$module_item key=key item=item}
         
            <li><a href="{$url_base}{$item.nazev_seo}">{$item.title}</a>- {$item.text|@strip_tags|truncate:210} <a href="{$url_base}{$item.nazev_seo}" title="detail">detail &raquo;</a></li>
          
        {/foreach}
      {/foreach}
      </ul>
      {/if}
      <br />
      {$search_message}
    </div>
  </div> 
</div> 